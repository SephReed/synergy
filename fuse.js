const {
	FuseBox,
	Sparky,
	SassPlugin,
	CopyPlugin,
	WebIndexPlugin,
	CSSPlugin,
	CSSResourcePlugin,
	JSONPlugin,
	QuantumPlugin
} = require("fuse-box");
const { src, task, watch, context, fuse } = require("fuse-box/sparky");


context(class {
	getConfig() {
		return FuseBox.init({
			homeDir: "src",
			output: "dist/$name.js",
			hash: this.isProduction,
			// hash: false,
			sourceMaps: true,
			target: "browser",
			alias: {
				Registry: "~/Registry",
			},
			plugins: [
				WebIndexPlugin({
					template: "src/index.html"
				}),
				JSONPlugin(),
				[
					SassPlugin({ importer: true }),
					CSSResourcePlugin({
						dist: "dist/css-resources",
						useOriginalFilenames: true,
					}), 
					CSSPlugin()
				],
				CopyPlugin({ files: [".jpg", ".png", ".svg"] }),
				this.isProduction && QuantumPlugin({
					bakeApiIntoBundle: "app",
					uglify: true,
					css : { clean : true},
					extendServerImport: true
				})
			]
		})
	}
	createBundle(fuse) {
		const app = fuse.bundle("app");
		if (!this.isProduction) {
			app.watch()
			app.hmr()
		}
		app.instructions(">index.ts");
		return app;
	}
});


task("default", async context => {
	const fuse = context.getConfig();
	fuse.dev();
	context.createBundle(fuse);
	await fuse.run();
});

task("dist", async context => {
	context.isProduction = true;
	const fuse = context.getConfig();
	fuse.dev(); // remove it later
	context.createBundle(fuse);
	await fuse.run();
});

// const { FuseBox, WebIndexPlugin } = require("fuse-box");
// const fuse = FuseBox.init({
//   homeDir: "src",
//   target: "browser@es6",
//   output: "dist/$name.js",
//   plugins: [WebIndexPlugin()],
// });
// fuse.dev(); // launch http server
// fuse
//   .bundle("app")
//   .instructions(" > index.ts")
//   .hmr()
//   .watch();
// fuse.run();