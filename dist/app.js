(function(FuseBox){FuseBox.$fuse$=FuseBox;
FuseBox.target = "browser";
FuseBox.pkg("default", {}, function(___scope___){
___scope___.file("index.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const el_tool_1 = require("el-tool");
const printSheets_1 = require("./UI/PrintSheets/printSheets");
const ProceduralOffice_1 = require("./UI/ProceduralOffice");
const ExportPays_1 = require("./UI/ExportPays/ExportPays");
const Instructions_1 = require("./Entities/Instructions/Instructions");
const Synergy_1 = require("./Synergy");
require("./main.scss");
const search = window.location.search;
const today = new Date();
if (search.indexOf("cards") !== -1) {
    document.title = `Synergy - Cards Print (${today.getFullYear()}-${today.getMonth()}-${today.getDate()})`;
    el_tool_1.setInnards(document.body, [
        printSheets_1.default("employee"),
        // genPrintSheet("project"),
        // genPrintSheet("agenda"),
        // genPrintSheet("event"),
        printSheets_1.default("option"),
        // genPrintSheet("quirk"),
        printSheets_1.default("specialty"),
        // genPrintSheet("mission"),
        printSheets_1.default("placeable"),
    ]);
}
else if (search.indexOf("randBg") !== -1) {
    let officeWrap;
    const replaceOffice = () => el_tool_1.setInnards(officeWrap, [ProceduralOffice_1.genProceduralOffice()]);
    el_tool_1.setInnards(document.body, [
        officeWrap = el_tool_1.div("OfficeWrap"),
        el_tool_1.create("button", { onClick: replaceOffice }, "ReRoll"),
    ]);
    replaceOffice();
}
else if (search.indexOf("exportPays") !== -1) {
    el_tool_1.setInnards(document.body, ExportPays_1.default());
}
else if (search.indexOf("instructions") !== -1) {
    document.title = `Synergy - Instructions (${today.getFullYear()}-${today.getMonth()}-${today.getDate()})`;
    const instructions = new Instructions_1.default();
    el_tool_1.setInnards(document.body, instructions.domNode);
}
else {
    const client = new Synergy_1.default();
    if (search.indexOf("board") !== -1) {
        document.title = "Synergy - Board Print";
        el_tool_1.setInnards(document.body, [client.board.domNode]);
    }
    else {
        el_tool_1.setInnards(document.body, [client.domNode]);
    }
}
// interface Layered extends Array<number | Layered> { }
// function flatten(flattenMe: Layered) {
// 	const flat: number[] = [];
// 	const permeate = (layered: Layered) => {
// 		layered.forEach((item) => {
// 			if(Array.isArray(item)) {
// 				permeate(item);
// 			} else {
// 				flat.push(item);
// 			}
// 		})
// 	}
// 	permeate(flattenMe);
// 	return flat;
// }
// Kris Bowman
//# sourceMappingURL=app.js.map?tm=1547331159142
});
___scope___.file("UI/PrintSheets/printSheets.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const el_tool_1 = require("el-tool");
const EmployeeDeck_1 = require("../../Entities/Decks/Employee/EmployeeDeck");
const Employee_1 = require("../../Entities/Decks/Employee");
const ProjectDeck_1 = require("../../Entities/Decks/Project/ProjectDeck");
const EventCardDeck_1 = require("../../Entities/Decks/Event/EventCardDeck");
const OptionCardDeck_1 = require("../../Entities/Decks/Option/OptionCardDeck");
const QuirkDeck_1 = require("../../Entities/Decks/Quirk/QuirkDeck");
const SpecialtyDeck_1 = require("../../Entities/Decks/Specialty/SpecialtyDeck");
const MissionDeck_1 = require("../../Entities/Decks/Mission/MissionDeck");
const PlaceableDeck_1 = require("../../Entities/Decks/Placeable/PlaceableDeck");
// import AgendaCard from "../../Entities/AgendaCard/AgendaCard";
const Instructions_1 = require("../../Entities/Instructions/Instructions");
require("./printSheet.scss");
function printSheets(type = "agenda") {
    return el_tool_1.div(`Print-Sheet ${type}`, () => {
        if (type === "employee") {
            return EmployeeDeck_1.rawEmployeeList
                .filter((raw) => raw.pay && raw.name !== "Examplo")
                .map((raw) => (new Employee_1.Employee(null, raw)))
                .map((card) => card.domNode);
        }
        else if (type === "project") {
            return ProjectDeck_1.ProjectList.filter((proj) => proj.raw.tasks).map((proj) => proj.domNode);
        }
        else if (type === "agenda") {
            // return AgendaList.map((card) => card.domNode);
            return Instructions_1.default.agendaCard.domNode;
        }
        else if (type === "event") {
            return EventCardDeck_1.EventCardList.filter((card) => (card.raw.description || "").length).map((card) => card.domNode);
        }
        else if (type === "option") {
            return OptionCardDeck_1.OptionCardList.filter((card) => (card.raw.description || "").length).map((card) => card.domNode);
        }
        else if (type === "quirk") {
            return QuirkDeck_1.QuirkCardList.map((card) => card.domNode);
        }
        else if (type === "specialty") {
            return SpecialtyDeck_1.SpecialtyList.map((card) => card.domNode);
        }
        else if (type === "mission") {
            return MissionDeck_1.MissionList.map((card) => card.domNode);
        }
        else if (type === "placeable") {
            return PlaceableDeck_1.PlaceableList.map((card) => card.domNode);
        }
    });
}
exports.default = printSheets;
//# sourceMappingURL=printSheets.js.map
});
___scope___.file("Entities/Decks/Employee/EmployeeDeck.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Employee_1 = require("./Employee");
const Deck_1 = require("../Deck");
const employeeSheet = require("../../../Registry/sheets/json/Synergy - Employees.json");
const rawEmployeeList = [];
exports.rawEmployeeList = rawEmployeeList;
// const nameProps = ["Name1", "Name2", "Name3", "Name4"];	
const colors = ["purple", "orange", "forest", "teal"];
employeeSheet.forEach((card, index) => {
    if (!card.Card) {
        return;
    }
    const raw = {
        teamColor: colors[index % colors.length],
        art: parseInt(card.Art) || 0,
        logic: parseInt(card.Logic) || 0,
        charisma: parseInt(card.Charisma) || 0,
        patience: parseInt(card.Patience) || 0,
        name: card.Name,
        animal: card.Animal,
        concept: card.Concept,
        pay: card.Pay,
        jobs: 0,
        minSyn: card.MinSyn,
    };
    rawEmployeeList.push(raw);
});
class EmployeeDeck extends Deck_1.default {
    constructor(parent) {
        super(parent, function () {
            return rawEmployeeList.map((raw) => new Employee_1.default(this, raw));
        }, Employee_1.default.getBackNode);
    }
    onClick() {
        const card = this.draw();
        this.game.subNodes.drawnCards.appendChild(card.domNode);
        const quirk = this.game.quirkDeck.draw();
        card.setQuirk(quirk);
    }
}
exports.default = EmployeeDeck;
//# sourceMappingURL=EmployeeDeck.js.map
});
___scope___.file("Entities/Decks/Employee/Employee.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const el_tool_1 = require("el-tool");
const omject_1 = require("omject");
const ProceduralOffice_1 = require("../../../UI/ProceduralOffice");
const Util_1 = require("../../../Util");
const employeeImages = require("../../../assets/Employees/Character/");
console.log(employeeImages);
require("./employee.scss");
const logo_png_1 = require("./logo.png");
console.log(logo_png_1.default);
const dragImage = new Image();
dragImage.src = logo_png_1.default;
const handFonts = [
    "Cedarville-Cursive",
    "Gloria-Hallelujah",
    "Gochi-Hand",
    // "Just-Another-Hand",
    "Kalam",
    "La-Belle-Aurore",
    "Nothing-You-Could-Do",
    "Over-the-Rainbow",
    "Reenie-Beanie",
    "Rock-Salt",
    "Shadows-Into-Light",
    // "Tangerine",
    "Walter-Turncoat",
];
class Employee extends omject_1.BasicOmjectClass {
    constructor(parent, raw) {
        super(parent);
        this.raw = raw;
        this.game = this.getAncestor("Synergy");
        // console.log(parent, this.game, this.parent());
    }
    get domNode() {
        if (this._domNode === undefined) {
            const { name, animal, art, logic, charisma, patience, concept, teamColor, pay, minSyn } = this.raw;
            this._domNode = el_tool_1.div(`Employee-Card front team_${teamColor}`, {
                attr: { draggable: "true" },
                on: {
                    dragstart: (event) => {
                        this.game.dragDepo.employeeCard = this;
                        if (this.isOnBoard()) {
                            event.dataTransfer.setDragImage(document.createElement('img'), 0, 0);
                        }
                        event.dataTransfer.effectAllowed = 'move';
                        event.dataTransfer.setData("text/plain", "employeeCard");
                    },
                    dragend: (event) => {
                        this.game.dragDepo.employeeCard = undefined;
                    }
                }
            }, [
                el_tool_1.div("Employee-bg", [
                    el_tool_1.div("Floor"),
                    el_tool_1.div("Desk"),
                    el_tool_1.div("Fade"),
                    el_tool_1.div("TeamColor"),
                ]),
                el_tool_1.div("Employee-fg", [
                    el_tool_1.div(`Name --font_${handFonts[Math.floor(Math.random() * handFonts.length)]}`, name.replace(/\(.\)/g, "")),
                    el_tool_1.div("Needs", [
                        el_tool_1.div("Synergy", Util_1.parseRawText(`${minSyn <= -10 ? "Any" : minSyn}SYN`)),
                        " / ",
                        el_tool_1.div("Cost", `${pay}k`),
                    ]),
                    el_tool_1.div("Photo", {
                        style: { transform: `rotate(${(Math.random() * 1) - .5}deg)` },
                    }, [
                        el_tool_1.div("Photo-bg", [ProceduralOffice_1.genProceduralOffice()]),
                        () => {
                            const svgPath = (animal ? employeeImages[animal.replace(/\-/g, "")] : null) || employeeImages["Frog"];
                            if (svgPath !== undefined) {
                                return { hackableHTML: `<img src=${svgPath.default}>` };
                            }
                            return null;
                        }
                    ]),
                    el_tool_1.div("Stats", [
                        ["ART", "art", art],
                        ["LOG", "logic", logic],
                        ["CHA", "charisma", charisma],
                        ["PAT", "patience", patience],
                    ].map(([label, name, level]) => {
                        return el_tool_1.div(`Stat ${name}`, [
                            el_tool_1.div("StatLabel", label),
                            ...((new Array(level)).fill(1).map(() => el_tool_1.span(`StatIcon ${name}`)) || []),
                        ]);
                    })),
                    this.quirkEl = el_tool_1.div("Quirk", [
                        el_tool_1.div("Info", "Place Quirk Card Here"),
                    ]),
                ]),
            ]);
        }
        return this._domNode;
    }
    static getBackNode() {
        return el_tool_1.div("Employee-Card back", "EMPLOYEE CARD");
    }
    moveToBoard() {
        this.game.board.officeSpaceEl.appendChild(this.domNode);
    }
    isOnBoard() {
        return this.game.board.officeSpaceEl.contains(this.domNode);
    }
    setQuirk(quirk) {
        this.quirk = quirk;
        this.quirkEl.appendChild(quirk.domNode);
    }
}
exports.default = Employee;
//# sourceMappingURL=Employee.js.map
});
___scope___.file("UI/ProceduralOffice/index.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var genProceduralOffice_1 = require("./genProceduralOffice");
exports.genProceduralOffice = genProceduralOffice_1.default;
//# sourceMappingURL=index.js.map
});
___scope___.file("UI/ProceduralOffice/genProceduralOffice.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const el_tool_1 = require("el-tool");
const seedrandom_1 = require("seedrandom");
require("./office.scss");
function genProceduralOffice(seed) {
    seed = seed || Math.random() + "";
    // const canvas = document.createElement("canvas");
    // canvas.width = canvas.height = 350;
    // const painter = canvas.getContext("2d");
    const arng = seedrandom_1.alea(seed);
    const random = (high = 1, low = 0, floor) => {
        const range = high - low;
        let num = low + (arng() * range);
        if (floor !== undefined) {
            num -= (num % floor);
        }
        return num;
    };
    const skies = [
        "linear-gradient(#00dbff, #fcfcfc)",
        "linear-gradient(#f5d9fc, #f9f4e8)",
        "linear-gradient(#abf8fb, #fcfefe)",
        "linear-gradient(#43497a, #586c6c)",
        "linear-gradient(#00ffe4, #edffff)",
    ];
    const skyColor = skies[random(skies.length, null, 1)];
    const numCols = random(3 + 1, 2, 1);
    const numRows = random(8 + 1, 4, 1);
    const frs = ["1fr", "1.5fr", "2fr"];
    const colSizes = (new Array(numCols)).fill(null).map(() => frs[random(frs.length, null, 1)]);
    const bgDistance = random() > 0.5 ? "bg-near" : "bg-far";
    const floorColors = [
        "#BBB",
        "#AAA",
        "#999",
        "rgb(53, 74, 117)",
        "rgb(81, 69, 54)",
    ];
    let wall;
    let floor;
    const office = el_tool_1.div(`Office ${bgDistance}`, { style: {
            backgroundImage: skyColor
        } }, [
        wall = el_tool_1.div("Wall", {
            style: {
                gridTemplateColumns: colSizes.join(" "),
                gridTemplateRows: (new Array(numRows)).fill("1fr").join(" "),
            },
        }),
        floor = el_tool_1.div("Floor", {
            style: {
                height: `${random(30, 15)}%`,
                backgroundColor: floorColors[random(floorColors.length, null, 1)],
            },
        }),
    ]);
    const allMaterials = ["glass", "wood", "brick", "stone", "paint"];
    const materials = [
        ...allMaterials.splice(random(allMaterials.length, null, 1), 1),
        ...allMaterials.splice(random(allMaterials.length, null, 1), 1),
    ];
    const paintIndex = materials.indexOf("paint");
    if (paintIndex !== -1) {
        const paints = ["paint-blue", "paint-beige", "paint-grey", "paint-dark-grey"];
        materials.splice(paintIndex, 1, paints[random(paints.length, null, 1)]);
    }
    const halfRows = Math.floor(numRows / 2);
    const noWindow = materials.indexOf("glass") === -1;
    let windowHeight;
    let materialNum;
    if (noWindow) {
        windowHeight = random(halfRows + 1, 2, 1);
        if (numCols === 3) {
            materialNum = 0;
        }
        else {
            const bestWindowMaterials = ["paint", "wood", "brick", "stone"];
            for (let i = 0; materialNum === undefined && i < bestWindowMaterials.length; i++) {
                const targetIndex = materials.indexOf(bestWindowMaterials[i]);
                if (targetIndex !== -1) {
                    materialNum = targetIndex;
                }
            }
        }
    }
    let matIndex = 0;
    for (let col = 0; col < numCols;) {
        const material = materials[matIndex];
        let span = Math.min(random(2 + 1, 1, 1), numCols - col);
        if (matIndex === 0) {
            span = Math.min(numCols - 1, span);
        }
        let rowsAboveWindow = numRows;
        if (matIndex % 2 === materialNum) {
            rowsAboveWindow -= (windowHeight + (numRows - halfRows));
        }
        if (rowsAboveWindow !== 0) {
            wall.appendChild(el_tool_1.div(`Material ${material}`, {
                style: {
                    gridRowStart: 1 + "",
                    gridRowEnd: `span ${rowsAboveWindow}`,
                    gridColumnStart: (col + 1) + "",
                    gridColumnEnd: `span ${span}`,
                },
            }));
        }
        if (matIndex % 2 === materialNum && noWindow) {
            wall.appendChild(el_tool_1.div(`Material glass`, {
                style: {
                    gridRowStart: rowsAboveWindow + 1 + "",
                    gridRowEnd: `span ${windowHeight}`,
                    gridColumnStart: (col + 1) + "",
                    gridColumnEnd: `span ${span}`,
                },
            }));
            wall.appendChild(el_tool_1.div(`Material ${material}`, {
                style: {
                    gridRowStart: halfRows + 1 + "",
                    gridRowEnd: `span ${numRows - halfRows}`,
                    gridColumnStart: (col + 1) + "",
                    gridColumnEnd: `span ${span}`,
                },
            }));
        }
        col += span;
        matIndex = (matIndex + 1) % materials.length;
    }
    // painter.fillStyle = skyColor;
    // painter.fillRect(0, 0, canvas.width, canvas.height);
    return office;
}
exports.default = genProceduralOffice;
//# sourceMappingURL=genProceduralOffice.js.map
});
___scope___.file("UI/ProceduralOffice/office.scss", function(exports, require, module, __filename, __dirname){


require("fuse-box-css")("default/UI/ProceduralOffice/office.scss", ".Office {\n  position: relative;\n  height: 350px;\n  width: 350px; }\n  .Office > .Wall {\n    display: grid;\n    height: 100%;\n    width: 100%; }\n    .Office > .Wall > .Material.brick {\n      background-image: url(\"/css-resources/assets/Employees/Backgrounds/Material/Brick.png\"); }\n    .Office > .Wall > .Material.stone {\n      background-image: url(\"/css-resources/assets/Employees/Backgrounds/Material/Stone.png\");\n      border: 1px solid #554; }\n    .Office > .Wall > .Material.wood {\n      background-image: url(\"/css-resources/assets/Employees/Backgrounds/Material/Board.png\"); }\n    .Office > .Wall > .Material.glass {\n      background-image: url(\"/css-resources/assets/Employees/Backgrounds/Material/Glass.png\");\n      border: 2px solid #444; }\n    .Office > .Wall > .Material.paint-grey {\n      background: #EEE; }\n    .Office > .Wall > .Material.paint-beige {\n      background: beige; }\n    .Office > .Wall > .Material.paint-dark-grey {\n      background: #888; }\n    .Office > .Wall > .Material.paint-blue {\n      background: #95adbd; }\n  .Office > .Floor {\n    position: absolute;\n    bottom: 0px;\n    width: 100%; }\n  .Office.bg-near > .Wall > .Material.brick {\n    background-size: 50px 50px; }\n  .Office.bg-near > .Wall > .Material.stone {\n    background-size: 180px 90px; }\n  .Office.bg-near > .Wall > .Material.wood {\n    background-size: 240px 40px; }\n  .Office.bg-near > .Wall > .Material.glass {\n    background-size: 100px 100px; }\n  .Office.bg-near > .Floor {\n    display: none; }\n  .Office.bg-far > .Wall > .Material.brick {\n    background-size: 34px 36px; }\n  .Office.bg-far > .Wall > .Material.stone {\n    background-size: 90px 45px; }\n  .Office.bg-far > .Wall > .Material.wood {\n    background-size: 120px 20px; }\n  .Office.bg-far > .Wall > .Material.glass {\n    background-size: 50px 50px; }\n\n/*# sourceMappingURL=/css-sourcemaps/071fc0219.map */")
});
___scope___.file("Util/index.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fromMD_1 = require("./fromMD");
exports.fromMD = fromMD_1.default;
const injectSymbols_1 = require("./injectSymbols");
exports.injectSymbols = injectSymbols_1.default;
// export {default as fromMD} from "./fromMD";
var minimizeWidth_1 = require("./minimizeWidth");
exports.minimizeWidth = minimizeWidth_1.default;
// export {default as injectSymbols} from "./injectSymbols";
function parseRawText(rawText = "") {
    return fromMD_1.default(injectSymbols_1.default(rawText));
}
exports.parseRawText = parseRawText;
//# sourceMappingURL=index.js.map
});
___scope___.file("Util/fromMD.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const MarkdownIt = require("markdown-it");
const MD = MarkdownIt({
    html: true,
    xhtmlOut: true,
});
function fromMD(str) {
    return { hackableHTML: MD.render(str) };
}
exports.default = fromMD;
//# sourceMappingURL=fromMD.js.map
});
___scope___.file("Util/injectSymbols.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function injectSymbols(rawHtml) {
    return rawHtml.replace(/{{(.+?)}}/g, (match, g1) => `<span class="Icon ${g1.toLowerCase()}">${g1.charAt(0)}</span>`)
        .replace(/_(.+?)_/g, (match, g1) => `<u>${g1}</u>`)
        .replace(/SYN/g, `<span class="Icon synergy">s</span>`);
}
exports.default = injectSymbols;
//# sourceMappingURL=injectSymbols.js.map
});
___scope___.file("Util/minimizeWidth.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function minimizeWidth(domNode) {
    const squinchFurther = () => {
        // console.log("Squink")
        const startHeight = domNode.offsetHeight;
        const startWidth = domNode.offsetWidth;
        if (startWidth === 0) {
            return;
        }
        domNode.style.width = (startWidth - 1) + "px";
        // console.log("Squink", startWidth, domNode.style.width);
        requestAnimationFrame(() => requestAnimationFrame(() => {
            if (domNode.offsetHeight !== startHeight) {
                domNode.style.width = startWidth + "px";
            }
            else {
                squinchFurther();
            }
        }));
    };
    requestAnimationFrame(() => requestAnimationFrame(squinchFurther));
    return domNode;
}
exports.default = minimizeWidth;
//# sourceMappingURL=minimizeWidth.js.map
});
___scope___.file("assets/Employees/Character/index.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const BlackCat = require("./Black-Cat.svg");
exports.BlackCat = BlackCat;
const Bunny = require("./Bunny.svg");
exports.Bunny = Bunny;
const Cockroach = require("./Cockroach.svg");
exports.Cockroach = Cockroach;
const Dragon = require("./Dragon.svg");
exports.Dragon = Dragon;
const Duck = require("./Duck.svg");
exports.Duck = Duck;
const Frog = require("./Frog.svg");
exports.Frog = Frog;
const Gopher = require("./Gopher.svg");
exports.Gopher = Gopher;
const Hippo = require("./Hippo.svg");
exports.Hippo = Hippo;
const Hound = require("./Hound.svg");
exports.Hound = Hound;
const Ladybug = require("./Ladybug.svg");
exports.Ladybug = Ladybug;
const Owl = require("./Owl.svg");
exports.Owl = Owl;
const Penguin = require("./Penguin.svg");
exports.Penguin = Penguin;
const Raccoon = require("./Raccoon.svg");
exports.Raccoon = Raccoon;
const Rat = require("./Rat.svg");
exports.Rat = Rat;
const Robot = require("./Robot.svg");
exports.Robot = Robot;
const Shark = require("./Shark.svg");
exports.Shark = Shark;
const TRex = require("./T-Rex.svg");
exports.TRex = TRex;
const Tiger = require("./Tiger.svg");
exports.Tiger = Tiger;
const Totoro = require("./Totoro.svg");
exports.Totoro = Totoro;
const Unicorn = require("./Unicorn.svg");
exports.Unicorn = Unicorn;
//# sourceMappingURL=index.js.map
});
___scope___.file("assets/Employees/Character/Black-Cat.svg", function(exports, require, module, __filename, __dirname){

module.exports.default = "/assets/5c04a3b1-Black-Cat.svg";
});
___scope___.file("assets/Employees/Character/Bunny.svg", function(exports, require, module, __filename, __dirname){

module.exports.default = "/assets/21b33843-Bunny.svg";
});
___scope___.file("assets/Employees/Character/Cockroach.svg", function(exports, require, module, __filename, __dirname){

module.exports.default = "/assets/5435c55e-Cockroach.svg";
});
___scope___.file("assets/Employees/Character/Dragon.svg", function(exports, require, module, __filename, __dirname){

module.exports.default = "/assets/38b47a7c-Dragon.svg";
});
___scope___.file("assets/Employees/Character/Duck.svg", function(exports, require, module, __filename, __dirname){

module.exports.default = "/assets/b7d1e154-Duck.svg";
});
___scope___.file("assets/Employees/Character/Frog.svg", function(exports, require, module, __filename, __dirname){

module.exports.default = "/assets/a0a187d1-Frog.svg";
});
___scope___.file("assets/Employees/Character/Gopher.svg", function(exports, require, module, __filename, __dirname){

module.exports.default = "/assets/a6fa7dea-Gopher.svg";
});
___scope___.file("assets/Employees/Character/Hippo.svg", function(exports, require, module, __filename, __dirname){

module.exports.default = "/assets/55ec8e83-Hippo.svg";
});
___scope___.file("assets/Employees/Character/Hound.svg", function(exports, require, module, __filename, __dirname){

module.exports.default = "/assets/914053d5-Hound.svg";
});
___scope___.file("assets/Employees/Character/Ladybug.svg", function(exports, require, module, __filename, __dirname){

module.exports.default = "/assets/b0a484d-Ladybug.svg";
});
___scope___.file("assets/Employees/Character/Owl.svg", function(exports, require, module, __filename, __dirname){

module.exports.default = "/assets/da1033d9-Owl.svg";
});
___scope___.file("assets/Employees/Character/Penguin.svg", function(exports, require, module, __filename, __dirname){

module.exports.default = "/assets/8d077303-Penguin.svg";
});
___scope___.file("assets/Employees/Character/Raccoon.svg", function(exports, require, module, __filename, __dirname){

module.exports.default = "/assets/43bee670-Raccoon.svg";
});
___scope___.file("assets/Employees/Character/Rat.svg", function(exports, require, module, __filename, __dirname){

module.exports.default = "/assets/9f9a5a6a-Rat.svg";
});
___scope___.file("assets/Employees/Character/Robot.svg", function(exports, require, module, __filename, __dirname){

module.exports.default = "/assets/de0d4e69-Robot.svg";
});
___scope___.file("assets/Employees/Character/Shark.svg", function(exports, require, module, __filename, __dirname){

module.exports.default = "/assets/47742b0e-Shark.svg";
});
___scope___.file("assets/Employees/Character/T-Rex.svg", function(exports, require, module, __filename, __dirname){

module.exports.default = "/assets/1f5e455b-T-Rex.svg";
});
___scope___.file("assets/Employees/Character/Tiger.svg", function(exports, require, module, __filename, __dirname){

module.exports.default = "/assets/7a5b10c0-Tiger.svg";
});
___scope___.file("assets/Employees/Character/Totoro.svg", function(exports, require, module, __filename, __dirname){

module.exports.default = "/assets/e9908cb0-Totoro.svg";
});
___scope___.file("assets/Employees/Character/Unicorn.svg", function(exports, require, module, __filename, __dirname){

module.exports.default = "/assets/595a97cf-Unicorn.svg";
});
___scope___.file("Entities/Decks/Employee/employee.scss", function(exports, require, module, __filename, __dirname){


require("fuse-box-css")("default/Entities/Decks/Employee/employee.scss", ".Employee-Card {\n  width: 3.5in;\n  height: 2.5in;\n  border-radius: 0.1in;\n  position: relative;\n  overflow: hidden; }\n  .Employee-Card.front {\n    display: inline-block; }\n    .Employee-Card.front > .Employee-bg, .Employee-Card.front > .Employee-fg,\n    .Employee-Card.front > .Employee-bg > * {\n      position: absolute;\n      top: 0px;\n      left: 0px;\n      height: 100%;\n      width: 100%; }\n    .Employee-Card.front > .Employee-bg > .Floor {\n      background-color: #b6b6b6;\n      background-size: 0.525in 0.525in;\n      background-image: linear-gradient(to right, #989898 1px, transparent 1px), linear-gradient(to bottom, #989898 1px, transparent 1px);\n      background-position: 43px 45px; }\n    .Employee-Card.front > .Employee-bg > .Desk {\n      display: block;\n      content: \"\";\n      background-image: url(\"/css-resources/assets/Desk.png\");\n      background-size: 3.5in auto;\n      background-position: top center;\n      background-repeat: no-repeat; }\n    .Employee-Card.front > .Employee-bg > .Fade {\n      background: rgba(255, 255, 255, 0.5); }\n    .Employee-Card.front > .Employee-bg > .TeamColor {\n      display: block;\n      content: \"\";\n      height: .25in;\n      box-shadow: 0px 0px 10px black;\n      background: #4a4a4a; }\n    .Employee-Card.front > .Employee-fg {\n      display: grid;\n      grid-template-columns: auto auto 1fr;\n      grid-template-rows: auto auto auto 1fr;\n      grid-template-areas: \"Photo Name Needs\" \"Photo Info Info\" \"Stats Info Info\" \". Info Info\"; }\n      .Employee-Card.front > .Employee-fg > * {\n        margin: 0.0625in; }\n      .Employee-Card.front > .Employee-fg > .Name, .Employee-Card.front > .Employee-fg > .Needs {\n        color: white; }\n      .Employee-Card.front > .Employee-fg > .Name {\n        position: relative;\n        grid-area: Name;\n        line-height: .25in;\n        margin-top: 0px;\n        margin-bottom: 0px; }\n        .Employee-Card.front > .Employee-fg > .Name.--font_Rock-Salt {\n          font-size: 13px;\n          letter-spacing: 2px; }\n        .Employee-Card.front > .Employee-fg > .Name.--font_Gloria-Hallelujah {\n          letter-spacing: 2px; }\n        .Employee-Card.front > .Employee-fg > .Name.--font_Reenie-Beanie {\n          font-size: 24px;\n          top: 1px; }\n        .Employee-Card.front > .Employee-fg > .Name.--font_La-Belle-Aurore {\n          font-size: 18px;\n          top: 4px; }\n        .Employee-Card.front > .Employee-fg > .Name.--font_Shadows-Into-Light {\n          letter-spacing: 2px; }\n        .Employee-Card.front > .Employee-fg > .Name.--font_Kalam {\n          top: 2px; }\n      .Employee-Card.front > .Employee-fg > .Needs {\n        grid-area: Needs;\n        text-align: right; }\n        .Employee-Card.front > .Employee-fg > .Needs > .Synergy, .Employee-Card.front > .Employee-fg > .Needs > .Cost {\n          display: inline-block; }\n          .Employee-Card.front > .Employee-fg > .Needs > .Synergy .Icon, .Employee-Card.front > .Employee-fg > .Needs > .Cost .Icon {\n            height: 1em;\n            width: 1em;\n            margin: 0px; }\n      .Employee-Card.front > .Employee-fg > .Photo {\n        grid-area: Photo;\n        width: 1.25in;\n        height: 1.25in;\n        border: 3px solid white;\n        background: #000;\n        position: relative;\n        overflow: hidden; }\n        .Employee-Card.front > .Employee-fg > .Photo > .Photo-bg {\n          height: 100%;\n          width: 100%;\n          filter: blur(1.05px); }\n          .Employee-Card.front > .Employee-fg > .Photo > .Photo-bg > .Office {\n            transform: translate(-50%, -50%) scale(0.35);\n            position: absolute;\n            top: 50%;\n            left: 50%; }\n          .Employee-Card.front > .Employee-fg > .Photo > .Photo-bg::after {\n            position: absolute;\n            content: \"\";\n            display: block;\n            top: 0px;\n            left: 0px;\n            height: 100%;\n            width: 100%;\n            opacity: 0.4;\n            box-shadow: inset 0 0 50px black, inset 0 0 20px black; }\n        .Employee-Card.front > .Employee-fg > .Photo > img {\n          position: absolute;\n          top: 50%;\n          left: 50%;\n          transform: translate(-50%, -45%);\n          max-height: 125%;\n          max-width: 125%; }\n      .Employee-Card.front > .Employee-fg > .Stats {\n        position: relative;\n        grid-area: Stats;\n        background: #fff3dc;\n        padding: .0625in;\n        margin-top: 0.03125in;\n        font-size: 14px;\n        border-radius: 3px 3px 1px 1px;\n        box-shadow: 0px 0px 25px rgba(0, 0, 0, 0.2); }\n        .Employee-Card.front > .Employee-fg > .Stats::before {\n          position: absolute;\n          display: block;\n          content: \"\";\n          top: 0px;\n          left: 5px;\n          border-bottom: 0.0625in solid wheat;\n          border-left: 0.03125in solid transparent;\n          border-right: 0.03125in solid transparent;\n          height: 0;\n          width: 0.25in;\n          transform: translate(0px, -100%); }\n        .Employee-Card.front > .Employee-fg > .Stats > .Stat:not(:last-child) {\n          border-bottom: 1px solid rgba(0, 0, 0, 0.1); }\n        .Employee-Card.front > .Employee-fg > .Stats > .Stat > .StatLabel {\n          display: inline-block;\n          width: 0.375in;\n          border-right: 1px solid rgba(0, 0, 0, 0.1);\n          font-family: monospace; }\n        .Employee-Card.front > .Employee-fg > .Stats > .Stat > .StatIcon {\n          display: inline-block;\n          margin-left: 2px;\n          height: 16px;\n          width: 16px;\n          border-radius: 20px;\n          background-size: contain;\n          vertical-align: sub; }\n          .Employee-Card.front > .Employee-fg > .Stats > .Stat > .StatIcon.art {\n            background-image: url(\"/css-resources/assets/Icon/Skill/Art.svg\"); }\n          .Employee-Card.front > .Employee-fg > .Stats > .Stat > .StatIcon.logic {\n            background-image: url(\"/css-resources/assets/Icon/Skill/Logic.svg\"); }\n          .Employee-Card.front > .Employee-fg > .Stats > .Stat > .StatIcon.charisma {\n            background-image: url(\"/css-resources/assets/Icon/Skill/Charisma.svg\"); }\n          .Employee-Card.front > .Employee-fg > .Stats > .Stat > .StatIcon.patience {\n            background-image: url(\"/css-resources/assets/Icon/Skill/Patience.svg\"); }\n          .Employee-Card.front > .Employee-fg > .Stats > .Stat > .StatIcon:nth-child(2) {\n            margin-left: 7px; }\n      .Employee-Card.front > .Employee-fg > .Quirk {\n        position: relative;\n        grid-area: Info;\n        border: 2px dashed #fffe;\n        border-radius: 0.1in;\n        background: rgba(250, 250, 250, 0.3);\n        width: 2in;\n        height: 2in; }\n        .Employee-Card.front > .Employee-fg > .Quirk > .Quirk-Card {\n          position: absolute;\n          top: -2px;\n          left: -2px; }\n        .Employee-Card.front > .Employee-fg > .Quirk > .Info {\n          position: absolute;\n          display: block;\n          content: \"Place Quirk Card Here\";\n          color: #fffe;\n          font-size: 20px;\n          text-align: center;\n          width: 1.5in;\n          top: 50%;\n          left: 50%;\n          transform: translate(-50%, -50%) rotate(-45deg); }\n  .Employee-Card.back {\n    color: transparent;\n    background-image: url(\"/css-resources/assets/Employees/Backgrounds/Card/Back/Employee.svg\");\n    background-size: contain;\n    background-position: center center;\n    background-color: #fff3dc; }\n\n/*# sourceMappingURL=/css-sourcemaps/02c90633e.map */")
});
___scope___.file("Entities/Decks/Employee/logo.png", function(exports, require, module, __filename, __dirname){

module.exports.default = "/assets/57d148b3-logo.png";
});
___scope___.file("Entities/Decks/Deck.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const el_tool_1 = require("el-tool");
const omject_1 = require("omject");
class Deck extends omject_1.BasicOmjectClass {
    constructor(parent, deckSource, cardBack) {
        super(parent);
        this.deckSource = deckSource;
        this.cardBack = cardBack;
        this.game = this.getAncestor("Synergy");
        this.reset();
    }
    get domNode() {
        if (!this._domNode) {
            this._domNode = el_tool_1.div("Deck", {
                onClick: () => this.onClick(),
            }, [
                !this.cardBack ? null : (typeof this.cardBack === "function" ? this.cardBack() : this.cardBack.cloneNode(true)),
            ]);
        }
        return this._domNode;
    }
    onClick() {
        const card = this.draw();
        this.game.subNodes.drawnCards.appendChild(card.domNode);
    }
    reset() {
        this.cards = [].concat(this.deckSource());
        this.shuffle();
    }
    shuffle() {
        const { cards } = this;
        const deckSize = cards.length;
        for (let i = 0; i < deckSize; i++) {
            const source = cards[i];
            const randomIndex = Math.floor(Math.random() * deckSize);
            cards[i] = cards[randomIndex];
            cards[randomIndex] = source;
        }
    }
    draw(numCards) {
        if (numCards === undefined || numCards === 1) {
            return this.cards.pop();
        }
        else {
            const out = [];
            for (let i = 0; i < numCards; i++) {
                out.push(this.cards.pop());
            }
            return out;
        }
    }
    placeCardsAtop(cards) {
        this.cards.concat(cards);
    }
    placeCardsAtBottom(cards) {
        this.cards.unshift(...cards);
    }
}
exports.default = Deck;
//# sourceMappingURL=Deck.js.map
});
___scope___.file("Registry/sheets/json/Synergy - Employees.json", function(exports, require, module, __filename, __dirname){

module.exports = [
  {
    "Card": 2,
    "Concept": "Lemon",
    "Art": 0,
    "Logic": 0,
    "Charisma": 0,
    "Patience": 3,
    "PayOffset": -1,
    "SynOffset": 0,
    "": 0,
    "Pay": 1,
    "MinSyn": -10,
    "Animal": "Hound",
    "Svg": "✓",
    "Name": "Morty (m)",
    "Gender": "m",
    "Hair Color": "Brown",
    "Hair Length": "Short",
    "Hair Type": "Blob",
    "Shirt Type or color": "Yellow t-shirt",
    "Head Accessories": "None",
    "Skin Color": "Beige"
  },
  {
    "Card": 3,
    "Concept": "Intern (Art)",
    "Art": 1,
    "Logic": 0,
    "Charisma": 0,
    "Patience": 3,
    "PayOffset": 0,
    "SynOffset": 0,
    "": 0,
    "Pay": 1,
    "MinSyn": -10,
    "Animal": "Gopher",
    "Svg": "✓",
    "Name": "Taylor (a)",
    "Gender": "a",
    "Hair Color": 0,
    "Hair Length": 0,
    "Hair Type": 0,
    "Shirt Type or color": 0,
    "Head Accessories": 0,
    "Skin Color": 0
  },
  {
    "Card": 5,
    "Concept": "Jr Artist (+PAT)",
    "Art": 1,
    "Logic": 0,
    "Charisma": 0,
    "Patience": 2,
    "PayOffset": 0,
    "SynOffset": -5,
    "": 0,
    "Pay": 1,
    "MinSyn": -5,
    "Animal": "Bunny",
    "Svg": "✓",
    "Name": "Kat (f)",
    "Gender": "f",
    "Hair Color": "Orange",
    "Hair Length": "Shoulder-length",
    "Hair Type": "Bob",
    "Shirt Type or color": "Red",
    "Head Accessories": "Fishbone Necklace",
    "Skin Color": "Light pink"
  },
  {
    "Card": 4,
    "Concept": "Jr Engineer",
    "Art": 0,
    "Logic": 1,
    "Charisma": 0,
    "Patience": 2,
    "PayOffset": 0,
    "SynOffset": 0,
    "": 0,
    "Pay": 1,
    "MinSyn": 0,
    "Animal": "Raccoon",
    "Svg": "✓",
    "Name": "Scott (m)",
    "Gender": "m",
    "Hair Color": 0,
    "Hair Length": 0,
    "Hair Type": 0,
    "Shirt Type or color": 0,
    "Head Accessories": 0,
    "Skin Color": 0
  },
  {
    "Card": 6,
    "Concept": "Jr Sales",
    "Art": 0,
    "Logic": 0,
    "Charisma": 2,
    "Patience": 2,
    "PayOffset": 0,
    "SynOffset": 5,
    "": 0,
    "Pay": 2,
    "MinSyn": 5,
    "Animal": "Rat",
    "Svg": "✓",
    "Name": "Aiden (m)",
    "Gender": "m",
    "Hair Color": "Brown",
    "Hair Length": "Short",
    "Hair Type": "Wavy",
    "Shirt Type or color": "Purple",
    "Head Accessories": 0,
    "Skin Color": "Light blue"
  },
  {
    "Card": 7,
    "Concept": "Sr Artist (+PAT)",
    "Art": 2,
    "Logic": 0,
    "Charisma": 0,
    "Patience": 1,
    "PayOffset": 0,
    "SynOffset": 0,
    "": 0,
    "Pay": 2,
    "MinSyn": 10,
    "Animal": "Frog",
    "Svg": "✓",
    "Name": "Micheal (m)",
    "Gender": "m",
    "Hair Color": 0,
    "Hair Length": 0,
    "Hair Type": 0,
    "Shirt Type or color": "Orange",
    "Head Accessories": 0,
    "Skin Color": "Light green"
  },
  {
    "Card": 8,
    "Concept": "Sr Engineer",
    "Art": 1,
    "Logic": 2,
    "Charisma": 0,
    "Patience": 1,
    "PayOffset": 0,
    "SynOffset": 0,
    "": 0,
    "Pay": 3,
    "MinSyn": 10,
    "Animal": "Owl",
    "Svg": "✓",
    "Name": "Joey (m)",
    "Gender": "m",
    "Hair Color": "Blonde",
    "Hair Length": "Short",
    "Hair Type": "Frizzy",
    "Shirt Type or color": "Blue",
    "Head Accessories": "Glasses",
    "Skin Color": "Freckled"
  },
  {
    "Card": 9,
    "Concept": "Sr Sales",
    "Art": 1,
    "Logic": 0,
    "Charisma": 3,
    "Patience": 1,
    "PayOffset": 0,
    "SynOffset": -5,
    "": 0,
    "Pay": 4,
    "MinSyn": 5,
    "Animal": "Shark",
    "Svg": "✓",
    "Name": "Ron (m)",
    "Gender": "m",
    "Hair Color": 0,
    "Hair Length": 0,
    "Hair Type": 0,
    "Shirt Type or color": 0,
    "Head Accessories": 0,
    "Skin Color": 0
  },
  {
    "Card": 10,
    "Concept": "Shalurkin (+ART)",
    "Art": 2,
    "Logic": 1,
    "Charisma": 1,
    "Patience": 0,
    "PayOffset": 0,
    "SynOffset": 0,
    "": 0,
    "Pay": 4,
    "MinSyn": 20,
    "Animal": 0,
    "Svg": 0,
    "Name": "Lemb (f)",
    "Gender": 0,
    "Hair Color": "Super light blonde",
    "Hair Length": "Ear-length",
    "Hair Type": "Bob",
    "Shirt Type or color": "White",
    "Head Accessories": 0,
    "Skin Color": 0
  },
  {
    "Card": "Jack",
    "Concept": "Jack of all trades (+ART)",
    "Art": 2,
    "Logic": 1,
    "Charisma": 1,
    "Patience": 2,
    "PayOffset": 0,
    "SynOffset": 5,
    "": 0,
    "Pay": 4,
    "MinSyn": 5,
    "Animal": 0,
    "Svg": 0,
    "Name": "Inigo ",
    "Gender": "m",
    "Hair Color": "Teal & Purple",
    "Hair Length": "Ear-Length",
    "Hair Type": "Bob",
    "Shirt Type or color": "Grey",
    "Head Accessories": "Nose ring",
    "Skin Color": "Tanned white"
  },
  {
    "Card": "Queen",
    "Concept": "Design God (PAT)",
    "Art": 3,
    "Logic": 1,
    "Charisma": 1,
    "Patience": 2,
    "PayOffset": 0,
    "SynOffset": 0,
    "": 0,
    "Pay": 5,
    "MinSyn": 0,
    "Animal": "Horse",
    "Svg": 0,
    "Name": "Clint (m)",
    "Gender": "m",
    "Hair Color": "-",
    "Hair Length": "-",
    "Hair Type": "Bald",
    "Shirt Type or color": "Button up (light blue?)",
    "Head Accessories": "Beard",
    "Skin Color": "Light pink"
  },
  {
    "Card": "King",
    "Concept": "Logic God (+ART)",
    "Art": 2,
    "Logic": 3,
    "Charisma": 1,
    "Patience": 0,
    "PayOffset": 0,
    "SynOffset": 0,
    "": 0,
    "Pay": 6,
    "MinSyn": 20,
    "Animal": "Robot",
    "Svg": "✓",
    "Name": "Arnold (m)",
    "Gender": "m",
    "Hair Color": "Dirty Blonde",
    "Hair Length": "Short",
    "Hair Type": "Wavy",
    "Shirt Type or color": "Black or Army pattern",
    "Head Accessories": "Sunglasses",
    "Skin Color": "Light brown"
  },
  {
    "Card": "Ace",
    "Concept": "Fragile Unicorn (-ART)",
    "Art": 1,
    "Logic": 2,
    "Charisma": 2,
    "Patience": 0,
    "PayOffset": 0,
    "SynOffset": -5,
    "": 0,
    "Pay": 5,
    "MinSyn": 15,
    "Animal": "Unicorn",
    "Svg": "✓",
    "Name": "Charlie (m)",
    "Gender": "m",
    "Hair Color": "Black",
    "Hair Length": "Short",
    "Hair Type": "Curly",
    "Shirt Type or color": "Brown",
    "Head Accessories": "Moustache ? um",
    "Skin Color": "Pale"
  },
  {
    "Card": 2,
    "Concept": "Lemon",
    "Art": 0,
    "Logic": 0,
    "Charisma": 0,
    "Patience": 0,
    "PayOffset": -1,
    "SynOffset": 0,
    "": 0,
    "Pay": 1,
    "MinSyn": 20,
    "Animal": "Giraffe",
    "Svg": 0,
    "Name": "Moe (m)",
    "Gender": 0,
    "Hair Color": "Black",
    "Hair Length": "Short",
    "Hair Type": "Curly",
    "Shirt Type or color": "Blue",
    "Head Accessories": "Bow tie",
    "Skin Color": "Yellow"
  },
  {
    "Card": 3,
    "Concept": "Intern (+LOG)",
    "Art": 0,
    "Logic": 1,
    "Charisma": 0,
    "Patience": 3,
    "PayOffset": 0,
    "SynOffset": 5,
    "": 0,
    "Pay": 1,
    "MinSyn": -5,
    "Animal": "Tiger",
    "Svg": "✓",
    "Name": "Chris (a)",
    "Gender": 0,
    "Hair Color": "White",
    "Hair Length": "Short",
    "Hair Type": "Straight",
    "Shirt Type or color": "Purple",
    "Head Accessories": "Glasses & white beard",
    "Skin Color": "Grey"
  },
  {
    "Card": 5,
    "Concept": "Jr Artist",
    "Art": 1,
    "Logic": 0,
    "Charisma": 0,
    "Patience": 2,
    "PayOffset": 0,
    "SynOffset": 0,
    "": 0,
    "Pay": 1,
    "MinSyn": 0,
    "Animal": "Chameleon",
    "Svg": 0,
    "Name": "Diana (f)",
    "Gender": 0,
    "Hair Color": "Dark Brown",
    "Hair Length": "Shoulder-length",
    "Hair Type": "Straight-bob",
    "Shirt Type or color": "Pink",
    "Head Accessories": 0,
    "Skin Color": "Pale"
  },
  {
    "Card": 4,
    "Concept": "Jr Engineer (+PAT)",
    "Art": 0,
    "Logic": 1,
    "Charisma": 0,
    "Patience": 2,
    "PayOffset": 0,
    "SynOffset": 0,
    "": 0,
    "Pay": 1,
    "MinSyn": 0,
    "Animal": "Armadillo",
    "Svg": 0,
    "Name": "Brad (m)",
    "Gender": 0,
    "Hair Color": "Black",
    "Hair Length": "Long",
    "Hair Type": "Dreads",
    "Shirt Type or color": "Green",
    "Head Accessories": 0,
    "Skin Color": "Dark Brown"
  },
  {
    "Card": 6,
    "Concept": "Jr Sales",
    "Art": 0,
    "Logic": 0,
    "Charisma": 2,
    "Patience": 2,
    "PayOffset": 0,
    "SynOffset": -5,
    "": 0,
    "Pay": 2,
    "MinSyn": -5,
    "Animal": 0,
    "Svg": 0,
    "Name": "Ed (m)",
    "Gender": 0,
    "Hair Color": "Black",
    "Hair Length": "Short",
    "Hair Type": 0,
    "Shirt Type or color": "Baby Blue",
    "Head Accessories": 0,
    "Skin Color": "Brown"
  },
  {
    "Card": 7,
    "Concept": "Sr Artist",
    "Art": 2,
    "Logic": 1,
    "Charisma": 0,
    "Patience": 1,
    "PayOffset": 0,
    "SynOffset": 0,
    "": 0,
    "Pay": 3,
    "MinSyn": 10,
    "Animal": 0,
    "Svg": 0,
    "Name": "Rafael (f)",
    "Gender": 0,
    "Hair Color": 0,
    "Hair Length": 0,
    "Hair Type": 0,
    "Shirt Type or color": "Red",
    "Head Accessories": 0,
    "Skin Color": "Light green"
  },
  {
    "Card": 8,
    "Concept": "Sr Engineer (+PAT)",
    "Art": 0,
    "Logic": 2,
    "Charisma": 0,
    "Patience": 1,
    "PayOffset": 0,
    "SynOffset": 5,
    "": 0,
    "Pay": 2,
    "MinSyn": 15,
    "Animal": "Penguin",
    "Svg": "✓",
    "Name": "Jodie (f)",
    "Gender": 0,
    "Hair Color": "Blonde highlights",
    "Hair Length": "Shoulder-length",
    "Hair Type": "Straight",
    "Shirt Type or color": "Blue button up",
    "Head Accessories": 0,
    "Skin Color": "Tan"
  },
  {
    "Card": 9,
    "Concept": "Sr Sales",
    "Art": 0,
    "Logic": 1,
    "Charisma": 3,
    "Patience": 1,
    "PayOffset": 0,
    "SynOffset": 0,
    "": 0,
    "Pay": 4,
    "MinSyn": 10,
    "Animal": 0,
    "Svg": 0,
    "Name": "Felix (m)",
    "Gender": 0,
    "Hair Color": "Blonde",
    "Hair Length": "Short",
    "Hair Type": "Straight",
    "Shirt Type or color": "Light pink button up",
    "Head Accessories": 0,
    "Skin Color": "Tanned white"
  },
  {
    "Card": 10,
    "Concept": "Shalurkin (+LOG)",
    "Art": 1,
    "Logic": 2,
    "Charisma": 1,
    "Patience": 0,
    "PayOffset": 0,
    "SynOffset": 0,
    "": 0,
    "Pay": 4,
    "MinSyn": 20,
    "Animal": "Ladybug",
    "Svg": "✓",
    "Name": "Jacqueline (f)",
    "Gender": 0,
    "Hair Color": "Blonde",
    "Hair Length": "Short",
    "Hair Type": "Curly",
    "Shirt Type or color": "Black",
    "Head Accessories": "Red lipstick",
    "Skin Color": "Pale"
  },
  {
    "Card": "Jack",
    "Concept": "Jack of all trades (+LOG)",
    "Art": 1,
    "Logic": 2,
    "Charisma": 1,
    "Patience": 2,
    "PayOffset": 0,
    "SynOffset": -5,
    "": 0,
    "Pay": 4,
    "MinSyn": -5,
    "Animal": 0,
    "Svg": 0,
    "Name": "Janice (f)",
    "Gender": 0,
    "Hair Color": "Dirty Blonde",
    "Hair Length": "Long",
    "Hair Type": "Braids",
    "Shirt Type or color": "Black",
    "Head Accessories": "Nose ring",
    "Skin Color": "Pale"
  },
  {
    "Card": "Queen",
    "Concept": "Design God (+LOG)",
    "Art": 3,
    "Logic": 2,
    "Charisma": 1,
    "Patience": 0,
    "PayOffset": 0,
    "SynOffset": 0,
    "": 0,
    "Pay": 6,
    "MinSyn": 20,
    "Animal": 0,
    "Svg": 0,
    "Name": "Jared (m)",
    "Gender": 0,
    "Hair Color": "Blonde",
    "Hair Length": "Shoulder-length",
    "Hair Type": "Frizzy",
    "Shirt Type or color": "Grey",
    "Head Accessories": "Glasses",
    "Skin Color": "Freckled"
  },
  {
    "Card": "King",
    "Concept": "Logic God (+PAT)",
    "Art": 1,
    "Logic": 3,
    "Charisma": 1,
    "Patience": 2,
    "PayOffset": 0,
    "SynOffset": 5,
    "": 0,
    "Pay": 5,
    "MinSyn": 5,
    "Animal": 0,
    "Svg": 0,
    "Name": "Dexter (m)",
    "Gender": 0,
    "Hair Color": "Orange",
    "Hair Length": 0,
    "Hair Type": 0,
    "Shirt Type or color": "White",
    "Head Accessories": "Black thick-rimmed glasses",
    "Skin Color": "Light pink"
  },
  {
    "Card": "Ace",
    "Concept": "Fragile Unicorn (-LOG)",
    "Art": 2,
    "Logic": 1,
    "Charisma": 2,
    "Patience": 0,
    "PayOffset": 0,
    "SynOffset": 0,
    "": 0,
    "Pay": 5,
    "MinSyn": 20,
    "Animal": "Alien",
    "Svg": 0,
    "Name": "Rick (m)",
    "Gender": 0,
    "Hair Color": "Blue",
    "Hair Length": "Shoulder-length",
    "Hair Type": "Mad Scientist",
    "Shirt Type or color": "White",
    "Head Accessories": 0,
    "Skin Color": "White"
  },
  {
    "Card": 2,
    "Concept": "Lemon",
    "Art": 0,
    "Logic": 0,
    "Charisma": 0,
    "Patience": 1,
    "PayOffset": -1,
    "SynOffset": 0,
    "": 0,
    "Pay": 1,
    "MinSyn": 10,
    "Animal": 0,
    "Svg": 0,
    "Name": "Karen (f)",
    "Gender": 0,
    "Hair Color": "Orange",
    "Hair Length": "Long",
    "Hair Type": 0,
    "Shirt Type or color": "Green",
    "Head Accessories": "Earrings",
    "Skin Color": "Pink"
  },
  {
    "Card": 3,
    "Concept": "Intern (CHA)",
    "Art": 0,
    "Logic": 0,
    "Charisma": 1,
    "Patience": 3,
    "PayOffset": 0,
    "SynOffset": 5,
    "": 0,
    "Pay": 1,
    "MinSyn": -5,
    "Animal": "Hippo",
    "Svg": "✓",
    "Name": "Pat (a)",
    "Gender": 0,
    "Hair Color": "Dark brown",
    "Hair Length": "Short",
    "Hair Type": "Curly",
    "Shirt Type or color": "Plaid",
    "Head Accessories": "Black thick-rimmed glasses",
    "Skin Color": "Pale"
  },
  {
    "Card": 5,
    "Concept": "Jr Artist",
    "Art": 1,
    "Logic": 0,
    "Charisma": 0,
    "Patience": 2,
    "PayOffset": 0,
    "SynOffset": 0,
    "": 0,
    "Pay": 1,
    "MinSyn": 0,
    "Animal": 0,
    "Svg": 0,
    "Name": "Joanne (f)",
    "Gender": 0,
    "Hair Color": "Black with purple highlights",
    "Hair Length": "Shoulder-length",
    "Hair Type": "Bob",
    "Shirt Type or color": 0,
    "Head Accessories": 0,
    "Skin Color": "Light brown"
  },
  {
    "Card": 4,
    "Concept": "Jr Engineer",
    "Art": 0,
    "Logic": 1,
    "Charisma": 0,
    "Patience": 2,
    "PayOffset": 0,
    "SynOffset": 5,
    "": 0,
    "Pay": 1,
    "MinSyn": 5,
    "Animal": 0,
    "Svg": 0,
    "Name": "Kelly (f)",
    "Gender": 0,
    "Hair Color": 0,
    "Hair Length": 0,
    "Hair Type": 0,
    "Shirt Type or color": 0,
    "Head Accessories": 0,
    "Skin Color": 0
  },
  {
    "Card": 6,
    "Concept": "Jr Sales (+PAT)",
    "Art": 0,
    "Logic": 0,
    "Charisma": 2,
    "Patience": 2,
    "PayOffset": 0,
    "SynOffset": 0,
    "": 0,
    "Pay": 2,
    "MinSyn": 0,
    "Animal": 0,
    "Svg": 0,
    "Name": "Jane (f)",
    "Gender": 0,
    "Hair Color": 0,
    "Hair Length": 0,
    "Hair Type": 0,
    "Shirt Type or color": 0,
    "Head Accessories": 0,
    "Skin Color": 0
  },
  {
    "Card": 7,
    "Concept": "Sr Artist",
    "Art": 2,
    "Logic": 0,
    "Charisma": 1,
    "Patience": 1,
    "PayOffset": 0,
    "SynOffset": 0,
    "": 0,
    "Pay": 3,
    "MinSyn": 10,
    "Animal": "Duck",
    "Svg": "✓",
    "Name": "Don (f)",
    "Gender": 0,
    "Hair Color": 0,
    "Hair Length": 0,
    "Hair Type": 0,
    "Shirt Type or color": "Purple",
    "Head Accessories": 0,
    "Skin Color": "Light green"
  },
  {
    "Card": 8,
    "Concept": "Sr Engineer",
    "Art": 0,
    "Logic": 2,
    "Charisma": 1,
    "Patience": 1,
    "PayOffset": 0,
    "SynOffset": -5,
    "": 0,
    "Pay": 3,
    "MinSyn": 5,
    "Animal": 0,
    "Svg": 0,
    "Name": "Eric (m)",
    "Gender": 0,
    "Hair Color": 0,
    "Hair Length": 0,
    "Hair Type": 0,
    "Shirt Type or color": 0,
    "Head Accessories": 0,
    "Skin Color": 0
  },
  {
    "Card": 9,
    "Concept": "Sr Sales (+PAT)",
    "Art": 0,
    "Logic": 0,
    "Charisma": 3,
    "Patience": 1,
    "PayOffset": 0,
    "SynOffset": 0,
    "": 0,
    "Pay": 3,
    "MinSyn": 10,
    "Animal": "Dragon",
    "Svg": "✓",
    "Name": "Lisa (f)",
    "Gender": 0,
    "Hair Color": 0,
    "Hair Length": 0,
    "Hair Type": 0,
    "Shirt Type or color": 0,
    "Head Accessories": 0,
    "Skin Color": 0
  },
  {
    "Card": 10,
    "Concept": "Shalurkin (+CHA)",
    "Art": 1,
    "Logic": 1,
    "Charisma": 2,
    "Patience": 0,
    "PayOffset": 0,
    "SynOffset": -5,
    "": 0,
    "Pay": 4,
    "MinSyn": 15,
    "Animal": "Black-Cat",
    "Svg": "✓",
    "Name": "Murphy",
    "Gender": "f",
    "Hair Color": "Black",
    "Hair Length": "Shoulder-length",
    "Hair Type": "Straight",
    "Shirt Type or color": "Black",
    "Head Accessories": "Glasses",
    "Skin Color": "Olive"
  },
  {
    "Card": "Jack",
    "Concept": "Jack of all trades (+CHA)",
    "Art": 1,
    "Logic": 1,
    "Charisma": 2,
    "Patience": 2,
    "PayOffset": 0,
    "SynOffset": 0,
    "": 0,
    "Pay": 4,
    "MinSyn": 0,
    "Animal": 0,
    "Svg": 0,
    "Name": "Jerome (m)",
    "Gender": 0,
    "Hair Color": "Black",
    "Hair Length": "Shaved",
    "Hair Type": "Buzz-cut",
    "Shirt Type or color": "Grey",
    "Head Accessories": "Glasses",
    "Skin Color": "Brown"
  },
  {
    "Card": "Queen",
    "Concept": "Design God (+CHA)",
    "Art": 3,
    "Logic": 1,
    "Charisma": 2,
    "Patience": 0,
    "PayOffset": 0,
    "SynOffset": 0,
    "": 0,
    "Pay": 6,
    "MinSyn": 20,
    "Animal": 0,
    "Svg": 0,
    "Name": "Coco (f)",
    "Gender": 0,
    "Hair Color": "Black",
    "Hair Length": "Ear-length",
    "Hair Type": "Wavy-bob",
    "Shirt Type or color": "Red",
    "Head Accessories": "Diamond earrings / pearl necklace",
    "Skin Color": "Pale"
  },
  {
    "Card": "King",
    "Concept": "Logic God (+CHA)",
    "Art": 1,
    "Logic": 3,
    "Charisma": 2,
    "Patience": 0,
    "PayOffset": 0,
    "SynOffset": -5,
    "": 0,
    "Pay": 6,
    "MinSyn": 15,
    "Animal": "fenix",
    "Svg": 0,
    "Name": "Camina (f)",
    "Gender": 0,
    "Hair Color": "Pink",
    "Hair Length": "Ear-length",
    "Hair Type": "Bob",
    "Shirt Type or color": "Purple",
    "Head Accessories": "Big earrings",
    "Skin Color": "Brown"
  },
  {
    "Card": "Ace",
    "Concept": "Fragile Unicorn (-CHA)",
    "Art": 2,
    "Logic": 2,
    "Charisma": 1,
    "Patience": 0,
    "PayOffset": 0,
    "SynOffset": 0,
    "": 0,
    "Pay": 5,
    "MinSyn": 20,
    "Animal": "pandolin",
    "Svg": 0,
    "Name": "Cleopatra (f)",
    "Gender": 0,
    "Hair Color": "Black",
    "Hair Length": "Short",
    "Hair Type": "Straight-bob",
    "Shirt Type or color": "Gold",
    "Head Accessories": "Big earrings",
    "Skin Color": "Tan"
  },
  {
    "Card": 2,
    "Concept": "Lemon",
    "Art": 0,
    "Logic": 0,
    "Charisma": 0,
    "Patience": 1,
    "PayOffset": -3,
    "SynOffset": 5,
    "": 0,
    "Pay": 3,
    "MinSyn": 15,
    "Animal": "red panda",
    "Svg": 0,
    "Name": "Derpo (a)",
    "Gender": 0,
    "Hair Color": "Green",
    "Hair Length": 0,
    "Hair Type": 0,
    "Shirt Type or color": 0,
    "Head Accessories": 0,
    "Skin Color": 0
  },
  {
    "Card": 3,
    "Concept": "General Purpose Intern",
    "Art": 0,
    "Logic": 0,
    "Charisma": 0,
    "Patience": 3,
    "PayOffset": 0,
    "SynOffset": 0,
    "": 0,
    "Pay": 0,
    "MinSyn": -10,
    "Animal": 0,
    "Svg": 0,
    "Name": "Jessie (a)",
    "Gender": 0,
    "Hair Color": 0,
    "Hair Length": 0,
    "Hair Type": 0,
    "Shirt Type or color": 0,
    "Head Accessories": 0,
    "Skin Color": 0
  },
  {
    "Card": 5,
    "Concept": "Jr Artist",
    "Art": 1,
    "Logic": 0,
    "Charisma": 0,
    "Patience": 3,
    "PayOffset": 0,
    "SynOffset": 0,
    "": 0,
    "Pay": 1,
    "MinSyn": -10,
    "Animal": 0,
    "Svg": 0,
    "Name": "Pted (m)",
    "Gender": 0,
    "Hair Color": 0,
    "Hair Length": 0,
    "Hair Type": 0,
    "Shirt Type or color": 0,
    "Head Accessories": 0,
    "Skin Color": 0
  },
  {
    "Card": 4,
    "Concept": "Jr Engineer",
    "Art": 0,
    "Logic": 1,
    "Charisma": 0,
    "Patience": 3,
    "PayOffset": 0,
    "SynOffset": -5,
    "": 0,
    "Pay": 1,
    "MinSyn": -15,
    "Animal": 0,
    "Svg": 0,
    "Name": "Gregerth (f)",
    "Gender": 0,
    "Hair Color": 0,
    "Hair Length": 0,
    "Hair Type": 0,
    "Shirt Type or color": 0,
    "Head Accessories": 0,
    "Skin Color": 0
  },
  {
    "Card": 6,
    "Concept": "Jr Sales",
    "Art": 0,
    "Logic": 0,
    "Charisma": 2,
    "Patience": 3,
    "PayOffset": 0,
    "SynOffset": 0,
    "": 0,
    "Pay": 2,
    "MinSyn": -10,
    "Animal": 0,
    "Svg": 0,
    "Name": "Hoopy (f)",
    "Gender": 0,
    "Hair Color": 0,
    "Hair Length": 0,
    "Hair Type": 0,
    "Shirt Type or color": 0,
    "Head Accessories": 0,
    "Skin Color": 0
  },
  {
    "Card": 7,
    "Concept": "Sr Artist",
    "Art": 2,
    "Logic": 0,
    "Charisma": 0,
    "Patience": 2,
    "PayOffset": 0,
    "SynOffset": 5,
    "": 0,
    "Pay": 2,
    "MinSyn": 5,
    "Animal": 0,
    "Svg": 0,
    "Name": "Leo (m)",
    "Gender": 0,
    "Hair Color": 0,
    "Hair Length": 0,
    "Hair Type": 0,
    "Shirt Type or color": "Blue",
    "Head Accessories": 0,
    "Skin Color": "Light green"
  },
  {
    "Card": 8,
    "Concept": "Sr Engineer",
    "Art": 0,
    "Logic": 2,
    "Charisma": 0,
    "Patience": 2,
    "PayOffset": 0,
    "SynOffset": 0,
    "": 0,
    "Pay": 2,
    "MinSyn": 0,
    "Animal": 0,
    "Svg": 0,
    "Name": "Bront (m)",
    "Gender": 0,
    "Hair Color": "White",
    "Hair Length": "Short",
    "Hair Type": "Fuzzy",
    "Shirt Type or color": "White & Black",
    "Head Accessories": 0,
    "Skin Color": 0
  },
  {
    "Card": 9,
    "Concept": "Sr Sales",
    "Art": 0,
    "Logic": 0,
    "Charisma": 3,
    "Patience": 2,
    "PayOffset": 0,
    "SynOffset": 0,
    "": 0,
    "Pay": 3,
    "MinSyn": 0,
    "Animal": 0,
    "Svg": 0,
    "Name": "Jhilson (f)",
    "Gender": 0,
    "Hair Color": 0,
    "Hair Length": 0,
    "Hair Type": 0,
    "Shirt Type or color": 0,
    "Head Accessories": 0,
    "Skin Color": 0
  },
  {
    "Card": 10,
    "Concept": "Shalurkin",
    "Art": 3,
    "Logic": 2,
    "Charisma": 1,
    "Patience": 0,
    "PayOffset": 1,
    "SynOffset": -5,
    "": 0,
    "Pay": 5,
    "MinSyn": 15,
    "Animal": "Totoro",
    "Svg": "✓",
    "Name": "Shalena (f)",
    "Gender": "f",
    "Hair Color": "Brown",
    "Hair Length": "Long",
    "Hair Type": "Braids",
    "Shirt Type or color": "Flannel or polka dot",
    "Head Accessories": "Necklace?",
    "Skin Color": "Pale"
  },
  {
    "Card": "Jack",
    "Concept": "Jack of all trades",
    "Art": 2,
    "Logic": 2,
    "Charisma": 2,
    "Patience": 3,
    "PayOffset": 1,
    "SynOffset": 0,
    "": 0,
    "Pay": 5,
    "MinSyn": -10,
    "Animal": "T-Rex",
    "Svg": "✓",
    "Name": "Misty",
    "Gender": "f",
    "Hair Color": "Dark brown",
    "Hair Length": "Long",
    "Hair Type": "Wavy",
    "Shirt Type or color": "Tan button up & vest",
    "Head Accessories": "Moustache",
    "Skin Color": "Light brown"
  },
  {
    "Card": "Queen",
    "Concept": "Queen of design",
    "Art": 3,
    "Logic": 1,
    "Charisma": 1,
    "Patience": 0,
    "PayOffset": 0,
    "SynOffset": -5,
    "": 0,
    "Pay": 5,
    "MinSyn": 15,
    "Animal": 0,
    "Svg": 0,
    "Name": "Bean (f)",
    "Gender": 0,
    "Hair Color": "White",
    "Hair Length": "Long",
    "Hair Type": "Wavy",
    "Shirt Type or color": "Teal",
    "Head Accessories": "Buck tooth",
    "Skin Color": "Pale"
  },
  {
    "Card": "King",
    "Concept": "King of engineering",
    "Art": 1,
    "Logic": 3,
    "Charisma": 2,
    "Patience": 3,
    "PayOffset": 0,
    "SynOffset": 0,
    "": 0,
    "Pay": 6,
    "MinSyn": -10,
    "Animal": 0,
    "Svg": 0,
    "Name": "Musky (m)",
    "Gender": 0,
    "Hair Color": "Dirty Blonde",
    "Hair Length": "Short",
    "Hair Type": "Blob",
    "Shirt Type or color": "Black",
    "Head Accessories": 0,
    "Skin Color": 0
  },
  {
    "Card": "Ace",
    "Concept": "Fragile Unicorn",
    "Art": 3,
    "Logic": 3,
    "Charisma": 3,
    "Patience": 1,
    "PayOffset": 1,
    "SynOffset": 0,
    "": 0,
    "Pay": 8,
    "MinSyn": 10,
    "Animal": "Cockroach",
    "Svg": "✓",
    "Name": "Seph (m)",
    "Gender": 0,
    "Hair Color": "Blonde",
    "Hair Length": "Long",
    "Hair Type": "Dreads",
    "Shirt Type or color": "Orange",
    "Head Accessories": 0,
    "Skin Color": "Tan"
  },
  {
    "Card": "Example",
    "Concept": 0,
    "Art": 1,
    "Logic": 2,
    "Charisma": 1,
    "Patience": 3,
    "PayOffset": 0,
    "SynOffset": 5,
    "": 0,
    "Pay": 4,
    "MinSyn": -5,
    "Animal": 0,
    "Svg": 0,
    "Name": "Examplo",
    "Gender": 0,
    "Hair Color": 0,
    "Hair Length": 0,
    "Hair Type": 0,
    "Shirt Type or color": 0,
    "Head Accessories": 0,
    "Skin Color": 0
  },
  {
    "Card": 0,
    "Concept": 0,
    "Art": 53,
    "Logic": 52,
    "Charisma": 53,
    "Patience": 76,
    "PayOffset": 0,
    "SynOffset": 0,
    "": 0,
    "Pay": 0,
    "MinSyn": 0,
    "Animal": 0,
    "Svg": 0,
    "Name": 0,
    "Gender": 0,
    "Hair Color": 0,
    "Hair Length": 0,
    "Hair Type": 0,
    "Shirt Type or color": 0,
    "Head Accessories": 0,
    "Skin Color": 0
  },
  {
    "Card": 0,
    "Concept": 0,
    "Art": 0,
    "Logic": 0,
    "Charisma": 0,
    "Patience": 0,
    "PayOffset": 0,
    "SynOffset": 0,
    "": 0,
    "Pay": 0,
    "MinSyn": 0,
    "Animal": 0,
    "Svg": 0,
    "Name": 0,
    "Gender": 0,
    "Hair Color": 0,
    "Hair Length": 0,
    "Hair Type": 0,
    "Shirt Type or color": 0,
    "Head Accessories": 0,
    "Skin Color": 0
  },
  {
    "Card": 0,
    "Concept": 0,
    "Art": 0,
    "Logic": 0,
    "Charisma": 0,
    "Patience": 0,
    "PayOffset": 0,
    "SynOffset": 0,
    "": 0,
    "Pay": 0,
    "MinSyn": 0,
    "Animal": 0,
    "Svg": 0,
    "Name": 0,
    "Gender": 0,
    "Hair Color": 0,
    "Hair Length": 0,
    "Hair Type": 0,
    "Shirt Type or color": 0,
    "Head Accessories": 0,
    "Skin Color": 0
  },
  {
    "Card": 0,
    "Concept": "At 0",
    "Art": 21,
    "Logic": 21,
    "Charisma": 21,
    "Patience": 13,
    "PayOffset": 0,
    "SynOffset": 0,
    "": 0,
    "Pay": 0,
    "MinSyn": 0,
    "Animal": 0,
    "Svg": 0,
    "Name": 0,
    "Gender": 0,
    "Hair Color": 0,
    "Hair Length": 0,
    "Hair Type": 0,
    "Shirt Type or color": 0,
    "Head Accessories": 0,
    "Skin Color": 0
  },
  {
    "Card": 0,
    "Concept": "At 0%",
    "Art": "40%",
    "Logic": "40%",
    "Charisma": "40%",
    "Patience": "25%",
    "PayOffset": 0,
    "SynOffset": 0,
    "": 0,
    "Pay": 0,
    "MinSyn": 0,
    "Animal": 0,
    "Svg": 0,
    "Name": 0,
    "Gender": 0,
    "Hair Color": 0,
    "Hair Length": 0,
    "Hair Type": 0,
    "Shirt Type or color": 0,
    "Head Accessories": 0,
    "Skin Color": 0
  },
  {
    "Card": 0,
    "Concept": "At 1",
    "Art": 15,
    "Logic": 15,
    "Charisma": 14,
    "Patience": 12,
    "PayOffset": 0,
    "SynOffset": 0,
    "": 0,
    "Pay": 0,
    "MinSyn": 0,
    "Animal": 0,
    "Svg": 0,
    "Name": 0,
    "Gender": 0,
    "Hair Color": 0,
    "Hair Length": 0,
    "Hair Type": 0,
    "Shirt Type or color": 0,
    "Head Accessories": 0,
    "Skin Color": 0
  },
  {
    "Card": 0,
    "Concept": "At 1%",
    "Art": "29%",
    "Logic": "29%",
    "Charisma": "27%",
    "Patience": "23%",
    "PayOffset": 0,
    "SynOffset": 0,
    "": 0,
    "Pay": 0,
    "MinSyn": 0,
    "Animal": 0,
    "Svg": 0,
    "Name": 0,
    "Gender": 0,
    "Hair Color": 0,
    "Hair Length": 0,
    "Hair Type": 0,
    "Shirt Type or color": 0,
    "Head Accessories": 0,
    "Skin Color": 0
  },
  {
    "Card": 0,
    "Concept": "At 2",
    "Art": 10,
    "Logic": 11,
    "Charisma": 12,
    "Patience": 17,
    "PayOffset": 0,
    "SynOffset": 0,
    "": 0,
    "Pay": 0,
    "MinSyn": 0,
    "Animal": 0,
    "Svg": 0,
    "Name": 0,
    "Gender": 0,
    "Hair Color": 0,
    "Hair Length": 0,
    "Hair Type": 0,
    "Shirt Type or color": 0,
    "Head Accessories": 0,
    "Skin Color": 0
  },
  {
    "Card": 0,
    "Concept": "At 2%",
    "Art": "19%",
    "Logic": "21%",
    "Charisma": "23%",
    "Patience": "33%",
    "PayOffset": 0,
    "SynOffset": 0,
    "": 0,
    "Pay": 0,
    "MinSyn": 0,
    "Animal": 0,
    "Svg": 0,
    "Name": 0,
    "Gender": 0,
    "Hair Color": 0,
    "Hair Length": 0,
    "Hair Type": 0,
    "Shirt Type or color": 0,
    "Head Accessories": 0,
    "Skin Color": 0
  },
  {
    "Card": 0,
    "Concept": "At 3",
    "Art": 6,
    "Logic": 5,
    "Charisma": 5,
    "Patience": 10,
    "PayOffset": 0,
    "SynOffset": 0,
    "": 0,
    "Pay": 0,
    "MinSyn": 0,
    "Animal": 0,
    "Svg": 0,
    "Name": 0,
    "Gender": 0,
    "Hair Color": 0,
    "Hair Length": 0,
    "Hair Type": 0,
    "Shirt Type or color": 0,
    "Head Accessories": 0,
    "Skin Color": 0
  },
  {
    "Card": 0,
    "Concept": "At 3%",
    "Art": "12%",
    "Logic": "10%",
    "Charisma": "10%",
    "Patience": "19%",
    "PayOffset": 0,
    "SynOffset": 0,
    "": 0,
    "Pay": 0,
    "MinSyn": 0,
    "Animal": 0,
    "Svg": 0,
    "Name": 0,
    "Gender": 0,
    "Hair Color": 0,
    "Hair Length": 0,
    "Hair Type": 0,
    "Shirt Type or color": 0,
    "Head Accessories": 0,
    "Skin Color": 0
  }
];
});
___scope___.file("Entities/Decks/Employee/index.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Employee_1 = require("./Employee");
exports.Employee = Employee_1.default;
var IRawEmployee_1 = require("./IRawEmployee");
exports.IRawEmployee = IRawEmployee_1.default;
var EmployeeDeck_1 = require("./EmployeeDeck");
exports.RawEmployeeList = EmployeeDeck_1.rawEmployeeList;
//# sourceMappingURL=index.js.map
});
___scope___.file("Entities/Decks/Employee/IRawEmployee.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//# sourceMappingURL=IRawEmployee.js.map
});
___scope___.file("Entities/Decks/Project/ProjectDeck.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Project_1 = require("./Project");
const Deck_1 = require("../Deck");
const ProjectSheet = require("../../../Registry/sheets/json/Synergy - Projects.json");
const ProjectList = [];
exports.ProjectList = ProjectList;
ProjectSheet.forEach((card) => {
    const raw = {
        name: card["Card Name"],
        description: card["Description"],
        tasks: card["Task Cost"],
        pay: card["Pay"],
        payOld: card["PayOld"],
        effect: card["Effect"],
        anchor: card["Anchor"],
    };
    ProjectList.push(new Project_1.default(raw));
});
class ProjectDeck extends Deck_1.default {
    constructor(parent) {
        super(parent, () => ProjectList, Project_1.default.getBackNode);
    }
}
exports.default = ProjectDeck;
//# sourceMappingURL=ProjectDeck.js.map
});
___scope___.file("Entities/Decks/Project/Project.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const el_tool_1 = require("el-tool");
const EmployeeDeck_1 = require("../Employee/EmployeeDeck");
const Util_1 = require("../../../Util");
require("./project.scss");
const EMPLOYEE_TASKS = 3;
class Project {
    constructor(raw) {
        this.raw = raw;
        const { name, description, tasks, pay, anchor } = raw;
        this.jobStages = this.parseTasks(tasks);
        this.pay = this.calculatePay();
        if (this.pay !== pay) {
            console.log(`Pays do not match for project "${name}".  Old = ${pay}, new = ${this.pay}`);
        }
        this.domNode = el_tool_1.div("Project-Card front", [
            el_tool_1.div("Header", [
                el_tool_1.div("Name", name),
                // div("Pay", `${this.pay}k :: ${pay}k :: ${Math.floor(raw.payOld * 2/3)}k`),
                // div("Pay", `${this.pay}k :: ${Math.floor(raw.payOld * 2/3)}k`),
                el_tool_1.div("Pay", `${this.pay}k`),
            ]),
            el_tool_1.div("Instructions", [
                ...this.jobStages.map((stage, stageNum) => el_tool_1.div("Stage", [
                    stageNum === 0 ? null : el_tool_1.div("ThenSpacer", "THEN"),
                    el_tool_1.div("Jobs", stage.map((job) => el_tool_1.div("Job card", [
                        !job.name ? null : el_tool_1.div("Name", job.name),
                        // div("StackHere"),
                        Util_1.minimizeWidth(el_tool_1.div("Count", (new Array(job.count)).fill(null).map(() => el_tool_1.div("TaskCheckbox", "✔")))),
                        el_tool_1.div("Rarity", [
                            (job.skillRarity * 100).toFixed(2),
                            "% ",
                        ]),
                        el_tool_1.div("Skills", () => {
                            const { artLevel, logicLevel, charismaLevel, patienceLevel } = job;
                            if (!artLevel && !logicLevel && !charismaLevel && !patienceLevel) {
                                return "Any";
                            }
                            return [
                                ["art", artLevel],
                                ["logic", logicLevel],
                                ["charisma", charismaLevel],
                                ["patience", patienceLevel],
                            ].map(([name, level]) => !level ? null : el_tool_1.div(`Skill ${name}`, (new Array(level)).fill(null).map(() => el_tool_1.div(`Token ${name}`))));
                        }),
                    ]))),
                ])),
                anchor ? el_tool_1.div("Anchor card", [
                    el_tool_1.div("Icon"),
                    "Anchor Project - You may pay -1 SYN to put this card back into any cofounders hand after completion"
                ]) : null,
            ]),
            description ? Util_1.minimizeWidth(el_tool_1.div("Description", Util_1.fromMD(description))) : null,
        ]);
    }
    parseTasks(instructions) {
        if (typeof instructions !== "string") {
            return [];
        }
        const stages = instructions.split(/\->/g).map((str) => str.trim());
        return stages.map((stage) => {
            const jobs = stage.match(/(\[[^\]]+?\])? ?\(\d+\) ?[1-3ALCP]+/g).map((str) => str.trim());
            return jobs.map((jobStr) => {
                const job = {};
                const nameMatch = jobStr.match(/\[[^\]]+?\]/);
                if (nameMatch !== null) {
                    job.name = nameMatch[0].slice(1, nameMatch.length - 2);
                }
                const counts = jobStr.match(/\(\d+\)|[1-3][ALCP]/g);
                counts.forEach((count) => {
                    const num = parseInt(count.replace(/\D/g, "")) || 0;
                    if (/\(\d+\)/.test(count)) {
                        job.count = num;
                    }
                    else {
                        if (count.includes("A")) {
                            job.artLevel = num;
                        }
                        else if (count.includes("L")) {
                            job.logicLevel = num;
                        }
                        else if (count.includes("C")) {
                            job.charismaLevel = num;
                        }
                        else if (count.includes("P")) {
                            job.patienceLevel = num;
                        }
                    }
                });
                job.artLevel = job.artLevel || 0;
                job.logicLevel = job.logicLevel || 0;
                job.charismaLevel = job.charismaLevel || 0;
                job.patienceLevel = job.patienceLevel || 0;
                return job;
            });
        });
    }
    calculatePay() {
        const trueEmployees = EmployeeDeck_1.rawEmployeeList.filter((emee) => emee.pay > 0); // filter incomplete employee rows
        let paySum = 0;
        this.jobStages.forEach((stage) => {
            stage.forEach((job) => {
                // employees who can do the job
                const canDoers = trueEmployees.filter((emee) => emee.art >= job.artLevel
                    && emee.logic >= job.logicLevel
                    && emee.charisma >= job.charismaLevel
                    && emee.patience >= job.patienceLevel).sort((a, b) => a.pay - b.pay);
                canDoers.forEach((emee) => emee.jobs++); // keep records of how many jobs an employee is assignable to
                job.skillRarity = canDoers.length / trueEmployees.length;
                const minSkillCost = canDoers[0].pay * job.count / EMPLOYEE_TASKS;
                // non-overqualified employees
                const shouldDoers = canDoers.filter((emee) => (!job.artLevel || emee.art - job.artLevel <= 1)
                    && (!job.logicLevel || emee.logic - job.logicLevel <= 1)
                    && (!job.charismaLevel || emee.charisma - job.charismaLevel <= 1)
                    && (!job.patienceLevel || emee.patience - job.patienceLevel <= 1)).sort((a, b) => a.pay - b.pay);
                const medianEmployee = shouldDoers[Math.floor(shouldDoers.length / 2)];
                const medianSkillCost = medianEmployee.pay * job.count / EMPLOYEE_TASKS;
                // const skillRarityBonus = 1 / job.skillRarity;
                const skillRarityBonus = Math.pow(1 / job.skillRarity, 2 / 3);
                job.pay = 2; // co-founder assign cost
                job.pay += (minSkillCost + medianSkillCost) / 2 * skillRarityBonus * this.raw.effect;
                paySum += job.pay;
            });
        });
        return paySum < 15 ? Math.floor(paySum) : Math.round(paySum / 5) * 5;
    }
    static getBackNode() {
        return el_tool_1.div("Project-Card back", [
            el_tool_1.div("Name", "PROJECT CARD"),
            el_tool_1.div("Icon", "+"),
        ]);
    }
}
exports.default = Project;
//# sourceMappingURL=Project.js.map
});
___scope___.file("Entities/Decks/Project/project.scss", function(exports, require, module, __filename, __dirname){


require("fuse-box-css")("default/Entities/Decks/Project/project.scss", ".Project-Card {\n  height: 3.5in;\n  width: 2.5in;\n  border-radius: 0.1in;\n  overflow: hidden;\n  display: inline-grid;\n  background-size: 0.125in 0.125in, 0.125in 0.125in, 1in 1in, 1in 1in;\n  background-position: .25in .25in;\n  background-color: #00cba0;\n  background-image: linear-gradient(to right, rgba(250, 250, 250, 0.2) 1px, transparent 1px), linear-gradient(to bottom, rgba(250, 250, 250, 0.2) 1px, transparent 1px), linear-gradient(to right, rgba(250, 250, 250, 0.2) 1px, transparent 1px), linear-gradient(to bottom, rgba(250, 250, 250, 0.2) 1px, transparent 1px); }\n  .Project-Card.front {\n    background-color: #4e92e2;\n    grid-template-rows: auto 1fr auto;\n    grid-template-columns: 1fr; }\n    .Project-Card.front > * {\n      padding: 0.0625in; }\n    .Project-Card.front > .Header {\n      display: flex;\n      background: #ffc600;\n      box-shadow: 0px 0px 10px black; }\n      .Project-Card.front > .Header > .Name {\n        font-size: 14px;\n        flex: 1; }\n      .Project-Card.front > .Header > .Pay {\n        text-align: right; }\n    .Project-Card.front > .Description {\n      font-size: 11px;\n      min-height: 40px;\n      justify-self: right;\n      border-top-left-radius: 4px;\n      color: white;\n      background: #2965ad99; }\n      .Project-Card.front > .Description p {\n        margin: 0px;\n        padding: 0px; }\n    .Project-Card.front > .Instructions .card {\n      background: white;\n      box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23);\n      padding: 3px;\n      border-radius: 2px; }\n    .Project-Card.front > .Instructions > .Stage > .ThenSpacer {\n      text-align: center;\n      font-style: italic;\n      position: relative;\n      font-size: 10px;\n      color: white; }\n      .Project-Card.front > .Instructions > .Stage > .ThenSpacer::after {\n        display: block;\n        content: \"\";\n        position: absolute;\n        top: 50%;\n        left: 0px;\n        height: 1px;\n        width: 100%;\n        background-image: linear-gradient(to right, transparent 0%, white 30%, transparent 40%, transparent 60%, white 70%, transparent 100%); }\n    .Project-Card.front > .Instructions > .Stage > .Jobs > .Job {\n      position: relative;\n      display: grid;\n      margin: 4px 0px;\n      grid-template-rows: auto auto;\n      grid-template-columns: 1fr auto;\n      grid-template-areas: \"Name Name\" \"Count Skills\"; }\n      .Project-Card.front > .Instructions > .Stage > .Jobs > .Job > .Rarity {\n        position: absolute;\n        text-shadow: 0px 0px 3px white;\n        background: rgba(250, 250, 250, 0.75);\n        bottom: 0px;\n        right: 0px;\n        font-size: 12px; }\n      .Project-Card.front > .Instructions > .Stage > .Jobs > .Job > .Name {\n        grid-area: Name;\n        font-size: 11px; }\n      .Project-Card.front > .Instructions > .Stage > .Jobs > .Job > .Count {\n        grid-area: Count; }\n        .Project-Card.front > .Instructions > .Stage > .Jobs > .Job > .Count > .TaskCheckbox {\n          display: inline-flex;\n          justify-content: center;\n          align-items: center;\n          height: 8mm;\n          width: 8mm;\n          margin: 1px;\n          border: 1px solid black;\n          border-radius: 1px;\n          color: rgba(0, 0, 0, 0.3); }\n      .Project-Card.front > .Instructions > .Stage > .Jobs > .Job > .Skills {\n        grid-area: Skills;\n        display: grid;\n        grid-template-rows: auto auto;\n        grid-template-columns: auto auto;\n        margin-left: auto; }\n        .Project-Card.front > .Instructions > .Stage > .Jobs > .Job > .Skills > .Skill {\n          display: inline-block;\n          white-space: nowrap; }\n          .Project-Card.front > .Instructions > .Stage > .Jobs > .Job > .Skills > .Skill > .Token {\n            display: inline-block;\n            position: relative;\n            margin-right: -12px;\n            height: 24px;\n            width: 24px;\n            border-radius: 20px;\n            background-size: contain;\n            vertical-align: sub; }\n            .Project-Card.front > .Instructions > .Stage > .Jobs > .Job > .Skills > .Skill > .Token.art {\n              background-image: url(\"/css-resources/assets/Icon/Skill/Art.svg\"); }\n            .Project-Card.front > .Instructions > .Stage > .Jobs > .Job > .Skills > .Skill > .Token.logic {\n              background-image: url(\"/css-resources/assets/Icon/Skill/Logic.svg\"); }\n            .Project-Card.front > .Instructions > .Stage > .Jobs > .Job > .Skills > .Skill > .Token.charisma {\n              background-image: url(\"/css-resources/assets/Icon/Skill/Charisma.svg\"); }\n            .Project-Card.front > .Instructions > .Stage > .Jobs > .Job > .Skills > .Skill > .Token.patience {\n              background-image: url(\"/css-resources/assets/Icon/Skill/Patience.svg\"); }\n            .Project-Card.front > .Instructions > .Stage > .Jobs > .Job > .Skills > .Skill > .Token:last-child {\n              margin-right: 2px; }\n            .Project-Card.front > .Instructions > .Stage > .Jobs > .Job > .Skills > .Skill > .Token:nth-child(1) {\n              z-index: 3; }\n            .Project-Card.front > .Instructions > .Stage > .Jobs > .Job > .Skills > .Skill > .Token:nth-child(2) {\n              z-index: 2; }\n            .Project-Card.front > .Instructions > .Stage > .Jobs > .Job > .Skills > .Skill > .Token:nth-child(3) {\n              z-index: 1; }\n            .Project-Card.front > .Instructions > .Stage > .Jobs > .Job > .Skills > .Skill > .Token::after {\n              position: absolute;\n              display: block;\n              content: \"\";\n              top: 0px;\n              left: 0px;\n              right: 0px;\n              bottom: 0px;\n              border-radius: inherit;\n              border: 2px solid rgba(0, 0, 0, 0.2);\n              background-blend-mode: multiply; }\n    .Project-Card.front > .Instructions > .Anchor {\n      font-size: 12px;\n      margin-top: .25in; }\n      .Project-Card.front > .Instructions > .Anchor > .Icon {\n        display: inline-block;\n        height: 20px;\n        width: 20px;\n        background-image: url(\"/css-resources/assets/Icon/Skill/Anchor.svg\");\n        background-size: contain;\n        margin-right: 0.0625in;\n        margin-bottom: -4px; }\n  .Project-Card.back {\n    position: relative;\n    display: flex;\n    flex-direction: column;\n    align-items: center;\n    justify-content: center;\n    color: #4a4a4a; }\n    .Project-Card.back > .Name {\n      position: absolute;\n      font-size: .125in;\n      font-family: monospace;\n      top: 0.13542in;\n      left: 0.28125in;\n      letter-spacing: 4.85px; }\n    .Project-Card.back > .Icon {\n      font-size: 1.5in;\n      color: #fff3dc; }\n\n/*# sourceMappingURL=/css-sourcemaps/28c918ce.map */")
});
___scope___.file("Registry/sheets/json/Synergy - Projects.json", function(exports, require, module, __filename, __dirname){

module.exports = [
  {
    "Card Name": "The Magic Touch",
    "Description": "Some things require rare minds to do",
    "Task Cost": "[Get an Inspiring Idea](2) 1C1A -> \n[Set Genius to it](6) 2A2L2C",
    "Tasks * Skills": 24,
    "Effect": 1,
    "Pay": 120,
    "Anchor": false,
    "": 0,
    "EffectOld": 3.5,
    "PayOld": 84
  },
  {
    "Card Name": "Grunt work",
    "Description": "What you mean you have nothing to do?  There's *plenty* of work to do.",
    "Task Cost": "[Move Things](8) Any",
    "Tasks * Skills": 2,
    "Effect": 1,
    "Pay": 10,
    "Anchor": true,
    "": 0,
    "EffectOld": 1.5,
    "PayOld": 3
  },
  {
    "Card Name": "Basic Simple Feature",
    "Description": 0,
    "Task Cost": "(5) 1A1C, (5) 1L2P",
    "Tasks * Skills": 4,
    "Effect": 1,
    "Pay": 20,
    "Anchor": false,
    "": 0,
    "EffectOld": 3,
    "PayOld": 12
  },
  {
    "Card Name": "Basic Intermediate Feature",
    "Description": 0,
    "Task Cost": "(5) 2A1C, (5) 2L -> (3)1C1P",
    "Tasks * Skills": 8,
    "Effect": 1,
    "Pay": 35,
    "Anchor": false,
    "": 0,
    "EffectOld": 3,
    "PayOld": 24
  },
  {
    "Card Name": "A Great Collab",
    "Description": "As hell froze over, the designers and engineers decided to work together.",
    "Task Cost": "[It will be pretty](3) 3A, \n  [It will work](3) 3L",
    "Tasks * Skills": 12,
    "Effect": 1,
    "Pay": 40,
    "Anchor": false,
    "": 0,
    "EffectOld": 3,
    "PayOld": 36
  },
  {
    "Card Name": "Ad Campaign",
    "Description": "I hear non-sequiturs and shrieking are pretty popular these days.",
    "Task Cost": "[Choose an Angle](2) 1L2C, \n  [Create an Aesthetic](1) 2A -> \n[Pump Out Content](5)1A, \n  [Plaster it Everywhere](5)1C ",
    "Tasks * Skills": 13,
    "Effect": 1,
    "Pay": 30,
    "Anchor": false,
    "": 0,
    "EffectOld": 3,
    "PayOld": 39
  },
  {
    "Card Name": "Happy Feature",
    "Description": "Oooh!  They added emojis! 😃",
    "Task Cost": "[Design](4) 1A -> \n[Implement](4) 2L -> \n[Market](4) 1C",
    "Tasks * Skills": 8,
    "Effect": 1,
    "Pay": 25,
    "Anchor": false,
    "": 0,
    "EffectOld": 3,
    "PayOld": 24
  },
  {
    "Card Name": "Complex Feature",
    "Description": 0,
    "Task Cost": "[Design](4) 2A1P -> \n[Implement](5) 2L1P -> \n[Market](2) 1C",
    "Tasks * Skills": 16,
    "Effect": 1,
    "Pay": 40,
    "Anchor": false,
    "": 0,
    "EffectOld": 3,
    "PayOld": 48
  },
  {
    "Card Name": "Futuristic Feature",
    "Description": "The hardest part of raising the bar is making it idiot proof.",
    "Task Cost": "[Design](4) 3A1C -> \n[Implement](4) 3L -> \n[Market](5) 3C",
    "Tasks * Skills": 24,
    "Effect": 1,
    "Pay": 70,
    "Anchor": false,
    "": 0,
    "EffectOld": 3,
    "PayOld": 72
  },
  {
    "Card Name": "Who let the designers out?",
    "Description": "The design team thought it would look really cool if...",
    "Task Cost": "[Half Bake a Flashy Idea](2) 1A -> \n[Make it Happen](15) 2L1C",
    "Tasks * Skills": 22,
    "Effect": 0.9,
    "Pay": 45,
    "Anchor": false,
    "": 0,
    "EffectOld": 3,
    "PayOld": 66
  },
  {
    "Card Name": "Simple redesign",
    "Description": "Obsessing over tiny details isn't neurotic if it's art.",
    "Task Cost": "[Plan Improvements](4) 1A1C -> \n[Implement Changes](3)1L ",
    "Tasks * Skills": 6,
    "Effect": 1,
    "Pay": 13,
    "Anchor": false,
    "": 0,
    "EffectOld": 3,
    "PayOld": 18
  },
  {
    "Card Name": "Major redesign",
    "Description": "The game has changed and it's time to catch up.",
    "Task Cost": "[Plan Improvements](4) 1A2C -> \n[Implement Changes](5)2L",
    "Tasks * Skills": 24,
    "Effect": 1,
    "Pay": 35,
    "Anchor": false,
    "": 0,
    "EffectOld": 3,
    "PayOld": 72
  },
  {
    "Card Name": "Rebrand",
    "Description": "What if we did, like, a space cockroach thing?",
    "Task Cost": "[Find an Identity](4)3C -> \n[Create a Style](2) 3A1C -> \n[Implement Updates](4)2L, (5)2A",
    "Tasks * Skills": 36,
    "Effect": 1,
    "Pay": 50,
    "Anchor": false,
    "": 0,
    "EffectOld": 3,
    "PayOld": 108
  },
  {
    "Card Name": "Video Tutorials",
    "Description": "\"Hey, I'm Dr. Tutor and I'm here to show you how to find your own legs.\"",
    "Task Cost": "[Record Explanations](8) 1L2C2P",
    "Tasks * Skills": 36,
    "Effect": 1,
    "Pay": 65,
    "Anchor": false,
    "": 0,
    "EffectOld": 2.25,
    "PayOld": 81
  },
  {
    "Card Name": "Cold calling",
    "Description": 0,
    "Task Cost": "[Sway Strangers](10) 2C2P, \n[Setup Accounts](5) 2L1C1P",
    "Tasks * Skills": 10,
    "Effect": 0.75,
    "Pay": 45,
    "Anchor": true,
    "": 0,
    "EffectOld": 1.7,
    "PayOld": 17
  },
  {
    "Card Name": "Mass Proofreading",
    "Description": "Its amazing how many typos their are in proffessional products.",
    "Task Cost": "[Spellcheck](10)1L2P",
    "Tasks * Skills": 0,
    "Effect": 1.1,
    "Pay": 25,
    "Anchor": 0,
    "": 0,
    "EffectOld": 0,
    "PayOld": 0
  },
  {
    "Card Name": "New service",
    "Description": 0,
    "Task Cost": "[Layout Guidelines](3)1L1P1C ->\n[Fufill the Service](10)1C",
    "Tasks * Skills": 0,
    "Effect": 1,
    "Pay": 20,
    "Anchor": false,
    "": 0,
    "EffectOld": 0,
    "PayOld": 0
  },
  {
    "Card Name": "New color",
    "Description": "If you liked our logo in red, you'll love it in slightly orangish red!",
    "Task Cost": "[Change Fill and Export](15)1A2P",
    "Tasks * Skills": 16,
    "Effect": 1,
    "Pay": 40,
    "Anchor": false,
    "": 0,
    "EffectOld": 3,
    "PayOld": 48
  },
  {
    "Card Name": "Accessibility Improvements",
    "Description": 0,
    "Task Cost": "[Improve Contrast] (4) 2A,\n  [Simplify Interface] (4) 2L",
    "Tasks * Skills": 0,
    "Effect": 1,
    "Pay": 25,
    "Anchor": false,
    "": 0,
    "EffectOld": 0,
    "PayOld": 0
  },
  {
    "Card Name": "File company taxes",
    "Description": 0,
    "Task Cost": "[Read Fine Print](6)1L2P,\n  [Navigate Beurocracy](3)1L1C2P",
    "Tasks * Skills": 0,
    "Effect": 1,
    "Pay": 25,
    "Anchor": false,
    "": 0,
    "EffectOld": 0,
    "PayOld": 0
  },
  {
    "Card Name": "Misc Engineering",
    "Description": 0,
    "Task Cost": "[Solve Puzzles](8)1L (4) 2L",
    "Tasks * Skills": 6,
    "Effect": 1,
    "Pay": 25,
    "Anchor": true,
    "": 0,
    "EffectOld": 2,
    "PayOld": 12
  },
  {
    "Card Name": "Misc Design",
    "Description": 0,
    "Task Cost": "[Make Things Nice](8)1A (4) 2A",
    "Tasks * Skills": 6,
    "Effect": 1,
    "Pay": 25,
    "Anchor": true,
    "": 0,
    "EffectOld": 2,
    "PayOld": 12
  },
  {
    "Card Name": "Misc Sales",
    "Description": 0,
    "Task Cost": "[Be Convincing](8)1C (4) 2C",
    "Tasks * Skills": 6,
    "Effect": 1,
    "Pay": 25,
    "Anchor": true,
    "": 0,
    "EffectOld": 2,
    "PayOld": 12
  },
  {
    "Card Name": "Misc Support",
    "Description": 0,
    "Task Cost": "[Be Hospitable](8)1P (4) 2P",
    "Tasks * Skills": 6,
    "Effect": 1,
    "Pay": 15,
    "Anchor": true,
    "": 0,
    "EffectOld": 2,
    "PayOld": 12
  },
  {
    "Card Name": "Community Engagement",
    "Description": 0,
    "Task Cost": "[Chat it Up](12)1P1C",
    "Tasks * Skills": 0,
    "Effect": 1.2,
    "Pay": 15,
    "Anchor": true,
    "": 0,
    "EffectOld": 0,
    "PayOld": 0
  },
  {
    "Card Name": "Archive",
    "Description": 0,
    "Task Cost": 0,
    "Tasks * Skills": 0,
    "Effect": 1,
    "Pay": 0,
    "Anchor": false,
    "": 0,
    "EffectOld": 0,
    "PayOld": 0
  },
  {
    "Card Name": "Data Entry",
    "Description": "The trick is to never cross your eyes, even when you're nodding off.",
    "Task Cost": "[First See then Type](10)1L2P",
    "Tasks * Skills": 30,
    "Effect": 1,
    "Pay": 20,
    "Anchor": true,
    "": 0,
    "EffectOld": 2,
    "PayOld": 60
  },
  {
    "Card Name": 0,
    "Description": 0,
    "Task Cost": 0,
    "Tasks * Skills": 0,
    "Effect": 0,
    "Pay": 0,
    "Anchor": 0,
    "": 0,
    "EffectOld": 0,
    "PayOld": 0
  }
];
});
___scope___.file("Entities/Decks/Event/EventCardDeck.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const EventCard_1 = require("./EventCard");
const Deck_1 = require("../Deck");
const EventCardSheet = require("~/Registry/sheets/json/Synergy - Events.json");
const EventCardList = [];
exports.EventCardList = EventCardList;
EventCardSheet.forEach((card) => {
    const raw = {
        name: card["Name"],
        description: card.Description,
        isMajor: card["Is Major Event"],
        actions: card["Actions"] + "",
    };
    const repeat = card.Repeat || 1;
    for (let i = 0; i < repeat; i++) {
        EventCardList.push(new EventCard_1.default(raw));
    }
});
class EventCardDeck extends Deck_1.default {
    constructor(parent) {
        super(parent, () => EventCardList, EventCard_1.default.getBackNode);
    }
}
exports.default = EventCardDeck;
//# sourceMappingURL=EventCardDeck.js.map
});
___scope___.file("Entities/Decks/Event/EventCard.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const el_tool_1 = require("el-tool");
const fromMD_1 = require("../../../Util/fromMD");
require("./eventCard.scss");
const signOffs = [
    "Best",
    "Sincerely",
    "Thanks and Pie",
    "Regards",
    "Cheers",
];
const companyWideEmails = [
    "Announcements",
    "Office",
    "Events",
];
// interface IAction {
// 	type: "good" | "bad" | "optional" | "choose",
// }
class EventCard {
    constructor(raw) {
        this.raw = raw;
        const emailer = companyWideEmails[Math.floor(Math.random() * companyWideEmails.length)];
        this.domNode = el_tool_1.div("Event-Card front", [
            el_tool_1.div("Title", [
                el_tool_1.div(`Avatar ${emailer.toLowerCase()}`, emailer.charAt(0)),
                el_tool_1.div("Subject", raw.name + ""),
                el_tool_1.div("From", `from: ${emailer}`),
                el_tool_1.div("Star"),
            ]),
            el_tool_1.div("Description", [
                fromMD_1.default(raw.description + ""),
            ]),
            el_tool_1.div("Actions", this.parseAction(raw.actions)),
        ]);
    }
    parseAction(rawActs = "") {
        return (rawActs.match(/#[^#]+/g) || []).map((rawAct) => {
            let type;
            const lines = rawAct.replace(/#(\w+)/, (match, g1) => {
                type = g1.toLowerCase();
                return "";
            });
            let allParts = [];
            lines.split(/\n+/g).filter((it) => it.length).map((line) => {
                const parts = line.match(/.+?(:|$)/g);
                parts.forEach((part, index) => {
                    const isCondition = parts.length > 1 && index === 0;
                    const isResult = parts.length > 1 && index !== 0;
                    allParts.push(el_tool_1.div(`Part ${isCondition ? "condition" : (isResult ? "result" : "")}`, parts.length !== 1 ? null : {
                        style: { gridColumnEnd: "span 2" },
                    }, fromMD_1.default(this.injectSymbols(part))));
                });
            });
            return el_tool_1.div(`Action ${type}`, allParts);
        });
    }
    injectSymbols(rawHtml) {
        return rawHtml.replace(/{{(.+?)}}/g, (match, g1) => `<span class="Icon ${g1.toLowerCase()}">${g1.charAt(0)}</span>`)
            .replace(/_(.+?)_/g, (match, g1) => `<u>${g1}</u>`)
            .replace(/SYN/g, `<span class="Icon synergy">s</span>`);
    }
    static getBackNode() {
        return el_tool_1.div("Event-Card back", "Event Card");
    }
}
exports.default = EventCard;
//# sourceMappingURL=EventCard.js.map
});
___scope___.file("Entities/Decks/Event/eventCard.scss", function(exports, require, module, __filename, __dirname){


require("fuse-box-css")("default/Entities/Decks/Event/eventCard.scss", ".Event-Card {\n  height: 2.5in;\n  width: 3.5in;\n  border-radius: 0.1in;\n  overflow: hidden;\n  display: inline-grid; }\n  .Event-Card.front {\n    display: inline-flex;\n    flex-direction: column;\n    overflow: hidden;\n    background: #fffcf7; }\n    .Event-Card.front > .Title {\n      grid-area: Title;\n      display: grid;\n      grid-template-rows: auto auto;\n      grid-template-columns: auto 1fr auto;\n      padding: 0.03125in;\n      background: #00cba0;\n      color: white;\n      border-bottom: 1px solid #AAA; }\n      .Event-Card.front > .Title > .Avatar {\n        display: flex;\n        align-items: center;\n        justify-content: center;\n        height: 32px;\n        width: 32px;\n        color: white;\n        border-radius: 32px;\n        grid-row-end: span 2;\n        margin-right: 8px; }\n        .Event-Card.front > .Title > .Avatar.announcements {\n          background: #2b82cf; }\n        .Event-Card.front > .Title > .Avatar.events {\n          background: #d48106; }\n        .Event-Card.front > .Title > .Avatar.office {\n          background: #42b263; }\n      .Event-Card.front > .Title > .From {\n        font-size: 12px; }\n      .Event-Card.front > .Title > .Star {\n        display: flex;\n        align-items: center;\n        grid-column-start: 3;\n        grid-row-start: 1;\n        grid-row-end: span 2; }\n        .Event-Card.front > .Title > .Star.major {\n          color: #ffdd00; }\n    .Event-Card.front > .Actions {\n      grid-area: Actions;\n      display: grid;\n      grid-template-columns: auto 1fr; }\n      .Event-Card.front > .Actions > .Action {\n        display: contents; }\n        .Event-Card.front > .Actions > .Action.bad > .Part {\n          background: #9f5454;\n          border-color: #f8c0c0; }\n          .Event-Card.front > .Actions > .Action.bad > .Part.condition {\n            color: #9f5454; }\n        .Event-Card.front > .Actions > .Action.good > .Part {\n          background: #507e5e;\n          border-color: #a3e2b5; }\n          .Event-Card.front > .Actions > .Action.good > .Part.condition {\n            color: #507e5e; }\n        .Event-Card.front > .Actions > .Action.choose > .Part, .Event-Card.front > .Actions > .Action.neutral > .Part {\n          background: #5e7a93;\n          border-color: #cbe2f5; }\n          .Event-Card.front > .Actions > .Action.choose > .Part.condition, .Event-Card.front > .Actions > .Action.neutral > .Part.condition {\n            color: #5e7a93; }\n        .Event-Card.front > .Actions > .Action.warn > .Part {\n          text-align: center;\n          background: #9f5454;\n          border-color: #f8c0c0; }\n          .Event-Card.front > .Actions > .Action.warn > .Part.condition {\n            color: #9f5454; }\n        .Event-Card.front > .Actions > .Action > .Part {\n          display: inline-block;\n          margin: 0.03125in;\n          margin-top: 0px;\n          padding: 4px;\n          font-size: 14px;\n          border: 3px solid;\n          border-radius: 10px;\n          box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.3);\n          color: white; }\n          .Event-Card.front > .Actions > .Action > .Part.condition {\n            background: #fffcf7;\n            border-right: none;\n            margin-right: 0px;\n            border-top-right-radius: 0px;\n            border-bottom-right-radius: 0px;\n            box-shadow: none; }\n          .Event-Card.front > .Actions > .Action > .Part.result {\n            border-top-left-radius: 0px;\n            border-bottom-left-radius: 0px;\n            margin-left: 0px; }\n    .Event-Card.front > .Description {\n      flex-shrink: 1;\n      grid-area: Description;\n      text-indent: 1em;\n      text-align: justify;\n      padding: .125in;\n      font-size: 14px;\n      overflow: hidden; }\n      .Event-Card.front > .Description em {\n        color: #4A90E2; }\n      .Event-Card.front > .Description > .SignOff {\n        text-indent: 0px;\n        margin-top: 1em;\n        color: #777;\n        font-style: italic; }\n    .Event-Card.front > .Footers {\n      font-size: 12px; }\n  .Event-Card.back {\n    background-image: url(\"/css-resources/assets/Employees/Backgrounds/Card/Back/Event.svg\");\n    background-size: contain;\n    background-position: center center;\n    background-color: white;\n    text-align: center;\n    padding-top: 170px;\n    font-size: 22px;\n    color: #4a4a4a; }\n\n/*# sourceMappingURL=/css-sourcemaps/038c1e964.map */")
});
___scope___.file("Registry/sheets/json/Synergy - Events.json", function(exports, require, module, __filename, __dirname){

module.exports = [
  {
    "Name": "Kitchen cleaning service",
    "Description": "The kitchen is a god damn mess again!",
    "Actions": "#WARN\nYou must draw another event card\n#BAD\nIf no *cleaners*:  SYN-2 and {{BLAME}} all {{MESSY}}\n",
    "Is Major Event": false,
    "Printer card": false,
    "Good Bad": 0,
    "Repeat": 6,
    "SYN Loss": -2,
    "SYN Gain": 0,
    "Blame Common": false,
    "Blame Alluring": false,
    "Blame Impulsive": false,
    "Blame Messy": true,
    "Deprecated": false
  },
  {
    "Name": "Quirk Check",
    "Description": "Let's see how the employees are getting along.  ",
    "Actions": "#WARN\nYou must draw another event card\n#NEUTRAL\n1: Roll the {{D20}} dice once.\n2: For _each_ {{EMEE}} (any order), place a touch token then resolve all {{REPEAT}}{{D20}}{{ADJACENT}}{{BESIDE}}.\n3: Once complete, clear all {{EMEE}} touch tokens.",
    "Is Major Event": false,
    "Printer card": false,
    "Good Bad": 0,
    "Repeat": 8,
    "SYN Loss": 0,
    "SYN Gain": 0,
    "Blame Common": false,
    "Blame Alluring": false,
    "Blame Impulsive": false,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "Death and Meetings, right?",
    "Description": "Meetings keep going wayyyyyyyyyyyy over time.\n  Like wayyyyyyyyyyyyyyyyyyyyyyyyyyyy...",
    "Actions": "#BAD\nAll *founders*: lose one {{TASK}}\n",
    "Is Major Event": false,
    "Printer card": false,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": 0,
    "SYN Gain": 0,
    "Blame Common": false,
    "Blame Alluring": false,
    "Blame Impulsive": false,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "Mysterious vacation season",
    "Description": "For some reason, a whole bunch of your employees all go on vacation at the same time.  They all say the same thing curious thing: \"I'm going to be out of cell service.\"  And then come back with dusty cars.  Strange.",
    "Actions": "#BAD\nAll {{EMEE}} with 2+ {{ART}}: lose one {{TASK}}\n",
    "Is Major Event": false,
    "Printer card": false,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": 0,
    "SYN Gain": 0,
    "Blame Common": false,
    "Blame Alluring": false,
    "Blame Impulsive": false,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "It's flu season",
    "Description": "[Plug your nose and sound pathetic while reading this] \"Hey boss.  Sorry I won't be able to make it into work today, I've got that thing that's going around the office.\" \nA choice employee gets sick and spreads it.",
    "Actions": "#BAD\nChoose any {{EMEE}}: them and all {{ADJACENT}} lose one {{TASK}} \n",
    "Is Major Event": false,
    "Printer card": false,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": 0,
    "SYN Gain": 0,
    "Blame Common": false,
    "Blame Alluring": false,
    "Blame Impulsive": false,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "Staff member gets rowdy",
    "Description": "It would be a shame to work so hard on the keg stand and not show it off.  But keg stands are a pretty silppery slope.  This could either make the party or kill it, depending on the level of implusiveness.",
    "Actions": "#BAD\nIf any {{IMPULSIVE}}: SYN-1 and {{BLAME}} all {{IMPULSIVE}}\n#GOOD\nOtherwise: SYN+1",
    "Is Major Event": false,
    "Printer card": false,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": -1,
    "SYN Gain": 1,
    "Blame Common": false,
    "Blame Alluring": false,
    "Blame Impulsive": true,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "Donuts!!\n",
    "Description": "Someone brings donuts to the office.  It would be lovely if rather than a few employees eating them all, everyone got one.",
    "Actions": "#BAD\nIf 2+ {{IMPULSIVE}}: {{BLAME}} all {{IMPULSIVE}}\n#GOOD\nOtherwise: SYN+1",
    "Is Major Event": false,
    "Printer card": false,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": 0,
    "SYN Gain": 1,
    "Blame Common": false,
    "Blame Alluring": false,
    "Blame Impulsive": true,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "Where the pens at?",
    "Description": "A signature is needed and no pens can be found.  Who's job is this?  Oh yeah, it's HR's.",
    "Actions": "#BAD\nIf no HR: SYN-1\n",
    "Is Major Event": false,
    "Printer card": false,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": -1,
    "SYN Gain": 0,
    "Blame Common": false,
    "Blame Alluring": false,
    "Blame Impulsive": false,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "Health Insurance Racket",
    "Description": "It's time to re-up on your health plan with Phalse Security Inc., and they've doubled the cost of their policy. ",
    "Actions": "#CHOOSE\nA: 0k for SYN-4 \n#CHOOSE\nB: 10k for no change\n#CHOOSE\nC: 20k for SYN+2",
    "Is Major Event": true,
    "Printer card": false,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": -4,
    "SYN Gain": 2,
    "Blame Common": false,
    "Blame Alluring": false,
    "Blame Impulsive": false,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "Office Fire",
    "Description": "First the huge error in accounting, and now this!?  Luckily you have good insurance, but you still lost your prized red stapler.",
    "Actions": "#BAD\nChoose any {{IMPULSIVE}}: remove them from play\nAll *workers*: lose one {{TASK}}\n",
    "Is Major Event": true,
    "Printer card": false,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": 0,
    "SYN Gain": 0,
    "Blame Common": false,
    "Blame Alluring": false,
    "Blame Impulsive": true,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "Major Investor",
    "Description": "Bill Jobs is interested in your company, and want's to invest.  He's mostly looking for size of company and happiness.",
    "Actions": "#GOOD\nIf 6+ {{EMEE}} and 15+ SYN: +80k",
    "Is Major Event": true,
    "Printer card": false,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": 0,
    "SYN Gain": 0,
    "Blame Common": false,
    "Blame Alluring": false,
    "Blame Impulsive": false,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "Printer doesn't work",
    "Description": "Nobody knows why the printer is not working, but nobody really knows why the things ever choose to work in the first place.",
    "Actions": "#WARN\nYou must draw another event card\n#BAD\nChoose any 2 {{EMEE}}: lose one {{TASK}}",
    "Is Major Event": false,
    "Printer card": true,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": -1,
    "SYN Gain": 1,
    "Blame Common": false,
    "Blame Alluring": false,
    "Blame Impulsive": false,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "Printer is out of ink",
    "Description": "Due to printer problems, we were unable to print a few of the cards.",
    "Actions": "#WARN\nYou must draw another event card\n#BAD\nUnavoidable: discard the top card from every deck",
    "Is Major Event": false,
    "Printer card": true,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": 0,
    "SYN Gain": 0,
    "Blame Common": false,
    "Blame Alluring": false,
    "Blame Impulsive": false,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "Printer is jammed",
    "Description": "The wonderous printer is jammed... again, and on employee review day, nonetheless.  The fact that with all the technological advancements we've made, these things still don't work really sets everyone off.",
    "Actions": "#WARN\nYou must draw another event card\n#BAD\nUnavoidable: subtract *quarter-count* {{EMEE}} from SYN",
    "Is Major Event": true,
    "Printer card": true,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": -5,
    "SYN Gain": 0,
    "Blame Common": false,
    "Blame Alluring": false,
    "Blame Impulsive": false,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "Printer is out of paper",
    "Description": "Going paperless was a good decision, but someone took the idea a bit too far.  There seems to be no paper anywhere in the entire office.  Perhaps HR knows where it is.",
    "Actions": "#WARN\nYou must draw another event card\n#BAD\nIf no HR: take two {{TASK}} from any {{EMEE}}\n#GOOD\nOtherwise: HR gets an extra {{TASK}}",
    "Is Major Event": 0,
    "Printer card": true,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": 0,
    "SYN Gain": 0,
    "Blame Common": false,
    "Blame Alluring": false,
    "Blame Impulsive": false,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "Printer won't print double-sided",
    "Description": "One of your investors is asking for a coprehensive tower of papers, and the printer seems adimant on doubling the page count.  There is a printing store across town...",
    "Actions": "#WARN\nYou must draw another event card\n#CHOOSE\nA: Take one {{TASK}} from any {{EMEE}}\n#CHOOSE\nB: Lose 10k",
    "Is Major Event": 0,
    "Printer card": true,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": 0,
    "SYN Gain": 0,
    "Blame Common": false,
    "Blame Alluring": false,
    "Blame Impulsive": false,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "Printer is unplugged",
    "Description": "No one can print.  No one knows why.  Eventually, someone notices the printer is unplugged.  Everyone feels stupid, but collaborate on blaming someone other than themselves. ",
    "Actions": "#WARN\nYou must draw another event card\n#BAD\nChoose any {{EMEE}} without {{COMMON}}: add one {{BLAME}}",
    "Is Major Event": 0,
    "Printer card": true,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": 0,
    "SYN Gain": 0,
    "Blame Common": true,
    "Blame Alluring": false,
    "Blame Impulsive": false,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "Printer is asleep",
    "Description": "Do asleep printers dream of electric sheep?  While troubleshooting the problem a few employees end up having a really productive conversation.",
    "Actions": "#WARN\nYou must draw another event card\n#GOOD\nUnavoidable: SYN+1",
    "Is Major Event": 0,
    "Printer card": true,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": 0,
    "SYN Gain": 1,
    "Blame Common": false,
    "Blame Alluring": false,
    "Blame Impulsive": false,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "Printer won't connect",
    "Description": "Nobody knows when consumers all came together to collectively declare their distaste for any form of direct connection.. but by all accounts, it never really happened.",
    "Actions": "#WARN\nYou must draw another event card\n#GOOD\nIf any {{EMEE}} have 3{{LOG}}: no effect\n#BAD\nOtherwise: Take 1 {{TASK}} from any *founder*",
    "Is Major Event": 0,
    "Printer card": true,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": 0,
    "SYN Gain": 0,
    "Blame Common": false,
    "Blame Alluring": false,
    "Blame Impulsive": false,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "Printer:  PC Load Letter",
    "Description": "\"PC Load Letter?  What the f*&^ does that mean?\" \nYou are faced with two options, one involves getting a new printer and taking a few bats to the old.  The other involves a sloppy fix.",
    "Actions": "#WARN\nYou must draw another event card\n#CHOOSE\nA: 6k for SYN+1\n#CHOOSE\nB: 0k for SYN-1",
    "Is Major Event": 0,
    "Printer card": true,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": -1,
    "SYN Gain": 1,
    "Blame Common": false,
    "Blame Alluring": false,
    "Blame Impulsive": false,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "Do we have a fax machine?",
    "Description": "A client needs some paperwork \"faxed\" immediately, but no one knows where the fax machine is.  As it turns out, your printer has a fax mode, and everything turns out great.",
    "Actions": "#GOOD\nUnavoidable: SYN+1",
    "Is Major Event": false,
    "Printer card": false,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": 0,
    "SYN Gain": 1,
    "Blame Common": false,
    "Blame Alluring": false,
    "Blame Impulsive": false,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "Conference Room Wars",
    "Description": "The conference room keeps getting overbooked.  Unfortunately, it's not really anyone's job to oversee it, so it is just going to keep happening.\n",
    "Actions": "#BAD\nUnavoidable: SYN-2",
    "Is Major Event": false,
    "Printer card": false,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": -2,
    "SYN Gain": 0,
    "Blame Common": false,
    "Blame Alluring": false,
    "Blame Impulsive": false,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "You got pwned",
    "Description": "Someone was watching russian youtube on their work laptop and let a big fat trojan in.  You're going to have to get some malware protection, and give that employee a few stink eyes.",
    "Actions": "#BAD\nChoose any {{EMEE}} with {{IMPULSIVE}}: add one {{BLAME}} \nUnavoidable: -8k\n",
    "Is Major Event": true,
    "Printer card": false,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": 0,
    "SYN Gain": 0,
    "Blame Common": false,
    "Blame Alluring": false,
    "Blame Impulsive": true,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "Revenge is a dish best served... hot?",
    "Description": "Firing people always sucks, especially if it's someone who believes in revenge.  One of the employees you've passed up on didn't take the news well.",
    "Actions": "#BAD\nChoose random discarded {{EMEE}}\nFor _each_ {{LOGIC}} they have: -5k",
    "Is Major Event": false,
    "Printer card": false,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": 0,
    "SYN Gain": 0,
    "Blame Common": false,
    "Blame Alluring": false,
    "Blame Impulsive": false,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "Who is IT anyway?",
    "Description": "An employee's laptop has stopped working and they can't get it to restart.  \nHopefully someone can fix it, otherwise you're going to have to buy a new one.",
    "Actions": "#BAD\nIf no {{EMEE}} with 3 {{LOG}}: -5k",
    "Is Major Event": false,
    "Printer card": false,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": 0,
    "SYN Gain": 0,
    "Blame Common": false,
    "Blame Alluring": false,
    "Blame Impulsive": false,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "Surprise blue shell",
    "Description": "Out of nowhere, a blue turtle-like shell shows up.  As a group, decide which founder has been the most vocal/dominant.  That's the founder who decides to touch the shell, and is immediately struck with bad luck.  If you're playing alone, you get an extra turn.  If no decision can be made, everyone loses all but one task.",
    "Actions": "#BAD\nChoose the most vocal *founder*: lose all {{TASK}} \n",
    "Is Major Event": false,
    "Printer card": false,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": 0,
    "SYN Gain": 0,
    "Blame Common": false,
    "Blame Alluring": false,
    "Blame Impulsive": false,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "Discompassionate Management",
    "Description": "Any lack of patience in upper management can cascade into masssive problems.  Employees not wanting to say the things they think, yes-maning, stress and stagnation.  Check your patience.",
    "Actions": "#BAD\nFor _each_ *founder* with no {{PAT}}: SYN-3\n",
    "Is Major Event": true,
    "Printer card": false,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": -12,
    "SYN Gain": 0,
    "Blame Common": false,
    "Blame Alluring": false,
    "Blame Impulsive": false,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "Sexual Harrassment",
    "Description": "HEY THERE SUGAR TITS!",
    "Actions": "#BAD\nFor _each_ {{ALLURING}} or {{IMPULSIVE}}: SYN-1 \nUnavoidable: {{BLAME}} all {{IMPULSIVE}}",
    "Is Major Event": true,
    "Printer card": false,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": -3,
    "SYN Gain": 0,
    "Blame Common": false,
    "Blame Alluring": true,
    "Blame Impulsive": true,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "Equal rights concerns",
    "Description": "You're under the spotlight for discrimination.  If you have more than 5 employees and less than four banner colors (including co-founders), take SYN-1 for each employee.",
    "Actions": 0,
    "Is Major Event": true,
    "Printer card": false,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": -5,
    "SYN Gain": 0,
    "Blame Common": false,
    "Blame Alluring": false,
    "Blame Impulsive": false,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "An Affair to Remember",
    "Description": "Rumors are amok that a few of your employees have been getting down to some unapproved business.  Add a blame token to all employees with the {{ALLURING}} quality.",
    "Actions": "#BAD\nUnavoidable: {{BLAME}} all {{ALLURING}}",
    "Is Major Event": false,
    "Printer card": false,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": 0,
    "SYN Gain": 0,
    "Blame Common": false,
    "Blame Alluring": true,
    "Blame Impulsive": false,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "All Staff Meeting",
    "Description": "All staff meeting day!  It might be a little boring, but it's the only time everyone really sees each other as humans and gets the scoop on company business.  \nYou may choose to follow it up with a happy hour.",
    "Actions": "#GOOD\nOptional: trade 2k for SYN+1 once\n",
    "Is Major Event": false,
    "Printer card": false,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": 0,
    "SYN Gain": 1,
    "Blame Common": false,
    "Blame Alluring": false,
    "Blame Impulsive": false,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "Holiday Party",
    "Description": "Who doesn't love sitting under the mistle-toe, carving hearts into pumpkins, and hiding them from the decending disco ball?\n",
    "Actions": "#GOOD\nUnavoidable: SYN+3 ",
    "Is Major Event": true,
    "Printer card": false,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": 0,
    "SYN Gain": 3,
    "Blame Common": false,
    "Blame Alluring": false,
    "Blame Impulsive": false,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "Staff wants to unionize",
    "Description": "A competitor's staff unionized and got some great new perks, so there are whispers in the office about starting a union.  \nIf you have any employees with PATIENCE 2 or LOG 2, the whispers subside.  If not, choose to pay 2K for a spontaneous outing and +2 SYN.",
    "Actions": 0,
    "Is Major Event": true,
    "Printer card": false,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": 0,
    "SYN Gain": 2,
    "Blame Common": false,
    "Blame Alluring": false,
    "Blame Impulsive": false,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "Please don't do that",
    "Description": "A lot of employees are uncomfortable with the actions being made by one employee, but don't want to bring it up with management.  Depending on the patience of your employees, they might decide to have a civilized conversation, or  just passive-aggresively \"ignore\" it.",
    "Actions": "#GOOD\nIf any {{EMEE}} has 3 {{PAT}}: SYN+1\n\n#BAD\nOtherwise: SYN-2",
    "Is Major Event": false,
    "Printer card": false,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": -2,
    "SYN Gain": 1,
    "Blame Common": false,
    "Blame Alluring": false,
    "Blame Impulsive": false,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "New Dress Code",
    "Description": "It's hard to tell if it was the revealing yoga pants or excessive pit stains, but one thing is clear: it's time for a dress code. ",
    "Actions": "#BAD\nUnavoidable: SYN-2\nFor _each_  {{COMMON}}: blame any without {{COMMON}}  ",
    "Is Major Event": false,
    "Printer card": false,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": -2,
    "SYN Gain": 0,
    "Blame Common": true,
    "Blame Alluring": false,
    "Blame Impulsive": false,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "Dial-up flashbacks",
    "Description": "The internet has been really spotty lately.  What year is it!?   If you have any logical geniuses, they reset the modem and it's no big deal.  But without them, it becomes a constant issue.",
    "Actions": "#BAD\nIf no {{EMEE}} with 3 {{LOG}}: SYN-2",
    "Is Major Event": false,
    "Printer card": false,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": -2,
    "SYN Gain": 0,
    "Blame Common": false,
    "Blame Alluring": false,
    "Blame Impulsive": false,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "Storm causes electricity outage",
    "Description": "A rogue bolt of lightning put the entire block out of power.  Fortunately, there was a lot of much needed meetings and paperwork that could be done by candlelight.  The whole experience turned out rather relaxing.",
    "Actions": "#GOOD\nUnavoidable: SYN+1",
    "Is Major Event": false,
    "Printer card": false,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": 0,
    "SYN Gain": 1,
    "Blame Common": false,
    "Blame Alluring": false,
    "Blame Impulsive": false,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "Unavoidable road work",
    "Description": "There's some serious m*&%er f*%$in road work right in the middle of everyones commute to the office and it's hitting **everyone**.",
    "Actions": "#BAD\nUnavoidable: SYN loses *half-count* {{EMEE}} ",
    "Is Major Event": true,
    "Printer card": false,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": -6,
    "SYN Gain": 0,
    "Blame Common": false,
    "Blame Alluring": false,
    "Blame Impulsive": false,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "Puppy day!",
    "Description": "Puppy day!  There's an amazing local service where you can have a bunch of puppies delivered to the office for a day.  It even better with dog or wolf onesies.",
    "Actions": "#GOOD\nOptional: trade 1k for SYN+1\nIf any {{EMEE}} have canine *pjs*: SYN+3 instead",
    "Is Major Event": false,
    "Printer card": false,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": 0,
    "SYN Gain": 3,
    "Blame Common": false,
    "Blame Alluring": false,
    "Blame Impulsive": false,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "Out of water",
    "Description": 0,
    "Actions": 0,
    "Is Major Event": false,
    "Printer card": false,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": 0,
    "SYN Gain": 0,
    "Blame Common": false,
    "Blame Alluring": false,
    "Blame Impulsive": false,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "It's Election season",
    "Description": "Everyone loves chatting about politics in the workplace, right? ",
    "Actions": "#BAD\nFor _each_ {{IMPULSIVE}}: SYN-1\nChoose 2 {{IMPULSIVE}}: add one {{BLAME}}",
    "Is Major Event": false,
    "Printer card": false,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": -2,
    "SYN Gain": 0,
    "Blame Common": false,
    "Blame Alluring": false,
    "Blame Impulsive": true,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "It's National Dressup Day!",
    "Description": "Everyone gets to wear their favorite onesie for a day!",
    "Actions": "#GOOD\nUnavoidable: SYN+2",
    "Is Major Event": false,
    "Printer card": false,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": 0,
    "SYN Gain": 3,
    "Blame Common": false,
    "Blame Alluring": false,
    "Blame Impulsive": false,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "Lunch Thief",
    "Description": "[Raindrops and NY Sax] This office never sleeps. The street lights shimmering off puddles should make me happy, but the only thing I can think about is the note they left on the fridge: \"Nyeahahaha!  You'll never catch me! - The Hungry Pirate.\"  I'll catch you lunch theif, or I'll die trying.\n",
    "Actions": "#BAD\nUnavoidable: SYN-2 and {{BLAME}} any without {{COMMON}}",
    "Is Major Event": false,
    "Printer card": false,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": -2,
    "SYN Gain": 0,
    "Blame Common": true,
    "Blame Alluring": false,
    "Blame Impulsive": false,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "New rules for decorating desks",
    "Description": "As funny as the fake-poop-on-desk bit is, it's really messing up all your candid office shots.  It seems you'll have to add some stipulations to what can and can't be put on a desk.",
    "Actions": "#BAD\nFor _each_ {{COMMON}}: {{BLAME}} any without {{COMMON}}",
    "Is Major Event": false,
    "Printer card": false,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": 0,
    "SYN Gain": 0,
    "Blame Common": true,
    "Blame Alluring": false,
    "Blame Impulsive": false,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "Ants!",
    "Description": "Scrambling along the walls, congregating in the halls, and climbing up employees pants: this ant situation is out of control. ",
    "Actions": "#BAD\nChoose any {{MESSY}}: Add one {{BLAME}}\nUnavoidable: SYN-1",
    "Is Major Event": false,
    "Printer card": false,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": -1,
    "SYN Gain": 0,
    "Blame Common": false,
    "Blame Alluring": false,
    "Blame Impulsive": false,
    "Blame Messy": true,
    "Deprecated": false
  },
  {
    "Name": "Key card app won't work",
    "Description": "Wasn't all this technology supposed to make lives easier?  Your office's built in key card solution is really more of a problem.  People are getting locked out during breaks and sometimes even when no one is around to help.",
    "Actions": "#BAD\nUnavoidable: SYN-1",
    "Is Major Event": false,
    "Printer card": false,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": -1,
    "SYN Gain": 0,
    "Blame Common": false,
    "Blame Alluring": false,
    "Blame Impulsive": false,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "Design flaw",
    "Description": "There was a major design flaw in one of your recent projects.  Hopefully you haven't begun implementation yet.",
    "Actions": "#BAD\nProject with the most {{TASK}}: remove all {{TASK}}",
    "Is Major Event": false,
    "Printer card": false,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": 0,
    "SYN Gain": 0,
    "Blame Common": false,
    "Blame Alluring": false,
    "Blame Impulsive": false,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "Office Tour",
    "Description": 0,
    "Actions": 0,
    "Is Major Event": false,
    "Printer card": false,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": 0,
    "SYN Gain": 0,
    "Blame Common": false,
    "Blame Alluring": false,
    "Blame Impulsive": false,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "Maternity/Paternity Leave",
    "Description": "The bun in the oven has come out and cooing baby sounds & sights fill the office periodically for the first few months.",
    "Actions": "#BAD\nChoose any {{EMEE}}: take two {{TASK}}\n#GOOD\nUnavoidable: SYN+2",
    "Is Major Event": false,
    "Printer card": false,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": 0,
    "SYN Gain": 2,
    "Blame Common": false,
    "Blame Alluring": false,
    "Blame Impulsive": false,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "Big Conference",
    "Description": "Everyone knows conferences are mostly partying on the company dime, but somehow they still end up being amazing networking opportunities. You may choose some employees to send to the big conference.",
    "Actions": "#GOOD\nChoose 0-6 {{EMEE}}: trade one {{TASK}} for SYN+1",
    "Is Major Event": true,
    "Printer card": false,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": 0,
    "SYN Gain": 5,
    "Blame Common": false,
    "Blame Alluring": false,
    "Blame Impulsive": false,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "Reply all fuckups",
    "Description": "D'oh!  That email reeeeallly wasn't meant for everyone.  In response, an informational packet goes out with instructions on how to set up the recall email option.",
    "Actions": "#BAD\nChoose any without {{COMMON}}: add one {{BLAME}}",
    "Is Major Event": false,
    "Printer card": false,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": 0,
    "SYN Gain": 0,
    "Blame Common": true,
    "Blame Alluring": false,
    "Blame Impulsive": false,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "TPS report",
    "Description": "People keep forgetting to put cover sheets on their tps reports... Did they see the memo?",
    "Actions": "#BAD\nUnavoidable: SYN-2",
    "Is Major Event": false,
    "Printer card": 0,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": -2,
    "SYN Gain": 0,
    "Blame Common": false,
    "Blame Alluring": false,
    "Blame Impulsive": false,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "A huge controversy",
    "Description": "We don't want to talk about it, we just want to move on.\n",
    "Actions": "#BAD\nFor each {{EMEE}} (max 8): SYN-1",
    "Is Major Event": true,
    "Printer card": 0,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": -8,
    "SYN Gain": 0,
    "Blame Common": false,
    "Blame Alluring": false,
    "Blame Impulsive": false,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "The state of communication",
    "Description": "Has your company gotten too big for personal connections amongst employees and management?  Depends on how patient the founders are vs the number of employees.",
    "Actions": "#NEUTRAL\nAdd all founder patience together and subtract *half-count* {{EMEE}}.  This is the SYN gained or lost.",
    "Is Major Event": true,
    "Printer card": 0,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": -5,
    "SYN Gain": 5,
    "Blame Common": false,
    "Blame Alluring": false,
    "Blame Impulsive": false,
    "Blame Messy": false,
    "Deprecated": false
  },
  {
    "Name": "Effecient Meetings",
    "Description": "Sheperding cats along the flimsy timeline of an agenda is no simple task.  Whoever has done it best deserves a reward.",
    "Actions": "#GOOD\nChoose the most *agenda card* friendly *founder*: add two {{TASK}}",
    "Is Major Event": 0,
    "Printer card": 0,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": 0,
    "SYN Gain": 0,
    "Blame Common": 0,
    "Blame Alluring": 0,
    "Blame Impulsive": 0,
    "Blame Messy": 0,
    "Deprecated": 0
  },
  {
    "Name": "We don't want the funk",
    "Description": "There's a strange funk in the restrooms.  Nobody knows if it's anyones fault, but that doesn't stop them from suspecting it.",
    "Actions": "#BAD\nChoose any {{IMPULSIVE}}: add one {{BLAME}}\nChoose any {{EMEE}} without {{COMMON}}: add one {{BLAME}}",
    "Is Major Event": 0,
    "Printer card": 0,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": 0,
    "SYN Gain": 0,
    "Blame Common": true,
    "Blame Alluring": 0,
    "Blame Impulsive": 0,
    "Blame Messy": true,
    "Deprecated": 0
  },
  {
    "Name": 0,
    "Description": 0,
    "Actions": 0,
    "Is Major Event": 0,
    "Printer card": 0,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": 0,
    "SYN Gain": 0,
    "Blame Common": true,
    "Blame Alluring": 0,
    "Blame Impulsive": true,
    "Blame Messy": false,
    "Deprecated": 0
  },
  {
    "Name": 0,
    "Description": 0,
    "Actions": 0,
    "Is Major Event": 0,
    "Printer card": 0,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": 0,
    "SYN Gain": 0,
    "Blame Common": 0,
    "Blame Alluring": 0,
    "Blame Impulsive": true,
    "Blame Messy": 0,
    "Deprecated": 0
  },
  {
    "Name": 0,
    "Description": 0,
    "Actions": 0,
    "Is Major Event": 0,
    "Printer card": 0,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": 0,
    "SYN Gain": 0,
    "Blame Common": true,
    "Blame Alluring": true,
    "Blame Impulsive": 0,
    "Blame Messy": 0,
    "Deprecated": 0
  },
  {
    "Name": 0,
    "Description": 0,
    "Actions": 0,
    "Is Major Event": 0,
    "Printer card": 0,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": 0,
    "SYN Gain": 0,
    "Blame Common": 0,
    "Blame Alluring": true,
    "Blame Impulsive": 0,
    "Blame Messy": 0,
    "Deprecated": 0
  },
  {
    "Name": 0,
    "Description": 0,
    "Actions": 0,
    "Is Major Event": 0,
    "Printer card": 0,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": 0,
    "SYN Gain": 0,
    "Blame Common": 0,
    "Blame Alluring": true,
    "Blame Impulsive": 0,
    "Blame Messy": 0,
    "Deprecated": 0
  },
  {
    "Name": 0,
    "Description": 0,
    "Actions": 0,
    "Is Major Event": 0,
    "Printer card": 0,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": 0,
    "SYN Gain": 0,
    "Blame Common": 0,
    "Blame Alluring": true,
    "Blame Impulsive": 0,
    "Blame Messy": 0,
    "Deprecated": 0
  },
  {
    "Name": 0,
    "Description": 0,
    "Actions": 0,
    "Is Major Event": 0,
    "Printer card": 0,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": 0,
    "SYN Gain": 0,
    "Blame Common": 0,
    "Blame Alluring": true,
    "Blame Impulsive": 0,
    "Blame Messy": 0,
    "Deprecated": 0
  },
  {
    "Name": 0,
    "Description": 0,
    "Actions": 0,
    "Is Major Event": 0,
    "Printer card": 0,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": 0,
    "SYN Gain": 0,
    "Blame Common": 0,
    "Blame Alluring": true,
    "Blame Impulsive": 0,
    "Blame Messy": 0,
    "Deprecated": 0
  },
  {
    "Name": 0,
    "Description": 0,
    "Actions": 0,
    "Is Major Event": 0,
    "Printer card": 0,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": 0,
    "SYN Gain": 0,
    "Blame Common": true,
    "Blame Alluring": 0,
    "Blame Impulsive": 0,
    "Blame Messy": 0,
    "Deprecated": 0
  },
  {
    "Name": 0,
    "Description": 0,
    "Actions": 0,
    "Is Major Event": 0,
    "Printer card": 0,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": 0,
    "SYN Gain": 0,
    "Blame Common": true,
    "Blame Alluring": 0,
    "Blame Impulsive": 0,
    "Blame Messy": 0,
    "Deprecated": 0
  },
  {
    "Name": 0,
    "Description": 0,
    "Actions": 0,
    "Is Major Event": 0,
    "Printer card": 0,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": 0,
    "SYN Gain": 0,
    "Blame Common": true,
    "Blame Alluring": false,
    "Blame Impulsive": 0,
    "Blame Messy": 0,
    "Deprecated": 0
  },
  {
    "Name": "The state of connection",
    "Description": "Starting at 12SYN, subtract one for each project in the completed projects pile.",
    "Actions": 0,
    "Is Major Event": 0,
    "Printer card": 0,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": 0,
    "SYN Gain": 0,
    "Blame Common": 0,
    "Blame Alluring": 0,
    "Blame Impulsive": 0,
    "Blame Messy": 0,
    "Deprecated": true
  },
  {
    "Name": 0,
    "Description": 0,
    "Actions": 0,
    "Is Major Event": 0,
    "Printer card": 0,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": 0,
    "SYN Gain": 0,
    "Blame Common": 0,
    "Blame Alluring": 0,
    "Blame Impulsive": 0,
    "Blame Messy": 0,
    "Deprecated": 0
  },
  {
    "Name": 0,
    "Description": 0,
    "Actions": 0,
    "Is Major Event": 0,
    "Printer card": 0,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": 0,
    "SYN Gain": 0,
    "Blame Common": 0,
    "Blame Alluring": 0,
    "Blame Impulsive": 0,
    "Blame Messy": 0,
    "Deprecated": 0
  },
  {
    "Name": 0,
    "Description": 0,
    "Actions": 0,
    "Is Major Event": 0,
    "Printer card": 0,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": 0,
    "SYN Gain": 0,
    "Blame Common": 0,
    "Blame Alluring": 0,
    "Blame Impulsive": 0,
    "Blame Messy": 0,
    "Deprecated": 0
  },
  {
    "Name": "38 letters",
    "Description": "7 lines",
    "Actions": 0,
    "Is Major Event": 0,
    "Printer card": 0,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": -68,
    "SYN Gain": 34,
    "Blame Common": "16%",
    "Blame Alluring": "12%",
    "Blame Impulsive": "12%",
    "Blame Messy": "4%",
    "Deprecated": 0
  },
  {
    "Name": 0,
    "Description": 0,
    "Actions": 0,
    "Is Major Event": 0,
    "Printer card": 0,
    "Good Bad": 0,
    "Repeat": 0,
    "SYN Loss": 0,
    "SYN Gain": 0,
    "Blame Common": 0,
    "Blame Alluring": 0,
    "Blame Impulsive": 0,
    "Blame Messy": "44%",
    "Deprecated": 0
  }
];
});
___scope___.file("Entities/Decks/Option/OptionCardDeck.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const OptionCard_1 = require("./OptionCard");
const Deck_1 = require("../Deck");
const OptionCardSheet = require("../../../Registry/sheets/json/Synergy - Options.json");
const OptionCardList = [];
exports.OptionCardList = OptionCardList;
OptionCardSheet.forEach((card) => {
    const raw = {
        name: card["Name"],
        description: card.Description,
        repeatable: card.Repeatable,
    };
    const repeat = card["Repeat Item"] || 1;
    for (let i = 0; i < repeat; i++) {
        OptionCardList.push(new OptionCard_1.default(raw));
    }
});
class OptionCardDeck extends Deck_1.default {
    constructor(parent) {
        super(parent, () => OptionCardList, OptionCard_1.default.getBackNode);
    }
}
exports.default = OptionCardDeck;
//# sourceMappingURL=OptionCardDeck.js.map
});
___scope___.file("Entities/Decks/Option/OptionCard.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const el_tool_1 = require("el-tool");
const Util_1 = require("../../../Util");
require("./optionCard.scss");
class OptionCard {
    constructor(raw) {
        this.raw = raw;
        this.isSquinched = new Map();
        let qualitiesEl;
        this.domNode = el_tool_1.div("Option-Card front", [
            el_tool_1.div("Title", [
                qualitiesEl = el_tool_1.div("Qualities", [
                // raw.repeatable ? div("Quality repeatable") : null,
                ]),
                el_tool_1.div("Name", raw.name),
                qualitiesEl.cloneNode(true),
            ]),
            el_tool_1.div("Description", [
                el_tool_1.div("Fade"),
                ...this.bubbleEls = this.createBubbles().map((bubble) => el_tool_1.div(`Bubble --${bubble.type}`, [
                    el_tool_1.div("Content", Util_1.fromMD(Util_1.injectSymbols(bubble.content))),
                ])),
            ]),
        ]);
        requestAnimationFrame(() => requestAnimationFrame(() => this.squinchBubbles()));
    }
    createBubbles() {
        const out = ((this.raw.description + "").split(/\n/g) || []).filter((str) => str.trim() !== "").map((content) => {
            let type = "left_bubble";
            if (content.startsWith(">")) {
                type = "right_bubble";
            }
            else if (content.startsWith("!")) {
                type = "alert";
            }
            return {
                type,
                content: content.replace(/^[>!]/, ""),
            };
        });
        if (this.raw.repeatable) {
            out.push({ type: "left_bubble", content: "**Repeatable**: You may return this card to your hand after the *actions phase*." });
        }
        return out;
    }
    squinchBubbles() {
        this.bubbleEls.forEach((bubble) => this.isSquinched.set(bubble, false));
        const squinchFurther = () => {
            let promises = this.bubbleEls.map((bubble) => {
                if (this.isSquinched.get(bubble)) {
                    return;
                }
                return new Promise((resolve) => {
                    const startHeight = bubble.offsetHeight;
                    const startWidth = bubble.offsetWidth;
                    bubble.style.width = (startWidth - 1) + "px";
                    requestAnimationFrame(() => requestAnimationFrame(() => {
                        if (bubble.offsetHeight !== startHeight) {
                            bubble.style.width = startWidth + "px";
                            this.isSquinched.set(bubble, true);
                        }
                        resolve();
                    }));
                });
            });
            promises = promises.filter((it) => !!it);
            if (promises.length) {
                Promise.all(promises).then(() => squinchFurther());
            }
            else {
                console.log("SQUINCHED");
            }
        };
        squinchFurther();
    }
    static getBackNode() {
        return el_tool_1.div("Option-Card back", "OptionCard CARD");
    }
}
exports.default = OptionCard;
//# sourceMappingURL=OptionCard.js.map
});
___scope___.file("Entities/Decks/Option/optionCard.scss", function(exports, require, module, __filename, __dirname){


require("fuse-box-css")("default/Entities/Decks/Option/optionCard.scss", ".Option-Card {\n  height: 3.5in;\n  width: 2.5in;\n  border-radius: 0.1in;\n  border: 0.1in solid #222;\n  display: inline-grid; }\n  .Option-Card.front {\n    grid-template-rows: auto 1fr;\n    color: black;\n    background: white; }\n    .Option-Card.front > .Title {\n      display: flex;\n      text-align: center;\n      background: #EEE;\n      border-bottom: 1px solid rgba(100, 100, 100, 0.3);\n      padding: .25em 0px; }\n      .Option-Card.front > .Title > .Name {\n        flex: 1; }\n      .Option-Card.front > .Title > .Qualities {\n        top: 0px;\n        right: 0px;\n        flex-shrink: 1; }\n        .Option-Card.front > .Title > .Qualities:first-child {\n          visibility: hidden; }\n        .Option-Card.front > .Title > .Qualities > .Quality {\n          height: 16px;\n          width: 16px;\n          border-radius: 16px;\n          margin: 0px 4px;\n          background: #EA2323; }\n    .Option-Card.front > .Description {\n      font-size: 13px;\n      position: relative; }\n      .Option-Card.front > .Description > .Fade {\n        position: absolute;\n        top: 0px;\n        left: 0px;\n        right: 0px;\n        bottom: 0px;\n        opacity: 0.5;\n        background-image: linear-gradient(to bottom, white, transparent);\n        mix-blend-mode: overlay; }\n      .Option-Card.front > .Description > .Bubble > .Content {\n        position: relative; }\n        .Option-Card.front > .Description > .Bubble > .Content > p {\n          margin: 0px; }\n      .Option-Card.front > .Description > .Bubble.--left_bubble, .Option-Card.front > .Description > .Bubble.--right_bubble {\n        max-width: 90%;\n        background: #e5e5ea;\n        border-radius: 0.75em;\n        padding: 0.25em 0.5em;\n        margin: 0.5em; }\n      .Option-Card.front > .Description > .Bubble.--right_bubble {\n        margin-left: auto;\n        background: #087;\n        color: white; }\n      .Option-Card.front > .Description > .Bubble.--alert {\n        margin: 1em auto;\n        color: #902424;\n        font-size: 12px;\n        font-style: italic;\n        text-align: center; }\n  .Option-Card.back {\n    background-image: radial-gradient(#0c7b6f, #00556c); }\n\n/*# sourceMappingURL=/css-sourcemaps/01c81274e.map */")
});
___scope___.file("Registry/sheets/json/Synergy - Options.json", function(exports, require, module, __filename, __dirname){

module.exports = [
  {
    "Name": "New Chairs",
    "Description": "You may spend 10k to get new office chairs for SYN+2.  \n>You may also assign one {{TASK}} from any *employee* with **1+** {{CHA}} to sell the old chairs for +2k.",
    "Repeat Item": 0,
    "Repeatable": false,
    "ImageId": 0,
    "syn++": true,
    "blame--": false,
    "task++": false
  },
  {
    "Name": "Laser Tag",
    "Description": "Take everyone out to laser tag after work for 1k per *employee*.  \n>If you have at least three *employees* with **2+** {{LOG}}, get SYN+1.\n>If you have at least four *employees* with **2+** {{CHA}}, get SYN+1.",
    "Repeat Item": 0,
    "Repeatable": false,
    "ImageId": 0,
    "syn++": true,
    "blame--": false,
    "task++": false
  },
  {
    "Name": "Change Snacks",
    "Description": "You are the unquestioned ruler of office snacks.  Nobody can question your rulings.\nChoose one of the following:\n>A. Spend -8k to improve the snacks and gain SYN+1.\n>B. Remove snacks entirely to save +6k, but lose SYN-1.\n",
    "Repeat Item": 0,
    "Repeatable": true,
    "ImageId": 0,
    "syn++": true,
    "blame--": false,
    "task++": false
  },
  {
    "Name": "One Fish, Two Fish",
    "Description": "You've found a great deal on an aquarium.\n>If you don't already have an aquarium, you may purchase one for 2k and place it on the board.\n>(For details on pet gameplay, see *Pets and Furniture* in the rule book)\n",
    "Repeat Item": 0,
    "Repeatable": false,
    "ImageId": "fishes",
    "syn++": true,
    "blame--": false,
    "task++": false
  },
  {
    "Name": "Pet Store",
    "Description": "You may purchase your choice of pet.\n>A. Fish for 4k\n>B. Cat for 5k\n>C. Dog for 6k\n(For details, see *Pets and Furniture* in the rule book)\n",
    "Repeat Item": 3,
    "Repeatable": false,
    "ImageId": 0,
    "syn++": true,
    "blame--": false,
    "task++": false
  },
  {
    "Name": "Furniture Store",
    "Description": "You may purchase your choice of furniture.\n>A. Couch for 10k\n>B. Beanbag for 8k\n>C. Arcade Cabinet for 14k\n(For details, see *Pets and Furniture* in the rule book)",
    "Repeat Item": 3,
    "Repeatable": false,
    "ImageId": 0,
    "syn++": true,
    "blame--": true,
    "task++": false
  },
  {
    "Name": "Taking feedback",
    "Description": "Put a poll out to get employees' \"honest\" feedback about the company direction and lifestyle.  \n>Any co-founder with 3 {{PAT}} may trade **1** {{TASK}} to read through it all and make actual changes for SYN+3.  \n>Otherwise, gain SYN+1 for at least trying.",
    "Repeat Item": 0,
    "Repeatable": false,
    "ImageId": 0,
    "syn++": true,
    "blame--": false,
    "task++": false
  },
  {
    "Name": "Give a vacation",
    "Description": "A little relaxation goes a long ways. \n>Play this card to remove **1** {{TASK}} from any *employee* and place it on this card.  At the beginning of next round, give {{TASK}}+2 to the same *employee*.\n>Does not apply to founders.\n>Card stays \"in use\" while {{TASK}} is placed on it.",
    "Repeat Item": 0,
    "Repeatable": true,
    "ImageId": 0,
    "syn++": false,
    "blame--": false,
    "task++": true
  },
  {
    "Name": "\"Take some time off\"\",\"A little space & careful attention goes a long ways. \n>Play this card to remove **1** {{TASK}} and **1** {{BLAME}} from any employee.  \n>Does not apply to founders.",
    "Description": 0,
    "Repeat Item": true,
    "Repeatable": 0,
    "ImageId": false,
    "syn++": true,
    "blame--": false
  },
  {
    "Name": "Add bathroom music",
    "Description": "Rare are those who take no shame in being heard amidst their bathroom duties.  \n>Add some privacy music to the restrooms and gain SYN+1.",
    "Repeat Item": 0,
    "Repeatable": false,
    "ImageId": 0,
    "syn++": true,
    "blame--": false,
    "task++": false
  },
  {
    "Name": "Art it up",
    "Description": "Spruce up the place with some art.  Choose one of the following:\n>A. Pay an outside artist -8k and gain SYN+1 in decor.\n>B. Pay -3k for materials and use **3** {{TASK}} from an *employee* with **3** {{ART}} and gain SYN+2. ",
    "Repeat Item": 0,
    "Repeatable": false,
    "ImageId": 0,
    "syn++": true,
    "blame--": false,
    "task++": false
  },
  {
    "Name": "Add plants",
    "Description": 0,
    "Repeat Item": 0,
    "Repeatable": false,
    "ImageId": 0,
    "syn++": false,
    "blame--": false,
    "task++": false
  },
  {
    "Name": "Paint the walls",
    "Description": "Pay -6k to put a fresh coat of paint up and gain SYN+1.",
    "Repeat Item": 0,
    "Repeatable": false,
    "ImageId": 0,
    "syn++": true,
    "blame--": false,
    "task++": false
  },
  {
    "Name": "Sod mound thinking corner",
    "Description": "Nothing helps an employee align their chakras with mother earth more than a grass mound thinking corner.\n> You may spend 10k and place this card on the board (with the same rules as employee cards).  \nWhile on the board, *employees* may place **1** {{TASK}} on the mound per *quarter*.\n> Each time the mound collects **4** {{TASK}}, clear them and gain SYN+1.\n> No assignment cost.  Discard freely. ",
    "Repeat Item": 0,
    "Repeatable": false,
    "ImageId": "sodMound",
    "syn++": true,
    "blame--": false,
    "task++": false
  },
  {
    "Name": "Slave driver",
    "Description": "You are the captain of crunch time.  You don't just run the company, you sprint it.\n>You may add **2** {{TASK}} to as many employees as you want losing SYN-1 for each.\n",
    "Repeat Item": 0,
    "Repeatable": true,
    "ImageId": 0,
    "syn++": false,
    "blame--": false,
    "task++": true
  },
  {
    "Name": "Fake it",
    "Description": "Of course we can do that project...\n>While *managing a project*, you may treat each *employee* as though all of their *skills* are increased by one.  The stress of being thrown into the deep end results in SYN-3.",
    "Repeat Item": 0,
    "Repeatable": true,
    "ImageId": 0,
    "syn++": false,
    "blame--": false,
    "task++": true
  },
  {
    "Name": "Nice Headphones",
    "Description": "!This card can only be played during a *quirk check*.\n> You may pay -3k per *employee* to make them immune to the effects of _all_ *directly adjacent* coworkers.  \nThis card does not stop *quirks* from affecting the employee who posseses them.",
    "Repeat Item": 0,
    "Repeatable": false,
    "ImageId": 0,
    "syn++": true,
    "blame--": false,
    "task++": true
  },
  {
    "Name": "Quit Using Business Jargon",
    "Description": "If *half-count* worth of the employees have at least **1** {{LOG}}, you may collectively quit using business jargon and gain SYN+3.",
    "Repeat Item": 0,
    "Repeatable": false,
    "ImageId": 0,
    "syn++": true,
    "blame--": false,
    "task++": false
  },
  {
    "Name": "Scapegoat",
    "Description": "Move **1** {{BLAME}} from any *worker* to any other *worker* _without_ {{COMMON}}.",
    "Repeat Item": 0,
    "Repeatable": true,
    "ImageId": 0,
    "syn++": false,
    "blame--": true,
    "task++": false
  },
  {
    "Name": "Office Yoga",
    "Description": "Take a second to breathe, maybe stretch your toes.  You can do it secretly, and it can be quite nice.\n>You may opt to bring yoga to the office for -4k, and remove **1** {{BLAME}} from any *employee* by creating an air of forgiveness.",
    "Repeat Item": 0,
    "Repeatable": true,
    "ImageId": 0,
    "syn++": false,
    "blame--": true,
    "task++": false
  },
  {
    "Name": "Team Building Exercise",
    "Description": "Lose **2** {{TASK}} and also SYN-3.  Nobody in their right mind would do this, right?  Surely, wasting company time and money to annoy employees is not something anyone would do...",
    "Repeat Item": 0,
    "Repeatable": true,
    "ImageId": 0,
    "syn++": false,
    "blame--": false,
    "task++": false
  },
  {
    "Name": "Company Activity After hours",
    "Description": 0,
    "Repeat Item": 0,
    "Repeatable": false,
    "ImageId": 0,
    "syn++": false,
    "blame--": false,
    "task++": false
  },
  {
    "Name": "Company throws conference",
    "Description": 0,
    "Repeat Item": 0,
    "Repeatable": false,
    "ImageId": 0,
    "syn++": false,
    "blame--": false,
    "task++": false
  },
  {
    "Name": "Pizza Party!",
    "Description": "All anybody really wants from life is cheese and bread.  So give them what they want.\n> You may spend -5k to get pizza for the office, and gain SYN+1.\n> If you have at least **3** employees with **2+** {{ART}}, you may spend an additional -5k to get a gluten free, vegan pizza and gain SYN+1.",
    "Repeat Item": 0,
    "Repeatable": false,
    "ImageId": 0,
    "syn++": true,
    "blame--": false,
    "task++": false
  },
  {
    "Name": "Did someone say Go-Karts?",
    "Description": 0,
    "Repeat Item": 0,
    "Repeatable": false,
    "ImageId": 0,
    "syn++": false,
    "blame--": false,
    "task++": false
  },
  {
    "Name": "Staff Video Competition",
    "Description": "There's nothing quite like seeing your usually crotchety coworker dancing in a gorilla outfit and a pink tutu to make folks remember we're all human.  Host a departmental video competition about what the company does and post on social media for + 2 SYN",
    "Repeat Item": 0,
    "Repeatable": false,
    "ImageId": 0,
    "syn++": false,
    "blame--": false,
    "task++": false
  },
  {
    "Name": "Company News",
    "Description": "You may start sending a monthly company newsletter sharing details of what's going on with the company and why.\n> Take **2** {{TASK}} from any *worker* with **1+** {{CHA}} and **1+** {{PAT}} to write the newsletter.  \n> The clarity given on the hard effort of others yields forgiveness, remove **1** {{BLAME}} from any *worker*. ",
    "Repeat Item": 0,
    "Repeatable": true,
    "ImageId": 0,
    "syn++": false,
    "blame--": true,
    "task++": false
  },
  {
    "Name": "Ergonomic Upgrades",
    "Description": "Standing desks, split keyboards, and yoga balls went on sale at the office supply store.  For $xx, take orders from employees and buy them for xx SYN",
    "Repeat Item": 0,
    "Repeatable": false,
    "ImageId": 0,
    "syn++": false,
    "blame--": false,
    "task++": false
  },
  {
    "Name": "Shake those Polaroids",
    "Description": "A wall of polaroids is a great way to hold onto company history. Spend -4k to buy a polaroid camera for the lunchroom and get SYN+1.",
    "Repeat Item": 0,
    "Repeatable": false,
    "ImageId": 0,
    "syn++": true,
    "blame--": false,
    "task++": false
  },
  {
    "Name": "Chair Massage",
    "Description": "You may pay -2k to buy any one of your *employees* a chair massage, and give them **1** extra {{TASK}}.\n> You may not buy a massage for the same *employee* twice in a row.",
    "Repeat Item": 0,
    "Repeatable": true,
    "ImageId": 0,
    "syn++": false,
    "blame--": false,
    "task++": true
  },
  {
    "Name": "Staff Barista",
    "Description": "Sweet black nectar of life.\n> You may bring in a barista for -1k per *employee*, and gain SYN+1.",
    "Repeat Item": 0,
    "Repeatable": false,
    "ImageId": 0,
    "syn++": true,
    "blame--": false,
    "task++": false
  },
  {
    "Name": "Community Free Library",
    "Description": "Add a free community library for SYN+1.",
    "Repeat Item": 0,
    "Repeatable": false,
    "ImageId": "library",
    "syn++": true,
    "blame--": false,
    "task++": false
  },
  {
    "Name": "Massive Reorg",
    "Description": "You may *fire* as many *employees* as you like without taking any {{BLAME}}\nFor each *employee* you *fire* lose SYN-1.",
    "Repeat Item": 0,
    "Repeatable": false,
    "ImageId": 0,
    "syn++": false,
    "blame--": true,
    "task++": false
  },
  {
    "Name": "Witch Hunt",
    "Description": "One of your *employees* has a nasty secret, that nobody but you can find.\n> You may out and then *fire* any one of your _most paid_ *employees* without getting {{BLAME}}, and gain SYN+2.",
    "Repeat Item": 0,
    "Repeatable": false,
    "ImageId": 0,
    "syn++": true,
    "blame--": true,
    "task++": false
  },
  {
    "Name": "Call for Artists!",
    "Description": "Employees often have side-projects that you don't know about until you ask. \nPut out a Call for Art to decorate the office and learn that one of your employees is more talented than they let on.  \n> You may treat any _single_ *employee* as though they have **3** {{ART}} until the end of this *quarter*.",
    "Repeat Item": 0,
    "Repeatable": false,
    "ImageId": 0,
    "syn++": false,
    "blame--": false,
    "task++": true
  },
  {
    "Name": "Super Research",
    "Description": "Pull the top **3** cards off any deck and discard as many as you like.  Put the rest back on top in any order.",
    "Repeat Item": 0,
    "Repeatable": false,
    "ImageId": 0,
    "syn++": false,
    "blame--": false,
    "task++": false
  },
  {
    "Name": "Check your horoscope",
    "Description": "!This card may only be played during a *quirk check*.\nA quality horoscope has changed the path you were going down.\n> You may reroll the dice _one_ time.",
    "Repeat Item": 0,
    "Repeatable": false,
    "ImageId": 0,
    "syn++": false,
    "blame--": false,
    "task++": false
  },
  {
    "Name": "Open Exchange",
    "Description": "All *founders* may trade as many cards as they like.",
    "Repeat Item": 0,
    "Repeatable": false,
    "ImageId": 0,
    "syn++": false,
    "blame--": false,
    "task++": false
  },
  {
    "Name": "Plagiarize",
    "Description": "Any _single_ *founder* can *spec-out and manage* any _single_ project, regardless of their skill levels.",
    "Repeat Item": 0,
    "Repeatable": false,
    "ImageId": 0,
    "syn++": false,
    "blame--": false,
    "task++": true
  },
  {
    "Name": "Gaslighting",
    "Description": "If you mess with the data enough, nobody will know what to trust or who to blame.  \n> Trade SYN-1 to create confusion and remove **1** {{BLAME}} from any *worker*.",
    "Repeat Item": 0,
    "Repeatable": false,
    "ImageId": 0,
    "syn++": false,
    "blame--": true,
    "task++": false
  },
  {
    "Name": "Downsize",
    "Description": "*Fire* up to three *employees* without taking any {{BLAME}}.",
    "Repeat Item": 0,
    "Repeatable": false,
    "ImageId": 0,
    "syn++": false,
    "blame--": true,
    "task++": false
  },
  {
    "Name": "Social Training Class",
    "Description": "Trade **3** {{TASK}} from any *employee* _and_ spend -10k, to remove their *quirk card*.",
    "Repeat Item": 0,
    "Repeatable": false,
    "ImageId": 0,
    "syn++": true,
    "blame--": true,
    "task++": true
  },
  {
    "Name": "Bootcamp",
    "Description": "Any _single_ *founder* may spend as many {{TASK}} as they want on the *acquire new specialty* action, within this single *quarter*.",
    "Repeat Item": 0,
    "Repeatable": false,
    "ImageId": 0,
    "syn++": false,
    "blame--": false,
    "task++": true
  },
  {
    "Name": "Request Extension",
    "Description": "!This card can only be played during mission failure.  See *lose conditions* for details.\nRather than failing, play this card to extend the mission deadline one more *quarter*.",
    "Repeat Item": 0,
    "Repeatable": false,
    "ImageId": 0,
    "syn++": false,
    "blame--": false,
    "task++": true
  },
  {
    "Name": "Save Face",
    "Description": "!This card can only be played if *founders* part ways (a founder gets **3** {{BLAME}}).  See *lose conditions* for details.\nRather than failing, you may play this card to make an inspiring apology and nullify _one_ {{BLAME}}.",
    "Repeat Item": 0,
    "Repeatable": false,
    "ImageId": 0,
    "syn++": false,
    "blame--": true,
    "task++": false
  },
  {
    "Name": "Epic Speech",
    "Description": "!This card can only be played in fallout (SYN = -10).  See *lose conditions* for details.\nRather than failing, you may play this card to motivate employees to hold on for one more *quarter*.",
    "Repeat Item": 0,
    "Repeatable": false,
    "ImageId": 0,
    "syn++": true,
    "blame--": false,
    "task++": false
  },
  {
    "Name": "Cook the books",
    "Description": "!This card can only be played in bankruptcy (out of funds).  See *lose conditions* for details.\nRather than failing, you may play this card to forge your records and stay in business for one more *quarter*.",
    "Repeat Item": 0,
    "Repeatable": false,
    "ImageId": 0,
    "syn++": false,
    "blame--": false,
    "task++": true
  },
  {
    "Name": 0,
    "Description": 0,
    "Repeat Item": 0,
    "Repeatable": false,
    "ImageId": 0,
    "syn++": false,
    "blame--": false,
    "task++": false
  },
  {
    "Name": 0,
    "Description": 0,
    "Repeat Item": 0,
    "Repeatable": 0,
    "ImageId": 0,
    "syn++": "42%",
    "blame--": "23%",
    "task++": "23%"
  }
];
});
___scope___.file("Entities/Decks/Quirk/QuirkDeck.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const QuirkCard_1 = require("./QuirkCard");
const Deck_1 = require("../Deck");
const QuirkCardSheet = require("../../../Registry/sheets/json/Synergy - Quirks.json");
const QuirkCardList = [];
exports.QuirkCardList = QuirkCardList;
QuirkCardSheet.forEach((card) => {
    const raw = {
        name: card["Name"],
        description: card.Description,
        common: card.Common,
        alluring: card.Alluring,
        impulsive: card.Impulsive,
        messy: card.Messy,
    };
    const repeat = card.Repeat || 1;
    for (let i = 0; i < repeat; i++) {
        QuirkCardList.push(new QuirkCard_1.default(raw));
    }
});
class QuirkCardDeck extends Deck_1.default {
    constructor(parent) {
        super(parent, () => QuirkCardList, QuirkCard_1.default.getBackNode);
    }
}
exports.default = QuirkCardDeck;
//# sourceMappingURL=QuirkDeck.js.map
});
___scope___.file("Entities/Decks/Quirk/QuirkCard.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const el_tool_1 = require("el-tool");
const Util_1 = require("../../../Util/");
require("./quirkCard.scss");
class QuirkCard {
    constructor(raw) {
        this.raw = raw;
        this.domNode = el_tool_1.div("Quirk-Card front", [
            el_tool_1.div("Quirk-bg", [
                el_tool_1.div("Floor"),
                el_tool_1.div("Desk"),
                el_tool_1.div("Fade"),
            ]),
            el_tool_1.div("Quirk-fg", [
                el_tool_1.div("Title", [
                    (raw.name || "").toUpperCase(),
                ]),
                el_tool_1.div("Description", [
                    Util_1.fromMD(Util_1.injectSymbols((raw.description + "").replace(/\n/g, "<br>"))),
                    () => {
                        const qualities = [
                            raw.alluring ? this.createQuality("Alluring") : null,
                            raw.common ? this.createQuality("Common") : null,
                            raw.impulsive ? this.createQuality("Impulsive") : null,
                            raw.messy ? this.createQuality("Messy") : null,
                        ].filter((it) => !!it);
                        return qualities.length ? el_tool_1.div("Qualities", qualities) : null;
                    },
                ]),
            ])
        ]);
    }
    createQuality(name) {
        return Util_1.fromMD(Util_1.injectSymbols(`{{${name.toUpperCase()}}} ${name}`));
    }
    static getBackNode() {
        return el_tool_1.div("Quirk-Card back", "Quirk Card");
    }
}
exports.default = QuirkCard;
//# sourceMappingURL=QuirkCard.js.map
});
___scope___.file("Entities/Decks/Quirk/quirkCard.scss", function(exports, require, module, __filename, __dirname){


require("fuse-box-css")("default/Entities/Decks/Quirk/quirkCard.scss", ".Quirk-Card {\n  position: relative;\n  height: 2in;\n  width: 2in;\n  border-radius: 0.1in;\n  overflow: hidden;\n  display: inline-grid; }\n  .Quirk-Card.front > .Quirk-bg > *, .Quirk-Card.front > .Quirk-fg {\n    position: absolute;\n    top: 0px;\n    left: 0px;\n    height: 100%;\n    width: 100%; }\n  .Quirk-Card.front > .Quirk-bg > *:not(.Fade) {\n    filter: blur(2px); }\n  .Quirk-Card.front > .Quirk-bg > .Floor {\n    background-color: #b6b6b6;\n    background-size: 0.525in 0.525in;\n    background-image: linear-gradient(to right, #989898 1px, transparent 1px), linear-gradient(to bottom, #989898 1px, transparent 1px);\n    background-position: 6px 9px; }\n  .Quirk-Card.front > .Quirk-bg > .Desk {\n    display: block;\n    content: \"\";\n    background-image: url(\"/css-resources/assets/Desk.png\");\n    background-size: 3.5in auto;\n    background-position: -138px -36px;\n    background-repeat: no-repeat; }\n  .Quirk-Card.front > .Quirk-bg > .Fade {\n    background-color: white;\n    opacity: 0.92; }\n  .Quirk-Card.front > .Quirk-fg {\n    grid-template-rows: auto 1fr auto;\n    padding: 0.0625in;\n    border-radius: inherit;\n    color: #111; }\n    .Quirk-Card.front > .Quirk-fg > .Title {\n      font-weight: bold;\n      font-size: 12px;\n      color: #999;\n      text-align: center;\n      border-bottom: 1px solid #999;\n      margin-bottom: 0.0625in; }\n    .Quirk-Card.front > .Quirk-fg > .Description > .Qualities {\n      margin-top: 10px;\n      padding-top: 10px;\n      border-top: #999 1px solid; }\n  .Quirk-Card.back {\n    background-size: contain;\n    background-position: center center;\n    background-color: white;\n    text-align: center;\n    padding-top: 170px;\n    font-size: 22px;\n    color: #4a4a4a; }\n\n/*# sourceMappingURL=/css-sourcemaps/0406137ac.map */")
});
___scope___.file("Registry/sheets/json/Synergy - Quirks.json", function(exports, require, module, __filename, __dirname){

module.exports = [
  {
    "Name": "Chatterbox",
    "Description": "{{BESIDE}} All coworkers *directly beside* this employee lose {{TASK}}-1 due to distraction.",
    "Repeat": 0,
    "d20 stuff": 0,
    "Common": true,
    "Alluring": false,
    "Impulsive": false,
    "Messy": false,
    "Positive, Negative, Neutral": 0
  },
  {
    "Name": "Aderol Addict",
    "Description": "{{D20}} If the *roll* is greater than 12, gain {{TASK}}+1.  For *rolls* less than 8, lose {{TASK}}-1.",
    "Repeat": 0,
    "d20 stuff": ">10 ? good : bad",
    "Common": false,
    "Alluring": true,
    "Impulsive": true,
    "Messy": false,
    "Positive, Negative, Neutral": 0
  },
  {
    "Name": "Stanky",
    "Description": "{{ADJACENT}} If more than two coworkers are *directly adjacent* this one lose SYN-1",
    "Repeat": 0,
    "d20 stuff": 0,
    "Common": false,
    "Alluring": false,
    "Impulsive": true,
    "Messy": true,
    "Positive, Negative, Neutral": 0
  },
  {
    "Name": "Slow to learn",
    "Description": "{{REPEAT}} This *employee* takes longer to learn things than expected.  Lose {{TASK}}-1.",
    "Repeat": 0,
    "d20 stuff": 0,
    "Common": true,
    "Alluring": false,
    "Impulsive": false,
    "Messy": false,
    "Positive, Negative, Neutral": 0
  },
  {
    "Name": "Super efficient",
    "Description": "{{REPEAT}} This *employee* gets more done in a day than expected.  Gain {{TASK}}+1.",
    "Repeat": 0,
    "d20 stuff": 0,
    "Common": false,
    "Alluring": false,
    "Impulsive": false,
    "Messy": false,
    "Positive, Negative, Neutral": 0
  },
  {
    "Name": "Predator",
    "Description": "{{ADJACENT}} This employee takes one {{TASK}} from all *directly adjacent* coworkers with less {{CHA}} as their own.",
    "Repeat": 0,
    "d20 stuff": 0,
    "Common": true,
    "Alluring": false,
    "Impulsive": true,
    "Messy": false,
    "Positive, Negative, Neutral": 0
  },
  {
    "Name": "Martyr",
    "Description": "{{REPEAT}} Takes one *blame* {{BLAME}} from any other *employee* as their own.  Does not apply to *founders*.  ",
    "Repeat": 0,
    "d20 stuff": 0,
    "Common": false,
    "Alluring": false,
    "Impulsive": false,
    "Messy": false,
    "Positive, Negative, Neutral": 0
  },
  {
    "Name": "Undervalued",
    "Description": "{{REPEAT}} This employee will occasionally accept less pay than they are worth.  Gain +3k.",
    "Repeat": 0,
    "d20 stuff": 0,
    "Common": true,
    "Alluring": false,
    "Impulsive": false,
    "Messy": false,
    "Positive, Negative, Neutral": 0
  },
  {
    "Name": "Wanderlust",
    "Description": "{{D20}} If *roll* is less than 3, employee leaves to go travel the world.  Discard and gain SYN+1.",
    "Repeat": 0,
    "d20 stuff": "1 ? bad",
    "Common": false,
    "Alluring": true,
    "Impulsive": false,
    "Messy": false,
    "Positive, Negative, Neutral": 0
  },
  {
    "Name": "Easily overlooked",
    "Description": "If this employee has any *skills* which are larger than that of the *founder* who is hiring them by more than 1, they get overlooked.  Discard them.",
    "Repeat": 5,
    "d20 stuff": 0,
    "Common": false,
    "Alluring": false,
    "Impulsive": false,
    "Messy": false,
    "Positive, Negative, Neutral": 0
  },
  {
    "Name": "Uncommonly Common",
    "Description": "No quirks... probably a serial killer.",
    "Repeat": 0,
    "d20 stuff": 0,
    "Common": true,
    "Alluring": false,
    "Impulsive": false,
    "Messy": false,
    "Positive, Negative, Neutral": 0
  },
  {
    "Name": "Extrovert",
    "Description": "{{BESIDE}} For _each_ coworker *directly beside* this one: if they have {{COMMON}} gain SYN+1; if not lose SYN-1 to trite chit-chat.",
    "Repeat": 0,
    "d20 stuff": 0,
    "Common": true,
    "Alluring": true,
    "Impulsive": false,
    "Messy": false,
    "Positive, Negative, Neutral": 0
  },
  {
    "Name": "Introvert",
    "Description": "{{ADJACENT}} If *directly adjacent* to more than one coworker, lose SYN-1 to social over stimulation.",
    "Repeat": 0,
    "d20 stuff": 0,
    "Common": false,
    "Alluring": false,
    "Impulsive": false,
    "Messy": false,
    "Positive, Negative, Neutral": 0
  },
  {
    "Name": "Office plant",
    "Description": "{{REPEAT}} If placed against a window, gain SYN+1 from the sunlight.",
    "Repeat": 0,
    "d20 stuff": 0,
    "Common": true,
    "Alluring": false,
    "Impulsive": false,
    "Messy": false,
    "Positive, Negative, Neutral": 0
  },
  {
    "Name": "OCD",
    "Description": "{{REPEAT}} If the total *employee* count is not a multiple of 3, lose SYN-1 because everything must be threes.",
    "Repeat": 0,
    "d20 stuff": 0,
    "Common": false,
    "Alluring": false,
    "Impulsive": true,
    "Messy": false,
    "Positive, Negative, Neutral": 0
  },
  {
    "Name": "Beloved",
    "Description": "{{REPEAT}} Gain SYN+1.\nIf this employee is *fired* or *quits*, lose SYN-3.",
    "Repeat": 0,
    "d20 stuff": 0,
    "Common": false,
    "Alluring": true,
    "Impulsive": false,
    "Messy": false,
    "Positive, Negative, Neutral": 0
  },
  {
    "Name": "Yes Man",
    "Description": "{{REPEAT}} Pull a project from the top of the projects pile and play it immediately due to lack of honest criticism.",
    "Repeat": 0,
    "d20 stuff": "17 ? bad",
    "Common": true,
    "Alluring": true,
    "Impulsive": false,
    "Messy": false,
    "Positive, Negative, Neutral": 0
  },
  {
    "Name": "Strong opinions",
    "Description": "{{BESIDE}} For _each_ employee *directly beside* this one with less than 2 {{PAT}}, lose SYN-1 to annoyance.\n",
    "Repeat": 3,
    "d20 stuff": 0,
    "Common": false,
    "Alluring": false,
    "Impulsive": true,
    "Messy": false,
    "Positive, Negative, Neutral": 0
  },
  {
    "Name": "Ethically-minded",
    "Description": "{{REPEAT}} If there is less than 15 SYN, this employee repectfully leaves.  Discard with no SYN loss.",
    "Repeat": 0,
    "d20 stuff": 0,
    "Common": true,
    "Alluring": false,
    "Impulsive": false,
    "Messy": false,
    "Positive, Negative, Neutral": 0
  },
  {
    "Name": "The Stain",
    "Description": "If this employee *quits* or is *fired*, flip over their card.  No one can work in their spot.",
    "Repeat": 0,
    "d20 stuff": 0,
    "Common": true,
    "Alluring": false,
    "Impulsive": true,
    "Messy": true,
    "Positive, Negative, Neutral": 0
  },
  {
    "Name": "Brings cool dog to office ",
    "Description": "{{REPEAT}} Gain SYN+1 for awesome doggos.",
    "Repeat": 0,
    "d20 stuff": 0,
    "Common": false,
    "Alluring": false,
    "Impulsive": false,
    "Messy": true,
    "Positive, Negative, Neutral": 0
  },
  {
    "Name": "Proud parent",
    "Description": "{{ADJACENT}} If _no_ employees *directly adjacent* to this one have {{COMMON}} or {{ALLURING}}, lose SYN-1 to \"my kid\" stories.",
    "Repeat": 0,
    "d20 stuff": 0,
    "Common": true,
    "Alluring": false,
    "Impulsive": false,
    "Messy": false,
    "Positive, Negative, Neutral": 0
  },
  {
    "Name": "The Great Motivator",
    "Description": "{{ADJACENT}} Add to SYN *half count* the number of *directly adjacent* employees.",
    "Repeat": 0,
    "d20 stuff": 0,
    "Common": false,
    "Alluring": true,
    "Impulsive": false,
    "Messy": false,
    "Positive, Negative, Neutral": 0
  },
  {
    "Name": "Workaholic",
    "Description": "{{D20}} If *roll* is 8 or 9, this employee *quits* from burnout (with SYN loss).  Otherwise they gain {{TASK}}+1.",
    "Repeat": 0,
    "d20 stuff": "8 || 9 ? bad",
    "Common": false,
    "Alluring": false,
    "Impulsive": false,
    "Messy": true,
    "Positive, Negative, Neutral": 0
  },
  {
    "Name": "Our Hero",
    "Description": "{{D20}} If *roll* is 11, this employee gains {{TASK}}+3 to help save the day.",
    "Repeat": 0,
    "d20 stuff": "11 ? good",
    "Common": false,
    "Alluring": true,
    "Impulsive": false,
    "Messy": false,
    "Positive, Negative, Neutral": 0
  },
  {
    "Name": "Cuddlywumpus",
    "Description": "Upon hiring, gain SYN+2 for having a giant teddy bear in the office.",
    "Repeat": 0,
    "d20 stuff": 0,
    "Common": false,
    "Alluring": true,
    "Impulsive": false,
    "Messy": false,
    "Positive, Negative, Neutral": 0
  },
  {
    "Name": "Picks nose a lot",
    "Description": "Upon hiring, lose SYN-1 for the occasional rogue bogie on your tail.",
    "Repeat": 0,
    "d20 stuff": 0,
    "Common": true,
    "Alluring": false,
    "Impulsive": false,
    "Messy": true,
    "Positive, Negative, Neutral": 0
  },
  {
    "Name": "Always late",
    "Description": "{{D20}} If *roll* is 4, lose {{TASK}}-1 to lost time.",
    "Repeat": 0,
    "d20 stuff": "4 ? bad",
    "Common": false,
    "Alluring": false,
    "Impulsive": false,
    "Messy": false,
    "Positive, Negative, Neutral": 0
  },
  {
    "Name": "Tries to finish your sentence",
    "Description": "Upon hiring, lose SYN-1 getting used to the minor grievance.",
    "Repeat": 0,
    "d20 stuff": 0,
    "Common": true,
    "Alluring": false,
    "Impulsive": false,
    "Messy": false,
    "Positive, Negative, Neutral": 0
  },
  {
    "Name": "Makes inappropriate jokes",
    "Description": "{{ADJACENT}} For _each_ *directly adjacent* coworker with more than one {{CHA}}, lose SYN-1 to cringe.",
    "Repeat": 0,
    "d20 stuff": 0,
    "Common": false,
    "Alluring": false,
    "Impulsive": true,
    "Messy": true,
    "Positive, Negative, Neutral": 0
  },
  {
    "Name": "Always cold",
    "Description": "Really wishes the thermostat wasn't set so low. (no effect)",
    "Repeat": 0,
    "d20 stuff": 0,
    "Common": true,
    "Alluring": false,
    "Impulsive": false,
    "Messy": false,
    "Positive, Negative, Neutral": 0
  },
  {
    "Name": "Quick Learner",
    "Description": "Upon hiring, gain {{TASK}}+2 immediately.",
    "Repeat": 0,
    "d20 stuff": 0,
    "Common": false,
    "Alluring": false,
    "Impulsive": false,
    "Messy": false,
    "Positive, Negative, Neutral": 0
  },
  {
    "Name": "Swindler",
    "Description": "Instead of the skills they said they have, they have these skills instead:\nART: *none*\nLOG: *none*\nCHA: {{CHA}}\nPAT: {{PAT}}",
    "Repeat": 0,
    "d20 stuff": 0,
    "Common": false,
    "Alluring": false,
    "Impulsive": false,
    "Messy": false,
    "Positive, Negative, Neutral": 0
  },
  {
    "Name": "Beloathed",
    "Description": "If this employee is *fired*, gain SYN+3 from collective relief.",
    "Repeat": 0,
    "d20 stuff": 0,
    "Common": false,
    "Alluring": false,
    "Impulsive": false,
    "Messy": true,
    "Positive, Negative, Neutral": 0
  },
  {
    "Name": "Remember the 90s?",
    "Description": "Really likes to bring up 90s nostalgia.  (no effect)",
    "Repeat": 0,
    "d20 stuff": 0,
    "Common": false,
    "Alluring": false,
    "Impulsive": false,
    "Messy": false,
    "Positive, Negative, Neutral": 0
  },
  {
    "Name": "Neat Freak",
    "Description": "{{BESIDE}} For _each_ employee *directly beside* this one with {{MESSY}}, lose SYN-1 to annoyance.\n",
    "Repeat": 0,
    "d20 stuff": 0,
    "Common": true,
    "Alluring": false,
    "Impulsive": false,
    "Messy": false,
    "Positive, Negative, Neutral": 0
  },
  {
    "Name": 0,
    "Description": 0,
    "Repeat": 0,
    "d20 stuff": 0,
    "Common": true,
    "Alluring": true,
    "Impulsive": false,
    "Messy": false,
    "Positive, Negative, Neutral": 0
  },
  {
    "Name": 0,
    "Description": 0,
    "Repeat": 0,
    "d20 stuff": 0,
    "Common": true,
    "Alluring": false,
    "Impulsive": true,
    "Messy": false,
    "Positive, Negative, Neutral": 0
  },
  {
    "Name": 0,
    "Description": 0,
    "Repeat": 0,
    "d20 stuff": 0,
    "Common": true,
    "Alluring": false,
    "Impulsive": false,
    "Messy": true,
    "Positive, Negative, Neutral": 0
  },
  {
    "Name": 0,
    "Description": 0,
    "Repeat": 0,
    "d20 stuff": 0,
    "Common": true,
    "Alluring": false,
    "Impulsive": false,
    "Messy": false,
    "Positive, Negative, Neutral": 0
  },
  {
    "Name": 0,
    "Description": 0,
    "Repeat": 0,
    "d20 stuff": 0,
    "Common": true,
    "Alluring": false,
    "Impulsive": false,
    "Messy": false,
    "Positive, Negative, Neutral": 0
  },
  {
    "Name": 0,
    "Description": 0,
    "Repeat": 0,
    "d20 stuff": 0,
    "Common": false,
    "Alluring": true,
    "Impulsive": false,
    "Messy": false,
    "Positive, Negative, Neutral": 0
  },
  {
    "Name": 0,
    "Description": 0,
    "Repeat": 0,
    "d20 stuff": 0,
    "Common": false,
    "Alluring": false,
    "Impulsive": true,
    "Messy": false,
    "Positive, Negative, Neutral": 0
  },
  {
    "Name": 0,
    "Description": 0,
    "Repeat": 0,
    "d20 stuff": 0,
    "Common": false,
    "Alluring": false,
    "Impulsive": true,
    "Messy": false,
    "Positive, Negative, Neutral": 0
  },
  {
    "Name": 0,
    "Description": 0,
    "Repeat": 0,
    "d20 stuff": 0,
    "Common": false,
    "Alluring": false,
    "Impulsive": false,
    "Messy": true,
    "Positive, Negative, Neutral": 0
  },
  {
    "Name": 0,
    "Description": 0,
    "Repeat": 0,
    "d20 stuff": 0,
    "Common": false,
    "Alluring": false,
    "Impulsive": false,
    "Messy": true,
    "Positive, Negative, Neutral": 0
  },
  {
    "Name": 0,
    "Description": 0,
    "Repeat": 0,
    "d20 stuff": 0,
    "Common": false,
    "Alluring": false,
    "Impulsive": false,
    "Messy": false,
    "Positive, Negative, Neutral": 0
  },
  {
    "Name": 0,
    "Description": 0,
    "Repeat": 0,
    "d20 stuff": 0,
    "Common": false,
    "Alluring": false,
    "Impulsive": false,
    "Messy": false,
    "Positive, Negative, Neutral": 0
  },
  {
    "Name": "Rubber Ducky",
    "Description": "{{BESIDE}} Any employees *directly beside* this one gain {{TASK}}+1 from helpful listening.",
    "Repeat": 0,
    "d20 stuff": 0,
    "Common": false,
    "Alluring": false,
    "Impulsive": false,
    "Messy": false,
    "Positive, Negative, Neutral": 0
  },
  {
    "Name": 0,
    "Description": 0,
    "Repeat": 0,
    "d20 stuff": 0,
    "Common": "41%",
    "Alluring": "20%",
    "Impulsive": "20%",
    "Messy": "20%",
    "Positive, Negative, Neutral": 0
  }
];
});
___scope___.file("Entities/Decks/Specialty/SpecialtyDeck.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Specialty_1 = require("./Specialty");
const Deck_1 = require("../Deck");
const SpecialtySheet = require("../../../Registry/sheets/json/Synergy - Specialties.json");
const CommonActions = SpecialtySheet.find((row) => row["id"] === "allPlayers")["Actions"];
exports.CommonActions = CommonActions;
const SpecialtyList = SpecialtySheet
    .filter((row) => ["allPlayers", "examp12le"].includes(row.id) === false)
    .map((row) => new Specialty_1.default({
    id: row["id"],
    name: row["Name"],
    specialActions: row["Actions"],
    overview: row["Overview"],
}));
exports.SpecialtyList = SpecialtyList;
class SpecialtyDeck extends Deck_1.default {
    constructor(parent) {
        super(parent, () => SpecialtyList, Specialty_1.default.getBackNode);
    }
}
exports.default = SpecialtyDeck;
//# sourceMappingURL=SpecialtyDeck.js.map
});
___scope___.file("Entities/Decks/Specialty/Specialty.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const el_tool_1 = require("el-tool");
const Util_1 = require("../../../Util");
const Instructions_1 = require("../../Instructions/Instructions");
const SpecialtyDeck_1 = require("./SpecialtyDeck");
// const CommonActions = "";
require("./Specialty.scss");
const agenda = Instructions_1.RawInstructions.find((inst) => inst.id === "agendaCardText");
const symbols = Instructions_1.RawInstructions.find((inst) => inst.id === "symbolGlossary");
class Specialty {
    constructor(raw) {
        this.raw = raw;
        this.domNode = el_tool_1.div(`Specialty-Card front ${raw.id}`, [
            el_tool_1.div("Specialty-bg", [
                el_tool_1.div("Fade", [
                    el_tool_1.div("Fill none", raw.name),
                    el_tool_1.div("Fill"),
                    el_tool_1.div("Fill"),
                    el_tool_1.div("Fill"),
                ]),
            ]),
            el_tool_1.div("Specialty-fg", [
                el_tool_1.div("Content", [
                    el_tool_1.div("Name", raw.name),
                    el_tool_1.div("Overview", [
                        el_tool_1.div("InlineTitle", "OVERVIEW:"),
                        el_tool_1.div("Blurb", raw.overview),
                    ]),
                    el_tool_1.div("FounderSpace", [
                        el_tool_1.div("PlaceHere", "Place Founder Character Card Here"),
                    ]),
                    el_tool_1.div("Actions", [
                        el_tool_1.div("ActionList special", [
                            el_tool_1.div("Title", "SPECIAL ACTIONS"),
                            el_tool_1.div("List", Specialty.parseActions(raw.specialActions)),
                        ]),
                        el_tool_1.div("ActionList common", [
                            el_tool_1.div("Title", "COMMON ACTIONS"),
                            Specialty.genCommonActions(),
                        ]),
                    ]),
                    el_tool_1.div("Agenda", [
                        el_tool_1.div("Title", "QUARTERLY AGENDA"),
                        Util_1.parseRawText(agenda.blurb),
                    ]),
                    el_tool_1.div("CheatSheet", [
                        el_tool_1.div("Symbols", [
                            el_tool_1.div("Title", symbols.title.toUpperCase()),
                            el_tool_1.div("List", Util_1.parseRawText(symbols.blurb.replace(/\n/g, "<br>"))),
                        ]),
                        el_tool_1.div("WorkerVenn", [
                        // div("Founder", "Founder"),
                        // div("Worker", "Worker"),
                        // div("Employee", "Employee"),
                        ]),
                    ]),
                ]),
            ]),
        ]);
    }
    generateMiniView() {
        const raw = this.raw;
        return el_tool_1.div(`Specialty-Card front mini_view ${raw.id}`, [
            el_tool_1.div("Specialty-bg", [
                el_tool_1.div("Fade", [
                    el_tool_1.div("Fill none", raw.name),
                    el_tool_1.div("Fill"),
                    el_tool_1.div("Fill"),
                    el_tool_1.div("Fill"),
                ]),
            ]),
            el_tool_1.div("Specialty-fg", [
                el_tool_1.div("IsMini", Util_1.fromMD("(Cut from full placard)")),
                el_tool_1.div("Content", [
                    el_tool_1.div("Name", raw.name),
                    el_tool_1.div("Overview", [
                        el_tool_1.div("Title", "OVERVIEW:"),
                        el_tool_1.div("Blurb", raw.overview),
                    ]),
                    el_tool_1.div("Actions", [
                        el_tool_1.div("ActionList special", [
                            el_tool_1.div("Title", "SPECIAL ACTIONS"),
                            el_tool_1.div("List", Specialty.parseActions(raw.specialActions)),
                        ]),
                    ]),
                ]),
            ]),
        ]);
    }
    static parseActions(acts) {
        return acts.split(/\n/g).map((act) => el_tool_1.div("Action", Util_1.fromMD(Util_1.injectSymbols(act))));
    }
    static genCommonActions() {
        return el_tool_1.div("List common_actions", Specialty.parseActions(SpecialtyDeck_1.CommonActions));
    }
    static getBackNode() {
        return el_tool_1.div("Specialty-Card back", "Specialty CARD");
    }
}
exports.default = Specialty;
//# sourceMappingURL=Specialty.js.map
});
___scope___.file("Entities/Instructions/Instructions.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const el_tool_1 = require("el-tool");
const Util_1 = require("../../Util");
const SpecialtyDeck_1 = require("../../Entities/Decks/Specialty/SpecialtyDeck");
const AgendaCard_1 = require("./AgendaCard");
const Employee_1 = require("../../Entities/Decks/Employee");
const OptionCardDeck_1 = require("../../Entities/Decks/Option/OptionCardDeck");
const ProjectDeck_1 = require("../../Entities/Decks/Project/ProjectDeck");
const MissionDeck_1 = require("../../Entities/Decks/Mission/MissionDeck");
const QuirkDeck_1 = require("../../Entities/Decks/Quirk/QuirkDeck");
const EventCardDeck_1 = require("../../Entities/Decks/Event/EventCardDeck");
const Board_1 = require("../../UI/Board/Board");
class SimpleComponent {
    get domNode() {
        if (this._domNode === undefined) {
            this.render();
            if (this._domNode === undefined) {
                throw new Error("Component._domNode must not be undefined after render()");
            }
        }
        return this._domNode;
    }
}
const EventCardSheet = require("~/Registry/sheets/json/Synergy - Instructions.json");
require("./instructions.scss");
const RawInstructions = EventCardSheet.map((card) => {
    return {
        id: card["id"] + "",
        title: card["Title"] + "",
        blurb: card["Blurb"] + "",
        isGlossary: card["Glossary Item"],
        majorItem: card["Major Item"],
    };
});
exports.RawInstructions = RawInstructions;
class Instructions extends SimpleComponent {
    render() {
        this._domNode = el_tool_1.div("Instructions-Book", [
            this.createInstructionFromId("firstThing"),
            this.createInstructionFromId("overview"),
            this.createInstructionFromId("firstTimePlaying"),
            this.createInstructionFromId("gettingStarted"),
            this.createInstructionFromId("iconGlossary"),
            this.createInstructionFromId("keytermGlossary"),
            ...RawInstructions
                .filter((item) => item.isGlossary)
                .sort((a, b) => a.title > b.title)
                .map((inst) => el_tool_1.addClass(this.createItem(inst), "glossaryItem")),
            this.createInstructionFromId("examplePlay"),
        ]);
    }
    createItem(inst) {
        // const isShort = inst.blurb.length < 250;
        const smallItem = inst.isGlossary && !inst.majorItem;
        return el_tool_1.div(`Instruction ${smallItem ? "isShort" : ""}`, [
            el_tool_1.div("Title", { hackableHTML: Util_1.injectSymbols(inst.title) + (smallItem ? " - " : "") }),
            el_tool_1.div("Blurb", Util_1.fromMD(Util_1.injectSymbols(inst.blurb.replace(/\n/g, "\n\n")))),
            this.getSpecialContent(inst),
        ]);
    }
    createInstructionFromId(id) {
        const inst = RawInstructions.find((item) => item.id === id);
        return el_tool_1.addClass(this.createItem(inst), id);
    }
    getSpecialContent(inst) {
        if (inst.id === "specialtyCards") {
            return el_tool_1.div("SpecialtyCards", SpecialtyDeck_1.SpecialtyList
                // .filter((card) => card.raw.id !== "example")
                .sort((a, b) => !a.raw.id.startsWith("example") && a.raw.name > b.raw.name)
                .map((spec) => {
                const desc = RawInstructions.find((it) => it.id === (spec.raw.id + "Description"));
                let card;
                let keys;
                if (spec.raw.id === "exampleSpecialty") {
                    card = el_tool_1.div("CropView", spec.domNode);
                    keys = ["founderSpace", "overview", "specialActions", "commonActions", "agenda", "cheatsheet"];
                }
                else {
                    card = spec.generateMiniView();
                }
                return this.createCardDesc(inst.id, card, desc ? desc.blurb.replace(/\n/g, "\n\n") : "N/A", keys);
            }));
        }
        else if (inst.id === "agendaCard") {
            const desc = RawInstructions.find((inst) => inst.id === "agendaCardDescription");
            return this.createCardDesc(inst.id, Instructions.agendaCard.domNode, desc.blurb);
        }
        else if (inst.id === "characterCard") {
            const desc = RawInstructions.find((inst) => inst.id === "characterCardDescription") || { blurb: "N/A" };
            const card = new Employee_1.Employee(null, Employee_1.RawEmployeeList.find((raw) => raw.name === "Examplo"));
            return this.createCardDesc(inst.id, card.domNode, desc.blurb, ["image", "pay", "quirk", "skills"]);
        }
        else if (inst.id === "commonActions") {
            const desc = RawInstructions.find((inst) => inst.id === "commonActionsDescription") || { blurb: "N/A" };
            const card = SpecialtyDeck_1.SpecialtyList.find((card) => card.raw.id === "exampleSpecialty");
            // const Specialty = require("../../Entities/Decks/Specialty/Specialty");
            // console.log(Specialty);
            // const card = Specialty.default.genCommonActions();
            return this.createCardDesc(inst.id, el_tool_1.div("CropView", card.domNode.cloneNode(true)), desc.blurb);
            // option card
        }
        else if (inst.id === "optionCard") {
            const desc = RawInstructions.find((inst) => inst.id === "optionCardDescription") || { blurb: "N/A" };
            const card = OptionCardDeck_1.OptionCardList.find((card) => card.raw.name === "Change Snacks");
            return this.createCardDesc(inst.id, card.domNode, desc.blurb, ["description", "actions", "repeatable"]);
            // project card
        }
        else if (inst.id === "projectCard") {
            const desc = RawInstructions.find((inst) => inst.id === "projectCardDescription") || { blurb: "N/A" };
            const card = ProjectDeck_1.ProjectList.find((card) => card.raw.name === "Ad Campaign");
            return this.createCardDesc(inst.id, card.domNode, desc.blurb, ["payout", "job", "tasks", "skills", "stageSplit"]);
            // mission card
        }
        else if (inst.id === "missionCard") {
            const desc = RawInstructions.find((inst) => inst.id === "missionCardDescription") || { blurb: "N/A" };
            const card = MissionDeck_1.MissionList.find((card) => card.raw.title === "The Churn Factory");
            return this.createCardDesc(inst.id, card.domNode, desc.blurb, ["specs", "goal", "endpointType"]);
            // quirk card
        }
        else if (inst.id === "quirkCard") {
            const desc = RawInstructions.find((inst) => inst.id === "quirkCardDescription") || { blurb: "N/A" };
            const card = QuirkDeck_1.QuirkCardList.find((card) => card.raw.name === "The Stain");
            return this.createCardDesc(inst.id, card.domNode, desc.blurb, ["specialEffects", "qualities"]);
            // event card
        }
        else if (inst.id === "eventCard") {
            const desc = RawInstructions.find((inst) => inst.id === "eventCardDescription") || { blurb: "N/A" };
            const card = EventCardDeck_1.EventCardList.find((card) => card.raw.name.startsWith("Donuts"));
            return this.createCardDesc(inst.id, card.domNode, desc.blurb, ["description", "actions"]);
            // half-count
        }
        else if (inst.id === "halfCount") {
            const fullCount = el_tool_1.create("tr", el_tool_1.create("td", "Full-Count"));
            const halfCount = el_tool_1.create("tr", el_tool_1.create("td", "Half-Count"));
            const quarterCount = el_tool_1.create("tr", el_tool_1.create("td", "Quarter-Count"));
            for (let i = 1; i <= 20; i++) {
                el_tool_1.create("td", { appendTo: fullCount }, i);
                el_tool_1.create("td", { appendTo: halfCount }, Math.floor(i / 2) + "");
                el_tool_1.create("td", { appendTo: quarterCount }, Math.floor(i / 4) + "");
            }
            return el_tool_1.create("table", { class: inst.id }, [
                fullCount,
                halfCount,
                quarterCount
            ]);
            // boardOverview
        }
        else if (inst.id === "boardOverview") {
            const card = new Board_1.default();
            const desc = RawInstructions.find((inst) => inst.id === "boardOverviewDescription") || { blurb: "N/A" };
            return this.createCardDesc(inst.id, el_tool_1.div("CropView", card.domNode), desc.blurb, [
                "synergyTrack", "officeSpace", "frontDoor", "missionCard", "projectCards", "cleaningService", "quarterTrack"
            ]);
        }
        return null;
    }
    createCardDesc(id, card, desc, keyMarkers = []) {
        return el_tool_1.div(`CardAndDesc ${id}`, [
            el_tool_1.div("Card", [
                card,
                el_tool_1.div("KeyMarkers", keyMarkers.map((keyMark, index) => el_tool_1.div(`KeyMarker ${keyMark}`, { style: {
                        transform: `rotate(${-2 + Math.random() * 4}deg)`,
                    } }, "ABCDEFGHI".charAt(index)))),
            ]),
            el_tool_1.div("Desc", Util_1.fromMD(Util_1.injectSymbols(desc))),
        ]);
    }
}
Instructions.agendaCard = new AgendaCard_1.AgendaCard(RawInstructions.find((inst) => inst.id === "agendaCardText"));
exports.default = Instructions;
//# sourceMappingURL=Instructions.js.map
});
___scope___.file("Entities/Instructions/AgendaCard/index.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AgendaCard_1 = require("./AgendaCard");
exports.AgendaCard = AgendaCard_1.default;
//# sourceMappingURL=index.js.map
});
___scope___.file("Entities/Instructions/AgendaCard/AgendaCard.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const el_tool_1 = require("el-tool");
const Util_1 = require("../../../Util");
require("./agendaCard.scss");
class AgendaCard {
    constructor(raw) {
        this.domNode = el_tool_1.div("Agenda-Card front", [
            el_tool_1.div("Title", "This Quarters Agenda"),
            el_tool_1.div("Blurb", Util_1.fromMD(Util_1.injectSymbols(raw.blurb))),
        ]);
    }
    static getBackNode() {
        return el_tool_1.div("Agenda-Card back", "AGENDA CARD");
    }
}
exports.default = AgendaCard;
//# sourceMappingURL=AgendaCard.js.map
});
___scope___.file("Entities/Instructions/AgendaCard/agendaCard.scss", function(exports, require, module, __filename, __dirname){


require("fuse-box-css")("default/Entities/Instructions/AgendaCard/agendaCard.scss", ".Agenda-Card {\n  height: 3.5in;\n  width: 2.5in;\n  border-radius: 0.1in;\n  overflow: hidden;\n  display: inline-grid; }\n  .Agenda-Card.front {\n    padding: .125in 0in;\n    font-size: 12px;\n    background: #FAFAFA;\n    grid-template-rows: auto 1fr; }\n    .Agenda-Card.front > .Title {\n      border-bottom: 1px solid black;\n      text-align: center;\n      margin-bottom: 2px; }\n    .Agenda-Card.front > .Blurb {\n      padding: 0in .125in;\n      padding-top: 2px; }\n      .Agenda-Card.front > .Blurb li {\n        margin: 0px; }\n      .Agenda-Card.front > .Blurb ol {\n        padding-left: 10px; }\n        .Agenda-Card.front > .Blurb ol ul {\n          padding-left: 2px; }\n  .Agenda-Card.back {\n    background-color: black; }\n\n.PrintSheet.agenda > .Agenda-Card {\n  margin: .125in; }\n\n/*# sourceMappingURL=/css-sourcemaps/03b66e235.map */")
});
___scope___.file("Entities/Decks/Mission/MissionDeck.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Mission_1 = require("./Mission");
const Deck_1 = require("../Deck");
const MissionSheet = require("../../../Registry/sheets/json/Synergy - Missions.json");
// const commonActions = MissionSheet.find((row) => row["id"] === "allPlayers")["Actions"];
// console.log(commonActions);
const MissionList = MissionSheet
    // .filter((row) => ["allPlayers", "examp12le"].includes(row.id) === false)
    .map((row) => new Mission_1.default({
    title: row["Title"],
    goal: row["Goal"],
    quarters: row["Quarters"],
    difficulty: row["Difficulty"],
    funds: row["Funds"],
    deadline: row["Deadline"],
}));
exports.MissionList = MissionList;
class MissionDeck extends Deck_1.default {
    constructor(parent) {
        super(parent, () => MissionList, Mission_1.default.getBackNode);
    }
}
exports.default = MissionDeck;
//# sourceMappingURL=MissionDeck.js.map
});
___scope___.file("Entities/Decks/Mission/Mission.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const el_tool_1 = require("el-tool");
const Util_1 = require("../../../Util");
require("./mission.scss");
class Mission {
    constructor(raw) {
        this.raw = raw;
        this.domNode = el_tool_1.div(`Mission-Card front`, [
            el_tool_1.div("Title", raw.title),
            el_tool_1.div("Info", [
                el_tool_1.div("Overview", [
                    el_tool_1.div("Row difficulty", [
                        el_tool_1.div("Name", "Difficulty: "),
                        el_tool_1.div("Value", [
                            "Beginner",
                            "Easy",
                            "Intermediate",
                            "Expert",
                        ][raw.difficulty]),
                    ]),
                    el_tool_1.div("Row length", [
                        el_tool_1.div("Name", "Length:"),
                        el_tool_1.div("Value", raw.quarters ? `~${raw.quarters * 8}min` : `N/A`),
                    ]),
                    el_tool_1.div("Row lengthBeginner", [
                        el_tool_1.div("Name", "Length (beginner):"),
                        el_tool_1.div("Value", raw.quarters ? `~${raw.quarters * 12}min` : `N/A`),
                    ]),
                    el_tool_1.div("Row funds", [
                        el_tool_1.div("Name", "Funds:"),
                        el_tool_1.div("Value", `${raw.funds}k`),
                    ]),
                    el_tool_1.div("Row ending", [
                        el_tool_1.div("Name", raw.deadline ? "Deadline:" : "Target Quarter:"),
                        el_tool_1.div("Value", raw.quarters ? `${raw.quarters}Q` : `N/A`),
                    ]),
                ]),
                el_tool_1.div("Goal", Util_1.fromMD(Util_1.injectSymbols(raw.goal))),
            ]),
        ]);
    }
    static getBackNode() {
        return el_tool_1.div("Mission-Card back", "Mission CARD");
    }
}
exports.default = Mission;
//# sourceMappingURL=Mission.js.map
});
___scope___.file("Entities/Decks/Mission/mission.scss", function(exports, require, module, __filename, __dirname){


require("fuse-box-css")("default/Entities/Decks/Mission/mission.scss", ".Mission-Card {\n  height: 3.5in;\n  width: 2.5in;\n  border-radius: 0.1in;\n  border: 0.1in solid #4a4a4a;\n  overflow: hidden;\n  display: inline-grid; }\n  .Mission-Card.front {\n    grid-template-rows: auto 1fr;\n    background-color: #4a4a4a; }\n    .Mission-Card.front > .Title {\n      color: #ffdd00;\n      text-align: center;\n      padding-bottom: 0.0625in; }\n    .Mission-Card.front > .Info {\n      background-color: white;\n      border-radius: .25in;\n      font-size: 14px; }\n      .Mission-Card.front > .Info > .Overview {\n        margin: 0.125in;\n        display: grid;\n        grid-template-columns: auto 1fr; }\n        .Mission-Card.front > .Info > .Overview > .Row {\n          display: contents; }\n          .Mission-Card.front > .Info > .Overview > .Row > .Name {\n            font-weight: bold;\n            margin-right: 1em; }\n      .Mission-Card.front > .Info > .Goal {\n        padding: 0.0625in; }\n  .Mission-Card.back {\n    background-color: black; }\n\n/*# sourceMappingURL=/css-sourcemaps/55050ea8.map */")
});
___scope___.file("Registry/sheets/json/Synergy - Missions.json", function(exports, require, module, __filename, __dirname){

module.exports = [
  {
    "Title": "The Churn Factory",
    "Goal": "**Fallout** (-10 SYN) is no longer a *lose condition*, and *employees* with 3{{PAT}} never *quit*.\n(33) 3P",
    "Quarters": 16,
    "Difficulty": 0,
    "Funds": 100,
    "Deadline": true
  },
  {
    "Title": "Agency",
    "Goal": "Starting with 2 random EMEEs, complete 20 non-anchor projects ",
    "Quarters": 12,
    "Difficulty": 2,
    "Funds": 100,
    "Deadline": true
  },
  {
    "Title": "Year 3 Profit",
    "Goal": "Have 200k at end of year 3",
    "Quarters": 12,
    "Difficulty": 1,
    "Funds": 100,
    "Deadline": false
  },
  {
    "Title": "Shoestring Budget",
    "Goal": "Starting at 20k, make it to year 3",
    "Quarters": 12,
    "Difficulty": 2,
    "Funds": 20,
    "Deadline": false
  },
  {
    "Title": "Small but Feasable",
    "Goal": "Starting at 80k, hire at least 6 EMEE without going bankrupt by the end of year 3",
    "Quarters": 12,
    "Difficulty": 0,
    "Funds": 80,
    "Deadline": false
  },
  {
    "Title": "Tech Giants",
    "Goal": "(33)\n[A Couple Nerds](6) 3L\n[Some Nice Visuals](6) 2A\n[Tons of Problem Solving](12) 2L\n[Selling it](9) 1C",
    "Quarters": 16,
    "Difficulty": 2,
    "Funds": 100,
    "Deadline": true
  },
  {
    "Title": "Millionaires",
    "Goal": "Make a million dollars without a time limit.  WARNING: can make for a very long game.",
    "Quarters": 0,
    "Difficulty": 1,
    "Funds": 100,
    "Deadline": true
  },
  {
    "Title": "A Quick Mil",
    "Goal": "Make a million dollars by the end of year 5",
    "Quarters": 20,
    "Difficulty": 3,
    "Funds": 100,
    "Deadline": true
  },
  {
    "Title": "The Texan Way",
    "Goal": "Get as big as possible.  Hire 15 EMEEs.",
    "Quarters": 12,
    "Difficulty": 2,
    "Funds": 150,
    "Deadline": true
  },
  {
    "Title": "Clean Up Crew",
    "Goal": "Choose fifteen (15) *employees* at random and place them in the office.  Draw *quirk cards* for each (ignoring the effects).  This is the company you must try to rescue.\nGoal: have 200k at the end of year 3.",
    "Quarters": 12,
    "Difficulty": 3,
    "Funds": 100,
    "Deadline": false
  },
  {
    "Title": "I Just Want to Dance!",
    "Goal": "Have 25SYN at end of year 3.",
    "Quarters": 12,
    "Difficulty": 0,
    "Funds": 100,
    "Deadline": false
  }
];
});
___scope___.file("UI/Board/Board.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const el_tool_1 = require("el-tool");
const omject_1 = require("omject");
require("./board.scss");
function toRGB(hsv) {
    const h = ((hsv.h % 1) + 1) % 1;
    const s = Math.max(0, Math.min(hsv.s, 1));
    const v = Math.max(0, Math.min(hsv.v, 1));
    const i = Math.floor(h * 6);
    const f = h * 6 - i;
    const p = v * (1 - s);
    const q = v * (1 - f * s);
    const t = v * (1 - (1 - f) * s);
    let R;
    let G;
    let B;
    switch (i % 6) {
        case 0:
            R = v;
            G = t;
            B = p;
            break;
        case 1:
            R = q;
            G = v;
            B = p;
            break;
        case 2:
            R = p;
            G = v;
            B = t;
            break;
        case 3:
            R = p;
            G = q;
            B = v;
            break;
        case 4:
            R = t;
            G = p;
            B = v;
            break;
        case 5:
            R = v;
            G = p;
            B = q;
            break;
    }
    return { r: Math.floor(R * 255), g: Math.floor(G * 255), b: Math.floor(B * 255) };
}
class Board extends omject_1.BasicOmjectClass {
    constructor(parent) {
        super(parent);
        this.game = this.getAncestor("Synergy");
        this.domNode = el_tool_1.div("Board", [
            el_tool_1.div("SynergyTrack", (new Array(36)).fill(null).map((_, index) => {
                const hsv = {
                    h: (index / 36) * .48,
                    s: 0.75,
                    v: 1 - ((index / 36) * .2),
                };
                const borderColor = toRGB(hsv);
                hsv.s -= 0.5;
                const fillColor = toRGB(hsv);
                return el_tool_1.div(`TrackStep ${index % 5 === 0 ? "big" : "small"}`, [
                    el_tool_1.div("Background", { style: {
                            background: `rgba(${fillColor.r}, ${fillColor.g}, ${fillColor.b}, 0.25)`,
                            borderColor: `rgba(${borderColor.r}, ${borderColor.g}, ${borderColor.b}, 1)`,
                        } }),
                    el_tool_1.div("Value", {
                    // style: {color: `rgba(${fillColor.r}, ${fillColor.g}, ${fillColor.b}, 1)`},
                    }, (index - 10) + ""),
                ]);
            })),
            el_tool_1.div("MissionControl", [
                el_tool_1.div("MissionCardSpace", "Mission Card"),
                el_tool_1.div("ProjectCardSpace", el_tool_1.div("Text", "Project Card")),
                el_tool_1.div("ProjectCardSpace", el_tool_1.div("Text", "Project Card")),
                el_tool_1.div("ProjectCardSpace", el_tool_1.div("Text", "Project Card")),
                el_tool_1.div("CleaningAndQuarters", [
                    el_tool_1.div("Cleaning", "Cleaning Service"),
                    el_tool_1.div("Quarters", "Quarter Calander"),
                ]),
                el_tool_1.div("ProjectCardSpace", el_tool_1.div("Text", "Project Card")),
                el_tool_1.div("ProjectCardSpace", el_tool_1.div("Text", "Project Card")),
                el_tool_1.div("ProjectCardSpace", el_tool_1.div("Text", "Project Card")),
            ]),
            el_tool_1.div("Building", [
                el_tool_1.div("Shadow"),
                this.officeSpaceEl = el_tool_1.div("OfficeSpace", {
                    on: {
                        dragover: (event) => {
                            event.preventDefault();
                            event.dataTransfer.dropEffect = 'move';
                            const dragID = event.dataTransfer.getData("text/plain");
                            if (dragID === "employeeCard") {
                                const employeeCard = this.game.dragDepo.employeeCard;
                                if (employeeCard.isOnBoard()) {
                                    const chunkSize = this.officeSpaceEl.offsetWidth / (7 * 6);
                                    console.log(chunkSize, chunkSize * 7 * 6);
                                    const bounds = this.officeSpaceEl.getBoundingClientRect();
                                    let x = event.clientX - bounds.left;
                                    x -= x % chunkSize;
                                    let y = event.clientY - bounds.top;
                                    y -= y % chunkSize;
                                    employeeCard.domNode.style.top = y + "px";
                                    employeeCard.domNode.style.left = x + "px";
                                }
                            }
                        },
                        drop: (event) => {
                            event.preventDefault();
                            const dragID = event.dataTransfer.getData("text/plain");
                            if (dragID === "employeeCard") {
                                const employeeCard = this.game.dragDepo.employeeCard;
                                employeeCard.moveToBoard();
                            }
                        },
                    },
                }),
                el_tool_1.div("Walls", [
                    el_tool_1.div("Brick top"),
                    el_tool_1.div("Brick bottom"),
                    el_tool_1.div("Brick left"),
                    el_tool_1.div("Brick right"),
                    el_tool_1.div("Window top_left width_3"),
                    el_tool_1.div("Window top_center width_5"),
                    el_tool_1.div("Window top_right width_3"),
                    //
                    el_tool_1.div("Window left width_7"),
                    //
                    el_tool_1.div("Window right_1 width_1"),
                    el_tool_1.div("Window right_2 width_1"),
                    el_tool_1.div("Window right_3 width_1"),
                    el_tool_1.div("Window right_bottom width_5"),
                    //
                    el_tool_1.div("Window bottom_left width_3"),
                    el_tool_1.div("Window bottom_center width_3"),
                    el_tool_1.div("Window bottom_right width_5"),
                ]),
            ]),
        ]);
    }
}
exports.default = Board;
//# sourceMappingURL=Board.js.map
});
___scope___.file("UI/Board/board.scss", function(exports, require, module, __filename, __dirname){


require("fuse-box-css")("default/UI/Board/board.scss", ".Board {\n  background: #4a7146;\n  background-image: url(\"/css-resources/assets/Employees/Backgrounds/Material/Floor/Grass.svg\");\n  flex-shrink: 0;\n  overflow: hidden; }\n  .Board > .SynergyTrack {\n    height: 1in;\n    display: flex;\n    align-items: center;\n    justify-content: center; }\n    .Board > .SynergyTrack > .TrackStep {\n      transform: rotate(45deg); }\n      .Board > .SynergyTrack > .TrackStep.small {\n        height: 10mm;\n        width: 10mm;\n        margin: 0px 8px; }\n      .Board > .SynergyTrack > .TrackStep.big {\n        height: 15mm;\n        width: 15mm;\n        margin: 0px 15px; }\n      .Board > .SynergyTrack > .TrackStep > .Background {\n        position: relative;\n        z-index: 1;\n        height: 100%;\n        width: 100%;\n        background: white;\n        opacity: 0.75;\n        border: 2px solid white;\n        border-radius: 8px;\n        border-radius: 12px; }\n      .Board > .SynergyTrack > .TrackStep > .Value {\n        position: absolute;\n        top: 50%;\n        left: 50%;\n        color: white;\n        opacity: 0.65;\n        font-size: 12px;\n        transform: translate(-50%, -50%) rotate(-45deg); }\n  .Board > .Building {\n    margin: 0in .25in .25in 1in;\n    padding: 0.25in;\n    position: relative; }\n    .Board > .Building > .Shadow {\n      position: absolute;\n      pointer-events: none;\n      top: 0px;\n      left: 0px;\n      height: 200%;\n      width: 200%;\n      background-image: url(\"/css-resources/assets/Employees/Backgrounds/Shadows/Box.svg\");\n      background-size: 100% 100%;\n      opacity: 0.1; }\n    .Board > .Building > .Walls {\n      pointer-events: none;\n      position: absolute;\n      top: 0px;\n      left: 0px;\n      right: 0px;\n      bottom: 0px;\n      overflow: hidden;\n      border-radius: 4px; }\n      .Board > .Building > .Walls > .Brick {\n        position: absolute;\n        background-size: 54.9px 54px; }\n        .Board > .Building > .Walls > .Brick.top, .Board > .Building > .Walls > .Brick.bottom {\n          height: .25in;\n          width: 100%;\n          background-size: 55.05px 54px;\n          background-image: url(\"/css-resources/assets/Employees/Backgrounds/Material/Brick.png\"); }\n        .Board > .Building > .Walls > .Brick.top {\n          top: 0px; }\n        .Board > .Building > .Walls > .Brick.bottom {\n          bottom: 0px;\n          background-position: 27px 0px; }\n        .Board > .Building > .Walls > .Brick.left, .Board > .Building > .Walls > .Brick.right {\n          height: calc(100% - .25in);\n          width: .25in;\n          overflow: hidden; }\n          .Board > .Building > .Walls > .Brick.left::after, .Board > .Building > .Walls > .Brick.right::after {\n            position: absolute;\n            content: \"\";\n            display: block;\n            background-size: inherit;\n            background-image: url(\"/css-resources/assets/Employees/Backgrounds/Material/Brick.png\");\n            height: 5000px;\n            width: 5000px;\n            transform: rotate(-90deg); }\n        .Board > .Building > .Walls > .Brick.left {\n          top: .25in;\n          left: 0px; }\n          .Board > .Building > .Walls > .Brick.left::after {\n            background-position: -10px 0px; }\n        .Board > .Building > .Walls > .Brick.right {\n          top: 0in;\n          right: 0px; }\n          .Board > .Building > .Walls > .Brick.right::after {\n            background-position: -6px 0px; }\n      .Board > .Building > .Walls > .Window {\n        position: absolute;\n        background-image: url(\"/css-resources/assets/Employees/Backgrounds/Material/Glass.png\");\n        background-color: #b7f7ffcc;\n        background-position: 0px -3px;\n        height: .25in; }\n        .Board > .Building > .Walls > .Window::after {\n          position: absolute;\n          content: \"\";\n          display: block;\n          background-image: url(\"/css-resources/assets/Employees/Backgrounds/Shadows/Window-Light.svg\");\n          background-size: 100% 100%;\n          width: 200%;\n          left: -50%;\n          opacity: 0.3; }\n        .Board > .Building > .Walls > .Window.width_1 {\n          background-size: 117px 30px;\n          width: 117px; }\n          .Board > .Building > .Walls > .Window.width_1::after {\n            height: 100px; }\n        .Board > .Building > .Walls > .Window.width_3 {\n          background-size: 112px 30px;\n          width: 336px; }\n          .Board > .Building > .Walls > .Window.width_3::after {\n            height: 300px; }\n        .Board > .Building > .Walls > .Window.width_5 {\n          background-size: 111px 30px;\n          width: 555px; }\n          .Board > .Building > .Walls > .Window.width_5::after {\n            height: 500px; }\n        .Board > .Building > .Walls > .Window.width_7 {\n          background-size: 111px 30px;\n          width: 777px; }\n          .Board > .Building > .Walls > .Window.width_7::after {\n            height: 700px; }\n        .Board > .Building > .Walls > .Window.top_left.top_left, .Board > .Building > .Walls > .Window.top_center.top_left, .Board > .Building > .Walls > .Window.top_right.top_left {\n          left: 160px; }\n        .Board > .Building > .Walls > .Window.top_left.top_center, .Board > .Building > .Walls > .Window.top_center.top_center, .Board > .Building > .Walls > .Window.top_right.top_center {\n          left: 820px; }\n        .Board > .Building > .Walls > .Window.top_left.top_right, .Board > .Building > .Walls > .Window.top_center.top_right, .Board > .Building > .Walls > .Window.top_right.top_right {\n          right: 132px; }\n        .Board > .Building > .Walls > .Window.top_left::after, .Board > .Building > .Walls > .Window.top_center::after, .Board > .Building > .Walls > .Window.top_right::after {\n          top: .25in; }\n        .Board > .Building > .Walls > .Window.bottom_left, .Board > .Building > .Walls > .Window.bottom_center, .Board > .Building > .Walls > .Window.bottom_right {\n          bottom: 0px; }\n          .Board > .Building > .Walls > .Window.bottom_left.bottom_left, .Board > .Building > .Walls > .Window.bottom_center.bottom_left, .Board > .Building > .Walls > .Window.bottom_right.bottom_left {\n            left: 132px; }\n          .Board > .Building > .Walls > .Window.bottom_left.bottom_center, .Board > .Building > .Walls > .Window.bottom_center.bottom_center, .Board > .Building > .Walls > .Window.bottom_right.bottom_center {\n            left: 791px; }\n          .Board > .Building > .Walls > .Window.bottom_left.bottom_right, .Board > .Building > .Walls > .Window.bottom_center.bottom_right, .Board > .Building > .Walls > .Window.bottom_right.bottom_right {\n            right: 160px; }\n          .Board > .Building > .Walls > .Window.bottom_left::after, .Board > .Building > .Walls > .Window.bottom_center::after, .Board > .Building > .Walls > .Window.bottom_right::after {\n            bottom: .25in;\n            transform: rotate(180deg); }\n        .Board > .Building > .Walls > .Window.left {\n          transform: rotate(90deg) scale(1, -1);\n          left: -377px;\n          top: 618px;\n          height: 25px; }\n          .Board > .Building > .Walls > .Window.left::after {\n            top: .25in; }\n        .Board > .Building > .Walls > .Window.right_1, .Board > .Building > .Walls > .Window.right_2, .Board > .Building > .Walls > .Window.right_3, .Board > .Building > .Walls > .Window.right_bottom {\n          transform: rotate(90deg);\n          right: -47px;\n          height: 25px; }\n          .Board > .Building > .Walls > .Window.right_1::after, .Board > .Building > .Walls > .Window.right_2::after, .Board > .Building > .Walls > .Window.right_3::after, .Board > .Building > .Walls > .Window.right_bottom::after {\n            top: .25in; }\n          .Board > .Building > .Walls > .Window.right_1.right_1, .Board > .Building > .Walls > .Window.right_2.right_1, .Board > .Building > .Walls > .Window.right_3.right_1, .Board > .Building > .Walls > .Window.right_bottom.right_1 {\n            top: 204px; }\n          .Board > .Building > .Walls > .Window.right_1.right_2, .Board > .Building > .Walls > .Window.right_2.right_2, .Board > .Building > .Walls > .Window.right_3.right_2, .Board > .Building > .Walls > .Window.right_bottom.right_2 {\n            top: 423px; }\n          .Board > .Building > .Walls > .Window.right_1.right_3, .Board > .Building > .Walls > .Window.right_2.right_3, .Board > .Building > .Walls > .Window.right_3.right_3, .Board > .Building > .Walls > .Window.right_bottom.right_3 {\n            top: 643px; }\n          .Board > .Building > .Walls > .Window.right_1.right_bottom, .Board > .Building > .Walls > .Window.right_2.right_bottom, .Board > .Building > .Walls > .Window.right_3.right_bottom, .Board > .Building > .Walls > .Window.right_bottom.right_bottom {\n            bottom: 452px;\n            right: -266px; }\n    .Board > .Building > .OfficeSpace {\n      position: relative;\n      height: 14.7in;\n      width: 21.525in;\n      background-color: #b6b6b6;\n      background-size: 0.525in 0.525in;\n      background-image: linear-gradient(to right, #989898 1px, transparent 1px), linear-gradient(to bottom, #989898 1px, transparent 1px); }\n      .Board > .Building > .OfficeSpace::after {\n        position: absolute;\n        content: \"\";\n        display: block;\n        top: 0px;\n        left: 0px;\n        right: 0px;\n        bottom: 0px;\n        background-image: url(\"/css-resources/assets/Employees/Backgrounds/Material/ConcreteTexturing-b&w.png\");\n        opacity: 0.2; }\n      .Board > .Building > .OfficeSpace > .Employee-Card {\n        position: absolute;\n        cursor: move;\n        transform: translate(7px, 6px);\n        z-index: 2; }\n  .Board > .MissionControl {\n    position: absolute;\n    bottom: 0px;\n    right: 0px;\n    height: 7.85in;\n    width: 11in;\n    background: rgba(255, 255, 255, 0.99);\n    z-index: 10;\n    display: grid;\n    grid-template-rows: 50% 50%;\n    grid-template-columns: repeat(4, 25%); }\n    .Board > .MissionControl > * {\n      border: 1px solid rgba(0, 0, 0, 0.25); }\n    .Board > .MissionControl > .ProjectCardSpace > .Text {\n      transform: rotate(-45deg); }\n    .Board > .MissionControl > .CleaningAndQuarters > * {\n      height: 50%;\n      border: inherit; }\n\nbody > .Board {\n  margin: 0px;\n  position: absolute; }\n\n/*# sourceMappingURL=/css-sourcemaps/0331de044.map */")
});
___scope___.file("Registry/sheets/json/Synergy - Instructions.json", function(exports, require, module, __filename, __dirname){

module.exports = [
  {
    "id": "accountant",
    "Title": "Accountant",
    "Blurb": "See *Specialty Placards*",
    "Glossary Item": true,
    "Major Item": 0
  },
  {
    "id": "accountantDescription",
    "Title": 0,
    "Blurb": "_Accountant Special Actions_\n**Deducticate**:",
    "Glossary Item": 0,
    "Major Item": 0
  },
  {
    "id": "accountingBook",
    "Title": "Accounting Book",
    "Blurb": "The accounting book is where you will keep track of company funds for each game.  Common line items include *payroll*, *project payout*, and miscellaneous costs from *event cards* and *option cards*. It is recommended you use a calculator.  Re-tallying funds is done at the end of each *quarter*.  If it is ever zero or less at then end of a *quarter*, the company fails and all players lose.",
    "Glossary Item": true,
    "Major Item": true
  },
  {
    "id": "actionPhase",
    "Title": "Action Phase",
    "Blurb": "See *Quarterly Agenda*",
    "Glossary Item": true,
    "Major Item": 0
  },
  {
    "id": "actions",
    "Title": "Actions",
    "Blurb": "All player actions are listed on their *specialty placard*.  For details on special actions, see *Specialty Placards*.  Otherwise, see *Common Actions*.",
    "Glossary Item": true,
    "Major Item": 0
  },
  {
    "id": "agendaCard",
    "Title": "Quarterly Agenda",
    "Blurb": "The quarterly agenda is printed on every *specialty placard*.  There are five phases which are repeated every quarter:",
    "Glossary Item": true,
    "Major Item": true
  },
  {
    "id": "agendaCardDescription",
    "Title": 0,
    "Blurb": "1. **Event Phase**: During the event phase, the *leading player* pulls two (2) *event cards*, reads them aloud, and resolves them.  For details on how to resolve see *Event Cards*.\n1. **Action Phase**: During the action phase, *founders* take turns doing one *action* (of any *task cost*) at a time clockwise until all players *pass*. (see *Passing Actions* for more details)\n1. **Clean Up Phase**: During the clean up phase, all leftover *task cubes* and *project touch cubes* are removed, often with negative impact towards *synergy*.  The four parts of the clean up phase are done in the order shown below.\n   - Every *employee* with *too low of patience* will quit.  Because this part of the phase happens first, *employees* will not *quit* as a result of synergy loss from the rest of the clean up phase.  See *Quitting Employees* for details.\n   - For each *project* that went untouched this *quater* (still have a *touch cube*), momentum is lost resulting in *synergy* being reduced by one (SYN-1).\n   - For each *employee* with two or more *task cubes* (2+ {{TASK}}), one *synergy* is lost (SYN-1).\n   - For each *employee* with three or more *task cubes* (3+ {{TASK}}), one *blame cube* is added to them.  This means an *employee* who did nothing the whole quarter gets both one *blame cube* and reduces the *synergy level*.\n1. **Payroll Phase**: During the payroll phase, the cost of all *workers* is summed together and added as a negative line item in the *accounting book*.  Then all the line items from this *quarter* are summed together to calculate current funds.  If funds equal zero or less at the end of a payroll phase, it results in a *lose condition*.\n   - Additionally, the company may choose to pay for *cleaning services* for the next quarter.  The *cleaning service* has a section on the board, in *mission control*, that both looks and functions similar to *employee cards*.  (TODO: elaborate) If you decide to pay for the service, place a single *task cube* on the cleaning service space on the board.\n1. **Tokens Phase**: During the tokens phase, players redistribute three (3) *task cubes* to all *workers*, and place *touch cubes* on all *project cards*.  This phase gets the board set up for the next *quarter*.",
    "Glossary Item": false,
    "Major Item": 0
  },
  {
    "id": "agendaCardText",
    "Title": 0,
    "Blurb": "1. ***Event Phase***: Draw an event card (unless 1st *quarter*)\n2. ***Action Phase***: Players take turns performing actions\n3. ***Clean Up Phase***: \n    - Any {{EMEE}} with *too low of patience* quits\n    - All {{EMEE}} with **2+** {{TASK}} get {{BLAME}}\n    - All {{EMEE}} with **3+** {{TASK}} lose SYN-1\n    - Clear all {{TASK}} from {{EMEE}}\n    - All *projects* with {{TOUCH}} lose SYN-1\n4. ***Payroll Phase***: Subtract all *worker* pays from funds\n5. ***Tokens Phase***:\n    - All {{EMEE}} get three **3** {{TASK}} (max)\n    - All *projects* get **1** {{TOUCH}} (max)\n6. Progress the *quarter track*\n",
    "Glossary Item": false,
    "Major Item": 0
  },
  {
    "id": "art",
    "Title": "Art Skill {{ART}}",
    "Blurb": "See *Skills*",
    "Glossary Item": true,
    "Major Item": 0
  },
  {
    "id": "blameCube",
    "Title": "Blame Cube {{BLAME}}",
    "Blurb": "Blame cubes symbolize interpersonal tensions and are represented by the symbol {{BLAME}}.  They are small red cubes (see *Board Overview* for imagery), and are placed on *workers*, usually as a result of *event cards*.  If an *employee* ever gets three or more blame cubes (3+ {{BLAME}}), they must be *fired* immediately.   *Founders* can also get blame cubes, usually as a result of *firing employees*.  If a *founder* ever gets three or more blame cubes (3+ {{BLAME}}), it is an immediate *lose condition*.  Blame cubes can be removed from *workers* by way of some *option cards* or the *human resources specialty*.",
    "Glossary Item": true,
    "Major Item": true
  },
  {
    "id": "boardOverview",
    "Title": "Board Overview",
    "Blurb": "TODO",
    "Glossary Item": true,
    "Major Item": true
  },
  {
    "id": "boardOverviewDescription",
    "Title": 0,
    "Blurb": "1. **Synergy Track**:\n1. **Office Space**:\n1. **Front Door**:\n1. **Mission Control**:\n1. **Mission Card Area**:\n1. **Project Cards Area**:\n1. **Cleaning Service**:\n1. **Quarter Track**:",
    "Glossary Item": false,
    "Major Item": false
  },
  {
    "id": "quarterTrack",
    "Title": "Quarter Track",
    "Blurb": "See *Board Overview*",
    "Glossary Item": true,
    "Major Item": 0
  },
  {
    "id": "characterCard",
    "Title": "Character Cards",
    "Blurb": "The character cards represent potential *employees* for your company, similar to a stack of resumes.  Characters are often represented by the symbol {{EMEE}}.  \n-------\n**To play a character card**, you must hire them.  See \"*Hiring Employees*\" for details.",
    "Glossary Item": true,
    "Major Item": true
  },
  {
    "id": "characterCardDescription",
    "Title": 0,
    "Blurb": "1. **Image**: The image displays a profile photo of the character.  Some mechanics in the game may reference *employees* by type of *onesie*. \n1. **Synergy Level/ Pay**: An employees synergy level is the mininum synergy required from the company, without which the employee will quit.  Their pay is how much you'll have to pay during the *payroll* phase of each round to keep them.\n1. **Quirk Space**: This is the space where a *quirk card* will be placed after *hiring* an employee (or, if using the *Recruiter* specialty, before *hiring*)\n1. **Skills**: The manilla folder contains a table of the four in-game types of *skill*.  Each *skill* uses icons to show it's level, ranging from 0 to 3.",
    "Glossary Item": false,
    "Major Item": 0
  },
  {
    "id": "characterDiscard",
    "Title": "Character Discard Pile",
    "Blurb": "Whenever an employee *quits*, is *fired*, or is otherwise discarded, they are put into a pile beside the draw pile.  The discard pile can be reshuffled to restart an empty draw pile.",
    "Glossary Item": false,
    "Major Item": 0
  },
  {
    "id": "charisma",
    "Title": "Charisma Skill {{CHA}}",
    "Blurb": "See *Skills*",
    "Glossary Item": true,
    "Major Item": 0
  },
  {
    "id": "cleaningService",
    "Title": "Cleaning Service",
    "Blurb": "TODO The cleaning service is an optional fee payed during the *payout phase*.  It is used to negate the effects of \"messy kitchen\" *event cards*.",
    "Glossary Item": true,
    "Major Item": true
  },
  {
    "id": "cleanUpPhase",
    "Title": "Clean Up Phase",
    "Blurb": "See *Quarterly Agenda*",
    "Glossary Item": true,
    "Major Item": 0
  },
  {
    "id": "commonActions",
    "Title": "Common Actions",
    "Blurb": "There are six (6) common actions which are available to all players, regardless of specialty.  They can be found listed on every *specialty placard*.",
    "Glossary Item": true,
    "Major Item": true
  },
  {
    "id": "commonActionsDescription",
    "Title": 0,
    "Blurb": "- **Play any card**: Players may play any card they have in their hand at no cost.  See *Character Cards*, *Project Cards*, and *Option Cards* for details on how to play each.\n- **Move yours task**: Players may move their tasks to as many projects as they want, with the same rules as when they are moved from *managing a project*.\n- **Draw a card**: Players may draw a card from either the *characters deck*, the *projects deck*, or the *options deck*.  It costs one *task cube* for each card drawn.  If drawing a card leads to a player having more than the *maximum hand size*, they must discard a card.\n- **Manage a project**: Players may spend one *task cube* to manage a project.  When managing a project, players may draw *task cubes* from any *employees* who meet the requirements.  Players are also allowed to draw from *founders*, if given consent.  See *Project Cards* for details.\n- **Spec-out and manage**: If a player spends three *task cubes*, they may spec-out a project before assigning it.  This means, all employees can be one skill level short (all skills combined) of whatever *jobs* their *task cubes* are being placed on, _if and only if_ the *founder* assigning it could do the *job* in normal conditions.  This means any *jobs* the *founder* could not place their own tasks on, they also can not spec-out.\n- **Acquire new specialty**: Each *quarter*, a player may place one (1) *task cube* on a *specialty placard* of their choosing.  Once they've placed three (3) *task cubes*, they may then have the new *specialty* alongside their other.  There is no limit to acquired specialties.\n- **Fire an employee**: Players may take one *blame cube* to fire an *employee*.  See *Firing Employees* for details.\n- **Move Desks**: Players may move *employees* around the office for the sake of getting better results from *quirk checks*.  Each *employee* that is moved loses one (1) *task cube*.",
    "Glossary Item": false,
    "Major Item": 0
  },
  {
    "id": "cultureChampion",
    "Title": "Culture Champion",
    "Blurb": "See *Specialties Placards*",
    "Glossary Item": true,
    "Major Item": 0
  },
  {
    "id": "cultureChampionDescription",
    "Title": 0,
    "Blurb": "_Culture Champion Special Actions_ \n**Explore**:\n**Entertain**:",
    "Glossary Item": 0,
    "Major Item": 0
  },
  {
    "id": "drawAgenda",
    "Title": "Drawing Secret Agendas",
    "Blurb": "Each player will *start with four (4) secret quarterly agendas*.  If two or more of the cards conflict, the player may show all of them to the other players, explain why they conflict, and may grab new cards to replace them.  If any of the cards shown do not conflict with any of the other cards shown, the player must put it back in their hand.",
    "Glossary Item": 0,
    "Major Item": 0
  },
  {
    "id": "designResearcher",
    "Title": "Design Researcher",
    "Blurb": "See *Specialty Placards*",
    "Glossary Item": true,
    "Major Item": 0
  },
  {
    "id": "designResearcherDescription",
    "Title": 0,
    "Blurb": "_Design Researcher Special Actions_ \n**Brain Storm**:  *maximum hand size*\n**Paradigm Shift**:",
    "Glossary Item": 0,
    "Major Item": 0
  },
  {
    "id": "drawCard",
    "Title": "Drawing Cards",
    "Blurb": "See *Common Actions*",
    "Glossary Item": true,
    "Major Item": 0
  },
  {
    "id": "employee",
    "Title": "Employees {{EMEE}}",
    "Blurb": "Employees are any *character cards* on the board.  *Founders* are not included in employees.  Employees are included in *workers*.",
    "Glossary Item": true,
    "Major Item": 0
  },
  {
    "id": "eventCard",
    "Title": "Event Cards",
    "Blurb": "Event cards are drawn at the beginning of each *quarter* during the *event phase*.  Often two (2) cards are drawn, though sometimes there may be more.  Event cards represent the chaotic external forces of the world upon the company.\nThe actions section on event cards are written in a very terse notation using icons, but there is still a grammer to them.  TODO, further explain how to read them.",
    "Glossary Item": true,
    "Major Item": true
  },
  {
    "id": "eventCardDescription",
    "Title": 0,
    "Blurb": "1. **Description**:  Most event cards have a description to set the backstory for the in game actions that follow.\n1. **Actions**:   All events result in some form of action.  There are many different styles of event card; some have choices, some have conditions, others are unavoidable.  Most of them will use some form of symbology which can be decoded using the symbol glossaries located on every *specialty placard*.  Here's an example:\n   - An event card that says: \"If 3+ {{MESSY}}: {{BLAME}} all {{MESSY}} and SYN-3\"\n   - Is equivalent to: \"If there are three or more employees with the messy quirk (3+ {{MESSY}}), add a blame token to every employee with the messy quirk ({{BLAME}} all {{MESSY}}), and move the synergy level down three (SYN-3)\"",
    "Glossary Item": false,
    "Major Item": 0
  },
  {
    "id": "eventPhase",
    "Title": "Event Phase",
    "Blurb": "See *Quarterly Agenda*",
    "Glossary Item": true,
    "Major Item": 0
  },
  {
    "id": "exampleSpecialtyDescription",
    "Title": 0,
    "Blurb": "1. **Founder Character Card**: In this space, the player places a *character card* chosen by them to be their *founder*.  Founder character cards do not get a quirk (players are quirky enough).\n1. **Overview**: Every specialty placard has an overview to help players quickly decide amongst them.\n1. **Special Actions**: All specialty placards come with special actions.  Details on each of them are listed below.\n1. **Common Actions**: See *Common Actions*.\n1. **Quarterly Agenda**: See *Quarterly Agenda*.\n1. **Symbol Glossary**: A quick reference guide to symbols is listed on every specialty placard.  If you ever see a symbol used, you should be able to replace it with the word shown in the glossary and form a coherent sentence.",
    "Glossary Item": false,
    "Major Item": false
  },
  {
    "id": "examplePlay",
    "Title": "Example Play",
    "Blurb": "If you haven't played in a while (or ever) the following is a quick way to get the overall gist of the gameplay:\n<br>\n- Three players get together to play Synergy.  \n   - The Mission card chosen is to make 200k and reach level 25 synergy.  \n   - Each player (founder) chooses a Character card and a Specialty (Human Resources, Accounting, Project Manager, etc.).\n   - Then they grab three task cubes, and begin the game.\n- They skip the Events phase, since it's the first Quarter (or round).\n- During the Actions phase, they take turns performing actions in any order.\n   - A few of the players choose the \"draw a card\" action, which costs a Task cube.  They both draw Project cards.  \n       - One of the Project cards pulled has skill requirements that the players meet, so it gets played onto the board.   \n       - A couple players move some of their Task cubes onto the Project to complete part of it.  \n       - The other Project card stays in the player's hand, to be played or discarded later.  \n    - Then one of the players spends two Task cubes to pull two Character cards.\n       - One of the Characters meets the requirements for their Project, so they hire them as an Employee. \n       - The Employee is placed on the board, and given a Quirk card (Messy in this example). \n       - The other Character card stays in the player's hand, to be hired or discarded later.\n   - All of the players are out of Task cubes, and done with their actions, so the game moves into the Cleanup phase.\n- During the Cleanup phase, no penalties occur.  No Projects went unworked on, and no tasks were wasted.\n- During the Payroll phase, the players add up the salaries of all Workers (themselves and their employee) record it, and subtract it from their funds.\n- During the Tokens phase, three Task cubes are given to every Founder and Employee they hired.  Also, one Touch cube is placed on the only Project.\n- This marks the end of the First Quarter, and the beginning of the Second Quarter.\n- For the Second Quarter, during the Event phase, the players draw an Event card.  \n   - The Event results in a Synergy loss, so the Synergy token is moved down the Synergy track.  \n   - The event also results in all Employees with the Messy Quirk getting a Blame Cube, and it affects the Employee they just hired.\n- Finally, the players enter their Second Action phase.\n   - Two of the players have Specialties which allow them to draw a free card after the Event phase, so they do.\n   - One player spends a Task to manage the Project on the board.  They move Task cubes from their Employee onto the Project.  Because the Project has been worked on this Quarter, the Touch cube is removed.  \n   - Another player moves a few of their Tasks to the Project, completing it.  The payout from the Project is recorded and added to their funds, and the project card is discarded.\n   - Then a player spends a Task to draw an Option card.  It allows them to trade money for synergy; they play it immediately.  The cost is recorded, and the Synergy token is moved up the track.\n   - Lastly, one of the players uses a Special action (listed on their Specialty placard) to trade in one Task cube and remove the Blame cube from their only Employee.\n- As the game goes on, the players use many different strategies and actions, but their core gameplay continues to include what's been shown: drawing cards, playing them, assigning Employees to Projects, and keeping Synergy up.",
    "Glossary Item": false,
    "Major Item": 0
  },
  {
    "id": "loseConditions",
    "Title": "Lose Conditions",
    "Blurb": "If any of the following happens, it results in an immediate failure (unless certain *option cards* or *mission cards* say otherwise).\n1. **Bankrupcy**: The company has zero or less *funds* (<= 0k) at the end of the *payroll phase*.\n2. **Fallout**: The company hits a *synergy level* of negative ten (SYN <= -10).\n3. **Parting Ways**: If any *founders* ever get three or more *blame cubes* (3+ {{BLAME}}).\n4. **Mission Failure**: If the *win conditions* for the selected *mission card* are not met within the right time.",
    "Glossary Item": true,
    "Major Item": true
  },
  {
    "id": "firingEmployees",
    "Title": "Firing Employees",
    "Blurb": "Firing employees is never a good thing, and will result in blame towards the *founders*.  If an *employee* reaches three or more *blame cubes* (3+ {{BLAME}}) they must be fired immediately and _all_ *founders* get one *blame cube*.  Alternatively, *founders* may choose to fire any *employee* during the *action phase* for no task cost, in which case only the *founder* whose action it is gets a *blame cube*.",
    "Glossary Item": true,
    "Major Item": true
  },
  {
    "id": "firstThing",
    "Title": "How to Read These Rules",
    "Blurb": "The most important thing to know about the rules of Synergy is how to look things up.  If you ever see a word or phrase *italicized*, this means you can look it up in the glossary.  The glossary is found on page [GLOSSARY_PAGE].",
    "Glossary Item": false,
    "Major Item": 0
  },
  {
    "id": "firstTimePlaying",
    "Title": "First Time Playing",
    "Blurb": "You do not need to know all the rules before starting the game.  As long as you know how to use the glossary, you should be able to follow the steps below (\"Getting Started\") and piece it together as you go.  That being said, if this is your very first playthrough, it is recommended that you make it easy on yourselves.  Here's a few tips for a first time play:\n1. While you don't need to know all the rules, reading \"Win Conditions\" and \"Lose Conditions\" in the glossary will help a lot.  Also, at the end of the rule book is a section called \"Example Play\" which can give you a very good idea of what the overall gameplay looks like.\n1. The easiest *mission* to start with is \"Tutorial Mission\".  After that \"I Just Want to Dance\" and \"Year 3 Profit\" are also quite easy.\n1. Instead of having players choose their *specialty placard*, just assign them at random. Specialties will make more sense as players become more advanced, but can easily be ignored for beginners.\n1. There are some *common actions* (listed on every *specialty placard*) that you can set aside learning until later.  In particular: Spec-out and manage, Acquire new specialty, Fire an employee, and Move desks.  All of these actions are useful, but not usually until many rounds into a game.\n1. If all else fails, make it up.\nAside from these changes, just follow the steps in \"Getting Started\" and you should be fine.",
    "Glossary Item": false,
    "Major Item": 0
  },
  {
    "id": "furniture",
    "Title": "Furniture",
    "Blurb": "See *Pets and Furniture*",
    "Glossary Item": true,
    "Major Item": false
  },
  {
    "id": "founder",
    "Title": "Founders",
    "Blurb": "Founder is mostly just a fancy term for player.  More specifically, it is the *character card* which represents the player in game.",
    "Glossary Item": true,
    "Major Item": 0
  },
  {
    "id": "gettingStarted",
    "Title": "Getting Started",
    "Blurb": "1. Set up the tabletop as shown in the image below (TODO: add image)\n1. Collectively choose a *mission card* (TODO: mini-mission card image)\n1. Deal three (3) *character cards* to each player (TODO: mini-char card image)\n   - Each player must privately select one (1) *character card* to be their *founder*\n   - The other two (2) *character cards* are placed in the *character discard pile*\n1. Have each player choose a *specialty*\n1. Give each player three *task cubes* ({{TASK}}+3)\n1. Collectively choose a player to hold the *accounting book* (TODO: create book)\n1. Finally, start moving through the *phases* which are listed on every *specialty placard* (under \"Quarterly Agenda\").  \n    - For the first round, the *event phase* is skipped.",
    "Glossary Item": false,
    "Major Item": 0
  },
  {
    "id": "glossary",
    "Title": "Glossary",
    "Blurb": "See *Glossary*",
    "Glossary Item": true,
    "Major Item": 0
  },
  {
    "id": "halfCount",
    "Title": "Half-Count",
    "Blurb": "Half-count is half of some number rounded down.  Half-count's are often used on *event cards* or *option cards* where a full count would result in too large of a number.  To do a half count of *employees*, count the total number of employees on the board, divide by two, and round downwards to the nearest whole number.  For the sake of inscrutable clarity, a table is listed below.",
    "Glossary Item": true,
    "Major Item": 0
  },
  {
    "id": "hiring",
    "Title": "Hiring",
    "Blurb": "Any player who possesses a *character card* may play it during the *action phase* at no *task* cost.  To hire a new *employee*, follow these steps: \n1. Find a place for them on the board, _making sure you follow the proper placement guidelines_.  See \"*office placement*\" to ensure that you meet fire safety codes.  \n2. Draw a *quirk card* and place it on the new employee, and immediatly do a *quirk check* for just this employee.\n    - [tip: the *recruiter specialty*, can pull a quirk before hiring]\n3. You now have a new *employee*.  In normal conditions, they will not get tasks until the next round.\n\nOnce an employee is hired, the only way to remove them is if they *quit* or by *firing* them, both of which result in penalties.",
    "Glossary Item": true,
    "Major Item": true
  },
  {
    "id": "humanResources",
    "Title": "Human Resources",
    "Blurb": "See *Specialty Placards*",
    "Glossary Item": true,
    "Major Item": 0
  },
  {
    "id": "humanResourcesDescription",
    "Title": 0,
    "Blurb": "_Human Resources Special Actions_ \n**Mediate**: The mediate action allows a *founder* to trade one *task cube* to help resolve some conflicts, resulting in the removal of one *blame cube* from any *worker*.  \n**Graduate**: The graduate action allows for a *founder* to trade one *task cube* to *fire an employee* without getting a *blame cube*, _if_ they have a greater level of the *charisma skill* than the employee being fired.  They do this by convincing everyone they aren't being fired, they're just graduating.\n",
    "Glossary Item": 0,
    "Major Item": 0
  },
  {
    "id": "iconGlossary",
    "Title": "Icon Glossary",
    "Blurb": "The following are all of the special icons used in Synergy to tersify various concepts. They are grouped together by use.  For details, check the keyterm glossary:\n- SYN: *Synergy Level*\n- {{BLAME}}: *Blame Cube*\n- {{TASK}}: *Task Cube*\n- {{TOUCH}}: *Touch Cube*\n- {{EMEE}}: *Employee*\n- {{ART}}: *Art Skill*\n- {{LOG}}: *Logic Skill*\n- {{CHA}}: *Charisma Skill*\n- {{PAT}}: *Patience Skill*\n- \n- {{REPEAT}}: *Quirk* Effect - Common \n- {{D20}}: *Quirk* Effect - D20 dice *roll*\n- {{BESIDE}}: *Quirk* Effect - *Directly Beside* \n- {{ADJACENT}}: *Quirk* Effect - *Directly Adjacent* \n- {{COMMON}}: *Common Quality*\n- {{IMPULSIVE}}: *Impulsive Quality*\n- {{ALLURING}}: *Alluring Quality*\n- {{MESSY}}: *Messy Quality*",
    "Glossary Item": false,
    "Major Item": 0
  },
  {
    "id": "italicized",
    "Title": "Italicized",
    "Blurb": "Italics were first developed in Italy, which is where they get their name.  They are meant to mimic the italian form of verticality, most noteably as seen in the leaning tower of pisa.",
    "Glossary Item": true,
    "Major Item": 0
  },
  {
    "id": "keytermGlossary",
    "Title": "Keyterm Glossary",
    "Blurb": "The following is a list of all the keyterms used in Synergy.  You will find them italicised (and often colored) on many of the cards in game.  This glossary is organized in alphabetical order.  If you'd like to read through the glossary in a more natural instructions order, it is recommended that you start at \"*Win Conditions*\" and work from there, following the trail of keyterms as they come up.",
    "Glossary Item": 0,
    "Major Item": 0
  },
  {
    "id": "leadingPlayer",
    "Title": "Leading Player",
    "Blurb": "The leading player is whichever player is holding the *quarterly agenda*.  It is their job to make sure each line item on the *quarterly agenda* is met, preferably in a timely manner.",
    "Glossary Item": false,
    "Major Item": 0
  },
  {
    "id": "logic",
    "Title": "Logic Skill {{LOG}}",
    "Blurb": "See *Skills*",
    "Glossary Item": true,
    "Major Item": 0
  },
  {
    "id": "managingProjects",
    "Title": "Managing Projects",
    "Blurb": "See *Common Actions*",
    "Glossary Item": true,
    "Major Item": 0
  },
  {
    "id": "maximumHandSize",
    "Title": "Maximum Hand Size (5)",
    "Blurb": "The maximum amount of cards a player is allowed to have in their hand is five (5).  If, at any point, a player is holding more than that, they must discard until they reach five again.  Players may not give eachother cards.",
    "Glossary Item": true,
    "Major Item": true
  },
  {
    "id": "missionCard",
    "Title": "Mission Cards",
    "Blurb": "Chosen at the start of the game, mission cards rerpesent the overall goal of your company.",
    "Glossary Item": true,
    "Major Item": true
  },
  {
    "id": "missionCardDescription",
    "Title": 0,
    "Blurb": "1. **Title**: \n1. **Specs**:\n   - **Difficulty**:\n   - **Length**:\n   - **Funds**:\n   - **Deadline**:\n1. **Goal**:\n1. **Endpoint Type**: Either \"deadline\" or \"target quarter\"",
    "Glossary Item": false,
    "Major Item": 0
  },
  {
    "id": "missionControl",
    "Title": "Mission Control",
    "Blurb": "See *Board Overview*",
    "Glossary Item": true,
    "Major Item": 0
  },
  {
    "id": "moveTasks",
    "Title": "Moving Tasks",
    "Blurb": "See *Common Actions*",
    "Glossary Item": true,
    "Major Item": 0
  },
  {
    "id": "nonSecretAgendas",
    "Title": "Non-Secret Agendas",
    "Blurb": "Each starting skill comes with a set of non-secret agendas.  The agendas are listed on the skill overview cards for easy reference, and also listed below:\nHuman Resources - For each option card in the discard pile, get +1 PVP\netc.",
    "Glossary Item": 0,
    "Major Item": 0
  },
  {
    "id": "officePlacement",
    "Title": "Office Placement",
    "Blurb": "It is very important that all employees must have walkable pathways to all doors.  All placements must follow these criteria:\n1. Cards can be placed either horizontally or vertically, but not diagonally.\n1. Cards must not split grid lines on the board.  All cards are sized to fit neatly on the grid.\n1. At least one of the long sides of every *character card* must have full access to a pathway.  There can be no obstructions and the short sides do not count.\n1. All walkable pathways may be no thinner than 4 blocks wide.  A helpful trick is to make sure you can slide a *quirk card* from a desk to all doors without overlapping any other desks.\nBy following these rules, you are certain to pass fire safety inspections.",
    "Glossary Item": true,
    "Major Item": true
  },
  {
    "id": "onesie",
    "Title": "Onesie",
    "Blurb": "Onesies are the animal costume pajamas each *character* is wearing in their photo.",
    "Glossary Item": true,
    "Major Item": 0
  },
  {
    "id": "optionCard",
    "Title": "Option Cards",
    "Blurb": "The option cards represent the internal working of the company, mostly minute things such as snacks, furniture, vacations, etc.  Option cards are often the best way to improve *synergy*, but for the most part they are a means of trading one thing for another.  *Task cubes*, *blame cubes*, *funds*, *synergy*, game rules, and more; all of these are things which might be traded for one another or affected by option cards.\n------\n**Playing an option card** is mostly done during the *actions phase*, though some may specify otherwise.  As long as you meet the requirements on a option card, including being in the correct *phase*, you may play it at any time.  Most cards can only be played once, and should then be put in the discard pile.   In some special cases, the card may be placed back into your hand after being played.\n",
    "Glossary Item": true,
    "Major Item": true
  },
  {
    "id": "optionCardDescription",
    "Title": 0,
    "Blurb": "1. **Description**:  Most option cards have some form of backstory to explain the circumstances behind the actions that follow.\n1. **Actions**: All option cards have an optional action that can be taken, and sometimes mulitple options.\n1. **Repeatable**: Some option cards are repeatable.  No option cards can be repeated twice in one quarter.",
    "Glossary Item": false,
    "Major Item": 0
  },
  {
    "id": "overview",
    "Title": "Overview of the Game",
    "Blurb": "Synergy is a game about managing the constant and chaotic decomposition of start ups.  Each game revolves around a chosen mission and along the way employees will cast blame at one another, synergy will rise and fall wildly, funds will dissappear like soap suds, and many mistakes will be made.  But **you will** find all the tools you need to be successful.  This game is designed to be hard, so expect to lose a few times while devising the best stategies for each type of mission.  While Synergy is by no means a realistic simulation of running a start up, much effort has been put into making it analagous to one, and we hope it touches on many of the often overlooked aspects of it.  If you have any suggestions or complaints, please send them to @SynergyTheGame on Twitter, and we'll do our best to incorporate them.",
    "Glossary Item": 0,
    "Major Item": 0
  },
  {
    "id": "patience",
    "Title": "Patience Skill {{PAT}}",
    "Blurb": "See *Skills*",
    "Glossary Item": true,
    "Major Item": 0
  },
  {
    "id": "petsAndFurniture",
    "Title": "Pets and Furniture",
    "Blurb": "There are two types of additions that can be added to the office space:\n- **Pets**:  Pets have upkeep costs, and create positive synergy over time.  In the top right corner of a pet card is their upkeep cost which must be payed during the *payroll phase*.  Also, listed is a conversion from tasks to synergy.  You may place _one_ *task cube* per quarter, from any *employee*, on a pet.  Once it reaches the listed amount, clear the *task tokens* and gain *synergy*.  Also, there is a heavy penalty for disowning pets.  If you ever choose to do so, you must lose one synergy (SYN-1) and one of the *founders* must take one *blame*.\n- **Furniture**: Furniture has no upkeep cost and can be removed without penalty.  Most furniture will have a conversion cost from *task cubes* into something else.  You may take a maximum of _one_ *task cube* from each *employee* per *quarter*.\nBoth of these types of office addition can be purchased through the \"Pet Store\" and \"Furniture Store\" *option cards*, respectively.  They also must both be placed using the *Office Placement* guidelines.",
    "Glossary Item": true,
    "Major Item": true
  },
  {
    "id": "payrollPhase",
    "Title": "Payroll Phase",
    "Blurb": "See *Quarterly Agenda*",
    "Glossary Item": true,
    "Major Item": 0
  },
  {
    "id": "phases",
    "Title": "Phases",
    "Blurb": "See *Quarterly Agenda*.",
    "Glossary Item": true,
    "Major Item": false
  },
  {
    "id": "playCards",
    "Title": "Play a Card",
    "Blurb": "Each type of card is played differently.  See *Character Cards*, *Project Cards*, or *Option Cards* for details.",
    "Glossary Item": true,
    "Major Item": 0
  },
  {
    "id": "projectCard",
    "Title": "Project Cards",
    "Blurb": "The project cards represent the actual work your company does, and are one of the only ways to increase *funds*.  They do not represnt your overall *mission*, but instead the work done towards or around that mission.  Once a project card has been placed, work is done on it in the form of *task cubes*.  For details, see \"*Managing a Project*\".\n-------\n**To play a project card**, simply place it in an open project space on the *mission control* section of the board.  If no spaces are open, you may discard any project of your choice.  Replacement and completion are the only ways to discard a project once it has begun.",
    "Glossary Item": true,
    "Major Item": true
  },
  {
    "id": "projectCardDescription",
    "Title": 0,
    "Blurb": "1. **Payout**: The amount of money earned after completing the last task.  You may collect the payout immediately.\n1. **Jobs**: Every project has at least one job, and usually more.  To complete a project, you must first complete all the jobs.\n1. **Task Count**: Every job has a task count.  Draw *task cubes* from *workers* and place them here to complete the job.\n1. **Required Skills**: Every job also has skill requirements.  These are the minimum *skills* a *worker* must have in order to move their *task cubes* onto the job.  Under normal circusmstanes, it is not enough for a *worker* to have just a couple of the skills, they must have all of them.\n1. **Stage Split**:  All jobs prior to a \"THEN\" must be completed before moving tasks onto any of the jobs after it.  Otherwise, jobs can be tackled in any order.",
    "Glossary Item": false,
    "Major Item": 0
  },
  {
    "id": "projectManager",
    "Title": "Project Manager",
    "Blurb": "See *Specialty Placards*\n",
    "Glossary Item": true,
    "Major Item": 0
  },
  {
    "id": "projectManagerDescription",
    "Title": 0,
    "Blurb": "_Project Manager Special Actions_ \n**Wrangle**: \n**Translate**:",
    "Glossary Item": 0,
    "Major Item": 0
  },
  {
    "id": "quarter",
    "Title": "Quarter",
    "Blurb": "A quarter is just a fancy term for one round of the game.  Quarters are kept track of on the boards *quarter track*",
    "Glossary Item": true,
    "Major Item": 0
  },
  {
    "id": "quarterCount",
    "Title": "Quarter-Count",
    "Blurb": "See *Half-Count*",
    "Glossary Item": true,
    "Major Item": 0
  },
  {
    "id": "quarterTrack",
    "Title": "Quarter Track",
    "Blurb": "The quarter track is a part of the board where the players keep track of the current in game *year* and *quarter*.  Many *mission cards* require certain taks to be done by some *year*.  See *Board Overview* for more details.",
    "Glossary Item": true,
    "Major Item": 0
  },
  {
    "id": "quirkCard",
    "Title": "Quirk Cards",
    "Blurb": "Quirk cards represent the strange nuances you only find in people after knowing them a while.  They are drawn an placed atop *employees* just after they have been hired, and commonly include actions that must be taken upon *hiring*, or any time a *quirk check* occurs.  ",
    "Glossary Item": true,
    "Major Item": true
  },
  {
    "id": "quirkCardDescription",
    "Title": 0,
    "Blurb": "1. **Effects**: See *Quirk Effects*\n1. **Qualities**:  See *Quirk Qualities*",
    "Glossary Item": false,
    "Major Item": 0
  },
  {
    "id": "quirkEffects",
    "Title": "Quirk Effects {{REPEAT}}{{D20}}{{ADJACENT}}{{BESIDE}}",
    "Blurb": "Most *Quirk Cards* have quirk effects which take effect the moment they are revealed.  These quirks may require an immediate action, or modify the rules in regards to the employee who owns them.  Additionally, many quirk cards have repeating effects which are reapplied every time a \"Quirk Check\" *event card* arises.  The four types of repeating quirk checks are:\n- **Unconditional {{REPEAT}}**: Repeats every quirk check, without condition.\n- **Roll {{D20}}**: Is based off a d20 roll.  During \"Quirk Check\" events, all employee quirk effects share a single roll.\n- **Directly Beside {{BESIDE}}**: Pertains to the employees sitting directly to either side (neighboring desks).  As a rule of thumb, if any two employees face the same direction and have touching sides, they are directly beside eachother.\n- **Directly Adjacent {{ADJACENT}}**: Pertains to employees sitting directly adjacent.  If any two employees touch in any way (including corners), they are directly adjacent to eachother.",
    "Glossary Item": true,
    "Major Item": true
  },
  {
    "id": "quirkQualities",
    "Title": "Quirk Qualities {{COMMON}}{{MESSY}}{{ALLURING}}{{IMPULSIVE}}",
    "Blurb": "There are four (4) quirk qualities which are repeated on many different *quirk cards*:\n- Common {{COMMON}}: tend to hive mind together around bad but popular ideas.\n- Messy {{MESSY}}: leave messes everywhere.\n- Alluring {{ALLURING}}: likely to be a distraction, and make people feel inadequate.\n- Impulsive {{IMPULSIVE}}: have a hard time keeping it to themselves, erradic.\nThese qualities are referenced frequently by *event cards*, especially in cases where things go awry and *blame* is cast.",
    "Glossary Item": true,
    "Major Item": true
  },
  {
    "id": "quit",
    "Title": "Quitting Employees",
    "Blurb": "If the *synergy level* (SYN) ever drops below the level needed to support an *employee*, they will quit during the *clean up phase*, and take some *synergy* with them.   _Every *employee*  who quits results in SYN-1_.  The minimum amount of SYN needed for each *employee* can be found at the top right corner of their card.\n",
    "Glossary Item": true,
    "Major Item": true
  },
  {
    "id": "recruiter",
    "Title": "Recruiter",
    "Blurb": "See *Specialty Placards*",
    "Glossary Item": true,
    "Major Item": 0
  },
  {
    "id": "recruiterDescription",
    "Title": 0,
    "Blurb": "_Recruiter Special Actions_ \n**Network**: Every round, recruiters get to draw one *employee card* for free.  It is best to do this immediately after the *event phase*, but any time before the end of the *actions phase* is still okay. *maximum hand size*\n**Deep Screen**: Sometimes an *employee* quirk can be bad enough to ruin them.  While *hiring an employee*, a recruiter may pay one *task cube* before pulling the *quirk card* to have the option to discard the *employee* if the *quirk card* is especially bad.  It is not okay to retro-actively use this action after the *quirk card* has already been pulled.\n",
    "Glossary Item": 0,
    "Major Item": 0
  },
  {
    "id": "runningOutOfCards",
    "Title": "Running Out of Cards",
    "Blurb": "All card decks in Synergy should have a discard pile.  If ever you run out of cards, simply shuffle the discard pile and keep going.",
    "Glossary Item": true,
    "Major Item": 0
  },
  {
    "id": "skills",
    "Title": "Skills {{ART}}{{LOG}}{{CHA}}{{PAT}}",
    "Blurb": "There are four skills listed on the bottom left corner of each *character card*:\n- Art (ART) {{ART}}: represents the ability to do creative/design centered projects\n- Logic (LOG) {{LOG}}: represents the ability to problem solve\n- Charisma (CHA) {{CHA}}: represents the ability to convince and sway (TODO: forced hire?)\n- Patience (PAT) {{PAT}}: represents the ability to endure\nThe primary function of skills is for figuring out which *projects* a *character* is able to work on.  To understand how character skills and projects relate, see *Project Cards*.",
    "Glossary Item": true,
    "Major Item": true
  },
  {
    "id": "specialtyCards",
    "Title": "Specialty Placards",
    "Blurb": "Specialties represent game mechanics which are specific to each player.  Each specialty comes with special actions, all of which are listed below.\n",
    "Glossary Item": true,
    "Major Item": true
  },
  {
    "id": "specOutAndManage",
    "Title": "Spec-out and manage",
    "Blurb": "See *Common Actions*",
    "Glossary Item": false,
    "Major Item": false
  },
  {
    "id": "symbolGlossary",
    "Title": "Symbol Glossary",
    "Blurb": "SYN Synergy Level\n{{BLAME}} Blame Cube\n{{TASK}} Task Cube\n{{TOUCH}} Touch Cube\n{{EMEE}} Employee\n{{ART}} Art Skill\n{{LOG}} Logic Skill\n{{CHA}} Charisma Skill\n{{PAT}} Patience Skill\n{{REPEAT}} Quirk Effect\n{{D20}} D20 Roll \n{{BESIDE}} Directly Beside\n{{ADJACENT}} Directly Adjacent \n{{COMMON}} Common Quirk\n{{IMPULSIVE}} Impulsive Quirk\n{{ALLURING}} Alluring Quirk\n{{MESSY}} Messy Quirk",
    "Glossary Item": false,
    "Major Item": 0
  },
  {
    "id": "synergyLevel",
    "Title": "Synergy Level SYN",
    "Blurb": "Represented by the symbol SYN, synergy represents the overall sense of happiness & purpose at the company.  Every *employee* requires a certain level of synergy, based off their *patience skill*.  If synergy is too low, they will quit (see *Quitting Employees* for details).  If the synergy level ever reaches negative ten (-10SYN), it is an immediate *lose condition*.",
    "Glossary Item": true,
    "Major Item": true
  },
  {
    "id": "synergyTrack",
    "Title": "Synergy Track",
    "Blurb": "The synergy track keeps track of the current *synergy level*.  For details see *Board Overview*.",
    "Glossary Item": true,
    "Major Item": 0
  },
  {
    "id": "taskCubes",
    "Title": "Task Cubes {{TASK}}",
    "Blurb": "Task cubes symbolize *worker* time, and are represented by the symbol {{TASK}}.  They are small green cubes (see *Board Overview* for imagery), and are placed on *workers* and *project cards*.",
    "Glossary Item": true,
    "Major Item": true
  },
  {
    "id": "tokensPhase",
    "Title": "Tokens Phase",
    "Blurb": "See *Quarterly Agenda*",
    "Glossary Item": true,
    "Major Item": 0
  },
  {
    "id": "tooLowOfPatience",
    "Title": "Too Low of Patience",
    "Blurb": "When the *synergy level* becomes too low for an employee's *patience*, they *quit*.  See *Quitting Employees* for more details.",
    "Glossary Item": true,
    "Major Item": 0
  },
  {
    "id": "touchCubes",
    "Title": "Touch Cubes {{TOUCH}}",
    "Blurb": "Touch cubes are used to help with keeping track of things where you might forget what you've done.  They are put on *projects* to keep track of whether or not they've been worked on within the *quarter*, and placed on *employees* while doing a *quirk check* to keep track of who's been checked.",
    "Glossary Item": false,
    "Major Item": true
  },
  {
    "id": "tradingCards",
    "Title": "Trading Cards",
    "Blurb": "Players may not trade cards (with some exceptions that arise from *event* or *option* cards).  The ",
    "Glossary Item": true,
    "Major Item": 0
  },
  {
    "id": "untouchedProjects",
    "Title": "Untouched Projects",
    "Blurb": "If a *touch cube* remains on a project at the end of a *quarter*, it results in a loss of *synergy*.  See the clean up phase section of *Quarterly Agenda* for details.",
    "Glossary Item": true,
    "Major Item": 0
  },
  {
    "id": "winCondition",
    "Title": "Win Conditions",
    "Blurb": "In order to win, you must meet whatever conditions are listed on your selected *mission card*, while also avoiding all *lose conditions*.  See *Mission Cards* for details.",
    "Glossary Item": true,
    "Major Item": true
  },
  {
    "id": "workers",
    "Title": "Workers",
    "Blurb": "Workers are all *employee cards* in play.  This includes those of the *founders* as well as *employees*.",
    "Glossary Item": true,
    "Major Item": 0
  }
];
});
___scope___.file("Entities/Instructions/instructions.scss", function(exports, require, module, __filename, __dirname){


require("fuse-box-css")("default/Entities/Instructions/instructions.scss", ".Instructions-Book {\n  max-width: 8.5in;\n  font-size: 13px;\n  background: #fffcf7;\n  padding: .5in 0in; }\n  .Instructions-Book em {\n    color: #5e7a93; }\n  .Instructions-Book > .Instruction {\n    page-break-inside: avoid; }\n    .Instructions-Book > .Instruction li {\n      margin-top: 5px; }\n    .Instructions-Book > .Instruction.firstThing > .Blurb {\n      font-size: 18px; }\n    .Instructions-Book > .Instruction.iconGlossary > .Blurb ul {\n      columns: 2; }\n    .Instructions-Book > .Instruction:not(.glossaryItem) {\n      padding: 0px 0.5in;\n      padding-bottom: 50px; }\n      .Instructions-Book > .Instruction:not(.glossaryItem) > .Title {\n        font-size: 30px;\n        border-bottom: 2px solid;\n        margin-bottom: 6px; }\n      .Instructions-Book > .Instruction:not(.glossaryItem) > .Blurb {\n        text-align: justify;\n        text-indent: 1em; }\n    .Instructions-Book > .Instruction.examplePlay > .Blurb > ul {\n      list-style-type: disclosure-closed; }\n    .Instructions-Book > .Instruction.glossaryItem {\n      margin: 20px 0.5in; }\n      .Instructions-Book > .Instruction.glossaryItem.isShort {\n        margin: 10px 0.5in; }\n        .Instructions-Book > .Instruction.glossaryItem.isShort .Title, .Instructions-Book > .Instruction.glossaryItem.isShort .Blurb, .Instructions-Book > .Instruction.glossaryItem.isShort p {\n          display: inline;\n          margin: 0px;\n          padding: 0px;\n          font-size: inherit; }\n      .Instructions-Book > .Instruction.glossaryItem > .Title {\n        display: inline-block;\n        font-weight: bold;\n        margin-right: 20px;\n        font-size: 16px; }\n        .Instructions-Book > .Instruction.glossaryItem > .Title .Icon {\n          font-size: 14px; }\n      .Instructions-Book > .Instruction.glossaryItem > .Blurb {\n        padding-left: 1em; }\n      .Instructions-Book > .Instruction.glossaryItem > table.halfCount {\n        border-collapse: collapse;\n        margin: auto; }\n        .Instructions-Book > .Instruction.glossaryItem > table.halfCount td {\n          text-align: center;\n          border: 1px solid; }\n          .Instructions-Book > .Instruction.glossaryItem > table.halfCount td:not(:first-child) {\n            width: 25px; }\n    .Instructions-Book > .Instruction .CardAndDesc {\n      display: flex;\n      margin: 10px; }\n      .Instructions-Book > .Instruction .CardAndDesc > .Card {\n        position: relative;\n        flex-shrink: 0;\n        margin-right: 10px; }\n        .Instructions-Book > .Instruction .CardAndDesc > .Card > *:first-child {\n          box-shadow: 1px 1px 10px rgba(0, 0, 0, 0.2); }\n        .Instructions-Book > .Instruction .CardAndDesc > .Card > .KeyMarkers {\n          position: absolute;\n          top: 0px;\n          left: 0px;\n          height: 100%;\n          width: 100%; }\n          .Instructions-Book > .Instruction .CardAndDesc > .Card > .KeyMarkers > .KeyMarker {\n            position: absolute;\n            z-index: 5;\n            background-image: linear-gradient(#E6EA4B, #FFFE8E 35%);\n            font-weight: bold;\n            width: 20px;\n            height: 20px;\n            display: flex;\n            align-items: center;\n            justify-content: center; }\n            .Instructions-Book > .Instruction .CardAndDesc > .Card > .KeyMarkers > .KeyMarker::after {\n              display: block;\n              content: \"\";\n              position: absolute;\n              top: 50%;\n              height: 50%;\n              width: 100%;\n              box-shadow: 0px 6px 5px rgba(0, 0, 0, 0.15); }\n      .Instructions-Book > .Instruction .CardAndDesc > .Desc ol {\n        list-style-type: upper-alpha; }\n      .Instructions-Book > .Instruction .CardAndDesc.agendaCard > .Desc > ol {\n        list-style-type: symbols; }\n      .Instructions-Book > .Instruction .CardAndDesc.commonActions ul {\n        padding-left: 0px;\n        list-style-type: none; }\n      .Instructions-Book > .Instruction .CardAndDesc.commonActions > .Card {\n        position: relative;\n        height: 2.5in;\n        width: 3.5in;\n        overflow: hidden; }\n        .Instructions-Book > .Instruction .CardAndDesc.commonActions > .Card .CropView > .Specialty-Card {\n          position: absolute;\n          top: -44px;\n          left: -361px; }\n      .Instructions-Book > .Instruction .CardAndDesc.characterCard .KeyMarker.image {\n        top: 1%;\n        left: 1%; }\n      .Instructions-Book > .Instruction .CardAndDesc.characterCard .KeyMarker.skills {\n        top: 53%;\n        left: 1%; }\n      .Instructions-Book > .Instruction .CardAndDesc.characterCard .KeyMarker.name {\n        top: 1%;\n        left: 34%; }\n      .Instructions-Book > .Instruction .CardAndDesc.characterCard .KeyMarker.pay {\n        top: 1%;\n        left: 73%; }\n      .Instructions-Book > .Instruction .CardAndDesc.characterCard .KeyMarker.quirk {\n        top: 13%;\n        left: 39%; }\n      .Instructions-Book > .Instruction .CardAndDesc.specialtyCards .Card .KeyMarker.founderSpace {\n        top: 8%;\n        left: -4%; }\n      .Instructions-Book > .Instruction .CardAndDesc.specialtyCards .Card .KeyMarker.overview {\n        top: 0%;\n        left: 20%; }\n      .Instructions-Book > .Instruction .CardAndDesc.specialtyCards .Card .KeyMarker.specialActions {\n        top: 7%;\n        left: 44%; }\n      .Instructions-Book > .Instruction .CardAndDesc.specialtyCards .Card .KeyMarker.commonActions {\n        top: 16%;\n        left: 44%; }\n      .Instructions-Book > .Instruction .CardAndDesc.specialtyCards .Card .KeyMarker.agenda {\n        top: 50%;\n        left: 44%; }\n      .Instructions-Book > .Instruction .CardAndDesc.specialtyCards .Card .KeyMarker.cheatsheet {\n        top: 59%;\n        left: -4%; }\n      .Instructions-Book > .Instruction .CardAndDesc.specialtyCards .Card > .CropView {\n        height: 2.5in;\n        width: 3.75in;\n        border-radius: 4px; }\n        .Instructions-Book > .Instruction .CardAndDesc.specialtyCards .Card > .CropView > .Specialty-Card {\n          transform: scale(0.5) translate(-50%, -50%); }\n      .Instructions-Book > .Instruction .CardAndDesc.optionCard .KeyMarker.description {\n        top: 12%;\n        left: -2%; }\n      .Instructions-Book > .Instruction .CardAndDesc.optionCard .KeyMarker.actions {\n        top: 38%;\n        left: 8%; }\n      .Instructions-Book > .Instruction .CardAndDesc.optionCard .KeyMarker.repeatable {\n        top: 65%;\n        left: -2%; }\n      .Instructions-Book > .Instruction .CardAndDesc.projectCard .KeyMarker.name {\n        top: 1%;\n        left: -6%; }\n      .Instructions-Book > .Instruction .CardAndDesc.projectCard .KeyMarker.payout {\n        top: 1%;\n        left: 79%; }\n      .Instructions-Book > .Instruction .CardAndDesc.projectCard .KeyMarker.job {\n        top: 11%;\n        left: -6%; }\n      .Instructions-Book > .Instruction .CardAndDesc.projectCard .KeyMarker.tasks {\n        top: 17%;\n        left: 5%; }\n      .Instructions-Book > .Instruction .CardAndDesc.projectCard .KeyMarker.skills {\n        top: 17%;\n        left: 60%; }\n      .Instructions-Book > .Instruction .CardAndDesc.projectCard .KeyMarker.stageSplit {\n        top: 43%;\n        left: 34%; }\n      .Instructions-Book > .Instruction .CardAndDesc.eventCard .KeyMarker.name {\n        top: 1%;\n        left: 6%; }\n      .Instructions-Book > .Instruction .CardAndDesc.eventCard .KeyMarker.majorIcon {\n        top: 4%;\n        left: 88%; }\n      .Instructions-Book > .Instruction .CardAndDesc.eventCard .KeyMarker.description {\n        top: 20%;\n        left: 1%; }\n      .Instructions-Book > .Instruction .CardAndDesc.eventCard .KeyMarker.actions {\n        top: 42%;\n        left: 1%; }\n      .Instructions-Book > .Instruction .CardAndDesc.quirkCard .KeyMarker.name {\n        top: 2%;\n        left: 23%; }\n      .Instructions-Book > .Instruction .CardAndDesc.quirkCard .KeyMarker.specialEffects {\n        top: 13%;\n        left: -8%; }\n      .Instructions-Book > .Instruction .CardAndDesc.quirkCard .KeyMarker.qualities {\n        top: 46%;\n        left: -8%; }\n      .Instructions-Book > .Instruction .CardAndDesc.missionCard .KeyMarker.title {\n        top: 2%;\n        left: 18%; }\n      .Instructions-Book > .Instruction .CardAndDesc.missionCard .KeyMarker.specs {\n        top: 12%;\n        left: -3%; }\n      .Instructions-Book > .Instruction .CardAndDesc.missionCard .KeyMarker.goal {\n        top: 37%;\n        left: -3%; }\n      .Instructions-Book > .Instruction .CardAndDesc.missionCard .KeyMarker.endpointType {\n        top: 80%;\n        left: -3%; }\n      .Instructions-Book > .Instruction .CardAndDesc.boardOverview {\n        display: flex;\n        flex-direction: column;\n        margin: 0px; }\n        .Instructions-Book > .Instruction .CardAndDesc.boardOverview > .Card {\n          margin: 10px 0px; }\n          .Instructions-Book > .Instruction .CardAndDesc.boardOverview > .Card .CropView {\n            width: 7.45in;\n            height: 5.25in; }\n            .Instructions-Book > .Instruction .CardAndDesc.boardOverview > .Card .CropView > .Board {\n              position: absolute;\n              top: 50%;\n              left: 50%;\n              transform: translate(-50%, -50%) scale(0.32); }\n          .Instructions-Book > .Instruction .CardAndDesc.boardOverview > .Card .KeyMarker.synergyTrack {\n            top: 1%;\n            left: -2%; }\n          .Instructions-Book > .Instruction .CardAndDesc.boardOverview > .Card .KeyMarker.officeSpace {\n            top: 10%;\n            left: 8%; }\n          .Instructions-Book > .Instruction .CardAndDesc.boardOverview > .Card .KeyMarker.frontDoor {\n            top: 80%;\n            left: -2%; }\n          .Instructions-Book > .Instruction .CardAndDesc.boardOverview > .Card .KeyMarker.missionControl {\n            top: 50%;\n            left: 50%; }\n          .Instructions-Book > .Instruction .CardAndDesc.boardOverview > .Card .KeyMarker.missionCard {\n            top: 51%;\n            left: 52%; }\n          .Instructions-Book > .Instruction .CardAndDesc.boardOverview > .Card .KeyMarker.projectCards {\n            top: 51%;\n            left: 64%; }\n          .Instructions-Book > .Instruction .CardAndDesc.boardOverview > .Card .KeyMarker.cleaningService {\n            top: 75%;\n            left: 52%; }\n          .Instructions-Book > .Instruction .CardAndDesc.boardOverview > .Card .KeyMarker.quarterTrack {\n            top: 87%;\n            left: 52%; }\n        .Instructions-Book > .Instruction .CardAndDesc.boardOverview > .Desc {\n          columns: 2; }\n\n/*# sourceMappingURL=/css-sourcemaps/df26d7d.map */")
});
___scope___.file("Entities/Decks/Specialty/Specialty.scss", function(exports, require, module, __filename, __dirname){


require("fuse-box-css")("default/Entities/Decks/Specialty/Specialty.scss", ".Specialty-Card {\n  height: 5in;\n  width: 7.5in;\n  font-size: 13px;\n  border-radius: 0.1in;\n  overflow: hidden;\n  display: inline-grid; }\n  .Specialty-Card p {\n    padding: 0px;\n    margin: 0px; }\n  .Specialty-Card.front {\n    position: relative; }\n    .Specialty-Card.front.mini_view {\n      width: 3.5in;\n      height: 2in; }\n      .Specialty-Card.front.mini_view > .Specialty-fg {\n        padding-left: 10px; }\n        .Specialty-Card.front.mini_view > .Specialty-fg .Title {\n          margin-left: 0px !important; }\n        .Specialty-Card.front.mini_view > .Specialty-fg > .IsMini {\n          position: absolute;\n          top: 17px;\n          right: 13px;\n          font-size: 10px; }\n        .Specialty-Card.front.mini_view > .Specialty-fg > .Content {\n          display: flex;\n          flex-direction: column; }\n          .Specialty-Card.front.mini_view > .Specialty-fg > .Content > .Overview {\n            margin: 10px 5px; }\n          .Specialty-Card.front.mini_view > .Specialty-fg > .Content > .Actions {\n            padding-left: 5px;\n            border-bottom: none; }\n    .Specialty-Card.front > .Specialty-bg, .Specialty-Card.front > .Specialty-bg > *, .Specialty-Card.front > .Specialty-fg {\n      position: absolute;\n      top: 0px;\n      left: 0px;\n      right: 0px;\n      bottom: 0px; }\n    .Specialty-Card.front.projectManager > .Specialty-bg {\n      background-color: #ffdd00; }\n    .Specialty-Card.front.humanResources > .Specialty-bg {\n      background-color: #e62e69; }\n    .Specialty-Card.front.designResearcher > .Specialty-bg {\n      background-color: #2b82cf; }\n    .Specialty-Card.front.recruiter > .Specialty-bg {\n      background-color: #fff3dc; }\n    .Specialty-Card.front.cultureChampion > .Specialty-bg {\n      background-color: #00cba0; }\n    .Specialty-Card.front.accountant > .Specialty-bg {\n      background-color: #42b263; }\n    .Specialty-Card.front.exampleSpecialty > .Specialty-bg {\n      background-color: #f5a623; }\n    .Specialty-Card.front > .Specialty-bg {\n      background-image: radial-gradient(circle at 20px 96%, rgba(0, 0, 0, 0.3), rgba(250, 250, 250, 0.2));\n      mix-blend-mode: multiply; }\n      .Specialty-Card.front > .Specialty-bg > .Fade {\n        display: grid;\n        margin: 0.09375in;\n        grid-template-rows: auto 1fr;\n        grid-template-columns: auto 1fr;\n        border-radius: 0.0625in;\n        overflow: hidden;\n        opacity: 0.8; }\n        .Specialty-Card.front > .Specialty-bg > .Fade > .Fill {\n          background: white; }\n          .Specialty-Card.front > .Specialty-bg > .Fade > .Fill:nth-child(2) {\n            position: relative;\n            right: -5px;\n            margin-right: -5px;\n            border-top-left-radius: inherit;\n            transform: skewX(-30deg);\n            margin-bottom: -3px; }\n          .Specialty-Card.front > .Specialty-bg > .Fade > .Fill.none {\n            visibility: hidden;\n            padding: 0.03125in 0.0625in;\n            font-size: 18px; }\n          .Specialty-Card.front > .Specialty-bg > .Fade > .Fill:nth-child(3) {\n            border-top-left-radius: inherit; }\n    .Specialty-Card.front > .Specialty-fg {\n      padding: .125in; }\n      .Specialty-Card.front > .Specialty-fg .Title, .Specialty-Card.front > .Specialty-fg .InlineTitle {\n        display: inline-block;\n        line-height: 10px;\n        font-size: 10px;\n        letter-spacing: 1px; }\n        .Specialty-Card.front > .Specialty-fg .Title.Title, .Specialty-Card.front > .Specialty-fg .InlineTitle.Title {\n          margin-left: -5px;\n          border-bottom: 1px solid; }\n        .Specialty-Card.front > .Specialty-fg .Title.InlineTitle, .Specialty-Card.front > .Specialty-fg .InlineTitle.InlineTitle {\n          margin: 0px 5px; }\n      .Specialty-Card.front > .Specialty-fg > .Content {\n        display: grid;\n        grid-template-rows: 24px auto auto 1fr;\n        grid-template-columns: auto auto 1fr;\n        grid-template-areas: \"Name Overview Overview \" \"FounderSpace FounderSpace Actions \" \"FounderSpace FounderSpace Agenda \" \"CheatSheet CheatSheet Agenda \"; }\n        .Specialty-Card.front > .Specialty-fg > .Content > .Name {\n          position: relative;\n          top: -3px;\n          left: 2px;\n          font-size: 18px;\n          color: white;\n          font-style: italic;\n          text-shadow: 2px 2px 0px rgba(0, 0, 0, 0.25); }\n        .Specialty-Card.front > .Specialty-fg > .Content > .Overview {\n          grid-area: Overview;\n          margin-left: 30px; }\n          .Specialty-Card.front > .Specialty-fg > .Content > .Overview > .Blurb {\n            display: inline-block;\n            font-size: 10px; }\n        .Specialty-Card.front > .Specialty-fg > .Content > .FounderSpace {\n          grid-area: FounderSpace;\n          position: relative;\n          height: 2.5in;\n          width: 3.5in;\n          border: 3px dashed;\n          border-radius: 0.1in;\n          color: #888;\n          opacity: 0.5;\n          margin: 5px;\n          margin-right: 15px; }\n          .Specialty-Card.front > .Specialty-fg > .Content > .FounderSpace > .PlaceHere {\n            position: absolute;\n            text-transform: uppercase;\n            text-align: center;\n            font-size: 18px;\n            top: 50%;\n            left: 50%;\n            transform: translate(-50%, -50%) rotate(-30deg); }\n        .Specialty-Card.front > .Specialty-fg > .Content > .Actions {\n          grid-area: Actions;\n          border-bottom: 1px solid rgba(128, 128, 128, 0.5);\n          padding-bottom: 10px;\n          margin-bottom: 10px;\n          margin-right: 10px; }\n          .Specialty-Card.front > .Specialty-fg > .Content > .Actions > .ActionList {\n            position: relative;\n            z-index: 1;\n            margin-bottom: 5px; }\n            .Specialty-Card.front > .Specialty-fg > .Content > .Actions > .ActionList > .List {\n              padding-left: 2px;\n              font-size: 12px; }\n        .Specialty-Card.front > .Specialty-fg > .Content > .Agenda {\n          grid-area: Agenda; }\n          .Specialty-Card.front > .Specialty-fg > .Content > .Agenda ol {\n            padding-left: 15px; }\n            .Specialty-Card.front > .Specialty-fg > .Content > .Agenda ol ul {\n              padding-left: 10px; }\n        .Specialty-Card.front > .Specialty-fg > .Content > .CheatSheet {\n          grid-area: CheatSheet;\n          margin: 0px 5px;\n          padding-left: 5px; }\n          .Specialty-Card.front > .Specialty-fg > .Content > .CheatSheet > .Symbols > .List {\n            line-height: 18px;\n            columns: 2; }\n          .Specialty-Card.front > .Specialty-fg > .Content > .CheatSheet > .WorkerVenn {\n            display: none;\n            margin-top: 10px;\n            background-image: url(\"/css-resources/assets/Illustration/WorkerHeirarchy.svg\");\n            background-size: contain;\n            height: 75px;\n            width: 125px;\n            background-color: rgba(255, 255, 255, 0.5);\n            border-radius: 3px; }\n  .Specialty-Card.back {\n    background-color: black; }\n\n.PrintSheet.agenda > .Agenda-Card {\n  margin: .125in; }\n\n/*# sourceMappingURL=/css-sourcemaps/66406aa4.map */")
});
___scope___.file("Registry/sheets/json/Synergy - Specialties.json", function(exports, require, module, __filename, __dirname){

module.exports = [
  {
    "id": "allPlayers",
    "Name": 0,
    "Actions": "Play any card\nMove your tasks to *projects* \nDraw a *Character*, *Project*, or *Option* card for **1** {{TASK}}\n*Manage a project* for **1** {{TASK}}\n*Spec-out and manage* a project for **3** {{TASK}}\n*Acquire new specialty* for **1** {{TASK}} over **3** *quarters*\n*Fire an employee* and take **1** {{BLAME}}\n*Move desks* and take **1** {{TASK}} from each {{EMEE}} moved",
    "Overview": 0
  },
  {
    "id": "exampleSpecialty",
    "Name": "Common Example",
    "Actions": "**Talentless**: For details on special actions, see *Specialty Placards*",
    "Overview": "The example specialty exists to show off common actions."
  },
  {
    "id": "humanResources",
    "Name": "Human Resources",
    "Actions": "**Mediate**: Remove **1** {{BLAME}} from any {{EMEE}} for **1** {{TASK}}\n**Graduate**: *Fire* {{EMEE}} with less {{CHA}} and get no {{BLAME}} for **1** {{TASK}}",
    "Overview": "Your specialty is resolving inter-personal conflicts, easing tensions and blame."
  },
  {
    "id": "designResearcher",
    "Name": "Design Researcher",
    "Actions": "**Brainstorm**: After *events phase*, draw one free *project*\n**Paradigm Shift**: Replace _all_ *projects* in hand for **2** {{TASK}}",
    "Overview": "Your specialty is digging into all the angles to find better projects quicker."
  },
  {
    "id": "recruiter",
    "Name": "Recruiter",
    "Actions": "**Network**: After *events phase*, draw one free *character*\n***Deep Screen***: Draw *quirk* before *hiring* for **1** {{TASK}}",
    "Overview": "Your specialty is knowing where to find the right people, leading to better employees overall."
  },
  {
    "id": "cultureChampion",
    "Name": "Culture Champion",
    "Actions": "**Explore**: After *events phase*, draw one free *option*\n**Remodel**: Move any number of desks for **2** {{TASK}}",
    "Overview": "Your specialty is keeping the overall vibe going well.  It's all about Synergy, man."
  },
  {
    "id": "projectManager",
    "Name": "Project Manager",
    "Actions": "**Wrangle**:  *Manage one project* for free each *quarter*\n**Translate**: *Spec-out an Manage* for only **2** {{TASK}}",
    "Overview": "Your specialty is getting things done by having more effecient task management."
  },
  {
    "id": "accountant",
    "Name": "Accountant",
    "Actions": "**Deducticate**: During *payroll phase*, save 1k per *worker*\n",
    "Overview": "Your specialty is navigating finances and saving money."
  }
];
});
___scope___.file("Entities/Decks/Placeable/PlaceableDeck.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Placeable_1 = require("./Placeable");
const Deck_1 = require("../Deck");
const PlaceableSheet = require("../../../Registry/sheets/json/Synergy - Placeables.json");
const PlaceableList = [];
exports.PlaceableList = PlaceableList;
PlaceableSheet.forEach((card) => {
    const raw = {
        id: card["id"],
        name: card["Name"],
        effect: card["Effect"],
        type: card["Type"],
        cost: card["Upkeep Cost"],
    };
    PlaceableList.push(new Placeable_1.default(raw));
});
class PlaceableDeck extends Deck_1.default {
    constructor(parent) {
        super(parent, () => PlaceableList, Placeable_1.default.getBackNode);
    }
}
exports.default = PlaceableDeck;
//# sourceMappingURL=PlaceableDeck.js.map
});
___scope___.file("Entities/Decks/Placeable/Placeable.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const el_tool_1 = require("el-tool");
const Util_1 = require("../../../Util/");
require("./placeable.scss");
class Placeable {
    constructor(raw) {
        this.raw = raw;
        this.domNode = el_tool_1.div(`Placeable-Card front ${raw.id}`, [
            el_tool_1.div("Topbar", [
                el_tool_1.div("Name", raw.name),
                el_tool_1.div("Effect", Util_1.parseRawText(raw.effect)),
                raw.cost ? el_tool_1.div("Cost", raw.cost + "k") : null,
            ]),
        ]);
    }
    static getBackNode() {
        return el_tool_1.div("Placeable-Card back", "Placeable Card");
    }
}
exports.default = Placeable;
//# sourceMappingURL=Placeable.js.map
});
___scope___.file("Entities/Decks/Placeable/placeable.scss", function(exports, require, module, __filename, __dirname){


require("fuse-box-css")("default/Entities/Decks/Placeable/placeable.scss", ".Placeable-Card {\n  position: relative;\n  height: 2in;\n  width: 2in;\n  border-radius: 0.1in;\n  overflow: hidden;\n  display: inline-block; }\n  .Placeable-Card.front {\n    background-size: contain;\n    background-position: bottom center;\n    font-size: 12px; }\n    .Placeable-Card.front.cat {\n      height: 1in;\n      width: 1.5in;\n      background-image: url(\"/css-resources/assets/Placeable/Cat.svg\"); }\n    .Placeable-Card.front.dog {\n      height: 1.5in;\n      width: 2in;\n      background-image: url(\"/css-resources/assets/Placeable/Dog.svg\"); }\n    .Placeable-Card.front.fish {\n      height: 1in;\n      width: 2.5in;\n      background-image: url(\"/css-resources/assets/Placeable/Fish.svg\"); }\n    .Placeable-Card.front.couch {\n      height: 1.5in;\n      width: 3.5in;\n      background-image: url(\"/css-resources/assets/Placeable/Couch.svg\"); }\n    .Placeable-Card.front.arcade {\n      height: 1.5in;\n      width: 1.5in;\n      background-image: url(\"/css-resources/assets/Placeable/ArcadeCabinet.svg\"); }\n      .Placeable-Card.front.arcade > .Topbar {\n        flex-direction: column; }\n    .Placeable-Card.front.beanbag {\n      height: 2in;\n      width: 2in;\n      background-image: url(\"/css-resources/assets/Placeable/Beanbag.svg\"); }\n    .Placeable-Card.front > .Topbar {\n      display: flex; }\n      .Placeable-Card.front > .Topbar > * {\n        padding: 2px;\n        background: rgba(255, 255, 255, 0.8);\n        border-radius: 2px;\n        margin: 3px; }\n      .Placeable-Card.front > .Topbar > .Effect {\n        flex: 1;\n        text-align: center; }\n  .Placeable-Card.back {\n    background-size: contain;\n    background-position: center center;\n    background-color: white;\n    text-align: center;\n    padding-top: 170px;\n    font-size: 22px;\n    color: #4a4a4a; }\n\n/*# sourceMappingURL=/css-sourcemaps/02d8729c2.map */")
});
___scope___.file("Registry/sheets/json/Synergy - Placeables.json", function(exports, require, module, __filename, __dirname){

module.exports = [
  {
    "id": "dog",
    "Name": "Doggo",
    "Effect": "**3** {{TASK}} = SYN+2",
    "Type": "pet",
    "Upkeep Cost": 3
  },
  {
    "id": "cat",
    "Name": "Cat",
    "Effect": "**2** {{TASK}} = SYN+1",
    "Type": "pet",
    "Upkeep Cost": 2
  },
  {
    "id": "fish",
    "Name": "Aquarium",
    "Effect": "**3** {{TASK}} = SYN+1",
    "Type": "pet",
    "Upkeep Cost": 1
  },
  {
    "id": "couch",
    "Name": "Couch",
    "Effect": "**2** {{TASK}} from any {{EMEE}} =  remove **1** {{BLAME}} from any {{EMEE}}",
    "Type": "furniture",
    "Upkeep Cost": 0
  },
  {
    "id": "beanbag",
    "Name": "Bean Bag",
    "Effect": "**4** {{TASK}} = SYN+1",
    "Type": "furniture",
    "Upkeep Cost": 0
  },
  {
    "id": "arcade",
    "Name": "Arcade Cabinet",
    "Effect": "**5** {{TASK}} = remove {{BLAME}} from any *worker*.",
    "Type": "furniture",
    "Upkeep Cost": 0
  },
  {
    "id": "sodMound",
    "Name": "Sod Mound",
    "Effect": "**4** {{TASK}} = SYN+1",
    "Type": "furniture",
    "Upkeep Cost": 0
  }
];
});
___scope___.file("UI/PrintSheets/printSheet.scss", function(exports, require, module, __filename, __dirname){


require("fuse-box-css")("default/UI/PrintSheets/printSheet.scss", ".Print-Sheet {\n  background: white;\n  page-break-inside: avoid; }\n  .Print-Sheet.quirk {\n    background: #f9f9f9; }\n  .Print-Sheet > * {\n    vertical-align: top;\n    border-radius: 0px; }\n\n/*# sourceMappingURL=/css-sourcemaps/0614084bd.map */")
});
___scope___.file("UI/ExportPays/ExportPays.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ProjectDeck_1 = require("../../Entities/Decks/Project/ProjectDeck");
const el_tool_1 = require("el-tool");
function ExportPays() {
    return el_tool_1.div("PayList", {
        hackableHTML: ProjectDeck_1.ProjectList.map((proj) => proj.pay).join("<br>"),
    });
}
exports.default = ExportPays;
//# sourceMappingURL=ExportPays.js.map
});
___scope___.file("Synergy.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const el_tool_1 = require("el-tool");
const vanilla_observables_1 = require("vanilla-observables");
const Setup_1 = require("./UI/Setup");
const omject_1 = require("omject");
const Decks_1 = require("./Entities/Decks");
const Board_1 = require("./UI/Board/Board");
require("./game.scss");
class Synergy extends vanilla_observables_1.BasicObservableClass {
    constructor() {
        super();
        this.omjectId = "Synergy";
        this.omjectAug = new omject_1.OmjectAug(this, null);
        this.parent = omject_1.parentFn(this);
        // Observable
        this.modalView = this.observable("modalView");
        this.projectDeck = this.observable("projectDeck");
        // Depos
        this.dragDepo = {};
        this.projectDeck.set(new Decks_1.ProjectDeck(this));
        this.employeeDeck = new Decks_1.EmployeeDeck(this);
        this.eventDeck = new Decks_1.EventCardDeck(this);
        this.optionDeck = new Decks_1.OptionCardDeck(this);
        this.agendaDeck = new Decks_1.AgendaDeck(this);
        this.quirkDeck = new Decks_1.QuirkDeck(this);
        this.subNodes = {};
        const nodes = this.subNodes;
        this.domNode = el_tool_1.div("Game", [
            nodes.modals = el_tool_1.div("Modals"),
            el_tool_1.div("PlayArea", [
                nodes.table = el_tool_1.div("Table", {
                    onClick: (event) => {
                        for (let ptr = event.target; !!ptr; ptr = ptr.parentNode) {
                            if (ptr.className && ptr.className.indexOf("Card") !== -1) {
                                return;
                            }
                        }
                        const classList = nodes.table.classList;
                        classList.contains("--zoomed_out") ? classList.remove("--zoomed_out") : classList.add("--zoomed_out");
                    },
                }, [
                    el_tool_1.div("Decks", [
                        this.employeeDeck.domNode,
                        this.projectDeck().domNode,
                        this.eventDeck.domNode,
                        this.optionDeck.domNode,
                        this.agendaDeck.domNode,
                        this.quirkDeck.domNode,
                    ]),
                    (this.board = new Board_1.default(this)).domNode,
                ]),
            ]),
            nodes.drawnCards = el_tool_1.div("DrawnCards"),
        ]);
        this.modalView.observe((view) => {
            let viewEl;
            if (view === "setup") {
                const setup = this.setup = (this.setup || new Setup_1.default(this));
                viewEl = setup.domNode;
            }
            this.subNodes.modals.style.display = viewEl ? "" : "none";
            el_tool_1.setInnards(this.subNodes.modals, [viewEl]);
        });
        // this.modalView.set("setup");
    }
    onBirth() { }
}
exports.default = Synergy;
//# sourceMappingURL=app.js.map?tm=1547182187717
});
___scope___.file("UI/Setup/index.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var SetupGame_1 = require("./SetupGame");
exports.default = SetupGame_1.default;
//# sourceMappingURL=index.js.map
});
___scope___.file("UI/Setup/SetupGame.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const el_tool_1 = require("el-tool");
const omject_1 = require("omject");
const Synergy_1 = require("../../Synergy");
require("./setup.scss");
class SetupGame extends omject_1.BasicOmjectClass {
    constructor(parent) {
        super(parent);
        this.game = this.getAncestor(Synergy_1.default);
        this.domNode = el_tool_1.div("SetupGame");
        this.views = {};
        this.updateUI();
    }
    updateUI() {
        let view;
        const players = this.game.players;
        console.log(players);
        if (players === undefined) {
            view = this.getnitNumPlayers();
        }
        else if (players.findIndex((player) => !player) !== -1) {
            view = this.getnitChooseCharacter();
        }
        else {
            this.game.modalView.set("none");
            return;
        }
        el_tool_1.setInnards(this.domNode, [view]);
    }
    getnitNumPlayers() {
        if (this.views.numPlayers) {
            return this.views.numPlayers;
        }
        const createSelection = (num) => el_tool_1.div("Num", {
            onClick: () => {
                this.game.players = ((new Array(num)).fill(undefined));
                this.updateUI();
            }
        }, num + "");
        return this.views.numPlayers = el_tool_1.div("SelectNumPlayers", [
            el_tool_1.div("Directions", "Select number of players"),
            el_tool_1.div("Selections", [
                createSelection(1),
                createSelection(2),
                createSelection(3),
                createSelection(4),
            ]),
        ]);
    }
    getnitChooseCharacter() {
        if (this.views.chooseCharacter) {
            return this.views.chooseCharacter;
        }
        const { game } = this;
        const choices = game.players.map((player) => {
            if (player !== undefined) {
                return null;
            }
            return game.employeeDeck.draw(3);
        });
        const chooseCharacter = el_tool_1.div("ChooseCharacter");
        const setPlayerCharacter = (playerNum, character) => {
            game.players[playerNum] = character;
        };
        const updateChoices = () => {
            const playerNum = choices.findIndex((choice) => !!choice);
            if (playerNum !== -1) {
                const chooseFrom = choices[playerNum];
                el_tool_1.setInnards(chooseCharacter, chooseFrom.map((employee) => el_tool_1.div("SelectEmployee", {
                    onClick: () => {
                        choices[playerNum] = undefined;
                        game.employeeDeck.placeCardsAtBottom(chooseFrom);
                        setPlayerCharacter(playerNum, employee);
                        updateChoices();
                    }
                }, [
                    employee.domNode
                ])));
            }
            else {
                game.employeeDeck.shuffle();
                this.updateUI();
            }
        };
        updateChoices();
        return this.views.chooseCharacter = chooseCharacter;
    }
}
exports.default = SetupGame;
//# sourceMappingURL=SetupGame.js.map
});
___scope___.file("UI/Setup/setup.scss", function(exports, require, module, __filename, __dirname){


require("fuse-box-css")("default/UI/Setup/setup.scss", ".SetupGame > .SelectNumPlayers > .Directions {\n  font-size: 30px; }\n\n.SetupGame > .SelectNumPlayers > .Selections {\n  display: grid;\n  grid-template-rows: auto auto;\n  grid-template-columns: auto auto; }\n  .SetupGame > .SelectNumPlayers > .Selections > .Num {\n    cursor: pointer;\n    display: flex;\n    border: 1px solid black;\n    font-size: 40px;\n    text-align: center;\n    height: 100px;\n    width: 100px;\n    border-radius: 5px;\n    margin: 20px;\n    align-items: center;\n    justify-content: center; }\n\n/*# sourceMappingURL=/css-sourcemaps/769da8e.map */")
});
___scope___.file("Entities/Decks/index.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var EmployeeDeck_1 = require("./Employee/EmployeeDeck");
exports.EmployeeDeck = EmployeeDeck_1.default;
var ProjectDeck_1 = require("./Project/ProjectDeck");
exports.ProjectDeck = ProjectDeck_1.default;
var OptionCardDeck_1 = require("./Option/OptionCardDeck");
exports.OptionCardDeck = OptionCardDeck_1.default;
var EventCardDeck_1 = require("./Event/EventCardDeck");
exports.EventCardDeck = EventCardDeck_1.default;
var AgendaDeck_1 = require("./Agenda/AgendaDeck");
exports.AgendaDeck = AgendaDeck_1.default;
var QuirkDeck_1 = require("./Quirk/QuirkDeck");
exports.QuirkDeck = QuirkDeck_1.default;
var SpecialtyDeck_1 = require("./Specialty/SpecialtyDeck");
exports.SpecialtyDeck = SpecialtyDeck_1.default;
//# sourceMappingURL=index.js.map
});
___scope___.file("Entities/Decks/Agenda/AgendaDeck.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Agenda_1 = require("./Agenda");
const Deck_1 = require("../Deck");
const AgendaSheet = require("../../../Registry/sheets/json/Synergy - Agendas.json");
const AgendaList = [];
exports.AgendaList = AgendaList;
AgendaSheet.forEach((card) => {
    const matchedItems = [];
    let footers = "";
    const description = card.Description.replace(/hiree|subtract/ig, (match) => {
        const keyword = match.toLowerCase();
        if (matchedItems.includes(keyword) === false) {
            if (matchedItems.length > 0) {
                footers += "\n";
            }
            matchedItems.push(keyword);
            const footerSymbol = (new Array(matchedItems.length)).fill("*").join("");
            footers += footerSymbol;
            if (keyword === "hiree") {
                footers += "Players do not count as hirees.  Terminated hirees still count.";
            }
            else if (keyword === "subtract") {
                footers += "Minimum PVP from this card is zero";
            }
            // return match + footerSymbol;
        }
        return match;
    });
    // const hireeIndex = description.indexOf("hiree");
    // const subtractIndex = description.indexOf("subtract");
    // if (hireeIndex !== -1 || subtractIndex !== -1)
    const raw = {
        name: card["Name"],
        description,
        footers,
    };
    AgendaList.push(new Agenda_1.default(raw));
});
class AgendaDeck extends Deck_1.default {
    constructor(parent) {
        super(parent, () => AgendaList, Agenda_1.default.getBackNode);
    }
}
exports.default = AgendaDeck;
//# sourceMappingURL=AgendaDeck.js.map
});
___scope___.file("Entities/Decks/Agenda/Agenda.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const el_tool_1 = require("el-tool");
require("./agenda.scss");
class Project {
    constructor(raw) {
        this.raw = raw;
        this.domNode = el_tool_1.div("Agenda-Card front", [
            el_tool_1.div("Name", raw.name),
            el_tool_1.div("Description", raw.description),
            el_tool_1.div("Footers", { hackableHTML: raw.footers.replace(/\n/g, "<br>") }),
        ]);
    }
    static getBackNode() {
        return el_tool_1.div("Agenda-Card back", "AGENDA CARD");
    }
}
exports.default = Project;
//# sourceMappingURL=Agenda.js.map
});
___scope___.file("Entities/Decks/Agenda/agenda.scss", function(exports, require, module, __filename, __dirname){


require("fuse-box-css")("default/Entities/Decks/Agenda/agenda.scss", "\n/*# sourceMappingURL=/css-sourcemaps/02786983e.map */")
});
___scope___.file("Registry/sheets/json/Synergy - Agendas.json", function(exports, require, module, __filename, __dirname){

module.exports = [
  {
    "Name": "Aquaholic",
    "Description": "For each hiree with aquatic onesies, 10 PVP",
    "PVP (Personal Victory Points)": 0,
    "hidePlayerCardsInfo": 0
  },
  {
    "Name": "Team Preference",
    "Description": "For each hiree with your team color, 3 PVP",
    "PVP (Personal Victory Points)": 0,
    "hidePlayerCardsInfo": 0
  },
  {
    "Name": "One of a kind",
    "Description": "Starting at 16 PVP, subtract 4 PVP for each hiree of your team color (min 0)",
    "PVP (Personal Victory Points)": 0,
    "hidePlayerCardsInfo": 0
  },
  {
    "Name": "Gotta be the best",
    "Description": "You can choose your best quality at the end.  Starting at 20 PVP, subtract 5 PVP for every hiree who has that quality at an equal or greater amount. (min 0)",
    "PVP (Personal Victory Points)": 0,
    "hidePlayerCardsInfo": 0
  },
  {
    "Name": "Straight up luck",
    "Description": "During PVP count, roll a d20.  If you roll 20, you get 15 PVP.  Otherwise divide the number by 2 and round down, that is your PVP",
    "PVP (Personal Victory Points)": 0,
    "hidePlayerCardsInfo": true
  },
  {
    "Name": "Some new skills",
    "Description": "For each skill you've picked up during gameplay, add 7 PVP",
    "PVP (Personal Victory Points)": 0,
    "hidePlayerCardsInfo": true
  },
  {
    "Name": "Tank it",
    "Description": "Starting at 20 PVP, substract 2 PVP for each quarter the company makes it after YR 2 (min 0)",
    "PVP (Personal Victory Points)": 0,
    "hidePlayerCardsInfo": 0
  },
  {
    "Name": "Eye of Bull",
    "Description": "Starting at 15PVP, subtract 3PVP for each employee with a red mouthed onesie.",
    "PVP (Personal Victory Points)": 0,
    "hidePlayerCardsInfo": 0
  }
];
});
___scope___.file("game.scss", function(exports, require, module, __filename, __dirname){


require("fuse-box-css")("default/game.scss", ".Game {\n  display: flex;\n  max-height: 100vh;\n  overflow: hidden; }\n  .Game > .Modals {\n    display: flex;\n    align-items: center;\n    justify-content: center;\n    position: fixed;\n    z-index: 10;\n    top: 0px;\n    left: 0px;\n    right: 0px;\n    bottom: 0px;\n    background: rgba(0, 0, 0, 0.5); }\n    .Game > .Modals > * {\n      background: #EEE;\n      padding: 20px; }\n  .Game > .PlayArea {\n    flex: 1;\n    overflow: scroll;\n    max-height: 100vh;\n    background: #333; }\n    .Game > .PlayArea > .Table {\n      min-width: 35in;\n      background: #4b433b;\n      border-radius: 10px;\n      background-size: 50%;\n      cursor: zoom-out;\n      display: flex; }\n      .Game > .PlayArea > .Table.--zoomed_out {\n        cursor: zoom-in;\n        transform: scale(0.5); }\n      .Game > .PlayArea > .Table > .Decks {\n        margin: 1in; }\n        .Game > .PlayArea > .Table > .Decks > .Deck {\n          cursor: pointer;\n          display: inline-block; }\n  .Game > .DrawnCards {\n    flex-basis: 3.75in;\n    flex-grow: 0;\n    flex-shrink: 1;\n    overflow-y: auto; }\n    .Game > .DrawnCards > * {\n      box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.2);\n      margin: 10px; }\n    .Game > .DrawnCards > .Project-Card, .Game > .DrawnCards > .Employee-Card {\n      display: inline-grid;\n      cursor: move; }\n\n/*# sourceMappingURL=/css-sourcemaps/38f95d2c.map */")
});
___scope___.file("main.scss", function(exports, require, module, __filename, __dirname){


require("fuse-box-css")("default/main.scss", "html, body {\n  margin: 0px;\n  padding: 0px;\n  min-height: 100vh; }\n\nbody {\n  box-sizing: border-box;\n  font-family: Roboto;\n  color: #333; }\n\n*, *::before, *::after {\n  box-sizing: inherit; }\n\np {\n  padding: 0px;\n  margin: 0px; }\n\nul, ol {\n  margin: 0px;\n  text-indent: 0px;\n  padding-left: 30px; }\n  ul ul, ul ol, ol ul, ol ol {\n    padding-left: 10px; }\n\n.Icon {\n  display: inline-flex;\n  align-items: center;\n  justify-content: center;\n  text-indent: 0px;\n  height: 1.5em;\n  width: 1.5em;\n  margin: -5px 0px -5px 0px;\n  border-radius: 2px;\n  color: transparent;\n  background-size: contain; }\n  .Icon.art {\n    background-image: url(\"/css-resources/assets/Icon/Skill/Art.svg\"); }\n  .Icon.log {\n    background-image: url(\"/css-resources/assets/Icon/Skill/Logic.svg\"); }\n  .Icon.cha {\n    background-image: url(\"/css-resources/assets/Icon/Skill/Charisma.svg\"); }\n  .Icon.pat {\n    background-image: url(\"/css-resources/assets/Icon/Skill/Patience.svg\"); }\n  .Icon.emee {\n    background-image: url(\"/css-resources/assets/Icon/Employee.svg\"); }\n  .Icon.task {\n    background-image: url(\"/css-resources/assets/Icon/Token/Task.svg\"); }\n  .Icon.blame {\n    background-image: url(\"/css-resources/assets/Icon/Token/Blame.svg\"); }\n  .Icon.touch {\n    background-image: url(\"/css-resources/assets/Icon/Token/Touch.svg\"); }\n  .Icon.d20 {\n    background-image: url(\"/css-resources/assets/Icon/D20.svg\"); }\n  .Icon.beside {\n    background-image: url(\"/css-resources/assets/Icon/Direction/Beside.svg\"); }\n  .Icon.adjacent {\n    background-image: url(\"/css-resources/assets/Icon/Direction/Adjacent.svg\"); }\n  .Icon.repeat {\n    background-image: url(\"/css-resources/assets/Icon/Direction/Repeat.svg\"); }\n  .Icon.common {\n    background-image: url(\"/css-resources/assets/Icon/Quality/Common.svg\"); }\n  .Icon.alluring {\n    background-image: url(\"/css-resources/assets/Icon/Quality/Alluring.svg\"); }\n  .Icon.impulsive {\n    background-image: url(\"/css-resources/assets/Icon/Quality/Impulsive.svg\"); }\n  .Icon.messy {\n    background-image: url(\"/css-resources/assets/Icon/Quality/Messy.svg\"); }\n  .Icon.synergy {\n    background-image: url(\"/css-resources/assets/Icon/SynergyLightning.png\"); }\n\n@font-face {\n  font-family: 'Cedarville-Cursive';\n  src: url(\"/css-resources/assets/Fonts/Cedarville_Cursive/Cedarville-Cursive.ttf\") format(\"truetype\"); }\n\n.--font_Cedarville-Cursive {\n  font-family: Cedarville-Cursive; }\n\n@font-face {\n  font-family: 'Gloria-Hallelujah';\n  src: url(\"/css-resources/assets/Fonts/Gloria_Hallelujah/GloriaHallelujah.ttf\") format(\"truetype\"); }\n\n.--font_Gloria-Hallelujah {\n  font-family: Gloria-Hallelujah; }\n\n@font-face {\n  font-family: 'Gochi-Hand';\n  src: url(\"/css-resources/assets/Fonts/Gochi_Hand/GochiHand-Regular.ttf\") format(\"truetype\"); }\n\n.--font_Gochi-Hand {\n  font-family: Gochi-Hand; }\n\n@font-face {\n  font-family: 'Just-Another-Hand';\n  src: url(\"/css-resources/assets/Fonts/Just_Another_Hand/JustAnotherHand-Regular.ttf\") format(\"truetype\"); }\n\n.--font_Just-Another-Hand {\n  font-family: Just-Another-Hand; }\n\n@font-face {\n  font-family: 'Kalam';\n  src: url(\"/css-resources/assets/Fonts/Kalam/Kalam-Regular.ttf\") format(\"truetype\"); }\n\n.--font_Kalam {\n  font-family: Kalam; }\n\n@font-face {\n  font-family: 'La-Belle-Aurore';\n  src: url(\"/css-resources/assets/Fonts/La_Belle_Aurore/LaBelleAurore.ttf\") format(\"truetype\"); }\n\n.--font_La-Belle-Aurore {\n  font-family: La-Belle-Aurore; }\n\n@font-face {\n  font-family: 'Nothing-You-Could-Do';\n  src: url(\"/css-resources/assets/Fonts/Nothing_You_Could_Do/NothingYouCouldDo.ttf\") format(\"truetype\"); }\n\n.--font_Nothing-You-Could-Do {\n  font-family: Nothing-You-Could-Do; }\n\n@font-face {\n  font-family: 'Over-the-Rainbow';\n  src: url(\"/css-resources/assets/Fonts/Over_the_Rainbow/OvertheRainbow.ttf\") format(\"truetype\"); }\n\n.--font_Over-the-Rainbow {\n  font-family: Over-the-Rainbow; }\n\n@font-face {\n  font-family: 'Reenie-Beanie';\n  src: url(\"/css-resources/assets/Fonts/Reenie_Beanie/ReenieBeanie.ttf\") format(\"truetype\"); }\n\n.--font_Reenie-Beanie {\n  font-family: Reenie-Beanie; }\n\n@font-face {\n  font-family: 'Rock-Salt';\n  src: url(\"/css-resources/assets/Fonts/Rock_Salt/RockSalt-Regular.ttf\") format(\"truetype\"); }\n\n.--font_Rock-Salt {\n  font-family: Rock-Salt; }\n\n@font-face {\n  font-family: 'Shadows-Into-Light';\n  src: url(\"/css-resources/assets/Fonts/Shadows_Into_Light/ShadowsIntoLight.ttf\") format(\"truetype\"); }\n\n.--font_Shadows-Into-Light {\n  font-family: Shadows-Into-Light; }\n\n@font-face {\n  font-family: 'Tangerine';\n  src: url(\"/css-resources/assets/Fonts/Tangerine/Tangerine-Regular.ttf\") format(\"truetype\"); }\n\n.--font_Tangerine {\n  font-family: Tangerine; }\n\n@font-face {\n  font-family: 'Walter-Turncoat';\n  src: url(\"/css-resources/assets/Fonts/Walter_Turncoat/WalterTurncoat-Regular.ttf\") format(\"truetype\"); }\n\n.--font_Walter-Turncoat {\n  font-family: Walter-Turncoat; }\n\n@font-face {\n  font-family: 'Roboto';\n  font-weight: 100;\n  src: url(\"/css-resources/assets/Fonts/Roboto/Roboto-Thin.ttf\") format(\"truetype\"); }\n\n@font-face {\n  font-family: 'Roboto';\n  font-weight: 300;\n  src: url(\"/css-resources/assets/Fonts/Roboto/Roboto-Light.ttf\") format(\"truetype\"); }\n\n@font-face {\n  font-family: 'Roboto';\n  src: url(\"/css-resources/assets/Fonts/Roboto/Roboto-Regular.ttf\") format(\"truetype\"); }\n\n@font-face {\n  font-family: 'Roboto';\n  font-weight: 500;\n  src: url(\"/css-resources/assets/Fonts/Roboto/Roboto-Medium.ttf\") format(\"truetype\"); }\n\n@font-face {\n  font-family: 'Roboto';\n  font-weight: 700;\n  src: url(\"/css-resources/assets/Fonts/Roboto/Roboto-Bold.ttf\") format(\"truetype\"); }\n\n@font-face {\n  font-family: 'Roboto';\n  font-style: italic;\n  font-weight: 100;\n  src: url(\"/css-resources/assets/Fonts/Roboto/Roboto-ThinItalic.ttf\") format(\"truetype\"); }\n\n@font-face {\n  font-family: 'Roboto';\n  font-style: italic;\n  font-weight: 300;\n  src: url(\"/css-resources/assets/Fonts/Roboto/Roboto-LightItalic.ttf\") format(\"truetype\"); }\n\n@font-face {\n  font-family: 'Roboto';\n  font-style: italic;\n  src: url(\"/css-resources/assets/Fonts/Roboto/Roboto-Italic.ttf\") format(\"truetype\"); }\n\n@font-face {\n  font-family: 'Roboto';\n  font-style: italic;\n  font-weight: 500;\n  src: url(\"/css-resources/assets/Fonts/Roboto/Roboto-MediumItalic.ttf\") format(\"truetype\"); }\n\n@font-face {\n  font-family: 'Roboto';\n  font-style: italic;\n  font-weight: 700;\n  src: url(\"/css-resources/assets/Fonts/Roboto/Roboto-BoldItalic.ttf\") format(\"truetype\"); }\n\n/*# sourceMappingURL=/css-sourcemaps/011a9583b.map */")
});
return ___scope___.entry = "index.js";
});
FuseBox.pkg("el-tool", {}, function(___scope___){
___scope___.file("src/index.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const El = require("./El");
tslib_1.__exportStar(require("./El"), exports);
exports.default = El;
//# sourceMappingURL=index.js.map
});
___scope___.file("src/El.js", function(exports, require, module, __filename, __dirname){

"use strict";
/**************************
*. BASIC HELPER FUNCTIONS
***************************/
Object.defineProperty(exports, "__esModule", { value: true });
function byId(id) { return document.getElementById(id); }
exports.byId = byId;
// adds and removes classes.  can take a string with spaces for mutiple classes
function classSplice(element, removeClasses, addClasses) {
    if (removeClasses) {
        const remList = Array.isArray(removeClasses) ? removeClasses : removeClasses.split(/\s+/g);
        remList.forEach((className) => element.classList.remove(className));
    }
    if (addClasses) {
        const addList = Array.isArray(addClasses) ? addClasses : addClasses.split(/\s+/g);
        addList.forEach((className) => element.classList.add(className));
    }
    return element;
}
exports.classSplice = classSplice;
function addClass(element, addClasses) {
    return classSplice(element, undefined, addClasses);
}
exports.addClass = addClass;
// remove all children from element
function removeChildren(parent) {
    let child = parent.firstChild;
    while (!!child) {
        parent.removeChild(child);
        child = parent.firstChild;
    }
}
exports.removeChildren = removeChildren;
function setStyle(root, style) {
    if (typeof style === "string") {
        root.style.cssText = style;
    }
    else {
        Object.keys(style).forEach((styleName) => root.style[styleName] = style[styleName]);
    }
}
exports.setStyle = setStyle;
/**************************
*. TYPES CHECKS
***************************/
function isHTMLElement(testMe) {
    return testMe instanceof HTMLElement;
}
exports.isHTMLElement = isHTMLElement;
function isRawHTML(testMe) {
    return typeof testMe === "object" && testMe.hasOwnProperty("hackableHTML");
}
exports.isRawHTML = isRawHTML;
function isProps(testMe) {
    return typeof testMe === "object" && !Array.isArray(testMe) && !isHTMLElement(testMe) && !isRawHTML(testMe);
}
exports.isProps = isProps;
/*************************
*. MAIN CREATION FN
*************************/
function create(tagName, propsOrInnards, innards) {
    const input = standardizeInput(null, propsOrInnards, innards);
    return privateCreate(tagName, input.props, input.innards);
}
exports.create = create;
function standardizeInput(classNameOrProps, propsOrInnards, innards) {
    let props;
    if (classNameOrProps && isProps(classNameOrProps)) {
        props = classNameOrProps;
    }
    else if (propsOrInnards && isProps(propsOrInnards)) {
        props = propsOrInnards;
    }
    else {
        props = {};
    }
    if (propsOrInnards && !isProps(propsOrInnards)) {
        if (innards !== undefined) {
            throw new Error("Can not double define innards");
        }
        innards = propsOrInnards;
    }
    if (typeof classNameOrProps === "string") {
        props.className = (props.className ? props.className + " " : "") + classNameOrProps;
    }
    return { props, innards };
}
function privateCreate(tagName, props, innards) {
    const out = document.createElement(tagName);
    if (props) {
        // event stuff
        if (props.on) {
            const eventListeners = props.on || {};
            Object.keys(eventListeners).forEach((eventType) => out.addEventListener(eventType, eventListeners[eventType]));
        }
        if (props.onClick) {
            out.addEventListener("click", props.onClick);
        }
        if (props.onKeyDown) {
            out.addEventListener("keydown", props.onKeyDown);
        }
        if (props.onMouseDown) {
            out.addEventListener("mousedown", props.onMouseDown);
        }
        // element html props, and element js props
        if (props.attr) {
            const attrs = props.attr || {};
            Object.keys(attrs).forEach((attr) => out.setAttribute(attr, attrs[attr]));
        }
        if (props.this) {
            const thisProps = props.this || {};
            Object.keys(thisProps).forEach((prop) => out[prop] = thisProps[prop]);
        }
        // prop style content
        if (props.text) {
            out.textContent += props.text;
        }
        if (props.innerHTML) {
            out.innerHTML += props.innerHTML;
        }
        // classing
        const classList = props.classList || [];
        if (props.className) {
            classList.push(props.className);
        }
        if (props.class) {
            classList.push(props.class);
        }
        out.className = classList.join(" ");
        if (props.id) {
            out.id = props.id;
        }
        // inline styles
        if (props.style) {
            setStyle(out, props.style);
        }
        // innards alternative
        if (props.innards) {
            append(out, props.innards);
        }
    }
    // add all chidren, or innards
    if (innards) {
        append(out, innards);
    }
    // append to parent at the end, so multiple reflows are not triggered
    if (props) {
        if (props.appendTo) {
            props.appendTo.appendChild(out);
        }
        if (props.prependTo) {
            const parent = props.prependTo;
            parent.insertBefore(out, parent.firstChild);
        }
    }
    return out;
}
function setInnards(root, innards) {
    removeChildren(root);
    append(root, innards);
}
exports.setInnards = setInnards;
// Append stuff to an element.
// Takes A. a single string as innerText,
// or B. an array consisting of
// 1. strings, numbers for innerText
// 2. null, undefined for ignoring
// 3. a RawHTML object which ensures users know of vulnerabilities while modifying innerHTML
// 4. HTMLElements to appendChild
// 5. A function which returns any of the above
function append(root, innards) {
    if (innards === undefined || innards === null) {
        return;
    }
    while (typeof innards === "function") {
        innards = innards();
    }
    let childList = Array.isArray(innards) ? innards : [innards];
    childList.forEach((child) => {
        if (typeof child === "function") {
            child = child();
        }
        if (child === undefined || child === null) {
            return;
        }
        if (typeof child === "number") {
            child = child + "";
        }
        if (isRawHTML(child)) {
            root.innerHTML += child.hackableHTML;
        }
        else if (typeof child === "string") {
            root.appendChild(new Text(child));
        }
        else {
            root.appendChild(child);
        }
    });
}
exports.append = append;
/*****************************
* REGULAR CREATION SHORTHANDS
******************************/
function hr() { return document.createElement("hr"); }
exports.hr = hr;
function br() { return document.createElement("br"); }
exports.br = br;
function div(classNameOrProps, propsOrInnards, innards) {
    const input = standardizeInput(classNameOrProps, propsOrInnards, innards);
    return create("div", input.props, input.innards);
}
exports.div = div;
function span(classNameOrProps, propsOrInnards, innards) {
    const input = standardizeInput(classNameOrProps, propsOrInnards, innards);
    return create("span", input.props, input.innards);
}
exports.span = span;
function a(classNameOrProps, propsOrInnards, innards) {
    const input = standardizeInput(classNameOrProps, propsOrInnards, innards);
    input.props.attr = input.props.attr || {};
    input.props.attr.href = input.props.href;
    return create("a", input.props, input.innards);
}
exports.a = a;
//# sourceMappingURL=El.js.map
});
return ___scope___.entry = "src/index.js";
});
FuseBox.pkg("entities", {}, function(___scope___){
___scope___.file("maps/entities.json", function(exports, require, module, __filename, __dirname){

module.exports = {"Aacute":"\u00C1","aacute":"\u00E1","Abreve":"\u0102","abreve":"\u0103","ac":"\u223E","acd":"\u223F","acE":"\u223E\u0333","Acirc":"\u00C2","acirc":"\u00E2","acute":"\u00B4","Acy":"\u0410","acy":"\u0430","AElig":"\u00C6","aelig":"\u00E6","af":"\u2061","Afr":"\uD835\uDD04","afr":"\uD835\uDD1E","Agrave":"\u00C0","agrave":"\u00E0","alefsym":"\u2135","aleph":"\u2135","Alpha":"\u0391","alpha":"\u03B1","Amacr":"\u0100","amacr":"\u0101","amalg":"\u2A3F","amp":"&","AMP":"&","andand":"\u2A55","And":"\u2A53","and":"\u2227","andd":"\u2A5C","andslope":"\u2A58","andv":"\u2A5A","ang":"\u2220","ange":"\u29A4","angle":"\u2220","angmsdaa":"\u29A8","angmsdab":"\u29A9","angmsdac":"\u29AA","angmsdad":"\u29AB","angmsdae":"\u29AC","angmsdaf":"\u29AD","angmsdag":"\u29AE","angmsdah":"\u29AF","angmsd":"\u2221","angrt":"\u221F","angrtvb":"\u22BE","angrtvbd":"\u299D","angsph":"\u2222","angst":"\u00C5","angzarr":"\u237C","Aogon":"\u0104","aogon":"\u0105","Aopf":"\uD835\uDD38","aopf":"\uD835\uDD52","apacir":"\u2A6F","ap":"\u2248","apE":"\u2A70","ape":"\u224A","apid":"\u224B","apos":"'","ApplyFunction":"\u2061","approx":"\u2248","approxeq":"\u224A","Aring":"\u00C5","aring":"\u00E5","Ascr":"\uD835\uDC9C","ascr":"\uD835\uDCB6","Assign":"\u2254","ast":"*","asymp":"\u2248","asympeq":"\u224D","Atilde":"\u00C3","atilde":"\u00E3","Auml":"\u00C4","auml":"\u00E4","awconint":"\u2233","awint":"\u2A11","backcong":"\u224C","backepsilon":"\u03F6","backprime":"\u2035","backsim":"\u223D","backsimeq":"\u22CD","Backslash":"\u2216","Barv":"\u2AE7","barvee":"\u22BD","barwed":"\u2305","Barwed":"\u2306","barwedge":"\u2305","bbrk":"\u23B5","bbrktbrk":"\u23B6","bcong":"\u224C","Bcy":"\u0411","bcy":"\u0431","bdquo":"\u201E","becaus":"\u2235","because":"\u2235","Because":"\u2235","bemptyv":"\u29B0","bepsi":"\u03F6","bernou":"\u212C","Bernoullis":"\u212C","Beta":"\u0392","beta":"\u03B2","beth":"\u2136","between":"\u226C","Bfr":"\uD835\uDD05","bfr":"\uD835\uDD1F","bigcap":"\u22C2","bigcirc":"\u25EF","bigcup":"\u22C3","bigodot":"\u2A00","bigoplus":"\u2A01","bigotimes":"\u2A02","bigsqcup":"\u2A06","bigstar":"\u2605","bigtriangledown":"\u25BD","bigtriangleup":"\u25B3","biguplus":"\u2A04","bigvee":"\u22C1","bigwedge":"\u22C0","bkarow":"\u290D","blacklozenge":"\u29EB","blacksquare":"\u25AA","blacktriangle":"\u25B4","blacktriangledown":"\u25BE","blacktriangleleft":"\u25C2","blacktriangleright":"\u25B8","blank":"\u2423","blk12":"\u2592","blk14":"\u2591","blk34":"\u2593","block":"\u2588","bne":"=\u20E5","bnequiv":"\u2261\u20E5","bNot":"\u2AED","bnot":"\u2310","Bopf":"\uD835\uDD39","bopf":"\uD835\uDD53","bot":"\u22A5","bottom":"\u22A5","bowtie":"\u22C8","boxbox":"\u29C9","boxdl":"\u2510","boxdL":"\u2555","boxDl":"\u2556","boxDL":"\u2557","boxdr":"\u250C","boxdR":"\u2552","boxDr":"\u2553","boxDR":"\u2554","boxh":"\u2500","boxH":"\u2550","boxhd":"\u252C","boxHd":"\u2564","boxhD":"\u2565","boxHD":"\u2566","boxhu":"\u2534","boxHu":"\u2567","boxhU":"\u2568","boxHU":"\u2569","boxminus":"\u229F","boxplus":"\u229E","boxtimes":"\u22A0","boxul":"\u2518","boxuL":"\u255B","boxUl":"\u255C","boxUL":"\u255D","boxur":"\u2514","boxuR":"\u2558","boxUr":"\u2559","boxUR":"\u255A","boxv":"\u2502","boxV":"\u2551","boxvh":"\u253C","boxvH":"\u256A","boxVh":"\u256B","boxVH":"\u256C","boxvl":"\u2524","boxvL":"\u2561","boxVl":"\u2562","boxVL":"\u2563","boxvr":"\u251C","boxvR":"\u255E","boxVr":"\u255F","boxVR":"\u2560","bprime":"\u2035","breve":"\u02D8","Breve":"\u02D8","brvbar":"\u00A6","bscr":"\uD835\uDCB7","Bscr":"\u212C","bsemi":"\u204F","bsim":"\u223D","bsime":"\u22CD","bsolb":"\u29C5","bsol":"\\","bsolhsub":"\u27C8","bull":"\u2022","bullet":"\u2022","bump":"\u224E","bumpE":"\u2AAE","bumpe":"\u224F","Bumpeq":"\u224E","bumpeq":"\u224F","Cacute":"\u0106","cacute":"\u0107","capand":"\u2A44","capbrcup":"\u2A49","capcap":"\u2A4B","cap":"\u2229","Cap":"\u22D2","capcup":"\u2A47","capdot":"\u2A40","CapitalDifferentialD":"\u2145","caps":"\u2229\uFE00","caret":"\u2041","caron":"\u02C7","Cayleys":"\u212D","ccaps":"\u2A4D","Ccaron":"\u010C","ccaron":"\u010D","Ccedil":"\u00C7","ccedil":"\u00E7","Ccirc":"\u0108","ccirc":"\u0109","Cconint":"\u2230","ccups":"\u2A4C","ccupssm":"\u2A50","Cdot":"\u010A","cdot":"\u010B","cedil":"\u00B8","Cedilla":"\u00B8","cemptyv":"\u29B2","cent":"\u00A2","centerdot":"\u00B7","CenterDot":"\u00B7","cfr":"\uD835\uDD20","Cfr":"\u212D","CHcy":"\u0427","chcy":"\u0447","check":"\u2713","checkmark":"\u2713","Chi":"\u03A7","chi":"\u03C7","circ":"\u02C6","circeq":"\u2257","circlearrowleft":"\u21BA","circlearrowright":"\u21BB","circledast":"\u229B","circledcirc":"\u229A","circleddash":"\u229D","CircleDot":"\u2299","circledR":"\u00AE","circledS":"\u24C8","CircleMinus":"\u2296","CirclePlus":"\u2295","CircleTimes":"\u2297","cir":"\u25CB","cirE":"\u29C3","cire":"\u2257","cirfnint":"\u2A10","cirmid":"\u2AEF","cirscir":"\u29C2","ClockwiseContourIntegral":"\u2232","CloseCurlyDoubleQuote":"\u201D","CloseCurlyQuote":"\u2019","clubs":"\u2663","clubsuit":"\u2663","colon":":","Colon":"\u2237","Colone":"\u2A74","colone":"\u2254","coloneq":"\u2254","comma":",","commat":"@","comp":"\u2201","compfn":"\u2218","complement":"\u2201","complexes":"\u2102","cong":"\u2245","congdot":"\u2A6D","Congruent":"\u2261","conint":"\u222E","Conint":"\u222F","ContourIntegral":"\u222E","copf":"\uD835\uDD54","Copf":"\u2102","coprod":"\u2210","Coproduct":"\u2210","copy":"\u00A9","COPY":"\u00A9","copysr":"\u2117","CounterClockwiseContourIntegral":"\u2233","crarr":"\u21B5","cross":"\u2717","Cross":"\u2A2F","Cscr":"\uD835\uDC9E","cscr":"\uD835\uDCB8","csub":"\u2ACF","csube":"\u2AD1","csup":"\u2AD0","csupe":"\u2AD2","ctdot":"\u22EF","cudarrl":"\u2938","cudarrr":"\u2935","cuepr":"\u22DE","cuesc":"\u22DF","cularr":"\u21B6","cularrp":"\u293D","cupbrcap":"\u2A48","cupcap":"\u2A46","CupCap":"\u224D","cup":"\u222A","Cup":"\u22D3","cupcup":"\u2A4A","cupdot":"\u228D","cupor":"\u2A45","cups":"\u222A\uFE00","curarr":"\u21B7","curarrm":"\u293C","curlyeqprec":"\u22DE","curlyeqsucc":"\u22DF","curlyvee":"\u22CE","curlywedge":"\u22CF","curren":"\u00A4","curvearrowleft":"\u21B6","curvearrowright":"\u21B7","cuvee":"\u22CE","cuwed":"\u22CF","cwconint":"\u2232","cwint":"\u2231","cylcty":"\u232D","dagger":"\u2020","Dagger":"\u2021","daleth":"\u2138","darr":"\u2193","Darr":"\u21A1","dArr":"\u21D3","dash":"\u2010","Dashv":"\u2AE4","dashv":"\u22A3","dbkarow":"\u290F","dblac":"\u02DD","Dcaron":"\u010E","dcaron":"\u010F","Dcy":"\u0414","dcy":"\u0434","ddagger":"\u2021","ddarr":"\u21CA","DD":"\u2145","dd":"\u2146","DDotrahd":"\u2911","ddotseq":"\u2A77","deg":"\u00B0","Del":"\u2207","Delta":"\u0394","delta":"\u03B4","demptyv":"\u29B1","dfisht":"\u297F","Dfr":"\uD835\uDD07","dfr":"\uD835\uDD21","dHar":"\u2965","dharl":"\u21C3","dharr":"\u21C2","DiacriticalAcute":"\u00B4","DiacriticalDot":"\u02D9","DiacriticalDoubleAcute":"\u02DD","DiacriticalGrave":"`","DiacriticalTilde":"\u02DC","diam":"\u22C4","diamond":"\u22C4","Diamond":"\u22C4","diamondsuit":"\u2666","diams":"\u2666","die":"\u00A8","DifferentialD":"\u2146","digamma":"\u03DD","disin":"\u22F2","div":"\u00F7","divide":"\u00F7","divideontimes":"\u22C7","divonx":"\u22C7","DJcy":"\u0402","djcy":"\u0452","dlcorn":"\u231E","dlcrop":"\u230D","dollar":"$","Dopf":"\uD835\uDD3B","dopf":"\uD835\uDD55","Dot":"\u00A8","dot":"\u02D9","DotDot":"\u20DC","doteq":"\u2250","doteqdot":"\u2251","DotEqual":"\u2250","dotminus":"\u2238","dotplus":"\u2214","dotsquare":"\u22A1","doublebarwedge":"\u2306","DoubleContourIntegral":"\u222F","DoubleDot":"\u00A8","DoubleDownArrow":"\u21D3","DoubleLeftArrow":"\u21D0","DoubleLeftRightArrow":"\u21D4","DoubleLeftTee":"\u2AE4","DoubleLongLeftArrow":"\u27F8","DoubleLongLeftRightArrow":"\u27FA","DoubleLongRightArrow":"\u27F9","DoubleRightArrow":"\u21D2","DoubleRightTee":"\u22A8","DoubleUpArrow":"\u21D1","DoubleUpDownArrow":"\u21D5","DoubleVerticalBar":"\u2225","DownArrowBar":"\u2913","downarrow":"\u2193","DownArrow":"\u2193","Downarrow":"\u21D3","DownArrowUpArrow":"\u21F5","DownBreve":"\u0311","downdownarrows":"\u21CA","downharpoonleft":"\u21C3","downharpoonright":"\u21C2","DownLeftRightVector":"\u2950","DownLeftTeeVector":"\u295E","DownLeftVectorBar":"\u2956","DownLeftVector":"\u21BD","DownRightTeeVector":"\u295F","DownRightVectorBar":"\u2957","DownRightVector":"\u21C1","DownTeeArrow":"\u21A7","DownTee":"\u22A4","drbkarow":"\u2910","drcorn":"\u231F","drcrop":"\u230C","Dscr":"\uD835\uDC9F","dscr":"\uD835\uDCB9","DScy":"\u0405","dscy":"\u0455","dsol":"\u29F6","Dstrok":"\u0110","dstrok":"\u0111","dtdot":"\u22F1","dtri":"\u25BF","dtrif":"\u25BE","duarr":"\u21F5","duhar":"\u296F","dwangle":"\u29A6","DZcy":"\u040F","dzcy":"\u045F","dzigrarr":"\u27FF","Eacute":"\u00C9","eacute":"\u00E9","easter":"\u2A6E","Ecaron":"\u011A","ecaron":"\u011B","Ecirc":"\u00CA","ecirc":"\u00EA","ecir":"\u2256","ecolon":"\u2255","Ecy":"\u042D","ecy":"\u044D","eDDot":"\u2A77","Edot":"\u0116","edot":"\u0117","eDot":"\u2251","ee":"\u2147","efDot":"\u2252","Efr":"\uD835\uDD08","efr":"\uD835\uDD22","eg":"\u2A9A","Egrave":"\u00C8","egrave":"\u00E8","egs":"\u2A96","egsdot":"\u2A98","el":"\u2A99","Element":"\u2208","elinters":"\u23E7","ell":"\u2113","els":"\u2A95","elsdot":"\u2A97","Emacr":"\u0112","emacr":"\u0113","empty":"\u2205","emptyset":"\u2205","EmptySmallSquare":"\u25FB","emptyv":"\u2205","EmptyVerySmallSquare":"\u25AB","emsp13":"\u2004","emsp14":"\u2005","emsp":"\u2003","ENG":"\u014A","eng":"\u014B","ensp":"\u2002","Eogon":"\u0118","eogon":"\u0119","Eopf":"\uD835\uDD3C","eopf":"\uD835\uDD56","epar":"\u22D5","eparsl":"\u29E3","eplus":"\u2A71","epsi":"\u03B5","Epsilon":"\u0395","epsilon":"\u03B5","epsiv":"\u03F5","eqcirc":"\u2256","eqcolon":"\u2255","eqsim":"\u2242","eqslantgtr":"\u2A96","eqslantless":"\u2A95","Equal":"\u2A75","equals":"=","EqualTilde":"\u2242","equest":"\u225F","Equilibrium":"\u21CC","equiv":"\u2261","equivDD":"\u2A78","eqvparsl":"\u29E5","erarr":"\u2971","erDot":"\u2253","escr":"\u212F","Escr":"\u2130","esdot":"\u2250","Esim":"\u2A73","esim":"\u2242","Eta":"\u0397","eta":"\u03B7","ETH":"\u00D0","eth":"\u00F0","Euml":"\u00CB","euml":"\u00EB","euro":"\u20AC","excl":"!","exist":"\u2203","Exists":"\u2203","expectation":"\u2130","exponentiale":"\u2147","ExponentialE":"\u2147","fallingdotseq":"\u2252","Fcy":"\u0424","fcy":"\u0444","female":"\u2640","ffilig":"\uFB03","fflig":"\uFB00","ffllig":"\uFB04","Ffr":"\uD835\uDD09","ffr":"\uD835\uDD23","filig":"\uFB01","FilledSmallSquare":"\u25FC","FilledVerySmallSquare":"\u25AA","fjlig":"fj","flat":"\u266D","fllig":"\uFB02","fltns":"\u25B1","fnof":"\u0192","Fopf":"\uD835\uDD3D","fopf":"\uD835\uDD57","forall":"\u2200","ForAll":"\u2200","fork":"\u22D4","forkv":"\u2AD9","Fouriertrf":"\u2131","fpartint":"\u2A0D","frac12":"\u00BD","frac13":"\u2153","frac14":"\u00BC","frac15":"\u2155","frac16":"\u2159","frac18":"\u215B","frac23":"\u2154","frac25":"\u2156","frac34":"\u00BE","frac35":"\u2157","frac38":"\u215C","frac45":"\u2158","frac56":"\u215A","frac58":"\u215D","frac78":"\u215E","frasl":"\u2044","frown":"\u2322","fscr":"\uD835\uDCBB","Fscr":"\u2131","gacute":"\u01F5","Gamma":"\u0393","gamma":"\u03B3","Gammad":"\u03DC","gammad":"\u03DD","gap":"\u2A86","Gbreve":"\u011E","gbreve":"\u011F","Gcedil":"\u0122","Gcirc":"\u011C","gcirc":"\u011D","Gcy":"\u0413","gcy":"\u0433","Gdot":"\u0120","gdot":"\u0121","ge":"\u2265","gE":"\u2267","gEl":"\u2A8C","gel":"\u22DB","geq":"\u2265","geqq":"\u2267","geqslant":"\u2A7E","gescc":"\u2AA9","ges":"\u2A7E","gesdot":"\u2A80","gesdoto":"\u2A82","gesdotol":"\u2A84","gesl":"\u22DB\uFE00","gesles":"\u2A94","Gfr":"\uD835\uDD0A","gfr":"\uD835\uDD24","gg":"\u226B","Gg":"\u22D9","ggg":"\u22D9","gimel":"\u2137","GJcy":"\u0403","gjcy":"\u0453","gla":"\u2AA5","gl":"\u2277","glE":"\u2A92","glj":"\u2AA4","gnap":"\u2A8A","gnapprox":"\u2A8A","gne":"\u2A88","gnE":"\u2269","gneq":"\u2A88","gneqq":"\u2269","gnsim":"\u22E7","Gopf":"\uD835\uDD3E","gopf":"\uD835\uDD58","grave":"`","GreaterEqual":"\u2265","GreaterEqualLess":"\u22DB","GreaterFullEqual":"\u2267","GreaterGreater":"\u2AA2","GreaterLess":"\u2277","GreaterSlantEqual":"\u2A7E","GreaterTilde":"\u2273","Gscr":"\uD835\uDCA2","gscr":"\u210A","gsim":"\u2273","gsime":"\u2A8E","gsiml":"\u2A90","gtcc":"\u2AA7","gtcir":"\u2A7A","gt":">","GT":">","Gt":"\u226B","gtdot":"\u22D7","gtlPar":"\u2995","gtquest":"\u2A7C","gtrapprox":"\u2A86","gtrarr":"\u2978","gtrdot":"\u22D7","gtreqless":"\u22DB","gtreqqless":"\u2A8C","gtrless":"\u2277","gtrsim":"\u2273","gvertneqq":"\u2269\uFE00","gvnE":"\u2269\uFE00","Hacek":"\u02C7","hairsp":"\u200A","half":"\u00BD","hamilt":"\u210B","HARDcy":"\u042A","hardcy":"\u044A","harrcir":"\u2948","harr":"\u2194","hArr":"\u21D4","harrw":"\u21AD","Hat":"^","hbar":"\u210F","Hcirc":"\u0124","hcirc":"\u0125","hearts":"\u2665","heartsuit":"\u2665","hellip":"\u2026","hercon":"\u22B9","hfr":"\uD835\uDD25","Hfr":"\u210C","HilbertSpace":"\u210B","hksearow":"\u2925","hkswarow":"\u2926","hoarr":"\u21FF","homtht":"\u223B","hookleftarrow":"\u21A9","hookrightarrow":"\u21AA","hopf":"\uD835\uDD59","Hopf":"\u210D","horbar":"\u2015","HorizontalLine":"\u2500","hscr":"\uD835\uDCBD","Hscr":"\u210B","hslash":"\u210F","Hstrok":"\u0126","hstrok":"\u0127","HumpDownHump":"\u224E","HumpEqual":"\u224F","hybull":"\u2043","hyphen":"\u2010","Iacute":"\u00CD","iacute":"\u00ED","ic":"\u2063","Icirc":"\u00CE","icirc":"\u00EE","Icy":"\u0418","icy":"\u0438","Idot":"\u0130","IEcy":"\u0415","iecy":"\u0435","iexcl":"\u00A1","iff":"\u21D4","ifr":"\uD835\uDD26","Ifr":"\u2111","Igrave":"\u00CC","igrave":"\u00EC","ii":"\u2148","iiiint":"\u2A0C","iiint":"\u222D","iinfin":"\u29DC","iiota":"\u2129","IJlig":"\u0132","ijlig":"\u0133","Imacr":"\u012A","imacr":"\u012B","image":"\u2111","ImaginaryI":"\u2148","imagline":"\u2110","imagpart":"\u2111","imath":"\u0131","Im":"\u2111","imof":"\u22B7","imped":"\u01B5","Implies":"\u21D2","incare":"\u2105","in":"\u2208","infin":"\u221E","infintie":"\u29DD","inodot":"\u0131","intcal":"\u22BA","int":"\u222B","Int":"\u222C","integers":"\u2124","Integral":"\u222B","intercal":"\u22BA","Intersection":"\u22C2","intlarhk":"\u2A17","intprod":"\u2A3C","InvisibleComma":"\u2063","InvisibleTimes":"\u2062","IOcy":"\u0401","iocy":"\u0451","Iogon":"\u012E","iogon":"\u012F","Iopf":"\uD835\uDD40","iopf":"\uD835\uDD5A","Iota":"\u0399","iota":"\u03B9","iprod":"\u2A3C","iquest":"\u00BF","iscr":"\uD835\uDCBE","Iscr":"\u2110","isin":"\u2208","isindot":"\u22F5","isinE":"\u22F9","isins":"\u22F4","isinsv":"\u22F3","isinv":"\u2208","it":"\u2062","Itilde":"\u0128","itilde":"\u0129","Iukcy":"\u0406","iukcy":"\u0456","Iuml":"\u00CF","iuml":"\u00EF","Jcirc":"\u0134","jcirc":"\u0135","Jcy":"\u0419","jcy":"\u0439","Jfr":"\uD835\uDD0D","jfr":"\uD835\uDD27","jmath":"\u0237","Jopf":"\uD835\uDD41","jopf":"\uD835\uDD5B","Jscr":"\uD835\uDCA5","jscr":"\uD835\uDCBF","Jsercy":"\u0408","jsercy":"\u0458","Jukcy":"\u0404","jukcy":"\u0454","Kappa":"\u039A","kappa":"\u03BA","kappav":"\u03F0","Kcedil":"\u0136","kcedil":"\u0137","Kcy":"\u041A","kcy":"\u043A","Kfr":"\uD835\uDD0E","kfr":"\uD835\uDD28","kgreen":"\u0138","KHcy":"\u0425","khcy":"\u0445","KJcy":"\u040C","kjcy":"\u045C","Kopf":"\uD835\uDD42","kopf":"\uD835\uDD5C","Kscr":"\uD835\uDCA6","kscr":"\uD835\uDCC0","lAarr":"\u21DA","Lacute":"\u0139","lacute":"\u013A","laemptyv":"\u29B4","lagran":"\u2112","Lambda":"\u039B","lambda":"\u03BB","lang":"\u27E8","Lang":"\u27EA","langd":"\u2991","langle":"\u27E8","lap":"\u2A85","Laplacetrf":"\u2112","laquo":"\u00AB","larrb":"\u21E4","larrbfs":"\u291F","larr":"\u2190","Larr":"\u219E","lArr":"\u21D0","larrfs":"\u291D","larrhk":"\u21A9","larrlp":"\u21AB","larrpl":"\u2939","larrsim":"\u2973","larrtl":"\u21A2","latail":"\u2919","lAtail":"\u291B","lat":"\u2AAB","late":"\u2AAD","lates":"\u2AAD\uFE00","lbarr":"\u290C","lBarr":"\u290E","lbbrk":"\u2772","lbrace":"{","lbrack":"[","lbrke":"\u298B","lbrksld":"\u298F","lbrkslu":"\u298D","Lcaron":"\u013D","lcaron":"\u013E","Lcedil":"\u013B","lcedil":"\u013C","lceil":"\u2308","lcub":"{","Lcy":"\u041B","lcy":"\u043B","ldca":"\u2936","ldquo":"\u201C","ldquor":"\u201E","ldrdhar":"\u2967","ldrushar":"\u294B","ldsh":"\u21B2","le":"\u2264","lE":"\u2266","LeftAngleBracket":"\u27E8","LeftArrowBar":"\u21E4","leftarrow":"\u2190","LeftArrow":"\u2190","Leftarrow":"\u21D0","LeftArrowRightArrow":"\u21C6","leftarrowtail":"\u21A2","LeftCeiling":"\u2308","LeftDoubleBracket":"\u27E6","LeftDownTeeVector":"\u2961","LeftDownVectorBar":"\u2959","LeftDownVector":"\u21C3","LeftFloor":"\u230A","leftharpoondown":"\u21BD","leftharpoonup":"\u21BC","leftleftarrows":"\u21C7","leftrightarrow":"\u2194","LeftRightArrow":"\u2194","Leftrightarrow":"\u21D4","leftrightarrows":"\u21C6","leftrightharpoons":"\u21CB","leftrightsquigarrow":"\u21AD","LeftRightVector":"\u294E","LeftTeeArrow":"\u21A4","LeftTee":"\u22A3","LeftTeeVector":"\u295A","leftthreetimes":"\u22CB","LeftTriangleBar":"\u29CF","LeftTriangle":"\u22B2","LeftTriangleEqual":"\u22B4","LeftUpDownVector":"\u2951","LeftUpTeeVector":"\u2960","LeftUpVectorBar":"\u2958","LeftUpVector":"\u21BF","LeftVectorBar":"\u2952","LeftVector":"\u21BC","lEg":"\u2A8B","leg":"\u22DA","leq":"\u2264","leqq":"\u2266","leqslant":"\u2A7D","lescc":"\u2AA8","les":"\u2A7D","lesdot":"\u2A7F","lesdoto":"\u2A81","lesdotor":"\u2A83","lesg":"\u22DA\uFE00","lesges":"\u2A93","lessapprox":"\u2A85","lessdot":"\u22D6","lesseqgtr":"\u22DA","lesseqqgtr":"\u2A8B","LessEqualGreater":"\u22DA","LessFullEqual":"\u2266","LessGreater":"\u2276","lessgtr":"\u2276","LessLess":"\u2AA1","lesssim":"\u2272","LessSlantEqual":"\u2A7D","LessTilde":"\u2272","lfisht":"\u297C","lfloor":"\u230A","Lfr":"\uD835\uDD0F","lfr":"\uD835\uDD29","lg":"\u2276","lgE":"\u2A91","lHar":"\u2962","lhard":"\u21BD","lharu":"\u21BC","lharul":"\u296A","lhblk":"\u2584","LJcy":"\u0409","ljcy":"\u0459","llarr":"\u21C7","ll":"\u226A","Ll":"\u22D8","llcorner":"\u231E","Lleftarrow":"\u21DA","llhard":"\u296B","lltri":"\u25FA","Lmidot":"\u013F","lmidot":"\u0140","lmoustache":"\u23B0","lmoust":"\u23B0","lnap":"\u2A89","lnapprox":"\u2A89","lne":"\u2A87","lnE":"\u2268","lneq":"\u2A87","lneqq":"\u2268","lnsim":"\u22E6","loang":"\u27EC","loarr":"\u21FD","lobrk":"\u27E6","longleftarrow":"\u27F5","LongLeftArrow":"\u27F5","Longleftarrow":"\u27F8","longleftrightarrow":"\u27F7","LongLeftRightArrow":"\u27F7","Longleftrightarrow":"\u27FA","longmapsto":"\u27FC","longrightarrow":"\u27F6","LongRightArrow":"\u27F6","Longrightarrow":"\u27F9","looparrowleft":"\u21AB","looparrowright":"\u21AC","lopar":"\u2985","Lopf":"\uD835\uDD43","lopf":"\uD835\uDD5D","loplus":"\u2A2D","lotimes":"\u2A34","lowast":"\u2217","lowbar":"_","LowerLeftArrow":"\u2199","LowerRightArrow":"\u2198","loz":"\u25CA","lozenge":"\u25CA","lozf":"\u29EB","lpar":"(","lparlt":"\u2993","lrarr":"\u21C6","lrcorner":"\u231F","lrhar":"\u21CB","lrhard":"\u296D","lrm":"\u200E","lrtri":"\u22BF","lsaquo":"\u2039","lscr":"\uD835\uDCC1","Lscr":"\u2112","lsh":"\u21B0","Lsh":"\u21B0","lsim":"\u2272","lsime":"\u2A8D","lsimg":"\u2A8F","lsqb":"[","lsquo":"\u2018","lsquor":"\u201A","Lstrok":"\u0141","lstrok":"\u0142","ltcc":"\u2AA6","ltcir":"\u2A79","lt":"<","LT":"<","Lt":"\u226A","ltdot":"\u22D6","lthree":"\u22CB","ltimes":"\u22C9","ltlarr":"\u2976","ltquest":"\u2A7B","ltri":"\u25C3","ltrie":"\u22B4","ltrif":"\u25C2","ltrPar":"\u2996","lurdshar":"\u294A","luruhar":"\u2966","lvertneqq":"\u2268\uFE00","lvnE":"\u2268\uFE00","macr":"\u00AF","male":"\u2642","malt":"\u2720","maltese":"\u2720","Map":"\u2905","map":"\u21A6","mapsto":"\u21A6","mapstodown":"\u21A7","mapstoleft":"\u21A4","mapstoup":"\u21A5","marker":"\u25AE","mcomma":"\u2A29","Mcy":"\u041C","mcy":"\u043C","mdash":"\u2014","mDDot":"\u223A","measuredangle":"\u2221","MediumSpace":"\u205F","Mellintrf":"\u2133","Mfr":"\uD835\uDD10","mfr":"\uD835\uDD2A","mho":"\u2127","micro":"\u00B5","midast":"*","midcir":"\u2AF0","mid":"\u2223","middot":"\u00B7","minusb":"\u229F","minus":"\u2212","minusd":"\u2238","minusdu":"\u2A2A","MinusPlus":"\u2213","mlcp":"\u2ADB","mldr":"\u2026","mnplus":"\u2213","models":"\u22A7","Mopf":"\uD835\uDD44","mopf":"\uD835\uDD5E","mp":"\u2213","mscr":"\uD835\uDCC2","Mscr":"\u2133","mstpos":"\u223E","Mu":"\u039C","mu":"\u03BC","multimap":"\u22B8","mumap":"\u22B8","nabla":"\u2207","Nacute":"\u0143","nacute":"\u0144","nang":"\u2220\u20D2","nap":"\u2249","napE":"\u2A70\u0338","napid":"\u224B\u0338","napos":"\u0149","napprox":"\u2249","natural":"\u266E","naturals":"\u2115","natur":"\u266E","nbsp":"\u00A0","nbump":"\u224E\u0338","nbumpe":"\u224F\u0338","ncap":"\u2A43","Ncaron":"\u0147","ncaron":"\u0148","Ncedil":"\u0145","ncedil":"\u0146","ncong":"\u2247","ncongdot":"\u2A6D\u0338","ncup":"\u2A42","Ncy":"\u041D","ncy":"\u043D","ndash":"\u2013","nearhk":"\u2924","nearr":"\u2197","neArr":"\u21D7","nearrow":"\u2197","ne":"\u2260","nedot":"\u2250\u0338","NegativeMediumSpace":"\u200B","NegativeThickSpace":"\u200B","NegativeThinSpace":"\u200B","NegativeVeryThinSpace":"\u200B","nequiv":"\u2262","nesear":"\u2928","nesim":"\u2242\u0338","NestedGreaterGreater":"\u226B","NestedLessLess":"\u226A","NewLine":"\n","nexist":"\u2204","nexists":"\u2204","Nfr":"\uD835\uDD11","nfr":"\uD835\uDD2B","ngE":"\u2267\u0338","nge":"\u2271","ngeq":"\u2271","ngeqq":"\u2267\u0338","ngeqslant":"\u2A7E\u0338","nges":"\u2A7E\u0338","nGg":"\u22D9\u0338","ngsim":"\u2275","nGt":"\u226B\u20D2","ngt":"\u226F","ngtr":"\u226F","nGtv":"\u226B\u0338","nharr":"\u21AE","nhArr":"\u21CE","nhpar":"\u2AF2","ni":"\u220B","nis":"\u22FC","nisd":"\u22FA","niv":"\u220B","NJcy":"\u040A","njcy":"\u045A","nlarr":"\u219A","nlArr":"\u21CD","nldr":"\u2025","nlE":"\u2266\u0338","nle":"\u2270","nleftarrow":"\u219A","nLeftarrow":"\u21CD","nleftrightarrow":"\u21AE","nLeftrightarrow":"\u21CE","nleq":"\u2270","nleqq":"\u2266\u0338","nleqslant":"\u2A7D\u0338","nles":"\u2A7D\u0338","nless":"\u226E","nLl":"\u22D8\u0338","nlsim":"\u2274","nLt":"\u226A\u20D2","nlt":"\u226E","nltri":"\u22EA","nltrie":"\u22EC","nLtv":"\u226A\u0338","nmid":"\u2224","NoBreak":"\u2060","NonBreakingSpace":"\u00A0","nopf":"\uD835\uDD5F","Nopf":"\u2115","Not":"\u2AEC","not":"\u00AC","NotCongruent":"\u2262","NotCupCap":"\u226D","NotDoubleVerticalBar":"\u2226","NotElement":"\u2209","NotEqual":"\u2260","NotEqualTilde":"\u2242\u0338","NotExists":"\u2204","NotGreater":"\u226F","NotGreaterEqual":"\u2271","NotGreaterFullEqual":"\u2267\u0338","NotGreaterGreater":"\u226B\u0338","NotGreaterLess":"\u2279","NotGreaterSlantEqual":"\u2A7E\u0338","NotGreaterTilde":"\u2275","NotHumpDownHump":"\u224E\u0338","NotHumpEqual":"\u224F\u0338","notin":"\u2209","notindot":"\u22F5\u0338","notinE":"\u22F9\u0338","notinva":"\u2209","notinvb":"\u22F7","notinvc":"\u22F6","NotLeftTriangleBar":"\u29CF\u0338","NotLeftTriangle":"\u22EA","NotLeftTriangleEqual":"\u22EC","NotLess":"\u226E","NotLessEqual":"\u2270","NotLessGreater":"\u2278","NotLessLess":"\u226A\u0338","NotLessSlantEqual":"\u2A7D\u0338","NotLessTilde":"\u2274","NotNestedGreaterGreater":"\u2AA2\u0338","NotNestedLessLess":"\u2AA1\u0338","notni":"\u220C","notniva":"\u220C","notnivb":"\u22FE","notnivc":"\u22FD","NotPrecedes":"\u2280","NotPrecedesEqual":"\u2AAF\u0338","NotPrecedesSlantEqual":"\u22E0","NotReverseElement":"\u220C","NotRightTriangleBar":"\u29D0\u0338","NotRightTriangle":"\u22EB","NotRightTriangleEqual":"\u22ED","NotSquareSubset":"\u228F\u0338","NotSquareSubsetEqual":"\u22E2","NotSquareSuperset":"\u2290\u0338","NotSquareSupersetEqual":"\u22E3","NotSubset":"\u2282\u20D2","NotSubsetEqual":"\u2288","NotSucceeds":"\u2281","NotSucceedsEqual":"\u2AB0\u0338","NotSucceedsSlantEqual":"\u22E1","NotSucceedsTilde":"\u227F\u0338","NotSuperset":"\u2283\u20D2","NotSupersetEqual":"\u2289","NotTilde":"\u2241","NotTildeEqual":"\u2244","NotTildeFullEqual":"\u2247","NotTildeTilde":"\u2249","NotVerticalBar":"\u2224","nparallel":"\u2226","npar":"\u2226","nparsl":"\u2AFD\u20E5","npart":"\u2202\u0338","npolint":"\u2A14","npr":"\u2280","nprcue":"\u22E0","nprec":"\u2280","npreceq":"\u2AAF\u0338","npre":"\u2AAF\u0338","nrarrc":"\u2933\u0338","nrarr":"\u219B","nrArr":"\u21CF","nrarrw":"\u219D\u0338","nrightarrow":"\u219B","nRightarrow":"\u21CF","nrtri":"\u22EB","nrtrie":"\u22ED","nsc":"\u2281","nsccue":"\u22E1","nsce":"\u2AB0\u0338","Nscr":"\uD835\uDCA9","nscr":"\uD835\uDCC3","nshortmid":"\u2224","nshortparallel":"\u2226","nsim":"\u2241","nsime":"\u2244","nsimeq":"\u2244","nsmid":"\u2224","nspar":"\u2226","nsqsube":"\u22E2","nsqsupe":"\u22E3","nsub":"\u2284","nsubE":"\u2AC5\u0338","nsube":"\u2288","nsubset":"\u2282\u20D2","nsubseteq":"\u2288","nsubseteqq":"\u2AC5\u0338","nsucc":"\u2281","nsucceq":"\u2AB0\u0338","nsup":"\u2285","nsupE":"\u2AC6\u0338","nsupe":"\u2289","nsupset":"\u2283\u20D2","nsupseteq":"\u2289","nsupseteqq":"\u2AC6\u0338","ntgl":"\u2279","Ntilde":"\u00D1","ntilde":"\u00F1","ntlg":"\u2278","ntriangleleft":"\u22EA","ntrianglelefteq":"\u22EC","ntriangleright":"\u22EB","ntrianglerighteq":"\u22ED","Nu":"\u039D","nu":"\u03BD","num":"#","numero":"\u2116","numsp":"\u2007","nvap":"\u224D\u20D2","nvdash":"\u22AC","nvDash":"\u22AD","nVdash":"\u22AE","nVDash":"\u22AF","nvge":"\u2265\u20D2","nvgt":">\u20D2","nvHarr":"\u2904","nvinfin":"\u29DE","nvlArr":"\u2902","nvle":"\u2264\u20D2","nvlt":"<\u20D2","nvltrie":"\u22B4\u20D2","nvrArr":"\u2903","nvrtrie":"\u22B5\u20D2","nvsim":"\u223C\u20D2","nwarhk":"\u2923","nwarr":"\u2196","nwArr":"\u21D6","nwarrow":"\u2196","nwnear":"\u2927","Oacute":"\u00D3","oacute":"\u00F3","oast":"\u229B","Ocirc":"\u00D4","ocirc":"\u00F4","ocir":"\u229A","Ocy":"\u041E","ocy":"\u043E","odash":"\u229D","Odblac":"\u0150","odblac":"\u0151","odiv":"\u2A38","odot":"\u2299","odsold":"\u29BC","OElig":"\u0152","oelig":"\u0153","ofcir":"\u29BF","Ofr":"\uD835\uDD12","ofr":"\uD835\uDD2C","ogon":"\u02DB","Ograve":"\u00D2","ograve":"\u00F2","ogt":"\u29C1","ohbar":"\u29B5","ohm":"\u03A9","oint":"\u222E","olarr":"\u21BA","olcir":"\u29BE","olcross":"\u29BB","oline":"\u203E","olt":"\u29C0","Omacr":"\u014C","omacr":"\u014D","Omega":"\u03A9","omega":"\u03C9","Omicron":"\u039F","omicron":"\u03BF","omid":"\u29B6","ominus":"\u2296","Oopf":"\uD835\uDD46","oopf":"\uD835\uDD60","opar":"\u29B7","OpenCurlyDoubleQuote":"\u201C","OpenCurlyQuote":"\u2018","operp":"\u29B9","oplus":"\u2295","orarr":"\u21BB","Or":"\u2A54","or":"\u2228","ord":"\u2A5D","order":"\u2134","orderof":"\u2134","ordf":"\u00AA","ordm":"\u00BA","origof":"\u22B6","oror":"\u2A56","orslope":"\u2A57","orv":"\u2A5B","oS":"\u24C8","Oscr":"\uD835\uDCAA","oscr":"\u2134","Oslash":"\u00D8","oslash":"\u00F8","osol":"\u2298","Otilde":"\u00D5","otilde":"\u00F5","otimesas":"\u2A36","Otimes":"\u2A37","otimes":"\u2297","Ouml":"\u00D6","ouml":"\u00F6","ovbar":"\u233D","OverBar":"\u203E","OverBrace":"\u23DE","OverBracket":"\u23B4","OverParenthesis":"\u23DC","para":"\u00B6","parallel":"\u2225","par":"\u2225","parsim":"\u2AF3","parsl":"\u2AFD","part":"\u2202","PartialD":"\u2202","Pcy":"\u041F","pcy":"\u043F","percnt":"%","period":".","permil":"\u2030","perp":"\u22A5","pertenk":"\u2031","Pfr":"\uD835\uDD13","pfr":"\uD835\uDD2D","Phi":"\u03A6","phi":"\u03C6","phiv":"\u03D5","phmmat":"\u2133","phone":"\u260E","Pi":"\u03A0","pi":"\u03C0","pitchfork":"\u22D4","piv":"\u03D6","planck":"\u210F","planckh":"\u210E","plankv":"\u210F","plusacir":"\u2A23","plusb":"\u229E","pluscir":"\u2A22","plus":"+","plusdo":"\u2214","plusdu":"\u2A25","pluse":"\u2A72","PlusMinus":"\u00B1","plusmn":"\u00B1","plussim":"\u2A26","plustwo":"\u2A27","pm":"\u00B1","Poincareplane":"\u210C","pointint":"\u2A15","popf":"\uD835\uDD61","Popf":"\u2119","pound":"\u00A3","prap":"\u2AB7","Pr":"\u2ABB","pr":"\u227A","prcue":"\u227C","precapprox":"\u2AB7","prec":"\u227A","preccurlyeq":"\u227C","Precedes":"\u227A","PrecedesEqual":"\u2AAF","PrecedesSlantEqual":"\u227C","PrecedesTilde":"\u227E","preceq":"\u2AAF","precnapprox":"\u2AB9","precneqq":"\u2AB5","precnsim":"\u22E8","pre":"\u2AAF","prE":"\u2AB3","precsim":"\u227E","prime":"\u2032","Prime":"\u2033","primes":"\u2119","prnap":"\u2AB9","prnE":"\u2AB5","prnsim":"\u22E8","prod":"\u220F","Product":"\u220F","profalar":"\u232E","profline":"\u2312","profsurf":"\u2313","prop":"\u221D","Proportional":"\u221D","Proportion":"\u2237","propto":"\u221D","prsim":"\u227E","prurel":"\u22B0","Pscr":"\uD835\uDCAB","pscr":"\uD835\uDCC5","Psi":"\u03A8","psi":"\u03C8","puncsp":"\u2008","Qfr":"\uD835\uDD14","qfr":"\uD835\uDD2E","qint":"\u2A0C","qopf":"\uD835\uDD62","Qopf":"\u211A","qprime":"\u2057","Qscr":"\uD835\uDCAC","qscr":"\uD835\uDCC6","quaternions":"\u210D","quatint":"\u2A16","quest":"?","questeq":"\u225F","quot":"\"","QUOT":"\"","rAarr":"\u21DB","race":"\u223D\u0331","Racute":"\u0154","racute":"\u0155","radic":"\u221A","raemptyv":"\u29B3","rang":"\u27E9","Rang":"\u27EB","rangd":"\u2992","range":"\u29A5","rangle":"\u27E9","raquo":"\u00BB","rarrap":"\u2975","rarrb":"\u21E5","rarrbfs":"\u2920","rarrc":"\u2933","rarr":"\u2192","Rarr":"\u21A0","rArr":"\u21D2","rarrfs":"\u291E","rarrhk":"\u21AA","rarrlp":"\u21AC","rarrpl":"\u2945","rarrsim":"\u2974","Rarrtl":"\u2916","rarrtl":"\u21A3","rarrw":"\u219D","ratail":"\u291A","rAtail":"\u291C","ratio":"\u2236","rationals":"\u211A","rbarr":"\u290D","rBarr":"\u290F","RBarr":"\u2910","rbbrk":"\u2773","rbrace":"}","rbrack":"]","rbrke":"\u298C","rbrksld":"\u298E","rbrkslu":"\u2990","Rcaron":"\u0158","rcaron":"\u0159","Rcedil":"\u0156","rcedil":"\u0157","rceil":"\u2309","rcub":"}","Rcy":"\u0420","rcy":"\u0440","rdca":"\u2937","rdldhar":"\u2969","rdquo":"\u201D","rdquor":"\u201D","rdsh":"\u21B3","real":"\u211C","realine":"\u211B","realpart":"\u211C","reals":"\u211D","Re":"\u211C","rect":"\u25AD","reg":"\u00AE","REG":"\u00AE","ReverseElement":"\u220B","ReverseEquilibrium":"\u21CB","ReverseUpEquilibrium":"\u296F","rfisht":"\u297D","rfloor":"\u230B","rfr":"\uD835\uDD2F","Rfr":"\u211C","rHar":"\u2964","rhard":"\u21C1","rharu":"\u21C0","rharul":"\u296C","Rho":"\u03A1","rho":"\u03C1","rhov":"\u03F1","RightAngleBracket":"\u27E9","RightArrowBar":"\u21E5","rightarrow":"\u2192","RightArrow":"\u2192","Rightarrow":"\u21D2","RightArrowLeftArrow":"\u21C4","rightarrowtail":"\u21A3","RightCeiling":"\u2309","RightDoubleBracket":"\u27E7","RightDownTeeVector":"\u295D","RightDownVectorBar":"\u2955","RightDownVector":"\u21C2","RightFloor":"\u230B","rightharpoondown":"\u21C1","rightharpoonup":"\u21C0","rightleftarrows":"\u21C4","rightleftharpoons":"\u21CC","rightrightarrows":"\u21C9","rightsquigarrow":"\u219D","RightTeeArrow":"\u21A6","RightTee":"\u22A2","RightTeeVector":"\u295B","rightthreetimes":"\u22CC","RightTriangleBar":"\u29D0","RightTriangle":"\u22B3","RightTriangleEqual":"\u22B5","RightUpDownVector":"\u294F","RightUpTeeVector":"\u295C","RightUpVectorBar":"\u2954","RightUpVector":"\u21BE","RightVectorBar":"\u2953","RightVector":"\u21C0","ring":"\u02DA","risingdotseq":"\u2253","rlarr":"\u21C4","rlhar":"\u21CC","rlm":"\u200F","rmoustache":"\u23B1","rmoust":"\u23B1","rnmid":"\u2AEE","roang":"\u27ED","roarr":"\u21FE","robrk":"\u27E7","ropar":"\u2986","ropf":"\uD835\uDD63","Ropf":"\u211D","roplus":"\u2A2E","rotimes":"\u2A35","RoundImplies":"\u2970","rpar":")","rpargt":"\u2994","rppolint":"\u2A12","rrarr":"\u21C9","Rrightarrow":"\u21DB","rsaquo":"\u203A","rscr":"\uD835\uDCC7","Rscr":"\u211B","rsh":"\u21B1","Rsh":"\u21B1","rsqb":"]","rsquo":"\u2019","rsquor":"\u2019","rthree":"\u22CC","rtimes":"\u22CA","rtri":"\u25B9","rtrie":"\u22B5","rtrif":"\u25B8","rtriltri":"\u29CE","RuleDelayed":"\u29F4","ruluhar":"\u2968","rx":"\u211E","Sacute":"\u015A","sacute":"\u015B","sbquo":"\u201A","scap":"\u2AB8","Scaron":"\u0160","scaron":"\u0161","Sc":"\u2ABC","sc":"\u227B","sccue":"\u227D","sce":"\u2AB0","scE":"\u2AB4","Scedil":"\u015E","scedil":"\u015F","Scirc":"\u015C","scirc":"\u015D","scnap":"\u2ABA","scnE":"\u2AB6","scnsim":"\u22E9","scpolint":"\u2A13","scsim":"\u227F","Scy":"\u0421","scy":"\u0441","sdotb":"\u22A1","sdot":"\u22C5","sdote":"\u2A66","searhk":"\u2925","searr":"\u2198","seArr":"\u21D8","searrow":"\u2198","sect":"\u00A7","semi":";","seswar":"\u2929","setminus":"\u2216","setmn":"\u2216","sext":"\u2736","Sfr":"\uD835\uDD16","sfr":"\uD835\uDD30","sfrown":"\u2322","sharp":"\u266F","SHCHcy":"\u0429","shchcy":"\u0449","SHcy":"\u0428","shcy":"\u0448","ShortDownArrow":"\u2193","ShortLeftArrow":"\u2190","shortmid":"\u2223","shortparallel":"\u2225","ShortRightArrow":"\u2192","ShortUpArrow":"\u2191","shy":"\u00AD","Sigma":"\u03A3","sigma":"\u03C3","sigmaf":"\u03C2","sigmav":"\u03C2","sim":"\u223C","simdot":"\u2A6A","sime":"\u2243","simeq":"\u2243","simg":"\u2A9E","simgE":"\u2AA0","siml":"\u2A9D","simlE":"\u2A9F","simne":"\u2246","simplus":"\u2A24","simrarr":"\u2972","slarr":"\u2190","SmallCircle":"\u2218","smallsetminus":"\u2216","smashp":"\u2A33","smeparsl":"\u29E4","smid":"\u2223","smile":"\u2323","smt":"\u2AAA","smte":"\u2AAC","smtes":"\u2AAC\uFE00","SOFTcy":"\u042C","softcy":"\u044C","solbar":"\u233F","solb":"\u29C4","sol":"/","Sopf":"\uD835\uDD4A","sopf":"\uD835\uDD64","spades":"\u2660","spadesuit":"\u2660","spar":"\u2225","sqcap":"\u2293","sqcaps":"\u2293\uFE00","sqcup":"\u2294","sqcups":"\u2294\uFE00","Sqrt":"\u221A","sqsub":"\u228F","sqsube":"\u2291","sqsubset":"\u228F","sqsubseteq":"\u2291","sqsup":"\u2290","sqsupe":"\u2292","sqsupset":"\u2290","sqsupseteq":"\u2292","square":"\u25A1","Square":"\u25A1","SquareIntersection":"\u2293","SquareSubset":"\u228F","SquareSubsetEqual":"\u2291","SquareSuperset":"\u2290","SquareSupersetEqual":"\u2292","SquareUnion":"\u2294","squarf":"\u25AA","squ":"\u25A1","squf":"\u25AA","srarr":"\u2192","Sscr":"\uD835\uDCAE","sscr":"\uD835\uDCC8","ssetmn":"\u2216","ssmile":"\u2323","sstarf":"\u22C6","Star":"\u22C6","star":"\u2606","starf":"\u2605","straightepsilon":"\u03F5","straightphi":"\u03D5","strns":"\u00AF","sub":"\u2282","Sub":"\u22D0","subdot":"\u2ABD","subE":"\u2AC5","sube":"\u2286","subedot":"\u2AC3","submult":"\u2AC1","subnE":"\u2ACB","subne":"\u228A","subplus":"\u2ABF","subrarr":"\u2979","subset":"\u2282","Subset":"\u22D0","subseteq":"\u2286","subseteqq":"\u2AC5","SubsetEqual":"\u2286","subsetneq":"\u228A","subsetneqq":"\u2ACB","subsim":"\u2AC7","subsub":"\u2AD5","subsup":"\u2AD3","succapprox":"\u2AB8","succ":"\u227B","succcurlyeq":"\u227D","Succeeds":"\u227B","SucceedsEqual":"\u2AB0","SucceedsSlantEqual":"\u227D","SucceedsTilde":"\u227F","succeq":"\u2AB0","succnapprox":"\u2ABA","succneqq":"\u2AB6","succnsim":"\u22E9","succsim":"\u227F","SuchThat":"\u220B","sum":"\u2211","Sum":"\u2211","sung":"\u266A","sup1":"\u00B9","sup2":"\u00B2","sup3":"\u00B3","sup":"\u2283","Sup":"\u22D1","supdot":"\u2ABE","supdsub":"\u2AD8","supE":"\u2AC6","supe":"\u2287","supedot":"\u2AC4","Superset":"\u2283","SupersetEqual":"\u2287","suphsol":"\u27C9","suphsub":"\u2AD7","suplarr":"\u297B","supmult":"\u2AC2","supnE":"\u2ACC","supne":"\u228B","supplus":"\u2AC0","supset":"\u2283","Supset":"\u22D1","supseteq":"\u2287","supseteqq":"\u2AC6","supsetneq":"\u228B","supsetneqq":"\u2ACC","supsim":"\u2AC8","supsub":"\u2AD4","supsup":"\u2AD6","swarhk":"\u2926","swarr":"\u2199","swArr":"\u21D9","swarrow":"\u2199","swnwar":"\u292A","szlig":"\u00DF","Tab":"\t","target":"\u2316","Tau":"\u03A4","tau":"\u03C4","tbrk":"\u23B4","Tcaron":"\u0164","tcaron":"\u0165","Tcedil":"\u0162","tcedil":"\u0163","Tcy":"\u0422","tcy":"\u0442","tdot":"\u20DB","telrec":"\u2315","Tfr":"\uD835\uDD17","tfr":"\uD835\uDD31","there4":"\u2234","therefore":"\u2234","Therefore":"\u2234","Theta":"\u0398","theta":"\u03B8","thetasym":"\u03D1","thetav":"\u03D1","thickapprox":"\u2248","thicksim":"\u223C","ThickSpace":"\u205F\u200A","ThinSpace":"\u2009","thinsp":"\u2009","thkap":"\u2248","thksim":"\u223C","THORN":"\u00DE","thorn":"\u00FE","tilde":"\u02DC","Tilde":"\u223C","TildeEqual":"\u2243","TildeFullEqual":"\u2245","TildeTilde":"\u2248","timesbar":"\u2A31","timesb":"\u22A0","times":"\u00D7","timesd":"\u2A30","tint":"\u222D","toea":"\u2928","topbot":"\u2336","topcir":"\u2AF1","top":"\u22A4","Topf":"\uD835\uDD4B","topf":"\uD835\uDD65","topfork":"\u2ADA","tosa":"\u2929","tprime":"\u2034","trade":"\u2122","TRADE":"\u2122","triangle":"\u25B5","triangledown":"\u25BF","triangleleft":"\u25C3","trianglelefteq":"\u22B4","triangleq":"\u225C","triangleright":"\u25B9","trianglerighteq":"\u22B5","tridot":"\u25EC","trie":"\u225C","triminus":"\u2A3A","TripleDot":"\u20DB","triplus":"\u2A39","trisb":"\u29CD","tritime":"\u2A3B","trpezium":"\u23E2","Tscr":"\uD835\uDCAF","tscr":"\uD835\uDCC9","TScy":"\u0426","tscy":"\u0446","TSHcy":"\u040B","tshcy":"\u045B","Tstrok":"\u0166","tstrok":"\u0167","twixt":"\u226C","twoheadleftarrow":"\u219E","twoheadrightarrow":"\u21A0","Uacute":"\u00DA","uacute":"\u00FA","uarr":"\u2191","Uarr":"\u219F","uArr":"\u21D1","Uarrocir":"\u2949","Ubrcy":"\u040E","ubrcy":"\u045E","Ubreve":"\u016C","ubreve":"\u016D","Ucirc":"\u00DB","ucirc":"\u00FB","Ucy":"\u0423","ucy":"\u0443","udarr":"\u21C5","Udblac":"\u0170","udblac":"\u0171","udhar":"\u296E","ufisht":"\u297E","Ufr":"\uD835\uDD18","ufr":"\uD835\uDD32","Ugrave":"\u00D9","ugrave":"\u00F9","uHar":"\u2963","uharl":"\u21BF","uharr":"\u21BE","uhblk":"\u2580","ulcorn":"\u231C","ulcorner":"\u231C","ulcrop":"\u230F","ultri":"\u25F8","Umacr":"\u016A","umacr":"\u016B","uml":"\u00A8","UnderBar":"_","UnderBrace":"\u23DF","UnderBracket":"\u23B5","UnderParenthesis":"\u23DD","Union":"\u22C3","UnionPlus":"\u228E","Uogon":"\u0172","uogon":"\u0173","Uopf":"\uD835\uDD4C","uopf":"\uD835\uDD66","UpArrowBar":"\u2912","uparrow":"\u2191","UpArrow":"\u2191","Uparrow":"\u21D1","UpArrowDownArrow":"\u21C5","updownarrow":"\u2195","UpDownArrow":"\u2195","Updownarrow":"\u21D5","UpEquilibrium":"\u296E","upharpoonleft":"\u21BF","upharpoonright":"\u21BE","uplus":"\u228E","UpperLeftArrow":"\u2196","UpperRightArrow":"\u2197","upsi":"\u03C5","Upsi":"\u03D2","upsih":"\u03D2","Upsilon":"\u03A5","upsilon":"\u03C5","UpTeeArrow":"\u21A5","UpTee":"\u22A5","upuparrows":"\u21C8","urcorn":"\u231D","urcorner":"\u231D","urcrop":"\u230E","Uring":"\u016E","uring":"\u016F","urtri":"\u25F9","Uscr":"\uD835\uDCB0","uscr":"\uD835\uDCCA","utdot":"\u22F0","Utilde":"\u0168","utilde":"\u0169","utri":"\u25B5","utrif":"\u25B4","uuarr":"\u21C8","Uuml":"\u00DC","uuml":"\u00FC","uwangle":"\u29A7","vangrt":"\u299C","varepsilon":"\u03F5","varkappa":"\u03F0","varnothing":"\u2205","varphi":"\u03D5","varpi":"\u03D6","varpropto":"\u221D","varr":"\u2195","vArr":"\u21D5","varrho":"\u03F1","varsigma":"\u03C2","varsubsetneq":"\u228A\uFE00","varsubsetneqq":"\u2ACB\uFE00","varsupsetneq":"\u228B\uFE00","varsupsetneqq":"\u2ACC\uFE00","vartheta":"\u03D1","vartriangleleft":"\u22B2","vartriangleright":"\u22B3","vBar":"\u2AE8","Vbar":"\u2AEB","vBarv":"\u2AE9","Vcy":"\u0412","vcy":"\u0432","vdash":"\u22A2","vDash":"\u22A8","Vdash":"\u22A9","VDash":"\u22AB","Vdashl":"\u2AE6","veebar":"\u22BB","vee":"\u2228","Vee":"\u22C1","veeeq":"\u225A","vellip":"\u22EE","verbar":"|","Verbar":"\u2016","vert":"|","Vert":"\u2016","VerticalBar":"\u2223","VerticalLine":"|","VerticalSeparator":"\u2758","VerticalTilde":"\u2240","VeryThinSpace":"\u200A","Vfr":"\uD835\uDD19","vfr":"\uD835\uDD33","vltri":"\u22B2","vnsub":"\u2282\u20D2","vnsup":"\u2283\u20D2","Vopf":"\uD835\uDD4D","vopf":"\uD835\uDD67","vprop":"\u221D","vrtri":"\u22B3","Vscr":"\uD835\uDCB1","vscr":"\uD835\uDCCB","vsubnE":"\u2ACB\uFE00","vsubne":"\u228A\uFE00","vsupnE":"\u2ACC\uFE00","vsupne":"\u228B\uFE00","Vvdash":"\u22AA","vzigzag":"\u299A","Wcirc":"\u0174","wcirc":"\u0175","wedbar":"\u2A5F","wedge":"\u2227","Wedge":"\u22C0","wedgeq":"\u2259","weierp":"\u2118","Wfr":"\uD835\uDD1A","wfr":"\uD835\uDD34","Wopf":"\uD835\uDD4E","wopf":"\uD835\uDD68","wp":"\u2118","wr":"\u2240","wreath":"\u2240","Wscr":"\uD835\uDCB2","wscr":"\uD835\uDCCC","xcap":"\u22C2","xcirc":"\u25EF","xcup":"\u22C3","xdtri":"\u25BD","Xfr":"\uD835\uDD1B","xfr":"\uD835\uDD35","xharr":"\u27F7","xhArr":"\u27FA","Xi":"\u039E","xi":"\u03BE","xlarr":"\u27F5","xlArr":"\u27F8","xmap":"\u27FC","xnis":"\u22FB","xodot":"\u2A00","Xopf":"\uD835\uDD4F","xopf":"\uD835\uDD69","xoplus":"\u2A01","xotime":"\u2A02","xrarr":"\u27F6","xrArr":"\u27F9","Xscr":"\uD835\uDCB3","xscr":"\uD835\uDCCD","xsqcup":"\u2A06","xuplus":"\u2A04","xutri":"\u25B3","xvee":"\u22C1","xwedge":"\u22C0","Yacute":"\u00DD","yacute":"\u00FD","YAcy":"\u042F","yacy":"\u044F","Ycirc":"\u0176","ycirc":"\u0177","Ycy":"\u042B","ycy":"\u044B","yen":"\u00A5","Yfr":"\uD835\uDD1C","yfr":"\uD835\uDD36","YIcy":"\u0407","yicy":"\u0457","Yopf":"\uD835\uDD50","yopf":"\uD835\uDD6A","Yscr":"\uD835\uDCB4","yscr":"\uD835\uDCCE","YUcy":"\u042E","yucy":"\u044E","yuml":"\u00FF","Yuml":"\u0178","Zacute":"\u0179","zacute":"\u017A","Zcaron":"\u017D","zcaron":"\u017E","Zcy":"\u0417","zcy":"\u0437","Zdot":"\u017B","zdot":"\u017C","zeetrf":"\u2128","ZeroWidthSpace":"\u200B","Zeta":"\u0396","zeta":"\u03B6","zfr":"\uD835\uDD37","Zfr":"\u2128","ZHcy":"\u0416","zhcy":"\u0436","zigrarr":"\u21DD","zopf":"\uD835\uDD6B","Zopf":"\u2124","Zscr":"\uD835\uDCB5","zscr":"\uD835\uDCCF","zwj":"\u200D","zwnj":"\u200C"};
});
return ___scope___.entry = "index.js";
});
FuseBox.pkg("events", {}, function(___scope___){
___scope___.file("index.js", function(exports, require, module, __filename, __dirname){

// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.
if (FuseBox.isServer) {
	module.exports = global.require("events");
} else {
	function EventEmitter() {
		this._events = this._events || {};
		this._maxListeners = this._maxListeners || undefined;
	}
	module.exports = EventEmitter;

	// Backwards-compat with node 0.10.x
	EventEmitter.EventEmitter = EventEmitter;

	EventEmitter.prototype._events = undefined;
	EventEmitter.prototype._maxListeners = undefined;

	// By default EventEmitters will print a warning if more than 10 listeners are
	// added to it. This is a useful default which helps finding memory leaks.
	EventEmitter.defaultMaxListeners = 10;

	// Obviously not all Emitters should be limited to 10. This function allows
	// that to be increased. Set to zero for unlimited.
	EventEmitter.prototype.setMaxListeners = function(n) {
		if (!isNumber(n) || n < 0 || isNaN(n)) throw TypeError("n must be a positive number");
		this._maxListeners = n;
		return this;
	};

	EventEmitter.prototype.emit = function(type) {
		var er, handler, len, args, i, listeners;

		if (!this._events) this._events = {};

		// If there is no 'error' event listener then throw.
		if (type === "error") {
			if (!this._events.error || (isObject(this._events.error) && !this._events.error.length)) {
				er = arguments[1];
				if (er instanceof Error) {
					throw er; // Unhandled 'error' event
				}
				throw TypeError('Uncaught, unspecified "error" event.');
			}
		}

		handler = this._events[type];

		if (isUndefined(handler)) return false;

		if (isFunction(handler)) {
			switch (arguments.length) {
				// fast cases
				case 1:
					handler.call(this);
					break;
				case 2:
					handler.call(this, arguments[1]);
					break;
				case 3:
					handler.call(this, arguments[1], arguments[2]);
					break;
				// slower
				default:
					args = Array.prototype.slice.call(arguments, 1);
					handler.apply(this, args);
			}
		} else if (isObject(handler)) {
			args = Array.prototype.slice.call(arguments, 1);
			listeners = handler.slice();
			len = listeners.length;
			for (i = 0; i < len; i++) listeners[i].apply(this, args);
		}

		return true;
	};

	EventEmitter.prototype.addListener = function(type, listener) {
		var m;

		if (!isFunction(listener)) throw TypeError("listener must be a function");

		if (!this._events) this._events = {};

		// To avoid recursion in the case that type === "newListener"! Before
		// adding it to the listeners, first emit "newListener".
		if (this._events.newListener) this.emit("newListener", type, isFunction(listener.listener) ? listener.listener : listener);

		if (!this._events[type])
			// Optimize the case of one listener. Don't need the extra array object.
			this._events[type] = listener;
		else if (isObject(this._events[type]))
			// If we've already got an array, just append.
			this._events[type].push(listener);
		// Adding the second element, need to change to array.
		else this._events[type] = [this._events[type], listener];

		// Check for listener leak
		if (isObject(this._events[type]) && !this._events[type].warned) {
			if (!isUndefined(this._maxListeners)) {
				m = this._maxListeners;
			} else {
				m = EventEmitter.defaultMaxListeners;
			}

			if (m && m > 0 && this._events[type].length > m) {
				this._events[type].warned = true;
				console.error(
					"(node) warning: possible EventEmitter memory " + "leak detected. %d listeners added. " + "Use emitter.setMaxListeners() to increase limit.",
					this._events[type].length
				);
				if (typeof console.trace === "function") {
					// not supported in IE 10
					console.trace();
				}
			}
		}

		return this;
	};

	EventEmitter.prototype.on = EventEmitter.prototype.addListener;

	EventEmitter.prototype.once = function(type, listener) {
		if (!isFunction(listener)) throw TypeError("listener must be a function");

		var fired = false;

		function g() {
			this.removeListener(type, g);

			if (!fired) {
				fired = true;
				listener.apply(this, arguments);
			}
		}

		g.listener = listener;
		this.on(type, g);

		return this;
	};

	// emits a 'removeListener' event iff the listener was removed
	EventEmitter.prototype.removeListener = function(type, listener) {
		var list, position, length, i;

		if (!isFunction(listener)) throw TypeError("listener must be a function");

		if (!this._events || !this._events[type]) return this;

		list = this._events[type];
		length = list.length;
		position = -1;

		if (list === listener || (isFunction(list.listener) && list.listener === listener)) {
			delete this._events[type];
			if (this._events.removeListener) this.emit("removeListener", type, listener);
		} else if (isObject(list)) {
			for (i = length; i-- > 0; ) {
				if (list[i] === listener || (list[i].listener && list[i].listener === listener)) {
					position = i;
					break;
				}
			}

			if (position < 0) return this;

			if (list.length === 1) {
				list.length = 0;
				delete this._events[type];
			} else {
				list.splice(position, 1);
			}

			if (this._events.removeListener) this.emit("removeListener", type, listener);
		}

		return this;
	};

	EventEmitter.prototype.removeAllListeners = function(type) {
		var key, listeners;

		if (!this._events) return this;

		// not listening for removeListener, no need to emit
		if (!this._events.removeListener) {
			if (arguments.length === 0) this._events = {};
			else if (this._events[type]) delete this._events[type];
			return this;
		}

		// emit removeListener for all listeners on all events
		if (arguments.length === 0) {
			for (key in this._events) {
				if (key === "removeListener") continue;
				this.removeAllListeners(key);
			}
			this.removeAllListeners("removeListener");
			this._events = {};
			return this;
		}

		listeners = this._events[type];

		if (isFunction(listeners)) {
			this.removeListener(type, listeners);
		} else if (listeners) {
			// LIFO order
			while (listeners.length) this.removeListener(type, listeners[listeners.length - 1]);
		}
		delete this._events[type];

		return this;
	};

	EventEmitter.prototype.listeners = function(type) {
		var ret;
		if (!this._events || !this._events[type]) ret = [];
		else if (isFunction(this._events[type])) ret = [this._events[type]];
		else ret = this._events[type].slice();
		return ret;
	};

	EventEmitter.prototype.listenerCount = function(type) {
		if (this._events) {
			var evlistener = this._events[type];

			if (isFunction(evlistener)) return 1;
			else if (evlistener) return evlistener.length;
		}
		return 0;
	};

	EventEmitter.listenerCount = function(emitter, type) {
		return emitter.listenerCount(type);
	};

	function isFunction(arg) {
		return typeof arg === "function";
	}

	function isNumber(arg) {
		return typeof arg === "number";
	}

	function isObject(arg) {
		return typeof arg === "object" && arg !== null;
	}

	function isUndefined(arg) {
		return arg === void 0;
	}
}

});
return ___scope___.entry = "index.js";
});
FuseBox.pkg("fuse-box-css", {}, function(___scope___){
___scope___.file("index.js", function(exports, require, module, __filename, __dirname){

/**
 * Listens to 'async' requets and if the name is a css file
 * wires it to `__fsbx_css`
 */

var runningInBrowser = FuseBox.isBrowser || FuseBox.target === "electron";

var cssHandler = function(__filename, contents) {
	if (runningInBrowser) {
		var styleId = __filename.replace(/[\.\/]+/g, "-");
		if (styleId.charAt(0) === "-") styleId = styleId.substring(1);
		var exists = document.getElementById(styleId);
		if (!exists) {
			//<link href="//fonts.googleapis.com/css?family=Covered+By+Your+Grace" rel="stylesheet" type="text/css">
			var s = document.createElement(contents ? "style" : "link");
			s.id = styleId;
			s.type = "text/css";
			if (contents) {
				s.innerHTML = contents;
			} else {
				s.rel = "stylesheet";
				s.href = __filename;
			}
			document.getElementsByTagName("head")[0].appendChild(s);
		} else {
			if (contents) {
				exists.innerHTML = contents;
			}
		}
	}
};
if (typeof FuseBox !== "undefined" && runningInBrowser) {
	FuseBox.on("async", function(name) {
		if (/\.css$/.test(name)) {
			cssHandler(name);
			return false;
		}
	});
}

module.exports = cssHandler;

});
return ___scope___.entry = "index.js";
});
FuseBox.pkg("fusebox-hot-reload", {}, function(___scope___){
___scope___.file("index.js", function(exports, require, module, __filename, __dirname){

"use strict";
/**
 * @module listens to `source-changed` socket events and actions hot reload
 */
Object.defineProperty(exports, "__esModule", { value: true });
var Client = require("fusebox-websocket").SocketClient, bundleErrors = {}, outputElement = document.createElement("div"), styleElement = document.createElement("style"), minimizeToggleId = "fuse-box-toggle-minimized", hideButtonId = "fuse-box-hide", expandedOutputClass = "fuse-box-expanded-output", localStoragePrefix = "__fuse-box_";
function storeSetting(key, value) {
    localStorage[localStoragePrefix + key] = value;
}
function getSetting(key) {
    return localStorage[localStoragePrefix + key] === "true" ? true : false;
}
var outputInBody = false, outputMinimized = getSetting(minimizeToggleId), outputHidden = false;
outputElement.id = "fuse-box-output";
styleElement.innerHTML = "\n    #" + outputElement.id + ", #" + outputElement.id + " * {\n        box-sizing: border-box;\n    }\n    #" + outputElement.id + " {\n        z-index: 999999999999;\n        position: fixed;\n        top: 10px;\n        right: 10px;\n        width: 400px;\n        overflow: auto;\n        background: #fdf3f1;\n        border: 1px solid #eca494;\n        border-radius: 5px;\n        font-family: \"Helvetica Neue\", Helvetica, Arial, sans-serif;\n        box-shadow: 0px 3px 6px 1px rgba(0,0,0,.15);\n    }\n    #" + outputElement.id + "." + expandedOutputClass + " {\n        height: auto;\n        width: auto;\n        left: 10px;\n        max-height: calc(100vh - 50px);\n    }\n    #" + outputElement.id + " .fuse-box-errors {\n        display: none;\n    }\n    #" + outputElement.id + "." + expandedOutputClass + " .fuse-box-errors {\n        display: block;\n        border-top: 1px solid #eca494;\n        padding: 0 10px;\n    }\n    #" + outputElement.id + " button {\n        border: 1px solid #eca494;\n        padding: 5px 10px;\n        border-radius: 4px;\n        margin-left: 5px;\n        background-color: white;\n        color: black;\n        box-shadow: 0px 2px 2px 0px rgba(0,0,0,.05);\n    }\n    #" + outputElement.id + " .fuse-box-header {\n        padding: 10px;\n    }\n    #" + outputElement.id + " .fuse-box-header h4 {\n        display: inline-block;\n        margin: 4px;\n    }";
styleElement.type = "text/css";
document.getElementsByTagName("head")[0].appendChild(styleElement);
function displayBundleErrors() {
    var errorMessages = Object.keys(bundleErrors).reduce(function (allMessages, bundleName) {
        var bundleMessages = bundleErrors[bundleName];
        return allMessages.concat(bundleMessages.map(function (message) {
            var messageOutput = message
                .replace(/\n/g, "<br>")
                .replace(/\t/g, "&nbsp;&nbps;&npbs;&nbps;")
                .replace(/ /g, "&nbsp;");
            return "<pre>" + messageOutput + "</pre>";
        }));
    }, []), errorOutput = errorMessages.join("");
    if (errorOutput && !outputHidden) {
        outputElement.innerHTML = "\n        <div class=\"fuse-box-header\" style=\"\">\n            <h4 style=\"\">Fuse Box Bundle Errors (" + errorMessages.length + "):</h4>\n            <div style=\"float: right;\">\n                <button id=\"" + minimizeToggleId + "\">" + (outputMinimized ? "Expand" : "Minimize") + "</button>\n                <button id=\"" + hideButtonId + "\">Hide</button>\n            </div>\n        </div>\n        <div class=\"fuse-box-errors\">\n            " + errorOutput + "\n        </div>\n        ";
        document.body.appendChild(outputElement);
        outputElement.className = outputMinimized ? "" : expandedOutputClass;
        outputInBody = true;
        document.getElementById(minimizeToggleId).onclick = function () {
            outputMinimized = !outputMinimized;
            storeSetting(minimizeToggleId, outputMinimized);
            displayBundleErrors();
        };
        document.getElementById(hideButtonId).onclick = function () {
            outputHidden = true;
            displayBundleErrors();
        };
    }
    else if (outputInBody) {
        document.body.removeChild(outputElement);
        outputInBody = false;
    }
}
exports.connect = function (port, uri, reloadFullPage) {
    if (FuseBox.isServer) {
        return;
    }
    port = port || window.location.port;
    var client = new Client({
        port: port,
        uri: uri
    });
    client.connect();
    client.on("page-reload", function (data) {
        return window.location.reload();
    });
    client.on("page-hmr", function (data) {
        FuseBox.flush();
        FuseBox.dynamic(data.path, data.content);
        if (FuseBox.mainFile) {
            try {
                FuseBox.import(FuseBox.mainFile);
            }
            catch (e) {
                if (typeof e === "string") {
                    if (/not found/.test(e)) {
                        return window.location.reload();
                    }
                }
                console.error(e);
            }
        }
    });
    client.on("source-changed", function (data) {
        console.info("%cupdate \"" + data.path + "\"", "color: #237abe");
        if (reloadFullPage) {
            return window.location.reload();
        }
        /**
         * If a plugin handles this request then we don't have to do anything
         **/
        for (var index = 0; index < FuseBox.plugins.length; index++) {
            var plugin = FuseBox.plugins[index];
            if (plugin.hmrUpdate && plugin.hmrUpdate(data)) {
                return;
            }
        }
        if (data.type === "hosted-css") {
            var fileId = data.path.replace(/^\//, "").replace(/[\.\/]+/g, "-");
            var existing = document.getElementById(fileId);
            if (existing) {
                existing.setAttribute("href", data.path + "?" + new Date().getTime());
            }
            else {
                var node = document.createElement("link");
                node.id = fileId;
                node.type = "text/css";
                node.rel = "stylesheet";
                node.href = data.path;
                document.getElementsByTagName("head")[0].appendChild(node);
            }
        }
        if (data.type === "js" || data.type === "css") {
            FuseBox.flush();
            FuseBox.dynamic(data.path, data.content);
            if (FuseBox.mainFile) {
                try {
                    FuseBox.import(FuseBox.mainFile);
                }
                catch (e) {
                    if (typeof e === "string") {
                        if (/not found/.test(e)) {
                            return window.location.reload();
                        }
                    }
                    console.error(e);
                }
            }
        }
    });
    client.on("error", function (error) {
        console.log(error);
    });
    client.on("bundle-error", function (_a) {
        var bundleName = _a.bundleName, message = _a.message;
        console.error("Bundle error in " + bundleName + ": " + message);
        var errorsForBundle = bundleErrors[bundleName] || [];
        errorsForBundle.push(message);
        bundleErrors[bundleName] = errorsForBundle;
        displayBundleErrors();
    });
    client.on("update-bundle-errors", function (_a) {
        var bundleName = _a.bundleName, messages = _a.messages;
        messages.forEach(function (message) { return console.error("Bundle error in " + bundleName + ": " + message); });
        bundleErrors[bundleName] = messages;
        displayBundleErrors();
    });
};

});
return ___scope___.entry = "index.js";
});
FuseBox.pkg("fusebox-websocket", {}, function(___scope___){
___scope___.file("index.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var events = require("events");
var SocketClient = /** @class */ (function () {
    function SocketClient(opts) {
        opts = opts || {};
        var port = opts.port || window.location.port;
        var protocol = location.protocol === "https:" ? "wss://" : "ws://";
        var domain = location.hostname || "localhost";
        this.url = opts.host || "" + protocol + domain + ":" + port;
        if (opts.uri) {
            this.url = opts.uri;
        }
        this.authSent = false;
        this.emitter = new events.EventEmitter();
    }
    SocketClient.prototype.reconnect = function (fn) {
        var _this = this;
        setTimeout(function () {
            _this.emitter.emit("reconnect", { message: "Trying to reconnect" });
            _this.connect(fn);
        }, 5000);
    };
    SocketClient.prototype.on = function (event, fn) {
        this.emitter.on(event, fn);
    };
    SocketClient.prototype.connect = function (fn) {
        var _this = this;
        console.log("%cConnecting to fusebox HMR at " + this.url, "color: #237abe");
        setTimeout(function () {
            _this.client = new WebSocket(_this.url);
            _this.bindEvents(fn);
        }, 0);
    };
    SocketClient.prototype.close = function () {
        this.client.close();
    };
    SocketClient.prototype.send = function (eventName, data) {
        if (this.client.readyState === 1) {
            this.client.send(JSON.stringify({ event: eventName, data: data || {} }));
        }
    };
    SocketClient.prototype.error = function (data) {
        this.emitter.emit("error", data);
    };
    /** Wires up the socket client messages to be emitted on our event emitter */
    SocketClient.prototype.bindEvents = function (fn) {
        var _this = this;
        this.client.onopen = function (event) {
            console.log("%cConnected", "color: #237abe");
            if (fn) {
                fn(_this);
            }
        };
        this.client.onerror = function (event) {
            _this.error({ reason: event.reason, message: "Socket error" });
        };
        this.client.onclose = function (event) {
            _this.emitter.emit("close", { message: "Socket closed" });
            if (event.code !== 1011) {
                _this.reconnect(fn);
            }
        };
        this.client.onmessage = function (event) {
            var data = event.data;
            if (data) {
                var item = JSON.parse(data);
                _this.emitter.emit(item.type, item.data);
                _this.emitter.emit("*", item);
            }
        };
    };
    return SocketClient;
}());
exports.SocketClient = SocketClient;

});
return ___scope___.entry = "index.js";
});
FuseBox.pkg("http", {}, function(___scope___){
___scope___.file("index.js", function(exports, require, module, __filename, __dirname){

if (FuseBox.isServer) {
	module.exports = global.require("http");
} else {
	module.exports = {};
}

});
return ___scope___.entry = "index.js";
});
FuseBox.pkg("linkify-it", {}, function(___scope___){
___scope___.file("index.js", function(exports, require, module, __filename, __dirname){
/* fuse:injection: */ var http = require("http");
'use strict';


////////////////////////////////////////////////////////////////////////////////
// Helpers

// Merge objects
//
function assign(obj /*from1, from2, from3, ...*/) {
  var sources = Array.prototype.slice.call(arguments, 1);

  sources.forEach(function (source) {
    if (!source) { return; }

    Object.keys(source).forEach(function (key) {
      obj[key] = source[key];
    });
  });

  return obj;
}

function _class(obj) { return Object.prototype.toString.call(obj); }
function isString(obj) { return _class(obj) === '[object String]'; }
function isObject(obj) { return _class(obj) === '[object Object]'; }
function isRegExp(obj) { return _class(obj) === '[object RegExp]'; }
function isFunction(obj) { return _class(obj) === '[object Function]'; }


function escapeRE(str) { return str.replace(/[.?*+^$[\]\\(){}|-]/g, '\\$&'); }

////////////////////////////////////////////////////////////////////////////////


var defaultOptions = {
  fuzzyLink: true,
  fuzzyEmail: true,
  fuzzyIP: false
};


function isOptionsObj(obj) {
  return Object.keys(obj || {}).reduce(function (acc, k) {
    return acc || defaultOptions.hasOwnProperty(k);
  }, false);
}


var defaultSchemas = {
  'http:': {
    validate: function (text, pos, self) {
      var tail = text.slice(pos);

      if (!self.re.http) {
        // compile lazily, because "host"-containing variables can change on tlds update.
        self.re.http =  new RegExp(
          '^\\/\\/' + self.re.src_auth + self.re.src_host_port_strict + self.re.src_path, 'i'
        );
      }
      if (self.re.http.test(tail)) {
        return tail.match(self.re.http)[0].length;
      }
      return 0;
    }
  },
  'https:':  'http:',
  'ftp:':    'http:',
  '//':      {
    validate: function (text, pos, self) {
      var tail = text.slice(pos);

      if (!self.re.no_http) {
      // compile lazily, because "host"-containing variables can change on tlds update.
        self.re.no_http =  new RegExp(
          '^' +
          self.re.src_auth +
          // Don't allow single-level domains, because of false positives like '//test'
          // with code comments
          '(?:localhost|(?:(?:' + self.re.src_domain + ')\\.)+' + self.re.src_domain_root + ')' +
          self.re.src_port +
          self.re.src_host_terminator +
          self.re.src_path,

          'i'
        );
      }

      if (self.re.no_http.test(tail)) {
        // should not be `://` & `///`, that protects from errors in protocol name
        if (pos >= 3 && text[pos - 3] === ':') { return 0; }
        if (pos >= 3 && text[pos - 3] === '/') { return 0; }
        return tail.match(self.re.no_http)[0].length;
      }
      return 0;
    }
  },
  'mailto:': {
    validate: function (text, pos, self) {
      var tail = text.slice(pos);

      if (!self.re.mailto) {
        self.re.mailto =  new RegExp(
          '^' + self.re.src_email_name + '@' + self.re.src_host_strict, 'i'
        );
      }
      if (self.re.mailto.test(tail)) {
        return tail.match(self.re.mailto)[0].length;
      }
      return 0;
    }
  }
};

/*eslint-disable max-len*/

// RE pattern for 2-character tlds (autogenerated by ./support/tlds_2char_gen.js)
var tlds_2ch_src_re = 'a[cdefgilmnoqrstuwxz]|b[abdefghijmnorstvwyz]|c[acdfghiklmnoruvwxyz]|d[ejkmoz]|e[cegrstu]|f[ijkmor]|g[abdefghilmnpqrstuwy]|h[kmnrtu]|i[delmnoqrst]|j[emop]|k[eghimnprwyz]|l[abcikrstuvy]|m[acdeghklmnopqrstuvwxyz]|n[acefgilopruz]|om|p[aefghklmnrstwy]|qa|r[eosuw]|s[abcdeghijklmnortuvxyz]|t[cdfghjklmnortvwz]|u[agksyz]|v[aceginu]|w[fs]|y[et]|z[amw]';

// DON'T try to make PRs with changes. Extend TLDs with LinkifyIt.tlds() instead
var tlds_default = 'biz|com|edu|gov|net|org|pro|web|xxx|aero|asia|coop|info|museum|name|shop|рф'.split('|');

/*eslint-enable max-len*/

////////////////////////////////////////////////////////////////////////////////

function resetScanCache(self) {
  self.__index__ = -1;
  self.__text_cache__   = '';
}

function createValidator(re) {
  return function (text, pos) {
    var tail = text.slice(pos);

    if (re.test(tail)) {
      return tail.match(re)[0].length;
    }
    return 0;
  };
}

function createNormalizer() {
  return function (match, self) {
    self.normalize(match);
  };
}

// Schemas compiler. Build regexps.
//
function compile(self) {

  // Load & clone RE patterns.
  var re = self.re = require('./lib/re')(self.__opts__);

  // Define dynamic patterns
  var tlds = self.__tlds__.slice();

  self.onCompile();

  if (!self.__tlds_replaced__) {
    tlds.push(tlds_2ch_src_re);
  }
  tlds.push(re.src_xn);

  re.src_tlds = tlds.join('|');

  function untpl(tpl) { return tpl.replace('%TLDS%', re.src_tlds); }

  re.email_fuzzy      = RegExp(untpl(re.tpl_email_fuzzy), 'i');
  re.link_fuzzy       = RegExp(untpl(re.tpl_link_fuzzy), 'i');
  re.link_no_ip_fuzzy = RegExp(untpl(re.tpl_link_no_ip_fuzzy), 'i');
  re.host_fuzzy_test  = RegExp(untpl(re.tpl_host_fuzzy_test), 'i');

  //
  // Compile each schema
  //

  var aliases = [];

  self.__compiled__ = {}; // Reset compiled data

  function schemaError(name, val) {
    throw new Error('(LinkifyIt) Invalid schema "' + name + '": ' + val);
  }

  Object.keys(self.__schemas__).forEach(function (name) {
    var val = self.__schemas__[name];

    // skip disabled methods
    if (val === null) { return; }

    var compiled = { validate: null, link: null };

    self.__compiled__[name] = compiled;

    if (isObject(val)) {
      if (isRegExp(val.validate)) {
        compiled.validate = createValidator(val.validate);
      } else if (isFunction(val.validate)) {
        compiled.validate = val.validate;
      } else {
        schemaError(name, val);
      }

      if (isFunction(val.normalize)) {
        compiled.normalize = val.normalize;
      } else if (!val.normalize) {
        compiled.normalize = createNormalizer();
      } else {
        schemaError(name, val);
      }

      return;
    }

    if (isString(val)) {
      aliases.push(name);
      return;
    }

    schemaError(name, val);
  });

  //
  // Compile postponed aliases
  //

  aliases.forEach(function (alias) {
    if (!self.__compiled__[self.__schemas__[alias]]) {
      // Silently fail on missed schemas to avoid errons on disable.
      // schemaError(alias, self.__schemas__[alias]);
      return;
    }

    self.__compiled__[alias].validate =
      self.__compiled__[self.__schemas__[alias]].validate;
    self.__compiled__[alias].normalize =
      self.__compiled__[self.__schemas__[alias]].normalize;
  });

  //
  // Fake record for guessed links
  //
  self.__compiled__[''] = { validate: null, normalize: createNormalizer() };

  //
  // Build schema condition
  //
  var slist = Object.keys(self.__compiled__)
                      .filter(function (name) {
                        // Filter disabled & fake schemas
                        return name.length > 0 && self.__compiled__[name];
                      })
                      .map(escapeRE)
                      .join('|');
  // (?!_) cause 1.5x slowdown
  self.re.schema_test   = RegExp('(^|(?!_)(?:[><\uff5c]|' + re.src_ZPCc + '))(' + slist + ')', 'i');
  self.re.schema_search = RegExp('(^|(?!_)(?:[><\uff5c]|' + re.src_ZPCc + '))(' + slist + ')', 'ig');

  self.re.pretest       = RegExp(
                            '(' + self.re.schema_test.source + ')|' +
                            '(' + self.re.host_fuzzy_test.source + ')|' +
                            '@',
                            'i');

  //
  // Cleanup
  //

  resetScanCache(self);
}

/**
 * class Match
 *
 * Match result. Single element of array, returned by [[LinkifyIt#match]]
 **/
function Match(self, shift) {
  var start = self.__index__,
      end   = self.__last_index__,
      text  = self.__text_cache__.slice(start, end);

  /**
   * Match#schema -> String
   *
   * Prefix (protocol) for matched string.
   **/
  this.schema    = self.__schema__.toLowerCase();
  /**
   * Match#index -> Number
   *
   * First position of matched string.
   **/
  this.index     = start + shift;
  /**
   * Match#lastIndex -> Number
   *
   * Next position after matched string.
   **/
  this.lastIndex = end + shift;
  /**
   * Match#raw -> String
   *
   * Matched string.
   **/
  this.raw       = text;
  /**
   * Match#text -> String
   *
   * Notmalized text of matched string.
   **/
  this.text      = text;
  /**
   * Match#url -> String
   *
   * Normalized url of matched string.
   **/
  this.url       = text;
}

function createMatch(self, shift) {
  var match = new Match(self, shift);

  self.__compiled__[match.schema].normalize(match, self);

  return match;
}


/**
 * class LinkifyIt
 **/

/**
 * new LinkifyIt(schemas, options)
 * - schemas (Object): Optional. Additional schemas to validate (prefix/validator)
 * - options (Object): { fuzzyLink|fuzzyEmail|fuzzyIP: true|false }
 *
 * Creates new linkifier instance with optional additional schemas.
 * Can be called without `new` keyword for convenience.
 *
 * By default understands:
 *
 * - `http(s)://...` , `ftp://...`, `mailto:...` & `//...` links
 * - "fuzzy" links and emails (example.com, foo@bar.com).
 *
 * `schemas` is an object, where each key/value describes protocol/rule:
 *
 * - __key__ - link prefix (usually, protocol name with `:` at the end, `skype:`
 *   for example). `linkify-it` makes shure that prefix is not preceeded with
 *   alphanumeric char and symbols. Only whitespaces and punctuation allowed.
 * - __value__ - rule to check tail after link prefix
 *   - _String_ - just alias to existing rule
 *   - _Object_
 *     - _validate_ - validator function (should return matched length on success),
 *       or `RegExp`.
 *     - _normalize_ - optional function to normalize text & url of matched result
 *       (for example, for @twitter mentions).
 *
 * `options`:
 *
 * - __fuzzyLink__ - recognige URL-s without `http(s):` prefix. Default `true`.
 * - __fuzzyIP__ - allow IPs in fuzzy links above. Can conflict with some texts
 *   like version numbers. Default `false`.
 * - __fuzzyEmail__ - recognize emails without `mailto:` prefix.
 *
 **/
function LinkifyIt(schemas, options) {
  if (!(this instanceof LinkifyIt)) {
    return new LinkifyIt(schemas, options);
  }

  if (!options) {
    if (isOptionsObj(schemas)) {
      options = schemas;
      schemas = {};
    }
  }

  this.__opts__           = assign({}, defaultOptions, options);

  // Cache last tested result. Used to skip repeating steps on next `match` call.
  this.__index__          = -1;
  this.__last_index__     = -1; // Next scan position
  this.__schema__         = '';
  this.__text_cache__     = '';

  this.__schemas__        = assign({}, defaultSchemas, schemas);
  this.__compiled__       = {};

  this.__tlds__           = tlds_default;
  this.__tlds_replaced__  = false;

  this.re = {};

  compile(this);
}


/** chainable
 * LinkifyIt#add(schema, definition)
 * - schema (String): rule name (fixed pattern prefix)
 * - definition (String|RegExp|Object): schema definition
 *
 * Add new rule definition. See constructor description for details.
 **/
LinkifyIt.prototype.add = function add(schema, definition) {
  this.__schemas__[schema] = definition;
  compile(this);
  return this;
};


/** chainable
 * LinkifyIt#set(options)
 * - options (Object): { fuzzyLink|fuzzyEmail|fuzzyIP: true|false }
 *
 * Set recognition options for links without schema.
 **/
LinkifyIt.prototype.set = function set(options) {
  this.__opts__ = assign(this.__opts__, options);
  return this;
};


/**
 * LinkifyIt#test(text) -> Boolean
 *
 * Searches linkifiable pattern and returns `true` on success or `false` on fail.
 **/
LinkifyIt.prototype.test = function test(text) {
  // Reset scan cache
  this.__text_cache__ = text;
  this.__index__      = -1;

  if (!text.length) { return false; }

  var m, ml, me, len, shift, next, re, tld_pos, at_pos;

  // try to scan for link with schema - that's the most simple rule
  if (this.re.schema_test.test(text)) {
    re = this.re.schema_search;
    re.lastIndex = 0;
    while ((m = re.exec(text)) !== null) {
      len = this.testSchemaAt(text, m[2], re.lastIndex);
      if (len) {
        this.__schema__     = m[2];
        this.__index__      = m.index + m[1].length;
        this.__last_index__ = m.index + m[0].length + len;
        break;
      }
    }
  }

  if (this.__opts__.fuzzyLink && this.__compiled__['http:']) {
    // guess schemaless links
    tld_pos = text.search(this.re.host_fuzzy_test);
    if (tld_pos >= 0) {
      // if tld is located after found link - no need to check fuzzy pattern
      if (this.__index__ < 0 || tld_pos < this.__index__) {
        if ((ml = text.match(this.__opts__.fuzzyIP ? this.re.link_fuzzy : this.re.link_no_ip_fuzzy)) !== null) {

          shift = ml.index + ml[1].length;

          if (this.__index__ < 0 || shift < this.__index__) {
            this.__schema__     = '';
            this.__index__      = shift;
            this.__last_index__ = ml.index + ml[0].length;
          }
        }
      }
    }
  }

  if (this.__opts__.fuzzyEmail && this.__compiled__['mailto:']) {
    // guess schemaless emails
    at_pos = text.indexOf('@');
    if (at_pos >= 0) {
      // We can't skip this check, because this cases are possible:
      // 192.168.1.1@gmail.com, my.in@example.com
      if ((me = text.match(this.re.email_fuzzy)) !== null) {

        shift = me.index + me[1].length;
        next  = me.index + me[0].length;

        if (this.__index__ < 0 || shift < this.__index__ ||
            (shift === this.__index__ && next > this.__last_index__)) {
          this.__schema__     = 'mailto:';
          this.__index__      = shift;
          this.__last_index__ = next;
        }
      }
    }
  }

  return this.__index__ >= 0;
};


/**
 * LinkifyIt#pretest(text) -> Boolean
 *
 * Very quick check, that can give false positives. Returns true if link MAY BE
 * can exists. Can be used for speed optimization, when you need to check that
 * link NOT exists.
 **/
LinkifyIt.prototype.pretest = function pretest(text) {
  return this.re.pretest.test(text);
};


/**
 * LinkifyIt#testSchemaAt(text, name, position) -> Number
 * - text (String): text to scan
 * - name (String): rule (schema) name
 * - position (Number): text offset to check from
 *
 * Similar to [[LinkifyIt#test]] but checks only specific protocol tail exactly
 * at given position. Returns length of found pattern (0 on fail).
 **/
LinkifyIt.prototype.testSchemaAt = function testSchemaAt(text, schema, pos) {
  // If not supported schema check requested - terminate
  if (!this.__compiled__[schema.toLowerCase()]) {
    return 0;
  }
  return this.__compiled__[schema.toLowerCase()].validate(text, pos, this);
};


/**
 * LinkifyIt#match(text) -> Array|null
 *
 * Returns array of found link descriptions or `null` on fail. We strongly
 * recommend to use [[LinkifyIt#test]] first, for best speed.
 *
 * ##### Result match description
 *
 * - __schema__ - link schema, can be empty for fuzzy links, or `//` for
 *   protocol-neutral  links.
 * - __index__ - offset of matched text
 * - __lastIndex__ - index of next char after mathch end
 * - __raw__ - matched text
 * - __text__ - normalized text
 * - __url__ - link, generated from matched text
 **/
LinkifyIt.prototype.match = function match(text) {
  var shift = 0, result = [];

  // Try to take previous element from cache, if .test() called before
  if (this.__index__ >= 0 && this.__text_cache__ === text) {
    result.push(createMatch(this, shift));
    shift = this.__last_index__;
  }

  // Cut head if cache was used
  var tail = shift ? text.slice(shift) : text;

  // Scan string until end reached
  while (this.test(tail)) {
    result.push(createMatch(this, shift));

    tail = tail.slice(this.__last_index__);
    shift += this.__last_index__;
  }

  if (result.length) {
    return result;
  }

  return null;
};


/** chainable
 * LinkifyIt#tlds(list [, keepOld]) -> this
 * - list (Array): list of tlds
 * - keepOld (Boolean): merge with current list if `true` (`false` by default)
 *
 * Load (or merge) new tlds list. Those are user for fuzzy links (without prefix)
 * to avoid false positives. By default this algorythm used:
 *
 * - hostname with any 2-letter root zones are ok.
 * - biz|com|edu|gov|net|org|pro|web|xxx|aero|asia|coop|info|museum|name|shop|рф
 *   are ok.
 * - encoded (`xn--...`) root zones are ok.
 *
 * If list is replaced, then exact match for 2-chars root zones will be checked.
 **/
LinkifyIt.prototype.tlds = function tlds(list, keepOld) {
  list = Array.isArray(list) ? list : [ list ];

  if (!keepOld) {
    this.__tlds__ = list.slice();
    this.__tlds_replaced__ = true;
    compile(this);
    return this;
  }

  this.__tlds__ = this.__tlds__.concat(list)
                                  .sort()
                                  .filter(function (el, idx, arr) {
                                    return el !== arr[idx - 1];
                                  })
                                  .reverse();

  compile(this);
  return this;
};

/**
 * LinkifyIt#normalize(match)
 *
 * Default normalizer (if schema does not define it's own).
 **/
LinkifyIt.prototype.normalize = function normalize(match) {

  // Do minimal possible changes by default. Need to collect feedback prior
  // to move forward https://github.com/markdown-it/linkify-it/issues/1

  if (!match.schema) { match.url = 'http://' + match.url; }

  if (match.schema === 'mailto:' && !/^mailto:/i.test(match.url)) {
    match.url = 'mailto:' + match.url;
  }
};


/**
 * LinkifyIt#onCompile()
 *
 * Override to modify basic RegExp-s.
 **/
LinkifyIt.prototype.onCompile = function onCompile() {
};


module.exports = LinkifyIt;

});
___scope___.file("lib/re.js", function(exports, require, module, __filename, __dirname){

'use strict';


module.exports = function (opts) {
  var re = {};

  // Use direct extract instead of `regenerate` to reduse browserified size
  re.src_Any = require('uc.micro/properties/Any/regex').source;
  re.src_Cc  = require('uc.micro/categories/Cc/regex').source;
  re.src_Z   = require('uc.micro/categories/Z/regex').source;
  re.src_P   = require('uc.micro/categories/P/regex').source;

  // \p{\Z\P\Cc\CF} (white spaces + control + format + punctuation)
  re.src_ZPCc = [ re.src_Z, re.src_P, re.src_Cc ].join('|');

  // \p{\Z\Cc} (white spaces + control)
  re.src_ZCc = [ re.src_Z, re.src_Cc ].join('|');

  // Experimental. List of chars, completely prohibited in links
  // because can separate it from other part of text
  var text_separators = '[><\uff5c]';

  // All possible word characters (everything without punctuation, spaces & controls)
  // Defined via punctuation & spaces to save space
  // Should be something like \p{\L\N\S\M} (\w but without `_`)
  re.src_pseudo_letter       = '(?:(?!' + text_separators + '|' + re.src_ZPCc + ')' + re.src_Any + ')';
  // The same as abothe but without [0-9]
  // var src_pseudo_letter_non_d = '(?:(?![0-9]|' + src_ZPCc + ')' + src_Any + ')';

  ////////////////////////////////////////////////////////////////////////////////

  re.src_ip4 =

    '(?:(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)';

  // Prohibit any of "@/[]()" in user/pass to avoid wrong domain fetch.
  re.src_auth    = '(?:(?:(?!' + re.src_ZCc + '|[@/\\[\\]()]).)+@)?';

  re.src_port =

    '(?::(?:6(?:[0-4]\\d{3}|5(?:[0-4]\\d{2}|5(?:[0-2]\\d|3[0-5])))|[1-5]?\\d{1,4}))?';

  re.src_host_terminator =

    '(?=$|' + text_separators + '|' + re.src_ZPCc + ')(?!-|_|:\\d|\\.-|\\.(?!$|' + re.src_ZPCc + '))';

  re.src_path =

    '(?:' +
      '[/?#]' +
        '(?:' +
          '(?!' + re.src_ZCc + '|' + text_separators + '|[()[\\]{}.,"\'?!\\-]).|' +
          '\\[(?:(?!' + re.src_ZCc + '|\\]).)*\\]|' +
          '\\((?:(?!' + re.src_ZCc + '|[)]).)*\\)|' +
          '\\{(?:(?!' + re.src_ZCc + '|[}]).)*\\}|' +
          '\\"(?:(?!' + re.src_ZCc + '|["]).)+\\"|' +
          "\\'(?:(?!" + re.src_ZCc + "|[']).)+\\'|" +
          "\\'(?=" + re.src_pseudo_letter + '|[-]).|' +  // allow `I'm_king` if no pair found
          '\\.{2,3}[a-zA-Z0-9%/]|' + // github has ... in commit range links. Restrict to
                                     // - english
                                     // - percent-encoded
                                     // - parts of file path
                                     // until more examples found.
          '\\.(?!' + re.src_ZCc + '|[.]).|' +
          (opts && opts['---'] ?
            '\\-(?!--(?:[^-]|$))(?:-*)|' // `---` => long dash, terminate
          :
            '\\-+|'
          ) +
          '\\,(?!' + re.src_ZCc + ').|' +      // allow `,,,` in paths
          '\\!(?!' + re.src_ZCc + '|[!]).|' +
          '\\?(?!' + re.src_ZCc + '|[?]).' +
        ')+' +
      '|\\/' +
    ')?';

  re.src_email_name =

    '[\\-;:&=\\+\\$,\\"\\.a-zA-Z0-9_]+';

  re.src_xn =

    'xn--[a-z0-9\\-]{1,59}';

  // More to read about domain names
  // http://serverfault.com/questions/638260/

  re.src_domain_root =

    // Allow letters & digits (http://test1)
    '(?:' +
      re.src_xn +
      '|' +
      re.src_pseudo_letter + '{1,63}' +
    ')';

  re.src_domain =

    '(?:' +
      re.src_xn +
      '|' +
      '(?:' + re.src_pseudo_letter + ')' +
      '|' +
      // don't allow `--` in domain names, because:
      // - that can conflict with markdown &mdash; / &ndash;
      // - nobody use those anyway
      '(?:' + re.src_pseudo_letter + '(?:-(?!-)|' + re.src_pseudo_letter + '){0,61}' + re.src_pseudo_letter + ')' +
    ')';

  re.src_host =

    '(?:' +
    // Don't need IP check, because digits are already allowed in normal domain names
    //   src_ip4 +
    // '|' +
      '(?:(?:(?:' + re.src_domain + ')\\.)*' + re.src_domain/*_root*/ + ')' +
    ')';

  re.tpl_host_fuzzy =

    '(?:' +
      re.src_ip4 +
    '|' +
      '(?:(?:(?:' + re.src_domain + ')\\.)+(?:%TLDS%))' +
    ')';

  re.tpl_host_no_ip_fuzzy =

    '(?:(?:(?:' + re.src_domain + ')\\.)+(?:%TLDS%))';

  re.src_host_strict =

    re.src_host + re.src_host_terminator;

  re.tpl_host_fuzzy_strict =

    re.tpl_host_fuzzy + re.src_host_terminator;

  re.src_host_port_strict =

    re.src_host + re.src_port + re.src_host_terminator;

  re.tpl_host_port_fuzzy_strict =

    re.tpl_host_fuzzy + re.src_port + re.src_host_terminator;

  re.tpl_host_port_no_ip_fuzzy_strict =

    re.tpl_host_no_ip_fuzzy + re.src_port + re.src_host_terminator;


  ////////////////////////////////////////////////////////////////////////////////
  // Main rules

  // Rude test fuzzy links by host, for quick deny
  re.tpl_host_fuzzy_test =

    'localhost|www\\.|\\.\\d{1,3}\\.|(?:\\.(?:%TLDS%)(?:' + re.src_ZPCc + '|>|$))';

  re.tpl_email_fuzzy =

      '(^|' + text_separators + '|\\(|' + re.src_ZCc + ')(' + re.src_email_name + '@' + re.tpl_host_fuzzy_strict + ')';

  re.tpl_link_fuzzy =
      // Fuzzy link can't be prepended with .:/\- and non punctuation.
      // but can start with > (markdown blockquote)
      '(^|(?![.:/\\-_@])(?:[$+<=>^`|\uff5c]|' + re.src_ZPCc + '))' +
      '((?![$+<=>^`|\uff5c])' + re.tpl_host_port_fuzzy_strict + re.src_path + ')';

  re.tpl_link_no_ip_fuzzy =
      // Fuzzy link can't be prepended with .:/\- and non punctuation.
      // but can start with > (markdown blockquote)
      '(^|(?![.:/\\-_@])(?:[$+<=>^`|\uff5c]|' + re.src_ZPCc + '))' +
      '((?![$+<=>^`|\uff5c])' + re.tpl_host_port_no_ip_fuzzy_strict + re.src_path + ')';

  return re;
};

});
return ___scope___.entry = "index.js";
});
FuseBox.pkg("markdown-it", {}, function(___scope___){
___scope___.file("index.js", function(exports, require, module, __filename, __dirname){

'use strict';


module.exports = require('./lib/');

});
___scope___.file("lib/index.js", function(exports, require, module, __filename, __dirname){
/* fuse:injection: */ var process = require("process");
// Main parser class

'use strict';


var utils        = require('./common/utils');
var helpers      = require('./helpers');
var Renderer     = require('./renderer');
var ParserCore   = require('./parser_core');
var ParserBlock  = require('./parser_block');
var ParserInline = require('./parser_inline');
var LinkifyIt    = require('linkify-it');
var mdurl        = require('mdurl');
var punycode     = require('punycode');


var config = {
  'default': require('./presets/default'),
  zero: require('./presets/zero'),
  commonmark: require('./presets/commonmark')
};

////////////////////////////////////////////////////////////////////////////////
//
// This validator can prohibit more than really needed to prevent XSS. It's a
// tradeoff to keep code simple and to be secure by default.
//
// If you need different setup - override validator method as you wish. Or
// replace it with dummy function and use external sanitizer.
//

var BAD_PROTO_RE = /^(vbscript|javascript|file|data):/;
var GOOD_DATA_RE = /^data:image\/(gif|png|jpeg|webp);/;

function validateLink(url) {
  // url should be normalized at this point, and existing entities are decoded
  var str = url.trim().toLowerCase();

  return BAD_PROTO_RE.test(str) ? (GOOD_DATA_RE.test(str) ? true : false) : true;
}

////////////////////////////////////////////////////////////////////////////////


var RECODE_HOSTNAME_FOR = [ 'http:', 'https:', 'mailto:' ];

function normalizeLink(url) {
  var parsed = mdurl.parse(url, true);

  if (parsed.hostname) {
    // Encode hostnames in urls like:
    // `http://host/`, `https://host/`, `mailto:user@host`, `//host/`
    //
    // We don't encode unknown schemas, because it's likely that we encode
    // something we shouldn't (e.g. `skype:name` treated as `skype:host`)
    //
    if (!parsed.protocol || RECODE_HOSTNAME_FOR.indexOf(parsed.protocol) >= 0) {
      try {
        parsed.hostname = punycode.toASCII(parsed.hostname);
      } catch (er) { /**/ }
    }
  }

  return mdurl.encode(mdurl.format(parsed));
}

function normalizeLinkText(url) {
  var parsed = mdurl.parse(url, true);

  if (parsed.hostname) {
    // Encode hostnames in urls like:
    // `http://host/`, `https://host/`, `mailto:user@host`, `//host/`
    //
    // We don't encode unknown schemas, because it's likely that we encode
    // something we shouldn't (e.g. `skype:name` treated as `skype:host`)
    //
    if (!parsed.protocol || RECODE_HOSTNAME_FOR.indexOf(parsed.protocol) >= 0) {
      try {
        parsed.hostname = punycode.toUnicode(parsed.hostname);
      } catch (er) { /**/ }
    }
  }

  return mdurl.decode(mdurl.format(parsed));
}


/**
 * class MarkdownIt
 *
 * Main parser/renderer class.
 *
 * ##### Usage
 *
 * ```javascript
 * // node.js, "classic" way:
 * var MarkdownIt = require('markdown-it'),
 *     md = new MarkdownIt();
 * var result = md.render('# markdown-it rulezz!');
 *
 * // node.js, the same, but with sugar:
 * var md = require('markdown-it')();
 * var result = md.render('# markdown-it rulezz!');
 *
 * // browser without AMD, added to "window" on script load
 * // Note, there are no dash.
 * var md = window.markdownit();
 * var result = md.render('# markdown-it rulezz!');
 * ```
 *
 * Single line rendering, without paragraph wrap:
 *
 * ```javascript
 * var md = require('markdown-it')();
 * var result = md.renderInline('__markdown-it__ rulezz!');
 * ```
 **/

/**
 * new MarkdownIt([presetName, options])
 * - presetName (String): optional, `commonmark` / `zero`
 * - options (Object)
 *
 * Creates parser instanse with given config. Can be called without `new`.
 *
 * ##### presetName
 *
 * MarkdownIt provides named presets as a convenience to quickly
 * enable/disable active syntax rules and options for common use cases.
 *
 * - ["commonmark"](https://github.com/markdown-it/markdown-it/blob/master/lib/presets/commonmark.js) -
 *   configures parser to strict [CommonMark](http://commonmark.org/) mode.
 * - [default](https://github.com/markdown-it/markdown-it/blob/master/lib/presets/default.js) -
 *   similar to GFM, used when no preset name given. Enables all available rules,
 *   but still without html, typographer & autolinker.
 * - ["zero"](https://github.com/markdown-it/markdown-it/blob/master/lib/presets/zero.js) -
 *   all rules disabled. Useful to quickly setup your config via `.enable()`.
 *   For example, when you need only `bold` and `italic` markup and nothing else.
 *
 * ##### options:
 *
 * - __html__ - `false`. Set `true` to enable HTML tags in source. Be careful!
 *   That's not safe! You may need external sanitizer to protect output from XSS.
 *   It's better to extend features via plugins, instead of enabling HTML.
 * - __xhtmlOut__ - `false`. Set `true` to add '/' when closing single tags
 *   (`<br />`). This is needed only for full CommonMark compatibility. In real
 *   world you will need HTML output.
 * - __breaks__ - `false`. Set `true` to convert `\n` in paragraphs into `<br>`.
 * - __langPrefix__ - `language-`. CSS language class prefix for fenced blocks.
 *   Can be useful for external highlighters.
 * - __linkify__ - `false`. Set `true` to autoconvert URL-like text to links.
 * - __typographer__  - `false`. Set `true` to enable [some language-neutral
 *   replacement](https://github.com/markdown-it/markdown-it/blob/master/lib/rules_core/replacements.js) +
 *   quotes beautification (smartquotes).
 * - __quotes__ - `“”‘’`, String or Array. Double + single quotes replacement
 *   pairs, when typographer enabled and smartquotes on. For example, you can
 *   use `'«»„“'` for Russian, `'„“‚‘'` for German, and
 *   `['«\xA0', '\xA0»', '‹\xA0', '\xA0›']` for French (including nbsp).
 * - __highlight__ - `null`. Highlighter function for fenced code blocks.
 *   Highlighter `function (str, lang)` should return escaped HTML. It can also
 *   return empty string if the source was not changed and should be escaped
 *   externaly. If result starts with <pre... internal wrapper is skipped.
 *
 * ##### Example
 *
 * ```javascript
 * // commonmark mode
 * var md = require('markdown-it')('commonmark');
 *
 * // default mode
 * var md = require('markdown-it')();
 *
 * // enable everything
 * var md = require('markdown-it')({
 *   html: true,
 *   linkify: true,
 *   typographer: true
 * });
 * ```
 *
 * ##### Syntax highlighting
 *
 * ```js
 * var hljs = require('highlight.js') // https://highlightjs.org/
 *
 * var md = require('markdown-it')({
 *   highlight: function (str, lang) {
 *     if (lang && hljs.getLanguage(lang)) {
 *       try {
 *         return hljs.highlight(lang, str, true).value;
 *       } catch (__) {}
 *     }
 *
 *     return ''; // use external default escaping
 *   }
 * });
 * ```
 *
 * Or with full wrapper override (if you need assign class to `<pre>`):
 *
 * ```javascript
 * var hljs = require('highlight.js') // https://highlightjs.org/
 *
 * // Actual default values
 * var md = require('markdown-it')({
 *   highlight: function (str, lang) {
 *     if (lang && hljs.getLanguage(lang)) {
 *       try {
 *         return '<pre class="hljs"><code>' +
 *                hljs.highlight(lang, str, true).value +
 *                '</code></pre>';
 *       } catch (__) {}
 *     }
 *
 *     return '<pre class="hljs"><code>' + md.utils.escapeHtml(str) + '</code></pre>';
 *   }
 * });
 * ```
 *
 **/
function MarkdownIt(presetName, options) {
  if (!(this instanceof MarkdownIt)) {
    return new MarkdownIt(presetName, options);
  }

  if (!options) {
    if (!utils.isString(presetName)) {
      options = presetName || {};
      presetName = 'default';
    }
  }

  /**
   * MarkdownIt#inline -> ParserInline
   *
   * Instance of [[ParserInline]]. You may need it to add new rules when
   * writing plugins. For simple rules control use [[MarkdownIt.disable]] and
   * [[MarkdownIt.enable]].
   **/
  this.inline = new ParserInline();

  /**
   * MarkdownIt#block -> ParserBlock
   *
   * Instance of [[ParserBlock]]. You may need it to add new rules when
   * writing plugins. For simple rules control use [[MarkdownIt.disable]] and
   * [[MarkdownIt.enable]].
   **/
  this.block = new ParserBlock();

  /**
   * MarkdownIt#core -> Core
   *
   * Instance of [[Core]] chain executor. You may need it to add new rules when
   * writing plugins. For simple rules control use [[MarkdownIt.disable]] and
   * [[MarkdownIt.enable]].
   **/
  this.core = new ParserCore();

  /**
   * MarkdownIt#renderer -> Renderer
   *
   * Instance of [[Renderer]]. Use it to modify output look. Or to add rendering
   * rules for new token types, generated by plugins.
   *
   * ##### Example
   *
   * ```javascript
   * var md = require('markdown-it')();
   *
   * function myToken(tokens, idx, options, env, self) {
   *   //...
   *   return result;
   * };
   *
   * md.renderer.rules['my_token'] = myToken
   * ```
   *
   * See [[Renderer]] docs and [source code](https://github.com/markdown-it/markdown-it/blob/master/lib/renderer.js).
   **/
  this.renderer = new Renderer();

  /**
   * MarkdownIt#linkify -> LinkifyIt
   *
   * [linkify-it](https://github.com/markdown-it/linkify-it) instance.
   * Used by [linkify](https://github.com/markdown-it/markdown-it/blob/master/lib/rules_core/linkify.js)
   * rule.
   **/
  this.linkify = new LinkifyIt();

  /**
   * MarkdownIt#validateLink(url) -> Boolean
   *
   * Link validation function. CommonMark allows too much in links. By default
   * we disable `javascript:`, `vbscript:`, `file:` schemas, and almost all `data:...` schemas
   * except some embedded image types.
   *
   * You can change this behaviour:
   *
   * ```javascript
   * var md = require('markdown-it')();
   * // enable everything
   * md.validateLink = function () { return true; }
   * ```
   **/
  this.validateLink = validateLink;

  /**
   * MarkdownIt#normalizeLink(url) -> String
   *
   * Function used to encode link url to a machine-readable format,
   * which includes url-encoding, punycode, etc.
   **/
  this.normalizeLink = normalizeLink;

  /**
   * MarkdownIt#normalizeLinkText(url) -> String
   *
   * Function used to decode link url to a human-readable format`
   **/
  this.normalizeLinkText = normalizeLinkText;


  // Expose utils & helpers for easy acces from plugins

  /**
   * MarkdownIt#utils -> utils
   *
   * Assorted utility functions, useful to write plugins. See details
   * [here](https://github.com/markdown-it/markdown-it/blob/master/lib/common/utils.js).
   **/
  this.utils = utils;

  /**
   * MarkdownIt#helpers -> helpers
   *
   * Link components parser functions, useful to write plugins. See details
   * [here](https://github.com/markdown-it/markdown-it/blob/master/lib/helpers).
   **/
  this.helpers = utils.assign({}, helpers);


  this.options = {};
  this.configure(presetName);

  if (options) { this.set(options); }
}


/** chainable
 * MarkdownIt.set(options)
 *
 * Set parser options (in the same format as in constructor). Probably, you
 * will never need it, but you can change options after constructor call.
 *
 * ##### Example
 *
 * ```javascript
 * var md = require('markdown-it')()
 *             .set({ html: true, breaks: true })
 *             .set({ typographer, true });
 * ```
 *
 * __Note:__ To achieve the best possible performance, don't modify a
 * `markdown-it` instance options on the fly. If you need multiple configurations
 * it's best to create multiple instances and initialize each with separate
 * config.
 **/
MarkdownIt.prototype.set = function (options) {
  utils.assign(this.options, options);
  return this;
};


/** chainable, internal
 * MarkdownIt.configure(presets)
 *
 * Batch load of all options and compenent settings. This is internal method,
 * and you probably will not need it. But if you with - see available presets
 * and data structure [here](https://github.com/markdown-it/markdown-it/tree/master/lib/presets)
 *
 * We strongly recommend to use presets instead of direct config loads. That
 * will give better compatibility with next versions.
 **/
MarkdownIt.prototype.configure = function (presets) {
  var self = this, presetName;

  if (utils.isString(presets)) {
    presetName = presets;
    presets = config[presetName];
    if (!presets) { throw new Error('Wrong `markdown-it` preset "' + presetName + '", check name'); }
  }

  if (!presets) { throw new Error('Wrong `markdown-it` preset, can\'t be empty'); }

  if (presets.options) { self.set(presets.options); }

  if (presets.components) {
    Object.keys(presets.components).forEach(function (name) {
      if (presets.components[name].rules) {
        self[name].ruler.enableOnly(presets.components[name].rules);
      }
      if (presets.components[name].rules2) {
        self[name].ruler2.enableOnly(presets.components[name].rules2);
      }
    });
  }
  return this;
};


/** chainable
 * MarkdownIt.enable(list, ignoreInvalid)
 * - list (String|Array): rule name or list of rule names to enable
 * - ignoreInvalid (Boolean): set `true` to ignore errors when rule not found.
 *
 * Enable list or rules. It will automatically find appropriate components,
 * containing rules with given names. If rule not found, and `ignoreInvalid`
 * not set - throws exception.
 *
 * ##### Example
 *
 * ```javascript
 * var md = require('markdown-it')()
 *             .enable(['sub', 'sup'])
 *             .disable('smartquotes');
 * ```
 **/
MarkdownIt.prototype.enable = function (list, ignoreInvalid) {
  var result = [];

  if (!Array.isArray(list)) { list = [ list ]; }

  [ 'core', 'block', 'inline' ].forEach(function (chain) {
    result = result.concat(this[chain].ruler.enable(list, true));
  }, this);

  result = result.concat(this.inline.ruler2.enable(list, true));

  var missed = list.filter(function (name) { return result.indexOf(name) < 0; });

  if (missed.length && !ignoreInvalid) {
    throw new Error('MarkdownIt. Failed to enable unknown rule(s): ' + missed);
  }

  return this;
};


/** chainable
 * MarkdownIt.disable(list, ignoreInvalid)
 * - list (String|Array): rule name or list of rule names to disable.
 * - ignoreInvalid (Boolean): set `true` to ignore errors when rule not found.
 *
 * The same as [[MarkdownIt.enable]], but turn specified rules off.
 **/
MarkdownIt.prototype.disable = function (list, ignoreInvalid) {
  var result = [];

  if (!Array.isArray(list)) { list = [ list ]; }

  [ 'core', 'block', 'inline' ].forEach(function (chain) {
    result = result.concat(this[chain].ruler.disable(list, true));
  }, this);

  result = result.concat(this.inline.ruler2.disable(list, true));

  var missed = list.filter(function (name) { return result.indexOf(name) < 0; });

  if (missed.length && !ignoreInvalid) {
    throw new Error('MarkdownIt. Failed to disable unknown rule(s): ' + missed);
  }
  return this;
};


/** chainable
 * MarkdownIt.use(plugin, params)
 *
 * Load specified plugin with given params into current parser instance.
 * It's just a sugar to call `plugin(md, params)` with curring.
 *
 * ##### Example
 *
 * ```javascript
 * var iterator = require('markdown-it-for-inline');
 * var md = require('markdown-it')()
 *             .use(iterator, 'foo_replace', 'text', function (tokens, idx) {
 *               tokens[idx].content = tokens[idx].content.replace(/foo/g, 'bar');
 *             });
 * ```
 **/
MarkdownIt.prototype.use = function (plugin /*, params, ... */) {
  var args = [ this ].concat(Array.prototype.slice.call(arguments, 1));
  plugin.apply(plugin, args);
  return this;
};


/** internal
 * MarkdownIt.parse(src, env) -> Array
 * - src (String): source string
 * - env (Object): environment sandbox
 *
 * Parse input string and returns list of block tokens (special token type
 * "inline" will contain list of inline tokens). You should not call this
 * method directly, until you write custom renderer (for example, to produce
 * AST).
 *
 * `env` is used to pass data between "distributed" rules and return additional
 * metadata like reference info, needed for the renderer. It also can be used to
 * inject data in specific cases. Usually, you will be ok to pass `{}`,
 * and then pass updated object to renderer.
 **/
MarkdownIt.prototype.parse = function (src, env) {
  if (typeof src !== 'string') {
    throw new Error('Input data should be a String');
  }

  var state = new this.core.State(src, this, env);

  this.core.process(state);

  return state.tokens;
};


/**
 * MarkdownIt.render(src [, env]) -> String
 * - src (String): source string
 * - env (Object): environment sandbox
 *
 * Render markdown string into html. It does all magic for you :).
 *
 * `env` can be used to inject additional metadata (`{}` by default).
 * But you will not need it with high probability. See also comment
 * in [[MarkdownIt.parse]].
 **/
MarkdownIt.prototype.render = function (src, env) {
  env = env || {};

  return this.renderer.render(this.parse(src, env), this.options, env);
};


/** internal
 * MarkdownIt.parseInline(src, env) -> Array
 * - src (String): source string
 * - env (Object): environment sandbox
 *
 * The same as [[MarkdownIt.parse]] but skip all block rules. It returns the
 * block tokens list with the single `inline` element, containing parsed inline
 * tokens in `children` property. Also updates `env` object.
 **/
MarkdownIt.prototype.parseInline = function (src, env) {
  var state = new this.core.State(src, this, env);

  state.inlineMode = true;
  this.core.process(state);

  return state.tokens;
};


/**
 * MarkdownIt.renderInline(src [, env]) -> String
 * - src (String): source string
 * - env (Object): environment sandbox
 *
 * Similar to [[MarkdownIt.render]] but for single paragraph content. Result
 * will NOT be wrapped into `<p>` tags.
 **/
MarkdownIt.prototype.renderInline = function (src, env) {
  env = env || {};

  return this.renderer.render(this.parseInline(src, env), this.options, env);
};


module.exports = MarkdownIt;

});
___scope___.file("lib/common/utils.js", function(exports, require, module, __filename, __dirname){

// Utilities
//
'use strict';


function _class(obj) { return Object.prototype.toString.call(obj); }

function isString(obj) { return _class(obj) === '[object String]'; }

var _hasOwnProperty = Object.prototype.hasOwnProperty;

function has(object, key) {
  return _hasOwnProperty.call(object, key);
}

// Merge objects
//
function assign(obj /*from1, from2, from3, ...*/) {
  var sources = Array.prototype.slice.call(arguments, 1);

  sources.forEach(function (source) {
    if (!source) { return; }

    if (typeof source !== 'object') {
      throw new TypeError(source + 'must be object');
    }

    Object.keys(source).forEach(function (key) {
      obj[key] = source[key];
    });
  });

  return obj;
}

// Remove element from array and put another array at those position.
// Useful for some operations with tokens
function arrayReplaceAt(src, pos, newElements) {
  return [].concat(src.slice(0, pos), newElements, src.slice(pos + 1));
}

////////////////////////////////////////////////////////////////////////////////

function isValidEntityCode(c) {
  /*eslint no-bitwise:0*/
  // broken sequence
  if (c >= 0xD800 && c <= 0xDFFF) { return false; }
  // never used
  if (c >= 0xFDD0 && c <= 0xFDEF) { return false; }
  if ((c & 0xFFFF) === 0xFFFF || (c & 0xFFFF) === 0xFFFE) { return false; }
  // control codes
  if (c >= 0x00 && c <= 0x08) { return false; }
  if (c === 0x0B) { return false; }
  if (c >= 0x0E && c <= 0x1F) { return false; }
  if (c >= 0x7F && c <= 0x9F) { return false; }
  // out of range
  if (c > 0x10FFFF) { return false; }
  return true;
}

function fromCodePoint(c) {
  /*eslint no-bitwise:0*/
  if (c > 0xffff) {
    c -= 0x10000;
    var surrogate1 = 0xd800 + (c >> 10),
        surrogate2 = 0xdc00 + (c & 0x3ff);

    return String.fromCharCode(surrogate1, surrogate2);
  }
  return String.fromCharCode(c);
}


var UNESCAPE_MD_RE  = /\\([!"#$%&'()*+,\-.\/:;<=>?@[\\\]^_`{|}~])/g;
var ENTITY_RE       = /&([a-z#][a-z0-9]{1,31});/gi;
var UNESCAPE_ALL_RE = new RegExp(UNESCAPE_MD_RE.source + '|' + ENTITY_RE.source, 'gi');

var DIGITAL_ENTITY_TEST_RE = /^#((?:x[a-f0-9]{1,8}|[0-9]{1,8}))/i;

var entities = require('./entities');

function replaceEntityPattern(match, name) {
  var code = 0;

  if (has(entities, name)) {
    return entities[name];
  }

  if (name.charCodeAt(0) === 0x23/* # */ && DIGITAL_ENTITY_TEST_RE.test(name)) {
    code = name[1].toLowerCase() === 'x' ?
      parseInt(name.slice(2), 16)
    :
      parseInt(name.slice(1), 10);
    if (isValidEntityCode(code)) {
      return fromCodePoint(code);
    }
  }

  return match;
}

/*function replaceEntities(str) {
  if (str.indexOf('&') < 0) { return str; }

  return str.replace(ENTITY_RE, replaceEntityPattern);
}*/

function unescapeMd(str) {
  if (str.indexOf('\\') < 0) { return str; }
  return str.replace(UNESCAPE_MD_RE, '$1');
}

function unescapeAll(str) {
  if (str.indexOf('\\') < 0 && str.indexOf('&') < 0) { return str; }

  return str.replace(UNESCAPE_ALL_RE, function (match, escaped, entity) {
    if (escaped) { return escaped; }
    return replaceEntityPattern(match, entity);
  });
}

////////////////////////////////////////////////////////////////////////////////

var HTML_ESCAPE_TEST_RE = /[&<>"]/;
var HTML_ESCAPE_REPLACE_RE = /[&<>"]/g;
var HTML_REPLACEMENTS = {
  '&': '&amp;',
  '<': '&lt;',
  '>': '&gt;',
  '"': '&quot;'
};

function replaceUnsafeChar(ch) {
  return HTML_REPLACEMENTS[ch];
}

function escapeHtml(str) {
  if (HTML_ESCAPE_TEST_RE.test(str)) {
    return str.replace(HTML_ESCAPE_REPLACE_RE, replaceUnsafeChar);
  }
  return str;
}

////////////////////////////////////////////////////////////////////////////////

var REGEXP_ESCAPE_RE = /[.?*+^$[\]\\(){}|-]/g;

function escapeRE(str) {
  return str.replace(REGEXP_ESCAPE_RE, '\\$&');
}

////////////////////////////////////////////////////////////////////////////////

function isSpace(code) {
  switch (code) {
    case 0x09:
    case 0x20:
      return true;
  }
  return false;
}

// Zs (unicode class) || [\t\f\v\r\n]
function isWhiteSpace(code) {
  if (code >= 0x2000 && code <= 0x200A) { return true; }
  switch (code) {
    case 0x09: // \t
    case 0x0A: // \n
    case 0x0B: // \v
    case 0x0C: // \f
    case 0x0D: // \r
    case 0x20:
    case 0xA0:
    case 0x1680:
    case 0x202F:
    case 0x205F:
    case 0x3000:
      return true;
  }
  return false;
}

////////////////////////////////////////////////////////////////////////////////

/*eslint-disable max-len*/
var UNICODE_PUNCT_RE = require('uc.micro/categories/P/regex');

// Currently without astral characters support.
function isPunctChar(ch) {
  return UNICODE_PUNCT_RE.test(ch);
}


// Markdown ASCII punctuation characters.
//
// !, ", #, $, %, &, ', (, ), *, +, ,, -, ., /, :, ;, <, =, >, ?, @, [, \, ], ^, _, `, {, |, }, or ~
// http://spec.commonmark.org/0.15/#ascii-punctuation-character
//
// Don't confuse with unicode punctuation !!! It lacks some chars in ascii range.
//
function isMdAsciiPunct(ch) {
  switch (ch) {
    case 0x21/* ! */:
    case 0x22/* " */:
    case 0x23/* # */:
    case 0x24/* $ */:
    case 0x25/* % */:
    case 0x26/* & */:
    case 0x27/* ' */:
    case 0x28/* ( */:
    case 0x29/* ) */:
    case 0x2A/* * */:
    case 0x2B/* + */:
    case 0x2C/* , */:
    case 0x2D/* - */:
    case 0x2E/* . */:
    case 0x2F/* / */:
    case 0x3A/* : */:
    case 0x3B/* ; */:
    case 0x3C/* < */:
    case 0x3D/* = */:
    case 0x3E/* > */:
    case 0x3F/* ? */:
    case 0x40/* @ */:
    case 0x5B/* [ */:
    case 0x5C/* \ */:
    case 0x5D/* ] */:
    case 0x5E/* ^ */:
    case 0x5F/* _ */:
    case 0x60/* ` */:
    case 0x7B/* { */:
    case 0x7C/* | */:
    case 0x7D/* } */:
    case 0x7E/* ~ */:
      return true;
    default:
      return false;
  }
}

// Hepler to unify [reference labels].
//
function normalizeReference(str) {
  // use .toUpperCase() instead of .toLowerCase()
  // here to avoid a conflict with Object.prototype
  // members (most notably, `__proto__`)
  return str.trim().replace(/\s+/g, ' ').toUpperCase();
}

////////////////////////////////////////////////////////////////////////////////

// Re-export libraries commonly used in both markdown-it and its plugins,
// so plugins won't have to depend on them explicitly, which reduces their
// bundled size (e.g. a browser build).
//
exports.lib                 = {};
exports.lib.mdurl           = require('mdurl');
exports.lib.ucmicro         = require('uc.micro');

exports.assign              = assign;
exports.isString            = isString;
exports.has                 = has;
exports.unescapeMd          = unescapeMd;
exports.unescapeAll         = unescapeAll;
exports.isValidEntityCode   = isValidEntityCode;
exports.fromCodePoint       = fromCodePoint;
// exports.replaceEntities     = replaceEntities;
exports.escapeHtml          = escapeHtml;
exports.arrayReplaceAt      = arrayReplaceAt;
exports.isSpace             = isSpace;
exports.isWhiteSpace        = isWhiteSpace;
exports.isMdAsciiPunct      = isMdAsciiPunct;
exports.isPunctChar         = isPunctChar;
exports.escapeRE            = escapeRE;
exports.normalizeReference  = normalizeReference;

});
___scope___.file("lib/common/entities.js", function(exports, require, module, __filename, __dirname){

// HTML5 entities map: { name -> utf16string }
//
'use strict';

/*eslint quotes:0*/
module.exports = require('entities/maps/entities.json');

});
___scope___.file("lib/helpers/index.js", function(exports, require, module, __filename, __dirname){

// Just a shortcut for bulk export
'use strict';


exports.parseLinkLabel       = require('./parse_link_label');
exports.parseLinkDestination = require('./parse_link_destination');
exports.parseLinkTitle       = require('./parse_link_title');

});
___scope___.file("lib/helpers/parse_link_label.js", function(exports, require, module, __filename, __dirname){

// Parse link label
//
// this function assumes that first character ("[") already matches;
// returns the end of the label
//
'use strict';

module.exports = function parseLinkLabel(state, start, disableNested) {
  var level, found, marker, prevPos,
      labelEnd = -1,
      max = state.posMax,
      oldPos = state.pos;

  state.pos = start + 1;
  level = 1;

  while (state.pos < max) {
    marker = state.src.charCodeAt(state.pos);
    if (marker === 0x5D /* ] */) {
      level--;
      if (level === 0) {
        found = true;
        break;
      }
    }

    prevPos = state.pos;
    state.md.inline.skipToken(state);
    if (marker === 0x5B /* [ */) {
      if (prevPos === state.pos - 1) {
        // increase level if we find text `[`, which is not a part of any token
        level++;
      } else if (disableNested) {
        state.pos = oldPos;
        return -1;
      }
    }
  }

  if (found) {
    labelEnd = state.pos;
  }

  // restore old state
  state.pos = oldPos;

  return labelEnd;
};

});
___scope___.file("lib/helpers/parse_link_destination.js", function(exports, require, module, __filename, __dirname){

// Parse link destination
//
'use strict';


var isSpace     = require('../common/utils').isSpace;
var unescapeAll = require('../common/utils').unescapeAll;


module.exports = function parseLinkDestination(str, pos, max) {
  var code, level,
      lines = 0,
      start = pos,
      result = {
        ok: false,
        pos: 0,
        lines: 0,
        str: ''
      };

  if (str.charCodeAt(pos) === 0x3C /* < */) {
    pos++;
    while (pos < max) {
      code = str.charCodeAt(pos);
      if (code === 0x0A /* \n */ || isSpace(code)) { return result; }
      if (code === 0x3E /* > */) {
        result.pos = pos + 1;
        result.str = unescapeAll(str.slice(start + 1, pos));
        result.ok = true;
        return result;
      }
      if (code === 0x5C /* \ */ && pos + 1 < max) {
        pos += 2;
        continue;
      }

      pos++;
    }

    // no closing '>'
    return result;
  }

  // this should be ... } else { ... branch

  level = 0;
  while (pos < max) {
    code = str.charCodeAt(pos);

    if (code === 0x20) { break; }

    // ascii control characters
    if (code < 0x20 || code === 0x7F) { break; }

    if (code === 0x5C /* \ */ && pos + 1 < max) {
      pos += 2;
      continue;
    }

    if (code === 0x28 /* ( */) {
      level++;
    }

    if (code === 0x29 /* ) */) {
      if (level === 0) { break; }
      level--;
    }

    pos++;
  }

  if (start === pos) { return result; }
  if (level !== 0) { return result; }

  result.str = unescapeAll(str.slice(start, pos));
  result.lines = lines;
  result.pos = pos;
  result.ok = true;
  return result;
};

});
___scope___.file("lib/helpers/parse_link_title.js", function(exports, require, module, __filename, __dirname){

// Parse link title
//
'use strict';


var unescapeAll = require('../common/utils').unescapeAll;


module.exports = function parseLinkTitle(str, pos, max) {
  var code,
      marker,
      lines = 0,
      start = pos,
      result = {
        ok: false,
        pos: 0,
        lines: 0,
        str: ''
      };

  if (pos >= max) { return result; }

  marker = str.charCodeAt(pos);

  if (marker !== 0x22 /* " */ && marker !== 0x27 /* ' */ && marker !== 0x28 /* ( */) { return result; }

  pos++;

  // if opening marker is "(", switch it to closing marker ")"
  if (marker === 0x28) { marker = 0x29; }

  while (pos < max) {
    code = str.charCodeAt(pos);
    if (code === marker) {
      result.pos = pos + 1;
      result.lines = lines;
      result.str = unescapeAll(str.slice(start + 1, pos));
      result.ok = true;
      return result;
    } else if (code === 0x0A) {
      lines++;
    } else if (code === 0x5C /* \ */ && pos + 1 < max) {
      pos++;
      if (str.charCodeAt(pos) === 0x0A) {
        lines++;
      }
    }

    pos++;
  }

  return result;
};

});
___scope___.file("lib/renderer.js", function(exports, require, module, __filename, __dirname){

/**
 * class Renderer
 *
 * Generates HTML from parsed token stream. Each instance has independent
 * copy of rules. Those can be rewritten with ease. Also, you can add new
 * rules if you create plugin and adds new token types.
 **/
'use strict';


var assign          = require('./common/utils').assign;
var unescapeAll     = require('./common/utils').unescapeAll;
var escapeHtml      = require('./common/utils').escapeHtml;


////////////////////////////////////////////////////////////////////////////////

var default_rules = {};


default_rules.code_inline = function (tokens, idx, options, env, slf) {
  var token = tokens[idx];

  return  '<code' + slf.renderAttrs(token) + '>' +
          escapeHtml(tokens[idx].content) +
          '</code>';
};


default_rules.code_block = function (tokens, idx, options, env, slf) {
  var token = tokens[idx];

  return  '<pre' + slf.renderAttrs(token) + '><code>' +
          escapeHtml(tokens[idx].content) +
          '</code></pre>\n';
};


default_rules.fence = function (tokens, idx, options, env, slf) {
  var token = tokens[idx],
      info = token.info ? unescapeAll(token.info).trim() : '',
      langName = '',
      highlighted, i, tmpAttrs, tmpToken;

  if (info) {
    langName = info.split(/\s+/g)[0];
  }

  if (options.highlight) {
    highlighted = options.highlight(token.content, langName) || escapeHtml(token.content);
  } else {
    highlighted = escapeHtml(token.content);
  }

  if (highlighted.indexOf('<pre') === 0) {
    return highlighted + '\n';
  }

  // If language exists, inject class gently, without modifying original token.
  // May be, one day we will add .clone() for token and simplify this part, but
  // now we prefer to keep things local.
  if (info) {
    i        = token.attrIndex('class');
    tmpAttrs = token.attrs ? token.attrs.slice() : [];

    if (i < 0) {
      tmpAttrs.push([ 'class', options.langPrefix + langName ]);
    } else {
      tmpAttrs[i][1] += ' ' + options.langPrefix + langName;
    }

    // Fake token just to render attributes
    tmpToken = {
      attrs: tmpAttrs
    };

    return  '<pre><code' + slf.renderAttrs(tmpToken) + '>'
          + highlighted
          + '</code></pre>\n';
  }


  return  '<pre><code' + slf.renderAttrs(token) + '>'
        + highlighted
        + '</code></pre>\n';
};


default_rules.image = function (tokens, idx, options, env, slf) {
  var token = tokens[idx];

  // "alt" attr MUST be set, even if empty. Because it's mandatory and
  // should be placed on proper position for tests.
  //
  // Replace content with actual value

  token.attrs[token.attrIndex('alt')][1] =
    slf.renderInlineAsText(token.children, options, env);

  return slf.renderToken(tokens, idx, options);
};


default_rules.hardbreak = function (tokens, idx, options /*, env */) {
  return options.xhtmlOut ? '<br />\n' : '<br>\n';
};
default_rules.softbreak = function (tokens, idx, options /*, env */) {
  return options.breaks ? (options.xhtmlOut ? '<br />\n' : '<br>\n') : '\n';
};


default_rules.text = function (tokens, idx /*, options, env */) {
  return escapeHtml(tokens[idx].content);
};


default_rules.html_block = function (tokens, idx /*, options, env */) {
  return tokens[idx].content;
};
default_rules.html_inline = function (tokens, idx /*, options, env */) {
  return tokens[idx].content;
};


/**
 * new Renderer()
 *
 * Creates new [[Renderer]] instance and fill [[Renderer#rules]] with defaults.
 **/
function Renderer() {

  /**
   * Renderer#rules -> Object
   *
   * Contains render rules for tokens. Can be updated and extended.
   *
   * ##### Example
   *
   * ```javascript
   * var md = require('markdown-it')();
   *
   * md.renderer.rules.strong_open  = function () { return '<b>'; };
   * md.renderer.rules.strong_close = function () { return '</b>'; };
   *
   * var result = md.renderInline(...);
   * ```
   *
   * Each rule is called as independent static function with fixed signature:
   *
   * ```javascript
   * function my_token_render(tokens, idx, options, env, renderer) {
   *   // ...
   *   return renderedHTML;
   * }
   * ```
   *
   * See [source code](https://github.com/markdown-it/markdown-it/blob/master/lib/renderer.js)
   * for more details and examples.
   **/
  this.rules = assign({}, default_rules);
}


/**
 * Renderer.renderAttrs(token) -> String
 *
 * Render token attributes to string.
 **/
Renderer.prototype.renderAttrs = function renderAttrs(token) {
  var i, l, result;

  if (!token.attrs) { return ''; }

  result = '';

  for (i = 0, l = token.attrs.length; i < l; i++) {
    result += ' ' + escapeHtml(token.attrs[i][0]) + '="' + escapeHtml(token.attrs[i][1]) + '"';
  }

  return result;
};


/**
 * Renderer.renderToken(tokens, idx, options) -> String
 * - tokens (Array): list of tokens
 * - idx (Numbed): token index to render
 * - options (Object): params of parser instance
 *
 * Default token renderer. Can be overriden by custom function
 * in [[Renderer#rules]].
 **/
Renderer.prototype.renderToken = function renderToken(tokens, idx, options) {
  var nextToken,
      result = '',
      needLf = false,
      token = tokens[idx];

  // Tight list paragraphs
  if (token.hidden) {
    return '';
  }

  // Insert a newline between hidden paragraph and subsequent opening
  // block-level tag.
  //
  // For example, here we should insert a newline before blockquote:
  //  - a
  //    >
  //
  if (token.block && token.nesting !== -1 && idx && tokens[idx - 1].hidden) {
    result += '\n';
  }

  // Add token name, e.g. `<img`
  result += (token.nesting === -1 ? '</' : '<') + token.tag;

  // Encode attributes, e.g. `<img src="foo"`
  result += this.renderAttrs(token);

  // Add a slash for self-closing tags, e.g. `<img src="foo" /`
  if (token.nesting === 0 && options.xhtmlOut) {
    result += ' /';
  }

  // Check if we need to add a newline after this tag
  if (token.block) {
    needLf = true;

    if (token.nesting === 1) {
      if (idx + 1 < tokens.length) {
        nextToken = tokens[idx + 1];

        if (nextToken.type === 'inline' || nextToken.hidden) {
          // Block-level tag containing an inline tag.
          //
          needLf = false;

        } else if (nextToken.nesting === -1 && nextToken.tag === token.tag) {
          // Opening tag + closing tag of the same type. E.g. `<li></li>`.
          //
          needLf = false;
        }
      }
    }
  }

  result += needLf ? '>\n' : '>';

  return result;
};


/**
 * Renderer.renderInline(tokens, options, env) -> String
 * - tokens (Array): list on block tokens to renter
 * - options (Object): params of parser instance
 * - env (Object): additional data from parsed input (references, for example)
 *
 * The same as [[Renderer.render]], but for single token of `inline` type.
 **/
Renderer.prototype.renderInline = function (tokens, options, env) {
  var type,
      result = '',
      rules = this.rules;

  for (var i = 0, len = tokens.length; i < len; i++) {
    type = tokens[i].type;

    if (typeof rules[type] !== 'undefined') {
      result += rules[type](tokens, i, options, env, this);
    } else {
      result += this.renderToken(tokens, i, options);
    }
  }

  return result;
};


/** internal
 * Renderer.renderInlineAsText(tokens, options, env) -> String
 * - tokens (Array): list on block tokens to renter
 * - options (Object): params of parser instance
 * - env (Object): additional data from parsed input (references, for example)
 *
 * Special kludge for image `alt` attributes to conform CommonMark spec.
 * Don't try to use it! Spec requires to show `alt` content with stripped markup,
 * instead of simple escaping.
 **/
Renderer.prototype.renderInlineAsText = function (tokens, options, env) {
  var result = '';

  for (var i = 0, len = tokens.length; i < len; i++) {
    if (tokens[i].type === 'text') {
      result += tokens[i].content;
    } else if (tokens[i].type === 'image') {
      result += this.renderInlineAsText(tokens[i].children, options, env);
    }
  }

  return result;
};


/**
 * Renderer.render(tokens, options, env) -> String
 * - tokens (Array): list on block tokens to renter
 * - options (Object): params of parser instance
 * - env (Object): additional data from parsed input (references, for example)
 *
 * Takes token stream and generates HTML. Probably, you will never need to call
 * this method directly.
 **/
Renderer.prototype.render = function (tokens, options, env) {
  var i, len, type,
      result = '',
      rules = this.rules;

  for (i = 0, len = tokens.length; i < len; i++) {
    type = tokens[i].type;

    if (type === 'inline') {
      result += this.renderInline(tokens[i].children, options, env);
    } else if (typeof rules[type] !== 'undefined') {
      result += rules[tokens[i].type](tokens, i, options, env, this);
    } else {
      result += this.renderToken(tokens, i, options, env);
    }
  }

  return result;
};

module.exports = Renderer;

});
___scope___.file("lib/parser_core.js", function(exports, require, module, __filename, __dirname){
/* fuse:injection: */ var process = require("process");
/** internal
 * class Core
 *
 * Top-level rules executor. Glues block/inline parsers and does intermediate
 * transformations.
 **/
'use strict';


var Ruler  = require('./ruler');


var _rules = [
  [ 'normalize',      require('./rules_core/normalize')      ],
  [ 'block',          require('./rules_core/block')          ],
  [ 'inline',         require('./rules_core/inline')         ],
  [ 'linkify',        require('./rules_core/linkify')        ],
  [ 'replacements',   require('./rules_core/replacements')   ],
  [ 'smartquotes',    require('./rules_core/smartquotes')    ]
];


/**
 * new Core()
 **/
function Core() {
  /**
   * Core#ruler -> Ruler
   *
   * [[Ruler]] instance. Keep configuration of core rules.
   **/
  this.ruler = new Ruler();

  for (var i = 0; i < _rules.length; i++) {
    this.ruler.push(_rules[i][0], _rules[i][1]);
  }
}


/**
 * Core.process(state)
 *
 * Executes core chain rules.
 **/
Core.prototype.process = function (state) {
  var i, l, rules;

  rules = this.ruler.getRules('');

  for (i = 0, l = rules.length; i < l; i++) {
    rules[i](state);
  }
};

Core.prototype.State = require('./rules_core/state_core');


module.exports = Core;

});
___scope___.file("lib/ruler.js", function(exports, require, module, __filename, __dirname){

/**
 * class Ruler
 *
 * Helper class, used by [[MarkdownIt#core]], [[MarkdownIt#block]] and
 * [[MarkdownIt#inline]] to manage sequences of functions (rules):
 *
 * - keep rules in defined order
 * - assign the name to each rule
 * - enable/disable rules
 * - add/replace rules
 * - allow assign rules to additional named chains (in the same)
 * - cacheing lists of active rules
 *
 * You will not need use this class directly until write plugins. For simple
 * rules control use [[MarkdownIt.disable]], [[MarkdownIt.enable]] and
 * [[MarkdownIt.use]].
 **/
'use strict';


/**
 * new Ruler()
 **/
function Ruler() {
  // List of added rules. Each element is:
  //
  // {
  //   name: XXX,
  //   enabled: Boolean,
  //   fn: Function(),
  //   alt: [ name2, name3 ]
  // }
  //
  this.__rules__ = [];

  // Cached rule chains.
  //
  // First level - chain name, '' for default.
  // Second level - diginal anchor for fast filtering by charcodes.
  //
  this.__cache__ = null;
}

////////////////////////////////////////////////////////////////////////////////
// Helper methods, should not be used directly


// Find rule index by name
//
Ruler.prototype.__find__ = function (name) {
  for (var i = 0; i < this.__rules__.length; i++) {
    if (this.__rules__[i].name === name) {
      return i;
    }
  }
  return -1;
};


// Build rules lookup cache
//
Ruler.prototype.__compile__ = function () {
  var self = this;
  var chains = [ '' ];

  // collect unique names
  self.__rules__.forEach(function (rule) {
    if (!rule.enabled) { return; }

    rule.alt.forEach(function (altName) {
      if (chains.indexOf(altName) < 0) {
        chains.push(altName);
      }
    });
  });

  self.__cache__ = {};

  chains.forEach(function (chain) {
    self.__cache__[chain] = [];
    self.__rules__.forEach(function (rule) {
      if (!rule.enabled) { return; }

      if (chain && rule.alt.indexOf(chain) < 0) { return; }

      self.__cache__[chain].push(rule.fn);
    });
  });
};


/**
 * Ruler.at(name, fn [, options])
 * - name (String): rule name to replace.
 * - fn (Function): new rule function.
 * - options (Object): new rule options (not mandatory).
 *
 * Replace rule by name with new function & options. Throws error if name not
 * found.
 *
 * ##### Options:
 *
 * - __alt__ - array with names of "alternate" chains.
 *
 * ##### Example
 *
 * Replace existing typographer replacement rule with new one:
 *
 * ```javascript
 * var md = require('markdown-it')();
 *
 * md.core.ruler.at('replacements', function replace(state) {
 *   //...
 * });
 * ```
 **/
Ruler.prototype.at = function (name, fn, options) {
  var index = this.__find__(name);
  var opt = options || {};

  if (index === -1) { throw new Error('Parser rule not found: ' + name); }

  this.__rules__[index].fn = fn;
  this.__rules__[index].alt = opt.alt || [];
  this.__cache__ = null;
};


/**
 * Ruler.before(beforeName, ruleName, fn [, options])
 * - beforeName (String): new rule will be added before this one.
 * - ruleName (String): name of added rule.
 * - fn (Function): rule function.
 * - options (Object): rule options (not mandatory).
 *
 * Add new rule to chain before one with given name. See also
 * [[Ruler.after]], [[Ruler.push]].
 *
 * ##### Options:
 *
 * - __alt__ - array with names of "alternate" chains.
 *
 * ##### Example
 *
 * ```javascript
 * var md = require('markdown-it')();
 *
 * md.block.ruler.before('paragraph', 'my_rule', function replace(state) {
 *   //...
 * });
 * ```
 **/
Ruler.prototype.before = function (beforeName, ruleName, fn, options) {
  var index = this.__find__(beforeName);
  var opt = options || {};

  if (index === -1) { throw new Error('Parser rule not found: ' + beforeName); }

  this.__rules__.splice(index, 0, {
    name: ruleName,
    enabled: true,
    fn: fn,
    alt: opt.alt || []
  });

  this.__cache__ = null;
};


/**
 * Ruler.after(afterName, ruleName, fn [, options])
 * - afterName (String): new rule will be added after this one.
 * - ruleName (String): name of added rule.
 * - fn (Function): rule function.
 * - options (Object): rule options (not mandatory).
 *
 * Add new rule to chain after one with given name. See also
 * [[Ruler.before]], [[Ruler.push]].
 *
 * ##### Options:
 *
 * - __alt__ - array with names of "alternate" chains.
 *
 * ##### Example
 *
 * ```javascript
 * var md = require('markdown-it')();
 *
 * md.inline.ruler.after('text', 'my_rule', function replace(state) {
 *   //...
 * });
 * ```
 **/
Ruler.prototype.after = function (afterName, ruleName, fn, options) {
  var index = this.__find__(afterName);
  var opt = options || {};

  if (index === -1) { throw new Error('Parser rule not found: ' + afterName); }

  this.__rules__.splice(index + 1, 0, {
    name: ruleName,
    enabled: true,
    fn: fn,
    alt: opt.alt || []
  });

  this.__cache__ = null;
};

/**
 * Ruler.push(ruleName, fn [, options])
 * - ruleName (String): name of added rule.
 * - fn (Function): rule function.
 * - options (Object): rule options (not mandatory).
 *
 * Push new rule to the end of chain. See also
 * [[Ruler.before]], [[Ruler.after]].
 *
 * ##### Options:
 *
 * - __alt__ - array with names of "alternate" chains.
 *
 * ##### Example
 *
 * ```javascript
 * var md = require('markdown-it')();
 *
 * md.core.ruler.push('my_rule', function replace(state) {
 *   //...
 * });
 * ```
 **/
Ruler.prototype.push = function (ruleName, fn, options) {
  var opt = options || {};

  this.__rules__.push({
    name: ruleName,
    enabled: true,
    fn: fn,
    alt: opt.alt || []
  });

  this.__cache__ = null;
};


/**
 * Ruler.enable(list [, ignoreInvalid]) -> Array
 * - list (String|Array): list of rule names to enable.
 * - ignoreInvalid (Boolean): set `true` to ignore errors when rule not found.
 *
 * Enable rules with given names. If any rule name not found - throw Error.
 * Errors can be disabled by second param.
 *
 * Returns list of found rule names (if no exception happened).
 *
 * See also [[Ruler.disable]], [[Ruler.enableOnly]].
 **/
Ruler.prototype.enable = function (list, ignoreInvalid) {
  if (!Array.isArray(list)) { list = [ list ]; }

  var result = [];

  // Search by name and enable
  list.forEach(function (name) {
    var idx = this.__find__(name);

    if (idx < 0) {
      if (ignoreInvalid) { return; }
      throw new Error('Rules manager: invalid rule name ' + name);
    }
    this.__rules__[idx].enabled = true;
    result.push(name);
  }, this);

  this.__cache__ = null;
  return result;
};


/**
 * Ruler.enableOnly(list [, ignoreInvalid])
 * - list (String|Array): list of rule names to enable (whitelist).
 * - ignoreInvalid (Boolean): set `true` to ignore errors when rule not found.
 *
 * Enable rules with given names, and disable everything else. If any rule name
 * not found - throw Error. Errors can be disabled by second param.
 *
 * See also [[Ruler.disable]], [[Ruler.enable]].
 **/
Ruler.prototype.enableOnly = function (list, ignoreInvalid) {
  if (!Array.isArray(list)) { list = [ list ]; }

  this.__rules__.forEach(function (rule) { rule.enabled = false; });

  this.enable(list, ignoreInvalid);
};


/**
 * Ruler.disable(list [, ignoreInvalid]) -> Array
 * - list (String|Array): list of rule names to disable.
 * - ignoreInvalid (Boolean): set `true` to ignore errors when rule not found.
 *
 * Disable rules with given names. If any rule name not found - throw Error.
 * Errors can be disabled by second param.
 *
 * Returns list of found rule names (if no exception happened).
 *
 * See also [[Ruler.enable]], [[Ruler.enableOnly]].
 **/
Ruler.prototype.disable = function (list, ignoreInvalid) {
  if (!Array.isArray(list)) { list = [ list ]; }

  var result = [];

  // Search by name and disable
  list.forEach(function (name) {
    var idx = this.__find__(name);

    if (idx < 0) {
      if (ignoreInvalid) { return; }
      throw new Error('Rules manager: invalid rule name ' + name);
    }
    this.__rules__[idx].enabled = false;
    result.push(name);
  }, this);

  this.__cache__ = null;
  return result;
};


/**
 * Ruler.getRules(chainName) -> Array
 *
 * Return array of active functions (rules) for given chain name. It analyzes
 * rules configuration, compiles caches if not exists and returns result.
 *
 * Default chain name is `''` (empty string). It can't be skipped. That's
 * done intentionally, to keep signature monomorphic for high speed.
 **/
Ruler.prototype.getRules = function (chainName) {
  if (this.__cache__ === null) {
    this.__compile__();
  }

  // Chain can be empty, if rules disabled. But we still have to return Array.
  return this.__cache__[chainName] || [];
};

module.exports = Ruler;

});
___scope___.file("lib/rules_core/normalize.js", function(exports, require, module, __filename, __dirname){

// Normalize input string

'use strict';


var NEWLINES_RE  = /\r[\n\u0085]?|[\u2424\u2028\u0085]/g;
var NULL_RE      = /\u0000/g;


module.exports = function inline(state) {
  var str;

  // Normalize newlines
  str = state.src.replace(NEWLINES_RE, '\n');

  // Replace NULL characters
  str = str.replace(NULL_RE, '\uFFFD');

  state.src = str;
};

});
___scope___.file("lib/rules_core/block.js", function(exports, require, module, __filename, __dirname){

'use strict';


module.exports = function block(state) {
  var token;

  if (state.inlineMode) {
    token          = new state.Token('inline', '', 0);
    token.content  = state.src;
    token.map      = [ 0, 1 ];
    token.children = [];
    state.tokens.push(token);
  } else {
    state.md.block.parse(state.src, state.md, state.env, state.tokens);
  }
};

});
___scope___.file("lib/rules_core/inline.js", function(exports, require, module, __filename, __dirname){

'use strict';

module.exports = function inline(state) {
  var tokens = state.tokens, tok, i, l;

  // Parse inlines
  for (i = 0, l = tokens.length; i < l; i++) {
    tok = tokens[i];
    if (tok.type === 'inline') {
      state.md.inline.parse(tok.content, state.md, state.env, tok.children);
    }
  }
};

});
___scope___.file("lib/rules_core/linkify.js", function(exports, require, module, __filename, __dirname){

// Replace link-like texts with link nodes.
//
// Currently restricted by `md.validateLink()` to http/https/ftp
//
'use strict';


var arrayReplaceAt = require('../common/utils').arrayReplaceAt;


function isLinkOpen(str) {
  return /^<a[>\s]/i.test(str);
}
function isLinkClose(str) {
  return /^<\/a\s*>/i.test(str);
}


module.exports = function linkify(state) {
  var i, j, l, tokens, token, currentToken, nodes, ln, text, pos, lastPos,
      level, htmlLinkLevel, url, fullUrl, urlText,
      blockTokens = state.tokens,
      links;

  if (!state.md.options.linkify) { return; }

  for (j = 0, l = blockTokens.length; j < l; j++) {
    if (blockTokens[j].type !== 'inline' ||
        !state.md.linkify.pretest(blockTokens[j].content)) {
      continue;
    }

    tokens = blockTokens[j].children;

    htmlLinkLevel = 0;

    // We scan from the end, to keep position when new tags added.
    // Use reversed logic in links start/end match
    for (i = tokens.length - 1; i >= 0; i--) {
      currentToken = tokens[i];

      // Skip content of markdown links
      if (currentToken.type === 'link_close') {
        i--;
        while (tokens[i].level !== currentToken.level && tokens[i].type !== 'link_open') {
          i--;
        }
        continue;
      }

      // Skip content of html tag links
      if (currentToken.type === 'html_inline') {
        if (isLinkOpen(currentToken.content) && htmlLinkLevel > 0) {
          htmlLinkLevel--;
        }
        if (isLinkClose(currentToken.content)) {
          htmlLinkLevel++;
        }
      }
      if (htmlLinkLevel > 0) { continue; }

      if (currentToken.type === 'text' && state.md.linkify.test(currentToken.content)) {

        text = currentToken.content;
        links = state.md.linkify.match(text);

        // Now split string to nodes
        nodes = [];
        level = currentToken.level;
        lastPos = 0;

        for (ln = 0; ln < links.length; ln++) {

          url = links[ln].url;
          fullUrl = state.md.normalizeLink(url);
          if (!state.md.validateLink(fullUrl)) { continue; }

          urlText = links[ln].text;

          // Linkifier might send raw hostnames like "example.com", where url
          // starts with domain name. So we prepend http:// in those cases,
          // and remove it afterwards.
          //
          if (!links[ln].schema) {
            urlText = state.md.normalizeLinkText('http://' + urlText).replace(/^http:\/\//, '');
          } else if (links[ln].schema === 'mailto:' && !/^mailto:/i.test(urlText)) {
            urlText = state.md.normalizeLinkText('mailto:' + urlText).replace(/^mailto:/, '');
          } else {
            urlText = state.md.normalizeLinkText(urlText);
          }

          pos = links[ln].index;

          if (pos > lastPos) {
            token         = new state.Token('text', '', 0);
            token.content = text.slice(lastPos, pos);
            token.level   = level;
            nodes.push(token);
          }

          token         = new state.Token('link_open', 'a', 1);
          token.attrs   = [ [ 'href', fullUrl ] ];
          token.level   = level++;
          token.markup  = 'linkify';
          token.info    = 'auto';
          nodes.push(token);

          token         = new state.Token('text', '', 0);
          token.content = urlText;
          token.level   = level;
          nodes.push(token);

          token         = new state.Token('link_close', 'a', -1);
          token.level   = --level;
          token.markup  = 'linkify';
          token.info    = 'auto';
          nodes.push(token);

          lastPos = links[ln].lastIndex;
        }
        if (lastPos < text.length) {
          token         = new state.Token('text', '', 0);
          token.content = text.slice(lastPos);
          token.level   = level;
          nodes.push(token);
        }

        // replace current node
        blockTokens[j].children = tokens = arrayReplaceAt(tokens, i, nodes);
      }
    }
  }
};

});
___scope___.file("lib/rules_core/replacements.js", function(exports, require, module, __filename, __dirname){

// Simple typographyc replacements
//
// (c) (C) → ©
// (tm) (TM) → ™
// (r) (R) → ®
// +- → ±
// (p) (P) -> §
// ... → … (also ?.... → ?.., !.... → !..)
// ???????? → ???, !!!!! → !!!, `,,` → `,`
// -- → &ndash;, --- → &mdash;
//
'use strict';

// TODO:
// - fractionals 1/2, 1/4, 3/4 -> ½, ¼, ¾
// - miltiplication 2 x 4 -> 2 × 4

var RARE_RE = /\+-|\.\.|\?\?\?\?|!!!!|,,|--/;

// Workaround for phantomjs - need regex without /g flag,
// or root check will fail every second time
var SCOPED_ABBR_TEST_RE = /\((c|tm|r|p)\)/i;

var SCOPED_ABBR_RE = /\((c|tm|r|p)\)/ig;
var SCOPED_ABBR = {
  c: '©',
  r: '®',
  p: '§',
  tm: '™'
};

function replaceFn(match, name) {
  return SCOPED_ABBR[name.toLowerCase()];
}

function replace_scoped(inlineTokens) {
  var i, token, inside_autolink = 0;

  for (i = inlineTokens.length - 1; i >= 0; i--) {
    token = inlineTokens[i];

    if (token.type === 'text' && !inside_autolink) {
      token.content = token.content.replace(SCOPED_ABBR_RE, replaceFn);
    }

    if (token.type === 'link_open' && token.info === 'auto') {
      inside_autolink--;
    }

    if (token.type === 'link_close' && token.info === 'auto') {
      inside_autolink++;
    }
  }
}

function replace_rare(inlineTokens) {
  var i, token, inside_autolink = 0;

  for (i = inlineTokens.length - 1; i >= 0; i--) {
    token = inlineTokens[i];

    if (token.type === 'text' && !inside_autolink) {
      if (RARE_RE.test(token.content)) {
        token.content = token.content
                    .replace(/\+-/g, '±')
                    // .., ..., ....... -> …
                    // but ?..... & !..... -> ?.. & !..
                    .replace(/\.{2,}/g, '…').replace(/([?!])…/g, '$1..')
                    .replace(/([?!]){4,}/g, '$1$1$1').replace(/,{2,}/g, ',')
                    // em-dash
                    .replace(/(^|[^-])---([^-]|$)/mg, '$1\u2014$2')
                    // en-dash
                    .replace(/(^|\s)--(\s|$)/mg, '$1\u2013$2')
                    .replace(/(^|[^-\s])--([^-\s]|$)/mg, '$1\u2013$2');
      }
    }

    if (token.type === 'link_open' && token.info === 'auto') {
      inside_autolink--;
    }

    if (token.type === 'link_close' && token.info === 'auto') {
      inside_autolink++;
    }
  }
}


module.exports = function replace(state) {
  var blkIdx;

  if (!state.md.options.typographer) { return; }

  for (blkIdx = state.tokens.length - 1; blkIdx >= 0; blkIdx--) {

    if (state.tokens[blkIdx].type !== 'inline') { continue; }

    if (SCOPED_ABBR_TEST_RE.test(state.tokens[blkIdx].content)) {
      replace_scoped(state.tokens[blkIdx].children);
    }

    if (RARE_RE.test(state.tokens[blkIdx].content)) {
      replace_rare(state.tokens[blkIdx].children);
    }

  }
};

});
___scope___.file("lib/rules_core/smartquotes.js", function(exports, require, module, __filename, __dirname){

// Convert straight quotation marks to typographic ones
//
'use strict';


var isWhiteSpace   = require('../common/utils').isWhiteSpace;
var isPunctChar    = require('../common/utils').isPunctChar;
var isMdAsciiPunct = require('../common/utils').isMdAsciiPunct;

var QUOTE_TEST_RE = /['"]/;
var QUOTE_RE = /['"]/g;
var APOSTROPHE = '\u2019'; /* ’ */


function replaceAt(str, index, ch) {
  return str.substr(0, index) + ch + str.substr(index + 1);
}

function process_inlines(tokens, state) {
  var i, token, text, t, pos, max, thisLevel, item, lastChar, nextChar,
      isLastPunctChar, isNextPunctChar, isLastWhiteSpace, isNextWhiteSpace,
      canOpen, canClose, j, isSingle, stack, openQuote, closeQuote;

  stack = [];

  for (i = 0; i < tokens.length; i++) {
    token = tokens[i];

    thisLevel = tokens[i].level;

    for (j = stack.length - 1; j >= 0; j--) {
      if (stack[j].level <= thisLevel) { break; }
    }
    stack.length = j + 1;

    if (token.type !== 'text') { continue; }

    text = token.content;
    pos = 0;
    max = text.length;

    /*eslint no-labels:0,block-scoped-var:0*/
    OUTER:
    while (pos < max) {
      QUOTE_RE.lastIndex = pos;
      t = QUOTE_RE.exec(text);
      if (!t) { break; }

      canOpen = canClose = true;
      pos = t.index + 1;
      isSingle = (t[0] === "'");

      // Find previous character,
      // default to space if it's the beginning of the line
      //
      lastChar = 0x20;

      if (t.index - 1 >= 0) {
        lastChar = text.charCodeAt(t.index - 1);
      } else {
        for (j = i - 1; j >= 0; j--) {
          if (tokens[j].type === 'softbreak' || tokens[j].type === 'hardbreak') break; // lastChar defaults to 0x20
          if (tokens[j].type !== 'text') continue;

          lastChar = tokens[j].content.charCodeAt(tokens[j].content.length - 1);
          break;
        }
      }

      // Find next character,
      // default to space if it's the end of the line
      //
      nextChar = 0x20;

      if (pos < max) {
        nextChar = text.charCodeAt(pos);
      } else {
        for (j = i + 1; j < tokens.length; j++) {
          if (tokens[j].type === 'softbreak' || tokens[j].type === 'hardbreak') break; // nextChar defaults to 0x20
          if (tokens[j].type !== 'text') continue;

          nextChar = tokens[j].content.charCodeAt(0);
          break;
        }
      }

      isLastPunctChar = isMdAsciiPunct(lastChar) || isPunctChar(String.fromCharCode(lastChar));
      isNextPunctChar = isMdAsciiPunct(nextChar) || isPunctChar(String.fromCharCode(nextChar));

      isLastWhiteSpace = isWhiteSpace(lastChar);
      isNextWhiteSpace = isWhiteSpace(nextChar);

      if (isNextWhiteSpace) {
        canOpen = false;
      } else if (isNextPunctChar) {
        if (!(isLastWhiteSpace || isLastPunctChar)) {
          canOpen = false;
        }
      }

      if (isLastWhiteSpace) {
        canClose = false;
      } else if (isLastPunctChar) {
        if (!(isNextWhiteSpace || isNextPunctChar)) {
          canClose = false;
        }
      }

      if (nextChar === 0x22 /* " */ && t[0] === '"') {
        if (lastChar >= 0x30 /* 0 */ && lastChar <= 0x39 /* 9 */) {
          // special case: 1"" - count first quote as an inch
          canClose = canOpen = false;
        }
      }

      if (canOpen && canClose) {
        // treat this as the middle of the word
        canOpen = false;
        canClose = isNextPunctChar;
      }

      if (!canOpen && !canClose) {
        // middle of word
        if (isSingle) {
          token.content = replaceAt(token.content, t.index, APOSTROPHE);
        }
        continue;
      }

      if (canClose) {
        // this could be a closing quote, rewind the stack to get a match
        for (j = stack.length - 1; j >= 0; j--) {
          item = stack[j];
          if (stack[j].level < thisLevel) { break; }
          if (item.single === isSingle && stack[j].level === thisLevel) {
            item = stack[j];

            if (isSingle) {
              openQuote = state.md.options.quotes[2];
              closeQuote = state.md.options.quotes[3];
            } else {
              openQuote = state.md.options.quotes[0];
              closeQuote = state.md.options.quotes[1];
            }

            // replace token.content *before* tokens[item.token].content,
            // because, if they are pointing at the same token, replaceAt
            // could mess up indices when quote length != 1
            token.content = replaceAt(token.content, t.index, closeQuote);
            tokens[item.token].content = replaceAt(
              tokens[item.token].content, item.pos, openQuote);

            pos += closeQuote.length - 1;
            if (item.token === i) { pos += openQuote.length - 1; }

            text = token.content;
            max = text.length;

            stack.length = j;
            continue OUTER;
          }
        }
      }

      if (canOpen) {
        stack.push({
          token: i,
          pos: t.index,
          single: isSingle,
          level: thisLevel
        });
      } else if (canClose && isSingle) {
        token.content = replaceAt(token.content, t.index, APOSTROPHE);
      }
    }
  }
}


module.exports = function smartquotes(state) {
  /*eslint max-depth:0*/
  var blkIdx;

  if (!state.md.options.typographer) { return; }

  for (blkIdx = state.tokens.length - 1; blkIdx >= 0; blkIdx--) {

    if (state.tokens[blkIdx].type !== 'inline' ||
        !QUOTE_TEST_RE.test(state.tokens[blkIdx].content)) {
      continue;
    }

    process_inlines(state.tokens[blkIdx].children, state);
  }
};

});
___scope___.file("lib/rules_core/state_core.js", function(exports, require, module, __filename, __dirname){

// Core state object
//
'use strict';

var Token = require('../token');


function StateCore(src, md, env) {
  this.src = src;
  this.env = env;
  this.tokens = [];
  this.inlineMode = false;
  this.md = md; // link to parser instance
}

// re-export Token class to use in core rules
StateCore.prototype.Token = Token;


module.exports = StateCore;

});
___scope___.file("lib/token.js", function(exports, require, module, __filename, __dirname){

// Token class

'use strict';


/**
 * class Token
 **/

/**
 * new Token(type, tag, nesting)
 *
 * Create new token and fill passed properties.
 **/
function Token(type, tag, nesting) {
  /**
   * Token#type -> String
   *
   * Type of the token (string, e.g. "paragraph_open")
   **/
  this.type     = type;

  /**
   * Token#tag -> String
   *
   * html tag name, e.g. "p"
   **/
  this.tag      = tag;

  /**
   * Token#attrs -> Array
   *
   * Html attributes. Format: `[ [ name1, value1 ], [ name2, value2 ] ]`
   **/
  this.attrs    = null;

  /**
   * Token#map -> Array
   *
   * Source map info. Format: `[ line_begin, line_end ]`
   **/
  this.map      = null;

  /**
   * Token#nesting -> Number
   *
   * Level change (number in {-1, 0, 1} set), where:
   *
   * -  `1` means the tag is opening
   * -  `0` means the tag is self-closing
   * - `-1` means the tag is closing
   **/
  this.nesting  = nesting;

  /**
   * Token#level -> Number
   *
   * nesting level, the same as `state.level`
   **/
  this.level    = 0;

  /**
   * Token#children -> Array
   *
   * An array of child nodes (inline and img tokens)
   **/
  this.children = null;

  /**
   * Token#content -> String
   *
   * In a case of self-closing tag (code, html, fence, etc.),
   * it has contents of this tag.
   **/
  this.content  = '';

  /**
   * Token#markup -> String
   *
   * '*' or '_' for emphasis, fence string for fence, etc.
   **/
  this.markup   = '';

  /**
   * Token#info -> String
   *
   * fence infostring
   **/
  this.info     = '';

  /**
   * Token#meta -> Object
   *
   * A place for plugins to store an arbitrary data
   **/
  this.meta     = null;

  /**
   * Token#block -> Boolean
   *
   * True for block-level tokens, false for inline tokens.
   * Used in renderer to calculate line breaks
   **/
  this.block    = false;

  /**
   * Token#hidden -> Boolean
   *
   * If it's true, ignore this element when rendering. Used for tight lists
   * to hide paragraphs.
   **/
  this.hidden   = false;
}


/**
 * Token.attrIndex(name) -> Number
 *
 * Search attribute index by name.
 **/
Token.prototype.attrIndex = function attrIndex(name) {
  var attrs, i, len;

  if (!this.attrs) { return -1; }

  attrs = this.attrs;

  for (i = 0, len = attrs.length; i < len; i++) {
    if (attrs[i][0] === name) { return i; }
  }
  return -1;
};


/**
 * Token.attrPush(attrData)
 *
 * Add `[ name, value ]` attribute to list. Init attrs if necessary
 **/
Token.prototype.attrPush = function attrPush(attrData) {
  if (this.attrs) {
    this.attrs.push(attrData);
  } else {
    this.attrs = [ attrData ];
  }
};


/**
 * Token.attrSet(name, value)
 *
 * Set `name` attribute to `value`. Override old value if exists.
 **/
Token.prototype.attrSet = function attrSet(name, value) {
  var idx = this.attrIndex(name),
      attrData = [ name, value ];

  if (idx < 0) {
    this.attrPush(attrData);
  } else {
    this.attrs[idx] = attrData;
  }
};


/**
 * Token.attrGet(name)
 *
 * Get the value of attribute `name`, or null if it does not exist.
 **/
Token.prototype.attrGet = function attrGet(name) {
  var idx = this.attrIndex(name), value = null;
  if (idx >= 0) {
    value = this.attrs[idx][1];
  }
  return value;
};


/**
 * Token.attrJoin(name, value)
 *
 * Join value to existing attribute via space. Or create new attribute if not
 * exists. Useful to operate with token classes.
 **/
Token.prototype.attrJoin = function attrJoin(name, value) {
  var idx = this.attrIndex(name);

  if (idx < 0) {
    this.attrPush([ name, value ]);
  } else {
    this.attrs[idx][1] = this.attrs[idx][1] + ' ' + value;
  }
};


module.exports = Token;

});
___scope___.file("lib/parser_block.js", function(exports, require, module, __filename, __dirname){

/** internal
 * class ParserBlock
 *
 * Block-level tokenizer.
 **/
'use strict';


var Ruler           = require('./ruler');


var _rules = [
  // First 2 params - rule name & source. Secondary array - list of rules,
  // which can be terminated by this one.
  [ 'table',      require('./rules_block/table'),      [ 'paragraph', 'reference' ] ],
  [ 'code',       require('./rules_block/code') ],
  [ 'fence',      require('./rules_block/fence'),      [ 'paragraph', 'reference', 'blockquote', 'list' ] ],
  [ 'blockquote', require('./rules_block/blockquote'), [ 'paragraph', 'reference', 'blockquote', 'list' ] ],
  [ 'hr',         require('./rules_block/hr'),         [ 'paragraph', 'reference', 'blockquote', 'list' ] ],
  [ 'list',       require('./rules_block/list'),       [ 'paragraph', 'reference', 'blockquote' ] ],
  [ 'reference',  require('./rules_block/reference') ],
  [ 'heading',    require('./rules_block/heading'),    [ 'paragraph', 'reference', 'blockquote' ] ],
  [ 'lheading',   require('./rules_block/lheading') ],
  [ 'html_block', require('./rules_block/html_block'), [ 'paragraph', 'reference', 'blockquote' ] ],
  [ 'paragraph',  require('./rules_block/paragraph') ]
];


/**
 * new ParserBlock()
 **/
function ParserBlock() {
  /**
   * ParserBlock#ruler -> Ruler
   *
   * [[Ruler]] instance. Keep configuration of block rules.
   **/
  this.ruler = new Ruler();

  for (var i = 0; i < _rules.length; i++) {
    this.ruler.push(_rules[i][0], _rules[i][1], { alt: (_rules[i][2] || []).slice() });
  }
}


// Generate tokens for input range
//
ParserBlock.prototype.tokenize = function (state, startLine, endLine) {
  var ok, i,
      rules = this.ruler.getRules(''),
      len = rules.length,
      line = startLine,
      hasEmptyLines = false,
      maxNesting = state.md.options.maxNesting;

  while (line < endLine) {
    state.line = line = state.skipEmptyLines(line);
    if (line >= endLine) { break; }

    // Termination condition for nested calls.
    // Nested calls currently used for blockquotes & lists
    if (state.sCount[line] < state.blkIndent) { break; }

    // If nesting level exceeded - skip tail to the end. That's not ordinary
    // situation and we should not care about content.
    if (state.level >= maxNesting) {
      state.line = endLine;
      break;
    }

    // Try all possible rules.
    // On success, rule should:
    //
    // - update `state.line`
    // - update `state.tokens`
    // - return true

    for (i = 0; i < len; i++) {
      ok = rules[i](state, line, endLine, false);
      if (ok) { break; }
    }

    // set state.tight if we had an empty line before current tag
    // i.e. latest empty line should not count
    state.tight = !hasEmptyLines;

    // paragraph might "eat" one newline after it in nested lists
    if (state.isEmpty(state.line - 1)) {
      hasEmptyLines = true;
    }

    line = state.line;

    if (line < endLine && state.isEmpty(line)) {
      hasEmptyLines = true;
      line++;
      state.line = line;
    }
  }
};


/**
 * ParserBlock.parse(str, md, env, outTokens)
 *
 * Process input string and push block tokens into `outTokens`
 **/
ParserBlock.prototype.parse = function (src, md, env, outTokens) {
  var state;

  if (!src) { return; }

  state = new this.State(src, md, env, outTokens);

  this.tokenize(state, state.line, state.lineMax);
};


ParserBlock.prototype.State = require('./rules_block/state_block');


module.exports = ParserBlock;

});
___scope___.file("lib/rules_block/table.js", function(exports, require, module, __filename, __dirname){

// GFM table, non-standard

'use strict';

var isSpace = require('../common/utils').isSpace;


function getLine(state, line) {
  var pos = state.bMarks[line] + state.blkIndent,
      max = state.eMarks[line];

  return state.src.substr(pos, max - pos);
}

function escapedSplit(str) {
  var result = [],
      pos = 0,
      max = str.length,
      ch,
      escapes = 0,
      lastPos = 0,
      backTicked = false,
      lastBackTick = 0;

  ch  = str.charCodeAt(pos);

  while (pos < max) {
    if (ch === 0x60/* ` */) {
      if (backTicked) {
        // make \` close code sequence, but not open it;
        // the reason is: `\` is correct code block
        backTicked = false;
        lastBackTick = pos;
      } else if (escapes % 2 === 0) {
        backTicked = true;
        lastBackTick = pos;
      }
    } else if (ch === 0x7c/* | */ && (escapes % 2 === 0) && !backTicked) {
      result.push(str.substring(lastPos, pos));
      lastPos = pos + 1;
    }

    if (ch === 0x5c/* \ */) {
      escapes++;
    } else {
      escapes = 0;
    }

    pos++;

    // If there was an un-closed backtick, go back to just after
    // the last backtick, but as if it was a normal character
    if (pos === max && backTicked) {
      backTicked = false;
      pos = lastBackTick + 1;
    }

    ch = str.charCodeAt(pos);
  }

  result.push(str.substring(lastPos));

  return result;
}


module.exports = function table(state, startLine, endLine, silent) {
  var ch, lineText, pos, i, nextLine, columns, columnCount, token,
      aligns, t, tableLines, tbodyLines;

  // should have at least two lines
  if (startLine + 2 > endLine) { return false; }

  nextLine = startLine + 1;

  if (state.sCount[nextLine] < state.blkIndent) { return false; }

  // if it's indented more than 3 spaces, it should be a code block
  if (state.sCount[nextLine] - state.blkIndent >= 4) { return false; }

  // first character of the second line should be '|', '-', ':',
  // and no other characters are allowed but spaces;
  // basically, this is the equivalent of /^[-:|][-:|\s]*$/ regexp

  pos = state.bMarks[nextLine] + state.tShift[nextLine];
  if (pos >= state.eMarks[nextLine]) { return false; }

  ch = state.src.charCodeAt(pos++);
  if (ch !== 0x7C/* | */ && ch !== 0x2D/* - */ && ch !== 0x3A/* : */) { return false; }

  while (pos < state.eMarks[nextLine]) {
    ch = state.src.charCodeAt(pos);

    if (ch !== 0x7C/* | */ && ch !== 0x2D/* - */ && ch !== 0x3A/* : */ && !isSpace(ch)) { return false; }

    pos++;
  }

  lineText = getLine(state, startLine + 1);

  columns = lineText.split('|');
  aligns = [];
  for (i = 0; i < columns.length; i++) {
    t = columns[i].trim();
    if (!t) {
      // allow empty columns before and after table, but not in between columns;
      // e.g. allow ` |---| `, disallow ` ---||--- `
      if (i === 0 || i === columns.length - 1) {
        continue;
      } else {
        return false;
      }
    }

    if (!/^:?-+:?$/.test(t)) { return false; }
    if (t.charCodeAt(t.length - 1) === 0x3A/* : */) {
      aligns.push(t.charCodeAt(0) === 0x3A/* : */ ? 'center' : 'right');
    } else if (t.charCodeAt(0) === 0x3A/* : */) {
      aligns.push('left');
    } else {
      aligns.push('');
    }
  }

  lineText = getLine(state, startLine).trim();
  if (lineText.indexOf('|') === -1) { return false; }
  if (state.sCount[startLine] - state.blkIndent >= 4) { return false; }
  columns = escapedSplit(lineText.replace(/^\||\|$/g, ''));

  // header row will define an amount of columns in the entire table,
  // and align row shouldn't be smaller than that (the rest of the rows can)
  columnCount = columns.length;
  if (columnCount > aligns.length) { return false; }

  if (silent) { return true; }

  token     = state.push('table_open', 'table', 1);
  token.map = tableLines = [ startLine, 0 ];

  token     = state.push('thead_open', 'thead', 1);
  token.map = [ startLine, startLine + 1 ];

  token     = state.push('tr_open', 'tr', 1);
  token.map = [ startLine, startLine + 1 ];

  for (i = 0; i < columns.length; i++) {
    token          = state.push('th_open', 'th', 1);
    token.map      = [ startLine, startLine + 1 ];
    if (aligns[i]) {
      token.attrs  = [ [ 'style', 'text-align:' + aligns[i] ] ];
    }

    token          = state.push('inline', '', 0);
    token.content  = columns[i].trim();
    token.map      = [ startLine, startLine + 1 ];
    token.children = [];

    token          = state.push('th_close', 'th', -1);
  }

  token     = state.push('tr_close', 'tr', -1);
  token     = state.push('thead_close', 'thead', -1);

  token     = state.push('tbody_open', 'tbody', 1);
  token.map = tbodyLines = [ startLine + 2, 0 ];

  for (nextLine = startLine + 2; nextLine < endLine; nextLine++) {
    if (state.sCount[nextLine] < state.blkIndent) { break; }

    lineText = getLine(state, nextLine).trim();
    if (lineText.indexOf('|') === -1) { break; }
    if (state.sCount[nextLine] - state.blkIndent >= 4) { break; }
    columns = escapedSplit(lineText.replace(/^\||\|$/g, ''));

    token = state.push('tr_open', 'tr', 1);
    for (i = 0; i < columnCount; i++) {
      token          = state.push('td_open', 'td', 1);
      if (aligns[i]) {
        token.attrs  = [ [ 'style', 'text-align:' + aligns[i] ] ];
      }

      token          = state.push('inline', '', 0);
      token.content  = columns[i] ? columns[i].trim() : '';
      token.children = [];

      token          = state.push('td_close', 'td', -1);
    }
    token = state.push('tr_close', 'tr', -1);
  }
  token = state.push('tbody_close', 'tbody', -1);
  token = state.push('table_close', 'table', -1);

  tableLines[1] = tbodyLines[1] = nextLine;
  state.line = nextLine;
  return true;
};

});
___scope___.file("lib/rules_block/code.js", function(exports, require, module, __filename, __dirname){

// Code block (4 spaces padded)

'use strict';


module.exports = function code(state, startLine, endLine/*, silent*/) {
  var nextLine, last, token;

  if (state.sCount[startLine] - state.blkIndent < 4) { return false; }

  last = nextLine = startLine + 1;

  while (nextLine < endLine) {
    if (state.isEmpty(nextLine)) {
      nextLine++;
      continue;
    }

    if (state.sCount[nextLine] - state.blkIndent >= 4) {
      nextLine++;
      last = nextLine;
      continue;
    }
    break;
  }

  state.line = last;

  token         = state.push('code_block', 'code', 0);
  token.content = state.getLines(startLine, last, 4 + state.blkIndent, true);
  token.map     = [ startLine, state.line ];

  return true;
};

});
___scope___.file("lib/rules_block/fence.js", function(exports, require, module, __filename, __dirname){

// fences (``` lang, ~~~ lang)

'use strict';


module.exports = function fence(state, startLine, endLine, silent) {
  var marker, len, params, nextLine, mem, token, markup,
      haveEndMarker = false,
      pos = state.bMarks[startLine] + state.tShift[startLine],
      max = state.eMarks[startLine];

  // if it's indented more than 3 spaces, it should be a code block
  if (state.sCount[startLine] - state.blkIndent >= 4) { return false; }

  if (pos + 3 > max) { return false; }

  marker = state.src.charCodeAt(pos);

  if (marker !== 0x7E/* ~ */ && marker !== 0x60 /* ` */) {
    return false;
  }

  // scan marker length
  mem = pos;
  pos = state.skipChars(pos, marker);

  len = pos - mem;

  if (len < 3) { return false; }

  markup = state.src.slice(mem, pos);
  params = state.src.slice(pos, max);

  if (params.indexOf(String.fromCharCode(marker)) >= 0) { return false; }

  // Since start is found, we can report success here in validation mode
  if (silent) { return true; }

  // search end of block
  nextLine = startLine;

  for (;;) {
    nextLine++;
    if (nextLine >= endLine) {
      // unclosed block should be autoclosed by end of document.
      // also block seems to be autoclosed by end of parent
      break;
    }

    pos = mem = state.bMarks[nextLine] + state.tShift[nextLine];
    max = state.eMarks[nextLine];

    if (pos < max && state.sCount[nextLine] < state.blkIndent) {
      // non-empty line with negative indent should stop the list:
      // - ```
      //  test
      break;
    }

    if (state.src.charCodeAt(pos) !== marker) { continue; }

    if (state.sCount[nextLine] - state.blkIndent >= 4) {
      // closing fence should be indented less than 4 spaces
      continue;
    }

    pos = state.skipChars(pos, marker);

    // closing code fence must be at least as long as the opening one
    if (pos - mem < len) { continue; }

    // make sure tail has spaces only
    pos = state.skipSpaces(pos);

    if (pos < max) { continue; }

    haveEndMarker = true;
    // found!
    break;
  }

  // If a fence has heading spaces, they should be removed from its inner block
  len = state.sCount[startLine];

  state.line = nextLine + (haveEndMarker ? 1 : 0);

  token         = state.push('fence', 'code', 0);
  token.info    = params;
  token.content = state.getLines(startLine + 1, nextLine, len, true);
  token.markup  = markup;
  token.map     = [ startLine, state.line ];

  return true;
};

});
___scope___.file("lib/rules_block/blockquote.js", function(exports, require, module, __filename, __dirname){

// Block quotes

'use strict';

var isSpace = require('../common/utils').isSpace;


module.exports = function blockquote(state, startLine, endLine, silent) {
  var adjustTab,
      ch,
      i,
      initial,
      l,
      lastLineEmpty,
      lines,
      nextLine,
      offset,
      oldBMarks,
      oldBSCount,
      oldIndent,
      oldParentType,
      oldSCount,
      oldTShift,
      spaceAfterMarker,
      terminate,
      terminatorRules,
      token,
      wasOutdented,
      oldLineMax = state.lineMax,
      pos = state.bMarks[startLine] + state.tShift[startLine],
      max = state.eMarks[startLine];

  // if it's indented more than 3 spaces, it should be a code block
  if (state.sCount[startLine] - state.blkIndent >= 4) { return false; }

  // check the block quote marker
  if (state.src.charCodeAt(pos++) !== 0x3E/* > */) { return false; }

  // we know that it's going to be a valid blockquote,
  // so no point trying to find the end of it in silent mode
  if (silent) { return true; }

  // skip spaces after ">" and re-calculate offset
  initial = offset = state.sCount[startLine] + pos - (state.bMarks[startLine] + state.tShift[startLine]);

  // skip one optional space after '>'
  if (state.src.charCodeAt(pos) === 0x20 /* space */) {
    // ' >   test '
    //     ^ -- position start of line here:
    pos++;
    initial++;
    offset++;
    adjustTab = false;
    spaceAfterMarker = true;
  } else if (state.src.charCodeAt(pos) === 0x09 /* tab */) {
    spaceAfterMarker = true;

    if ((state.bsCount[startLine] + offset) % 4 === 3) {
      // '  >\t  test '
      //       ^ -- position start of line here (tab has width===1)
      pos++;
      initial++;
      offset++;
      adjustTab = false;
    } else {
      // ' >\t  test '
      //    ^ -- position start of line here + shift bsCount slightly
      //         to make extra space appear
      adjustTab = true;
    }
  } else {
    spaceAfterMarker = false;
  }

  oldBMarks = [ state.bMarks[startLine] ];
  state.bMarks[startLine] = pos;

  while (pos < max) {
    ch = state.src.charCodeAt(pos);

    if (isSpace(ch)) {
      if (ch === 0x09) {
        offset += 4 - (offset + state.bsCount[startLine] + (adjustTab ? 1 : 0)) % 4;
      } else {
        offset++;
      }
    } else {
      break;
    }

    pos++;
  }

  oldBSCount = [ state.bsCount[startLine] ];
  state.bsCount[startLine] = state.sCount[startLine] + 1 + (spaceAfterMarker ? 1 : 0);

  lastLineEmpty = pos >= max;

  oldSCount = [ state.sCount[startLine] ];
  state.sCount[startLine] = offset - initial;

  oldTShift = [ state.tShift[startLine] ];
  state.tShift[startLine] = pos - state.bMarks[startLine];

  terminatorRules = state.md.block.ruler.getRules('blockquote');

  oldParentType = state.parentType;
  state.parentType = 'blockquote';
  wasOutdented = false;

  // Search the end of the block
  //
  // Block ends with either:
  //  1. an empty line outside:
  //     ```
  //     > test
  //
  //     ```
  //  2. an empty line inside:
  //     ```
  //     >
  //     test
  //     ```
  //  3. another tag:
  //     ```
  //     > test
  //      - - -
  //     ```
  for (nextLine = startLine + 1; nextLine < endLine; nextLine++) {
    // check if it's outdented, i.e. it's inside list item and indented
    // less than said list item:
    //
    // ```
    // 1. anything
    //    > current blockquote
    // 2. checking this line
    // ```
    if (state.sCount[nextLine] < state.blkIndent) wasOutdented = true;

    pos = state.bMarks[nextLine] + state.tShift[nextLine];
    max = state.eMarks[nextLine];

    if (pos >= max) {
      // Case 1: line is not inside the blockquote, and this line is empty.
      break;
    }

    if (state.src.charCodeAt(pos++) === 0x3E/* > */ && !wasOutdented) {
      // This line is inside the blockquote.

      // skip spaces after ">" and re-calculate offset
      initial = offset = state.sCount[nextLine] + pos - (state.bMarks[nextLine] + state.tShift[nextLine]);

      // skip one optional space after '>'
      if (state.src.charCodeAt(pos) === 0x20 /* space */) {
        // ' >   test '
        //     ^ -- position start of line here:
        pos++;
        initial++;
        offset++;
        adjustTab = false;
        spaceAfterMarker = true;
      } else if (state.src.charCodeAt(pos) === 0x09 /* tab */) {
        spaceAfterMarker = true;

        if ((state.bsCount[nextLine] + offset) % 4 === 3) {
          // '  >\t  test '
          //       ^ -- position start of line here (tab has width===1)
          pos++;
          initial++;
          offset++;
          adjustTab = false;
        } else {
          // ' >\t  test '
          //    ^ -- position start of line here + shift bsCount slightly
          //         to make extra space appear
          adjustTab = true;
        }
      } else {
        spaceAfterMarker = false;
      }

      oldBMarks.push(state.bMarks[nextLine]);
      state.bMarks[nextLine] = pos;

      while (pos < max) {
        ch = state.src.charCodeAt(pos);

        if (isSpace(ch)) {
          if (ch === 0x09) {
            offset += 4 - (offset + state.bsCount[nextLine] + (adjustTab ? 1 : 0)) % 4;
          } else {
            offset++;
          }
        } else {
          break;
        }

        pos++;
      }

      lastLineEmpty = pos >= max;

      oldBSCount.push(state.bsCount[nextLine]);
      state.bsCount[nextLine] = state.sCount[nextLine] + 1 + (spaceAfterMarker ? 1 : 0);

      oldSCount.push(state.sCount[nextLine]);
      state.sCount[nextLine] = offset - initial;

      oldTShift.push(state.tShift[nextLine]);
      state.tShift[nextLine] = pos - state.bMarks[nextLine];
      continue;
    }

    // Case 2: line is not inside the blockquote, and the last line was empty.
    if (lastLineEmpty) { break; }

    // Case 3: another tag found.
    terminate = false;
    for (i = 0, l = terminatorRules.length; i < l; i++) {
      if (terminatorRules[i](state, nextLine, endLine, true)) {
        terminate = true;
        break;
      }
    }

    if (terminate) {
      // Quirk to enforce "hard termination mode" for paragraphs;
      // normally if you call `tokenize(state, startLine, nextLine)`,
      // paragraphs will look below nextLine for paragraph continuation,
      // but if blockquote is terminated by another tag, they shouldn't
      state.lineMax = nextLine;

      if (state.blkIndent !== 0) {
        // state.blkIndent was non-zero, we now set it to zero,
        // so we need to re-calculate all offsets to appear as
        // if indent wasn't changed
        oldBMarks.push(state.bMarks[nextLine]);
        oldBSCount.push(state.bsCount[nextLine]);
        oldTShift.push(state.tShift[nextLine]);
        oldSCount.push(state.sCount[nextLine]);
        state.sCount[nextLine] -= state.blkIndent;
      }

      break;
    }

    oldBMarks.push(state.bMarks[nextLine]);
    oldBSCount.push(state.bsCount[nextLine]);
    oldTShift.push(state.tShift[nextLine]);
    oldSCount.push(state.sCount[nextLine]);

    // A negative indentation means that this is a paragraph continuation
    //
    state.sCount[nextLine] = -1;
  }

  oldIndent = state.blkIndent;
  state.blkIndent = 0;

  token        = state.push('blockquote_open', 'blockquote', 1);
  token.markup = '>';
  token.map    = lines = [ startLine, 0 ];

  state.md.block.tokenize(state, startLine, nextLine);

  token        = state.push('blockquote_close', 'blockquote', -1);
  token.markup = '>';

  state.lineMax = oldLineMax;
  state.parentType = oldParentType;
  lines[1] = state.line;

  // Restore original tShift; this might not be necessary since the parser
  // has already been here, but just to make sure we can do that.
  for (i = 0; i < oldTShift.length; i++) {
    state.bMarks[i + startLine] = oldBMarks[i];
    state.tShift[i + startLine] = oldTShift[i];
    state.sCount[i + startLine] = oldSCount[i];
    state.bsCount[i + startLine] = oldBSCount[i];
  }
  state.blkIndent = oldIndent;

  return true;
};

});
___scope___.file("lib/rules_block/hr.js", function(exports, require, module, __filename, __dirname){

// Horizontal rule

'use strict';

var isSpace = require('../common/utils').isSpace;


module.exports = function hr(state, startLine, endLine, silent) {
  var marker, cnt, ch, token,
      pos = state.bMarks[startLine] + state.tShift[startLine],
      max = state.eMarks[startLine];

  // if it's indented more than 3 spaces, it should be a code block
  if (state.sCount[startLine] - state.blkIndent >= 4) { return false; }

  marker = state.src.charCodeAt(pos++);

  // Check hr marker
  if (marker !== 0x2A/* * */ &&
      marker !== 0x2D/* - */ &&
      marker !== 0x5F/* _ */) {
    return false;
  }

  // markers can be mixed with spaces, but there should be at least 3 of them

  cnt = 1;
  while (pos < max) {
    ch = state.src.charCodeAt(pos++);
    if (ch !== marker && !isSpace(ch)) { return false; }
    if (ch === marker) { cnt++; }
  }

  if (cnt < 3) { return false; }

  if (silent) { return true; }

  state.line = startLine + 1;

  token        = state.push('hr', 'hr', 0);
  token.map    = [ startLine, state.line ];
  token.markup = Array(cnt + 1).join(String.fromCharCode(marker));

  return true;
};

});
___scope___.file("lib/rules_block/list.js", function(exports, require, module, __filename, __dirname){

// Lists

'use strict';

var isSpace = require('../common/utils').isSpace;


// Search `[-+*][\n ]`, returns next pos after marker on success
// or -1 on fail.
function skipBulletListMarker(state, startLine) {
  var marker, pos, max, ch;

  pos = state.bMarks[startLine] + state.tShift[startLine];
  max = state.eMarks[startLine];

  marker = state.src.charCodeAt(pos++);
  // Check bullet
  if (marker !== 0x2A/* * */ &&
      marker !== 0x2D/* - */ &&
      marker !== 0x2B/* + */) {
    return -1;
  }

  if (pos < max) {
    ch = state.src.charCodeAt(pos);

    if (!isSpace(ch)) {
      // " -test " - is not a list item
      return -1;
    }
  }

  return pos;
}

// Search `\d+[.)][\n ]`, returns next pos after marker on success
// or -1 on fail.
function skipOrderedListMarker(state, startLine) {
  var ch,
      start = state.bMarks[startLine] + state.tShift[startLine],
      pos = start,
      max = state.eMarks[startLine];

  // List marker should have at least 2 chars (digit + dot)
  if (pos + 1 >= max) { return -1; }

  ch = state.src.charCodeAt(pos++);

  if (ch < 0x30/* 0 */ || ch > 0x39/* 9 */) { return -1; }

  for (;;) {
    // EOL -> fail
    if (pos >= max) { return -1; }

    ch = state.src.charCodeAt(pos++);

    if (ch >= 0x30/* 0 */ && ch <= 0x39/* 9 */) {

      // List marker should have no more than 9 digits
      // (prevents integer overflow in browsers)
      if (pos - start >= 10) { return -1; }

      continue;
    }

    // found valid marker
    if (ch === 0x29/* ) */ || ch === 0x2e/* . */) {
      break;
    }

    return -1;
  }


  if (pos < max) {
    ch = state.src.charCodeAt(pos);

    if (!isSpace(ch)) {
      // " 1.test " - is not a list item
      return -1;
    }
  }
  return pos;
}

function markTightParagraphs(state, idx) {
  var i, l,
      level = state.level + 2;

  for (i = idx + 2, l = state.tokens.length - 2; i < l; i++) {
    if (state.tokens[i].level === level && state.tokens[i].type === 'paragraph_open') {
      state.tokens[i + 2].hidden = true;
      state.tokens[i].hidden = true;
      i += 2;
    }
  }
}


module.exports = function list(state, startLine, endLine, silent) {
  var ch,
      contentStart,
      i,
      indent,
      indentAfterMarker,
      initial,
      isOrdered,
      itemLines,
      l,
      listLines,
      listTokIdx,
      markerCharCode,
      markerValue,
      max,
      nextLine,
      offset,
      oldIndent,
      oldLIndent,
      oldParentType,
      oldTShift,
      oldTight,
      pos,
      posAfterMarker,
      prevEmptyEnd,
      start,
      terminate,
      terminatorRules,
      token,
      isTerminatingParagraph = false,
      tight = true;

  // if it's indented more than 3 spaces, it should be a code block
  if (state.sCount[startLine] - state.blkIndent >= 4) { return false; }

  // limit conditions when list can interrupt
  // a paragraph (validation mode only)
  if (silent && state.parentType === 'paragraph') {
    // Next list item should still terminate previous list item;
    //
    // This code can fail if plugins use blkIndent as well as lists,
    // but I hope the spec gets fixed long before that happens.
    //
    if (state.tShift[startLine] >= state.blkIndent) {
      isTerminatingParagraph = true;
    }
  }

  // Detect list type and position after marker
  if ((posAfterMarker = skipOrderedListMarker(state, startLine)) >= 0) {
    isOrdered = true;
    start = state.bMarks[startLine] + state.tShift[startLine];
    markerValue = Number(state.src.substr(start, posAfterMarker - start - 1));

    // If we're starting a new ordered list right after
    // a paragraph, it should start with 1.
    if (isTerminatingParagraph && markerValue !== 1) return false;

  } else if ((posAfterMarker = skipBulletListMarker(state, startLine)) >= 0) {
    isOrdered = false;

  } else {
    return false;
  }

  // If we're starting a new unordered list right after
  // a paragraph, first line should not be empty.
  if (isTerminatingParagraph) {
    if (state.skipSpaces(posAfterMarker) >= state.eMarks[startLine]) return false;
  }

  // We should terminate list on style change. Remember first one to compare.
  markerCharCode = state.src.charCodeAt(posAfterMarker - 1);

  // For validation mode we can terminate immediately
  if (silent) { return true; }

  // Start list
  listTokIdx = state.tokens.length;

  if (isOrdered) {
    token       = state.push('ordered_list_open', 'ol', 1);
    if (markerValue !== 1) {
      token.attrs = [ [ 'start', markerValue ] ];
    }

  } else {
    token       = state.push('bullet_list_open', 'ul', 1);
  }

  token.map    = listLines = [ startLine, 0 ];
  token.markup = String.fromCharCode(markerCharCode);

  //
  // Iterate list items
  //

  nextLine = startLine;
  prevEmptyEnd = false;
  terminatorRules = state.md.block.ruler.getRules('list');

  oldParentType = state.parentType;
  state.parentType = 'list';

  while (nextLine < endLine) {
    pos = posAfterMarker;
    max = state.eMarks[nextLine];

    initial = offset = state.sCount[nextLine] + posAfterMarker - (state.bMarks[startLine] + state.tShift[startLine]);

    while (pos < max) {
      ch = state.src.charCodeAt(pos);

      if (ch === 0x09) {
        offset += 4 - (offset + state.bsCount[nextLine]) % 4;
      } else if (ch === 0x20) {
        offset++;
      } else {
        break;
      }

      pos++;
    }

    contentStart = pos;

    if (contentStart >= max) {
      // trimming space in "-    \n  3" case, indent is 1 here
      indentAfterMarker = 1;
    } else {
      indentAfterMarker = offset - initial;
    }

    // If we have more than 4 spaces, the indent is 1
    // (the rest is just indented code block)
    if (indentAfterMarker > 4) { indentAfterMarker = 1; }

    // "  -  test"
    //  ^^^^^ - calculating total length of this thing
    indent = initial + indentAfterMarker;

    // Run subparser & write tokens
    token        = state.push('list_item_open', 'li', 1);
    token.markup = String.fromCharCode(markerCharCode);
    token.map    = itemLines = [ startLine, 0 ];

    oldIndent = state.blkIndent;
    oldTight = state.tight;
    oldTShift = state.tShift[startLine];
    oldLIndent = state.sCount[startLine];
    state.blkIndent = indent;
    state.tight = true;
    state.tShift[startLine] = contentStart - state.bMarks[startLine];
    state.sCount[startLine] = offset;

    if (contentStart >= max && state.isEmpty(startLine + 1)) {
      // workaround for this case
      // (list item is empty, list terminates before "foo"):
      // ~~~~~~~~
      //   -
      //
      //     foo
      // ~~~~~~~~
      state.line = Math.min(state.line + 2, endLine);
    } else {
      state.md.block.tokenize(state, startLine, endLine, true);
    }

    // If any of list item is tight, mark list as tight
    if (!state.tight || prevEmptyEnd) {
      tight = false;
    }
    // Item become loose if finish with empty line,
    // but we should filter last element, because it means list finish
    prevEmptyEnd = (state.line - startLine) > 1 && state.isEmpty(state.line - 1);

    state.blkIndent = oldIndent;
    state.tShift[startLine] = oldTShift;
    state.sCount[startLine] = oldLIndent;
    state.tight = oldTight;

    token        = state.push('list_item_close', 'li', -1);
    token.markup = String.fromCharCode(markerCharCode);

    nextLine = startLine = state.line;
    itemLines[1] = nextLine;
    contentStart = state.bMarks[startLine];

    if (nextLine >= endLine) { break; }

    //
    // Try to check if list is terminated or continued.
    //
    if (state.sCount[nextLine] < state.blkIndent) { break; }

    // fail if terminating block found
    terminate = false;
    for (i = 0, l = terminatorRules.length; i < l; i++) {
      if (terminatorRules[i](state, nextLine, endLine, true)) {
        terminate = true;
        break;
      }
    }
    if (terminate) { break; }

    // fail if list has another type
    if (isOrdered) {
      posAfterMarker = skipOrderedListMarker(state, nextLine);
      if (posAfterMarker < 0) { break; }
    } else {
      posAfterMarker = skipBulletListMarker(state, nextLine);
      if (posAfterMarker < 0) { break; }
    }

    if (markerCharCode !== state.src.charCodeAt(posAfterMarker - 1)) { break; }
  }

  // Finalize list
  if (isOrdered) {
    token = state.push('ordered_list_close', 'ol', -1);
  } else {
    token = state.push('bullet_list_close', 'ul', -1);
  }
  token.markup = String.fromCharCode(markerCharCode);

  listLines[1] = nextLine;
  state.line = nextLine;

  state.parentType = oldParentType;

  // mark paragraphs tight if needed
  if (tight) {
    markTightParagraphs(state, listTokIdx);
  }

  return true;
};

});
___scope___.file("lib/rules_block/reference.js", function(exports, require, module, __filename, __dirname){

'use strict';


var normalizeReference   = require('../common/utils').normalizeReference;
var isSpace              = require('../common/utils').isSpace;


module.exports = function reference(state, startLine, _endLine, silent) {
  var ch,
      destEndPos,
      destEndLineNo,
      endLine,
      href,
      i,
      l,
      label,
      labelEnd,
      oldParentType,
      res,
      start,
      str,
      terminate,
      terminatorRules,
      title,
      lines = 0,
      pos = state.bMarks[startLine] + state.tShift[startLine],
      max = state.eMarks[startLine],
      nextLine = startLine + 1;

  // if it's indented more than 3 spaces, it should be a code block
  if (state.sCount[startLine] - state.blkIndent >= 4) { return false; }

  if (state.src.charCodeAt(pos) !== 0x5B/* [ */) { return false; }

  // Simple check to quickly interrupt scan on [link](url) at the start of line.
  // Can be useful on practice: https://github.com/markdown-it/markdown-it/issues/54
  while (++pos < max) {
    if (state.src.charCodeAt(pos) === 0x5D /* ] */ &&
        state.src.charCodeAt(pos - 1) !== 0x5C/* \ */) {
      if (pos + 1 === max) { return false; }
      if (state.src.charCodeAt(pos + 1) !== 0x3A/* : */) { return false; }
      break;
    }
  }

  endLine = state.lineMax;

  // jump line-by-line until empty one or EOF
  terminatorRules = state.md.block.ruler.getRules('reference');

  oldParentType = state.parentType;
  state.parentType = 'reference';

  for (; nextLine < endLine && !state.isEmpty(nextLine); nextLine++) {
    // this would be a code block normally, but after paragraph
    // it's considered a lazy continuation regardless of what's there
    if (state.sCount[nextLine] - state.blkIndent > 3) { continue; }

    // quirk for blockquotes, this line should already be checked by that rule
    if (state.sCount[nextLine] < 0) { continue; }

    // Some tags can terminate paragraph without empty line.
    terminate = false;
    for (i = 0, l = terminatorRules.length; i < l; i++) {
      if (terminatorRules[i](state, nextLine, endLine, true)) {
        terminate = true;
        break;
      }
    }
    if (terminate) { break; }
  }

  str = state.getLines(startLine, nextLine, state.blkIndent, false).trim();
  max = str.length;

  for (pos = 1; pos < max; pos++) {
    ch = str.charCodeAt(pos);
    if (ch === 0x5B /* [ */) {
      return false;
    } else if (ch === 0x5D /* ] */) {
      labelEnd = pos;
      break;
    } else if (ch === 0x0A /* \n */) {
      lines++;
    } else if (ch === 0x5C /* \ */) {
      pos++;
      if (pos < max && str.charCodeAt(pos) === 0x0A) {
        lines++;
      }
    }
  }

  if (labelEnd < 0 || str.charCodeAt(labelEnd + 1) !== 0x3A/* : */) { return false; }

  // [label]:   destination   'title'
  //         ^^^ skip optional whitespace here
  for (pos = labelEnd + 2; pos < max; pos++) {
    ch = str.charCodeAt(pos);
    if (ch === 0x0A) {
      lines++;
    } else if (isSpace(ch)) {
      /*eslint no-empty:0*/
    } else {
      break;
    }
  }

  // [label]:   destination   'title'
  //            ^^^^^^^^^^^ parse this
  res = state.md.helpers.parseLinkDestination(str, pos, max);
  if (!res.ok) { return false; }

  href = state.md.normalizeLink(res.str);
  if (!state.md.validateLink(href)) { return false; }

  pos = res.pos;
  lines += res.lines;

  // save cursor state, we could require to rollback later
  destEndPos = pos;
  destEndLineNo = lines;

  // [label]:   destination   'title'
  //                       ^^^ skipping those spaces
  start = pos;
  for (; pos < max; pos++) {
    ch = str.charCodeAt(pos);
    if (ch === 0x0A) {
      lines++;
    } else if (isSpace(ch)) {
      /*eslint no-empty:0*/
    } else {
      break;
    }
  }

  // [label]:   destination   'title'
  //                          ^^^^^^^ parse this
  res = state.md.helpers.parseLinkTitle(str, pos, max);
  if (pos < max && start !== pos && res.ok) {
    title = res.str;
    pos = res.pos;
    lines += res.lines;
  } else {
    title = '';
    pos = destEndPos;
    lines = destEndLineNo;
  }

  // skip trailing spaces until the rest of the line
  while (pos < max) {
    ch = str.charCodeAt(pos);
    if (!isSpace(ch)) { break; }
    pos++;
  }

  if (pos < max && str.charCodeAt(pos) !== 0x0A) {
    if (title) {
      // garbage at the end of the line after title,
      // but it could still be a valid reference if we roll back
      title = '';
      pos = destEndPos;
      lines = destEndLineNo;
      while (pos < max) {
        ch = str.charCodeAt(pos);
        if (!isSpace(ch)) { break; }
        pos++;
      }
    }
  }

  if (pos < max && str.charCodeAt(pos) !== 0x0A) {
    // garbage at the end of the line
    return false;
  }

  label = normalizeReference(str.slice(1, labelEnd));
  if (!label) {
    // CommonMark 0.20 disallows empty labels
    return false;
  }

  // Reference can not terminate anything. This check is for safety only.
  /*istanbul ignore if*/
  if (silent) { return true; }

  if (typeof state.env.references === 'undefined') {
    state.env.references = {};
  }
  if (typeof state.env.references[label] === 'undefined') {
    state.env.references[label] = { title: title, href: href };
  }

  state.parentType = oldParentType;

  state.line = startLine + lines + 1;
  return true;
};

});
___scope___.file("lib/rules_block/heading.js", function(exports, require, module, __filename, __dirname){

// heading (#, ##, ...)

'use strict';

var isSpace = require('../common/utils').isSpace;


module.exports = function heading(state, startLine, endLine, silent) {
  var ch, level, tmp, token,
      pos = state.bMarks[startLine] + state.tShift[startLine],
      max = state.eMarks[startLine];

  // if it's indented more than 3 spaces, it should be a code block
  if (state.sCount[startLine] - state.blkIndent >= 4) { return false; }

  ch  = state.src.charCodeAt(pos);

  if (ch !== 0x23/* # */ || pos >= max) { return false; }

  // count heading level
  level = 1;
  ch = state.src.charCodeAt(++pos);
  while (ch === 0x23/* # */ && pos < max && level <= 6) {
    level++;
    ch = state.src.charCodeAt(++pos);
  }

  if (level > 6 || (pos < max && !isSpace(ch))) { return false; }

  if (silent) { return true; }

  // Let's cut tails like '    ###  ' from the end of string

  max = state.skipSpacesBack(max, pos);
  tmp = state.skipCharsBack(max, 0x23, pos); // #
  if (tmp > pos && isSpace(state.src.charCodeAt(tmp - 1))) {
    max = tmp;
  }

  state.line = startLine + 1;

  token        = state.push('heading_open', 'h' + String(level), 1);
  token.markup = '########'.slice(0, level);
  token.map    = [ startLine, state.line ];

  token          = state.push('inline', '', 0);
  token.content  = state.src.slice(pos, max).trim();
  token.map      = [ startLine, state.line ];
  token.children = [];

  token        = state.push('heading_close', 'h' + String(level), -1);
  token.markup = '########'.slice(0, level);

  return true;
};

});
___scope___.file("lib/rules_block/lheading.js", function(exports, require, module, __filename, __dirname){

// lheading (---, ===)

'use strict';


module.exports = function lheading(state, startLine, endLine/*, silent*/) {
  var content, terminate, i, l, token, pos, max, level, marker,
      nextLine = startLine + 1, oldParentType,
      terminatorRules = state.md.block.ruler.getRules('paragraph');

  // if it's indented more than 3 spaces, it should be a code block
  if (state.sCount[startLine] - state.blkIndent >= 4) { return false; }

  oldParentType = state.parentType;
  state.parentType = 'paragraph'; // use paragraph to match terminatorRules

  // jump line-by-line until empty one or EOF
  for (; nextLine < endLine && !state.isEmpty(nextLine); nextLine++) {
    // this would be a code block normally, but after paragraph
    // it's considered a lazy continuation regardless of what's there
    if (state.sCount[nextLine] - state.blkIndent > 3) { continue; }

    //
    // Check for underline in setext header
    //
    if (state.sCount[nextLine] >= state.blkIndent) {
      pos = state.bMarks[nextLine] + state.tShift[nextLine];
      max = state.eMarks[nextLine];

      if (pos < max) {
        marker = state.src.charCodeAt(pos);

        if (marker === 0x2D/* - */ || marker === 0x3D/* = */) {
          pos = state.skipChars(pos, marker);
          pos = state.skipSpaces(pos);

          if (pos >= max) {
            level = (marker === 0x3D/* = */ ? 1 : 2);
            break;
          }
        }
      }
    }

    // quirk for blockquotes, this line should already be checked by that rule
    if (state.sCount[nextLine] < 0) { continue; }

    // Some tags can terminate paragraph without empty line.
    terminate = false;
    for (i = 0, l = terminatorRules.length; i < l; i++) {
      if (terminatorRules[i](state, nextLine, endLine, true)) {
        terminate = true;
        break;
      }
    }
    if (terminate) { break; }
  }

  if (!level) {
    // Didn't find valid underline
    return false;
  }

  content = state.getLines(startLine, nextLine, state.blkIndent, false).trim();

  state.line = nextLine + 1;

  token          = state.push('heading_open', 'h' + String(level), 1);
  token.markup   = String.fromCharCode(marker);
  token.map      = [ startLine, state.line ];

  token          = state.push('inline', '', 0);
  token.content  = content;
  token.map      = [ startLine, state.line - 1 ];
  token.children = [];

  token          = state.push('heading_close', 'h' + String(level), -1);
  token.markup   = String.fromCharCode(marker);

  state.parentType = oldParentType;

  return true;
};

});
___scope___.file("lib/rules_block/html_block.js", function(exports, require, module, __filename, __dirname){

// HTML block

'use strict';


var block_names = require('../common/html_blocks');
var HTML_OPEN_CLOSE_TAG_RE = require('../common/html_re').HTML_OPEN_CLOSE_TAG_RE;

// An array of opening and corresponding closing sequences for html tags,
// last argument defines whether it can terminate a paragraph or not
//
var HTML_SEQUENCES = [
  [ /^<(script|pre|style)(?=(\s|>|$))/i, /<\/(script|pre|style)>/i, true ],
  [ /^<!--/,        /-->/,   true ],
  [ /^<\?/,         /\?>/,   true ],
  [ /^<![A-Z]/,     />/,     true ],
  [ /^<!\[CDATA\[/, /\]\]>/, true ],
  [ new RegExp('^</?(' + block_names.join('|') + ')(?=(\\s|/?>|$))', 'i'), /^$/, true ],
  [ new RegExp(HTML_OPEN_CLOSE_TAG_RE.source + '\\s*$'),  /^$/, false ]
];


module.exports = function html_block(state, startLine, endLine, silent) {
  var i, nextLine, token, lineText,
      pos = state.bMarks[startLine] + state.tShift[startLine],
      max = state.eMarks[startLine];

  // if it's indented more than 3 spaces, it should be a code block
  if (state.sCount[startLine] - state.blkIndent >= 4) { return false; }

  if (!state.md.options.html) { return false; }

  if (state.src.charCodeAt(pos) !== 0x3C/* < */) { return false; }

  lineText = state.src.slice(pos, max);

  for (i = 0; i < HTML_SEQUENCES.length; i++) {
    if (HTML_SEQUENCES[i][0].test(lineText)) { break; }
  }

  if (i === HTML_SEQUENCES.length) { return false; }

  if (silent) {
    // true if this sequence can be a terminator, false otherwise
    return HTML_SEQUENCES[i][2];
  }

  nextLine = startLine + 1;

  // If we are here - we detected HTML block.
  // Let's roll down till block end.
  if (!HTML_SEQUENCES[i][1].test(lineText)) {
    for (; nextLine < endLine; nextLine++) {
      if (state.sCount[nextLine] < state.blkIndent) { break; }

      pos = state.bMarks[nextLine] + state.tShift[nextLine];
      max = state.eMarks[nextLine];
      lineText = state.src.slice(pos, max);

      if (HTML_SEQUENCES[i][1].test(lineText)) {
        if (lineText.length !== 0) { nextLine++; }
        break;
      }
    }
  }

  state.line = nextLine;

  token         = state.push('html_block', '', 0);
  token.map     = [ startLine, nextLine ];
  token.content = state.getLines(startLine, nextLine, state.blkIndent, true);

  return true;
};

});
___scope___.file("lib/common/html_blocks.js", function(exports, require, module, __filename, __dirname){

// List of valid html blocks names, accorting to commonmark spec
// http://jgm.github.io/CommonMark/spec.html#html-blocks

'use strict';


module.exports = [
  'address',
  'article',
  'aside',
  'base',
  'basefont',
  'blockquote',
  'body',
  'caption',
  'center',
  'col',
  'colgroup',
  'dd',
  'details',
  'dialog',
  'dir',
  'div',
  'dl',
  'dt',
  'fieldset',
  'figcaption',
  'figure',
  'footer',
  'form',
  'frame',
  'frameset',
  'h1',
  'h2',
  'h3',
  'h4',
  'h5',
  'h6',
  'head',
  'header',
  'hr',
  'html',
  'iframe',
  'legend',
  'li',
  'link',
  'main',
  'menu',
  'menuitem',
  'meta',
  'nav',
  'noframes',
  'ol',
  'optgroup',
  'option',
  'p',
  'param',
  'section',
  'source',
  'summary',
  'table',
  'tbody',
  'td',
  'tfoot',
  'th',
  'thead',
  'title',
  'tr',
  'track',
  'ul'
];

});
___scope___.file("lib/common/html_re.js", function(exports, require, module, __filename, __dirname){

// Regexps to match html elements

'use strict';

var attr_name     = '[a-zA-Z_:][a-zA-Z0-9:._-]*';

var unquoted      = '[^"\'=<>`\\x00-\\x20]+';
var single_quoted = "'[^']*'";
var double_quoted = '"[^"]*"';

var attr_value  = '(?:' + unquoted + '|' + single_quoted + '|' + double_quoted + ')';

var attribute   = '(?:\\s+' + attr_name + '(?:\\s*=\\s*' + attr_value + ')?)';

var open_tag    = '<[A-Za-z][A-Za-z0-9\\-]*' + attribute + '*\\s*\\/?>';

var close_tag   = '<\\/[A-Za-z][A-Za-z0-9\\-]*\\s*>';
var comment     = '<!---->|<!--(?:-?[^>-])(?:-?[^-])*-->';
var processing  = '<[?].*?[?]>';
var declaration = '<![A-Z]+\\s+[^>]*>';
var cdata       = '<!\\[CDATA\\[[\\s\\S]*?\\]\\]>';

var HTML_TAG_RE = new RegExp('^(?:' + open_tag + '|' + close_tag + '|' + comment +
                        '|' + processing + '|' + declaration + '|' + cdata + ')');
var HTML_OPEN_CLOSE_TAG_RE = new RegExp('^(?:' + open_tag + '|' + close_tag + ')');

module.exports.HTML_TAG_RE = HTML_TAG_RE;
module.exports.HTML_OPEN_CLOSE_TAG_RE = HTML_OPEN_CLOSE_TAG_RE;

});
___scope___.file("lib/rules_block/paragraph.js", function(exports, require, module, __filename, __dirname){

// Paragraph

'use strict';


module.exports = function paragraph(state, startLine/*, endLine*/) {
  var content, terminate, i, l, token, oldParentType,
      nextLine = startLine + 1,
      terminatorRules = state.md.block.ruler.getRules('paragraph'),
      endLine = state.lineMax;

  oldParentType = state.parentType;
  state.parentType = 'paragraph';

  // jump line-by-line until empty one or EOF
  for (; nextLine < endLine && !state.isEmpty(nextLine); nextLine++) {
    // this would be a code block normally, but after paragraph
    // it's considered a lazy continuation regardless of what's there
    if (state.sCount[nextLine] - state.blkIndent > 3) { continue; }

    // quirk for blockquotes, this line should already be checked by that rule
    if (state.sCount[nextLine] < 0) { continue; }

    // Some tags can terminate paragraph without empty line.
    terminate = false;
    for (i = 0, l = terminatorRules.length; i < l; i++) {
      if (terminatorRules[i](state, nextLine, endLine, true)) {
        terminate = true;
        break;
      }
    }
    if (terminate) { break; }
  }

  content = state.getLines(startLine, nextLine, state.blkIndent, false).trim();

  state.line = nextLine;

  token          = state.push('paragraph_open', 'p', 1);
  token.map      = [ startLine, state.line ];

  token          = state.push('inline', '', 0);
  token.content  = content;
  token.map      = [ startLine, state.line ];
  token.children = [];

  token          = state.push('paragraph_close', 'p', -1);

  state.parentType = oldParentType;

  return true;
};

});
___scope___.file("lib/rules_block/state_block.js", function(exports, require, module, __filename, __dirname){

// Parser state class

'use strict';

var Token = require('../token');
var isSpace = require('../common/utils').isSpace;


function StateBlock(src, md, env, tokens) {
  var ch, s, start, pos, len, indent, offset, indent_found;

  this.src = src;

  // link to parser instance
  this.md     = md;

  this.env = env;

  //
  // Internal state vartiables
  //

  this.tokens = tokens;

  this.bMarks = [];  // line begin offsets for fast jumps
  this.eMarks = [];  // line end offsets for fast jumps
  this.tShift = [];  // offsets of the first non-space characters (tabs not expanded)
  this.sCount = [];  // indents for each line (tabs expanded)

  // An amount of virtual spaces (tabs expanded) between beginning
  // of each line (bMarks) and real beginning of that line.
  //
  // It exists only as a hack because blockquotes override bMarks
  // losing information in the process.
  //
  // It's used only when expanding tabs, you can think about it as
  // an initial tab length, e.g. bsCount=21 applied to string `\t123`
  // means first tab should be expanded to 4-21%4 === 3 spaces.
  //
  this.bsCount = [];

  // block parser variables
  this.blkIndent  = 0; // required block content indent
                       // (for example, if we are in list)
  this.line       = 0; // line index in src
  this.lineMax    = 0; // lines count
  this.tight      = false;  // loose/tight mode for lists
  this.ddIndent   = -1; // indent of the current dd block (-1 if there isn't any)

  // can be 'blockquote', 'list', 'root', 'paragraph' or 'reference'
  // used in lists to determine if they interrupt a paragraph
  this.parentType = 'root';

  this.level = 0;

  // renderer
  this.result = '';

  // Create caches
  // Generate markers.
  s = this.src;
  indent_found = false;

  for (start = pos = indent = offset = 0, len = s.length; pos < len; pos++) {
    ch = s.charCodeAt(pos);

    if (!indent_found) {
      if (isSpace(ch)) {
        indent++;

        if (ch === 0x09) {
          offset += 4 - offset % 4;
        } else {
          offset++;
        }
        continue;
      } else {
        indent_found = true;
      }
    }

    if (ch === 0x0A || pos === len - 1) {
      if (ch !== 0x0A) { pos++; }
      this.bMarks.push(start);
      this.eMarks.push(pos);
      this.tShift.push(indent);
      this.sCount.push(offset);
      this.bsCount.push(0);

      indent_found = false;
      indent = 0;
      offset = 0;
      start = pos + 1;
    }
  }

  // Push fake entry to simplify cache bounds checks
  this.bMarks.push(s.length);
  this.eMarks.push(s.length);
  this.tShift.push(0);
  this.sCount.push(0);
  this.bsCount.push(0);

  this.lineMax = this.bMarks.length - 1; // don't count last fake line
}

// Push new token to "stream".
//
StateBlock.prototype.push = function (type, tag, nesting) {
  var token = new Token(type, tag, nesting);
  token.block = true;

  if (nesting < 0) { this.level--; }
  token.level = this.level;
  if (nesting > 0) { this.level++; }

  this.tokens.push(token);
  return token;
};

StateBlock.prototype.isEmpty = function isEmpty(line) {
  return this.bMarks[line] + this.tShift[line] >= this.eMarks[line];
};

StateBlock.prototype.skipEmptyLines = function skipEmptyLines(from) {
  for (var max = this.lineMax; from < max; from++) {
    if (this.bMarks[from] + this.tShift[from] < this.eMarks[from]) {
      break;
    }
  }
  return from;
};

// Skip spaces from given position.
StateBlock.prototype.skipSpaces = function skipSpaces(pos) {
  var ch;

  for (var max = this.src.length; pos < max; pos++) {
    ch = this.src.charCodeAt(pos);
    if (!isSpace(ch)) { break; }
  }
  return pos;
};

// Skip spaces from given position in reverse.
StateBlock.prototype.skipSpacesBack = function skipSpacesBack(pos, min) {
  if (pos <= min) { return pos; }

  while (pos > min) {
    if (!isSpace(this.src.charCodeAt(--pos))) { return pos + 1; }
  }
  return pos;
};

// Skip char codes from given position
StateBlock.prototype.skipChars = function skipChars(pos, code) {
  for (var max = this.src.length; pos < max; pos++) {
    if (this.src.charCodeAt(pos) !== code) { break; }
  }
  return pos;
};

// Skip char codes reverse from given position - 1
StateBlock.prototype.skipCharsBack = function skipCharsBack(pos, code, min) {
  if (pos <= min) { return pos; }

  while (pos > min) {
    if (code !== this.src.charCodeAt(--pos)) { return pos + 1; }
  }
  return pos;
};

// cut lines range from source.
StateBlock.prototype.getLines = function getLines(begin, end, indent, keepLastLF) {
  var i, lineIndent, ch, first, last, queue, lineStart,
      line = begin;

  if (begin >= end) {
    return '';
  }

  queue = new Array(end - begin);

  for (i = 0; line < end; line++, i++) {
    lineIndent = 0;
    lineStart = first = this.bMarks[line];

    if (line + 1 < end || keepLastLF) {
      // No need for bounds check because we have fake entry on tail.
      last = this.eMarks[line] + 1;
    } else {
      last = this.eMarks[line];
    }

    while (first < last && lineIndent < indent) {
      ch = this.src.charCodeAt(first);

      if (isSpace(ch)) {
        if (ch === 0x09) {
          lineIndent += 4 - (lineIndent + this.bsCount[line]) % 4;
        } else {
          lineIndent++;
        }
      } else if (first - lineStart < this.tShift[line]) {
        // patched tShift masked characters to look like spaces (blockquotes, list markers)
        lineIndent++;
      } else {
        break;
      }

      first++;
    }

    if (lineIndent > indent) {
      // partially expanding tabs in code blocks, e.g '\t\tfoobar'
      // with indent=2 becomes '  \tfoobar'
      queue[i] = new Array(lineIndent - indent + 1).join(' ') + this.src.slice(first, last);
    } else {
      queue[i] = this.src.slice(first, last);
    }
  }

  return queue.join('');
};

// re-export Token class to use in block rules
StateBlock.prototype.Token = Token;


module.exports = StateBlock;

});
___scope___.file("lib/parser_inline.js", function(exports, require, module, __filename, __dirname){

/** internal
 * class ParserInline
 *
 * Tokenizes paragraph content.
 **/
'use strict';


var Ruler           = require('./ruler');


////////////////////////////////////////////////////////////////////////////////
// Parser rules

var _rules = [
  [ 'text',            require('./rules_inline/text') ],
  [ 'newline',         require('./rules_inline/newline') ],
  [ 'escape',          require('./rules_inline/escape') ],
  [ 'backticks',       require('./rules_inline/backticks') ],
  [ 'strikethrough',   require('./rules_inline/strikethrough').tokenize ],
  [ 'emphasis',        require('./rules_inline/emphasis').tokenize ],
  [ 'link',            require('./rules_inline/link') ],
  [ 'image',           require('./rules_inline/image') ],
  [ 'autolink',        require('./rules_inline/autolink') ],
  [ 'html_inline',     require('./rules_inline/html_inline') ],
  [ 'entity',          require('./rules_inline/entity') ]
];

var _rules2 = [
  [ 'balance_pairs',   require('./rules_inline/balance_pairs') ],
  [ 'strikethrough',   require('./rules_inline/strikethrough').postProcess ],
  [ 'emphasis',        require('./rules_inline/emphasis').postProcess ],
  [ 'text_collapse',   require('./rules_inline/text_collapse') ]
];


/**
 * new ParserInline()
 **/
function ParserInline() {
  var i;

  /**
   * ParserInline#ruler -> Ruler
   *
   * [[Ruler]] instance. Keep configuration of inline rules.
   **/
  this.ruler = new Ruler();

  for (i = 0; i < _rules.length; i++) {
    this.ruler.push(_rules[i][0], _rules[i][1]);
  }

  /**
   * ParserInline#ruler2 -> Ruler
   *
   * [[Ruler]] instance. Second ruler used for post-processing
   * (e.g. in emphasis-like rules).
   **/
  this.ruler2 = new Ruler();

  for (i = 0; i < _rules2.length; i++) {
    this.ruler2.push(_rules2[i][0], _rules2[i][1]);
  }
}


// Skip single token by running all rules in validation mode;
// returns `true` if any rule reported success
//
ParserInline.prototype.skipToken = function (state) {
  var ok, i, pos = state.pos,
      rules = this.ruler.getRules(''),
      len = rules.length,
      maxNesting = state.md.options.maxNesting,
      cache = state.cache;


  if (typeof cache[pos] !== 'undefined') {
    state.pos = cache[pos];
    return;
  }

  if (state.level < maxNesting) {
    for (i = 0; i < len; i++) {
      // Increment state.level and decrement it later to limit recursion.
      // It's harmless to do here, because no tokens are created. But ideally,
      // we'd need a separate private state variable for this purpose.
      //
      state.level++;
      ok = rules[i](state, true);
      state.level--;

      if (ok) { break; }
    }
  } else {
    // Too much nesting, just skip until the end of the paragraph.
    //
    // NOTE: this will cause links to behave incorrectly in the following case,
    //       when an amount of `[` is exactly equal to `maxNesting + 1`:
    //
    //       [[[[[[[[[[[[[[[[[[[[[foo]()
    //
    // TODO: remove this workaround when CM standard will allow nested links
    //       (we can replace it by preventing links from being parsed in
    //       validation mode)
    //
    state.pos = state.posMax;
  }

  if (!ok) { state.pos++; }
  cache[pos] = state.pos;
};


// Generate tokens for input range
//
ParserInline.prototype.tokenize = function (state) {
  var ok, i,
      rules = this.ruler.getRules(''),
      len = rules.length,
      end = state.posMax,
      maxNesting = state.md.options.maxNesting;

  while (state.pos < end) {
    // Try all possible rules.
    // On success, rule should:
    //
    // - update `state.pos`
    // - update `state.tokens`
    // - return true

    if (state.level < maxNesting) {
      for (i = 0; i < len; i++) {
        ok = rules[i](state, false);
        if (ok) { break; }
      }
    }

    if (ok) {
      if (state.pos >= end) { break; }
      continue;
    }

    state.pending += state.src[state.pos++];
  }

  if (state.pending) {
    state.pushPending();
  }
};


/**
 * ParserInline.parse(str, md, env, outTokens)
 *
 * Process input string and push inline tokens into `outTokens`
 **/
ParserInline.prototype.parse = function (str, md, env, outTokens) {
  var i, rules, len;
  var state = new this.State(str, md, env, outTokens);

  this.tokenize(state);

  rules = this.ruler2.getRules('');
  len = rules.length;

  for (i = 0; i < len; i++) {
    rules[i](state);
  }
};


ParserInline.prototype.State = require('./rules_inline/state_inline');


module.exports = ParserInline;

});
___scope___.file("lib/rules_inline/text.js", function(exports, require, module, __filename, __dirname){

// Skip text characters for text token, place those to pending buffer
// and increment current pos

'use strict';


// Rule to skip pure text
// '{}$%@~+=:' reserved for extentions

// !, ", #, $, %, &, ', (, ), *, +, ,, -, ., /, :, ;, <, =, >, ?, @, [, \, ], ^, _, `, {, |, }, or ~

// !!!! Don't confuse with "Markdown ASCII Punctuation" chars
// http://spec.commonmark.org/0.15/#ascii-punctuation-character
function isTerminatorChar(ch) {
  switch (ch) {
    case 0x0A/* \n */:
    case 0x21/* ! */:
    case 0x23/* # */:
    case 0x24/* $ */:
    case 0x25/* % */:
    case 0x26/* & */:
    case 0x2A/* * */:
    case 0x2B/* + */:
    case 0x2D/* - */:
    case 0x3A/* : */:
    case 0x3C/* < */:
    case 0x3D/* = */:
    case 0x3E/* > */:
    case 0x40/* @ */:
    case 0x5B/* [ */:
    case 0x5C/* \ */:
    case 0x5D/* ] */:
    case 0x5E/* ^ */:
    case 0x5F/* _ */:
    case 0x60/* ` */:
    case 0x7B/* { */:
    case 0x7D/* } */:
    case 0x7E/* ~ */:
      return true;
    default:
      return false;
  }
}

module.exports = function text(state, silent) {
  var pos = state.pos;

  while (pos < state.posMax && !isTerminatorChar(state.src.charCodeAt(pos))) {
    pos++;
  }

  if (pos === state.pos) { return false; }

  if (!silent) { state.pending += state.src.slice(state.pos, pos); }

  state.pos = pos;

  return true;
};

// Alternative implementation, for memory.
//
// It costs 10% of performance, but allows extend terminators list, if place it
// to `ParcerInline` property. Probably, will switch to it sometime, such
// flexibility required.

/*
var TERMINATOR_RE = /[\n!#$%&*+\-:<=>@[\\\]^_`{}~]/;

module.exports = function text(state, silent) {
  var pos = state.pos,
      idx = state.src.slice(pos).search(TERMINATOR_RE);

  // first char is terminator -> empty text
  if (idx === 0) { return false; }

  // no terminator -> text till end of string
  if (idx < 0) {
    if (!silent) { state.pending += state.src.slice(pos); }
    state.pos = state.src.length;
    return true;
  }

  if (!silent) { state.pending += state.src.slice(pos, pos + idx); }

  state.pos += idx;

  return true;
};*/

});
___scope___.file("lib/rules_inline/newline.js", function(exports, require, module, __filename, __dirname){

// Proceess '\n'

'use strict';

var isSpace = require('../common/utils').isSpace;


module.exports = function newline(state, silent) {
  var pmax, max, pos = state.pos;

  if (state.src.charCodeAt(pos) !== 0x0A/* \n */) { return false; }

  pmax = state.pending.length - 1;
  max = state.posMax;

  // '  \n' -> hardbreak
  // Lookup in pending chars is bad practice! Don't copy to other rules!
  // Pending string is stored in concat mode, indexed lookups will cause
  // convertion to flat mode.
  if (!silent) {
    if (pmax >= 0 && state.pending.charCodeAt(pmax) === 0x20) {
      if (pmax >= 1 && state.pending.charCodeAt(pmax - 1) === 0x20) {
        state.pending = state.pending.replace(/ +$/, '');
        state.push('hardbreak', 'br', 0);
      } else {
        state.pending = state.pending.slice(0, -1);
        state.push('softbreak', 'br', 0);
      }

    } else {
      state.push('softbreak', 'br', 0);
    }
  }

  pos++;

  // skip heading spaces for next line
  while (pos < max && isSpace(state.src.charCodeAt(pos))) { pos++; }

  state.pos = pos;
  return true;
};

});
___scope___.file("lib/rules_inline/escape.js", function(exports, require, module, __filename, __dirname){

// Process escaped chars and hardbreaks

'use strict';

var isSpace = require('../common/utils').isSpace;

var ESCAPED = [];

for (var i = 0; i < 256; i++) { ESCAPED.push(0); }

'\\!"#$%&\'()*+,./:;<=>?@[]^_`{|}~-'
  .split('').forEach(function (ch) { ESCAPED[ch.charCodeAt(0)] = 1; });


module.exports = function escape(state, silent) {
  var ch, pos = state.pos, max = state.posMax;

  if (state.src.charCodeAt(pos) !== 0x5C/* \ */) { return false; }

  pos++;

  if (pos < max) {
    ch = state.src.charCodeAt(pos);

    if (ch < 256 && ESCAPED[ch] !== 0) {
      if (!silent) { state.pending += state.src[pos]; }
      state.pos += 2;
      return true;
    }

    if (ch === 0x0A) {
      if (!silent) {
        state.push('hardbreak', 'br', 0);
      }

      pos++;
      // skip leading whitespaces from next line
      while (pos < max) {
        ch = state.src.charCodeAt(pos);
        if (!isSpace(ch)) { break; }
        pos++;
      }

      state.pos = pos;
      return true;
    }
  }

  if (!silent) { state.pending += '\\'; }
  state.pos++;
  return true;
};

});
___scope___.file("lib/rules_inline/backticks.js", function(exports, require, module, __filename, __dirname){

// Parse backticks

'use strict';

module.exports = function backtick(state, silent) {
  var start, max, marker, matchStart, matchEnd, token,
      pos = state.pos,
      ch = state.src.charCodeAt(pos);

  if (ch !== 0x60/* ` */) { return false; }

  start = pos;
  pos++;
  max = state.posMax;

  while (pos < max && state.src.charCodeAt(pos) === 0x60/* ` */) { pos++; }

  marker = state.src.slice(start, pos);

  matchStart = matchEnd = pos;

  while ((matchStart = state.src.indexOf('`', matchEnd)) !== -1) {
    matchEnd = matchStart + 1;

    while (matchEnd < max && state.src.charCodeAt(matchEnd) === 0x60/* ` */) { matchEnd++; }

    if (matchEnd - matchStart === marker.length) {
      if (!silent) {
        token         = state.push('code_inline', 'code', 0);
        token.markup  = marker;
        token.content = state.src.slice(pos, matchStart)
                                 .replace(/[ \n]+/g, ' ')
                                 .trim();
      }
      state.pos = matchEnd;
      return true;
    }
  }

  if (!silent) { state.pending += marker; }
  state.pos += marker.length;
  return true;
};

});
___scope___.file("lib/rules_inline/strikethrough.js", function(exports, require, module, __filename, __dirname){

// ~~strike through~~
//
'use strict';


// Insert each marker as a separate text token, and add it to delimiter list
//
module.exports.tokenize = function strikethrough(state, silent) {
  var i, scanned, token, len, ch,
      start = state.pos,
      marker = state.src.charCodeAt(start);

  if (silent) { return false; }

  if (marker !== 0x7E/* ~ */) { return false; }

  scanned = state.scanDelims(state.pos, true);
  len = scanned.length;
  ch = String.fromCharCode(marker);

  if (len < 2) { return false; }

  if (len % 2) {
    token         = state.push('text', '', 0);
    token.content = ch;
    len--;
  }

  for (i = 0; i < len; i += 2) {
    token         = state.push('text', '', 0);
    token.content = ch + ch;

    state.delimiters.push({
      marker: marker,
      jump:   i,
      token:  state.tokens.length - 1,
      level:  state.level,
      end:    -1,
      open:   scanned.can_open,
      close:  scanned.can_close
    });
  }

  state.pos += scanned.length;

  return true;
};


// Walk through delimiter list and replace text tokens with tags
//
module.exports.postProcess = function strikethrough(state) {
  var i, j,
      startDelim,
      endDelim,
      token,
      loneMarkers = [],
      delimiters = state.delimiters,
      max = state.delimiters.length;

  for (i = 0; i < max; i++) {
    startDelim = delimiters[i];

    if (startDelim.marker !== 0x7E/* ~ */) {
      continue;
    }

    if (startDelim.end === -1) {
      continue;
    }

    endDelim = delimiters[startDelim.end];

    token         = state.tokens[startDelim.token];
    token.type    = 's_open';
    token.tag     = 's';
    token.nesting = 1;
    token.markup  = '~~';
    token.content = '';

    token         = state.tokens[endDelim.token];
    token.type    = 's_close';
    token.tag     = 's';
    token.nesting = -1;
    token.markup  = '~~';
    token.content = '';

    if (state.tokens[endDelim.token - 1].type === 'text' &&
        state.tokens[endDelim.token - 1].content === '~') {

      loneMarkers.push(endDelim.token - 1);
    }
  }

  // If a marker sequence has an odd number of characters, it's splitted
  // like this: `~~~~~` -> `~` + `~~` + `~~`, leaving one marker at the
  // start of the sequence.
  //
  // So, we have to move all those markers after subsequent s_close tags.
  //
  while (loneMarkers.length) {
    i = loneMarkers.pop();
    j = i + 1;

    while (j < state.tokens.length && state.tokens[j].type === 's_close') {
      j++;
    }

    j--;

    if (i !== j) {
      token = state.tokens[j];
      state.tokens[j] = state.tokens[i];
      state.tokens[i] = token;
    }
  }
};

});
___scope___.file("lib/rules_inline/emphasis.js", function(exports, require, module, __filename, __dirname){

// Process *this* and _that_
//
'use strict';


// Insert each marker as a separate text token, and add it to delimiter list
//
module.exports.tokenize = function emphasis(state, silent) {
  var i, scanned, token,
      start = state.pos,
      marker = state.src.charCodeAt(start);

  if (silent) { return false; }

  if (marker !== 0x5F /* _ */ && marker !== 0x2A /* * */) { return false; }

  scanned = state.scanDelims(state.pos, marker === 0x2A);

  for (i = 0; i < scanned.length; i++) {
    token         = state.push('text', '', 0);
    token.content = String.fromCharCode(marker);

    state.delimiters.push({
      // Char code of the starting marker (number).
      //
      marker: marker,

      // Total length of these series of delimiters.
      //
      length: scanned.length,

      // An amount of characters before this one that's equivalent to
      // current one. In plain English: if this delimiter does not open
      // an emphasis, neither do previous `jump` characters.
      //
      // Used to skip sequences like "*****" in one step, for 1st asterisk
      // value will be 0, for 2nd it's 1 and so on.
      //
      jump:   i,

      // A position of the token this delimiter corresponds to.
      //
      token:  state.tokens.length - 1,

      // Token level.
      //
      level:  state.level,

      // If this delimiter is matched as a valid opener, `end` will be
      // equal to its position, otherwise it's `-1`.
      //
      end:    -1,

      // Boolean flags that determine if this delimiter could open or close
      // an emphasis.
      //
      open:   scanned.can_open,
      close:  scanned.can_close
    });
  }

  state.pos += scanned.length;

  return true;
};


// Walk through delimiter list and replace text tokens with tags
//
module.exports.postProcess = function emphasis(state) {
  var i,
      startDelim,
      endDelim,
      token,
      ch,
      isStrong,
      delimiters = state.delimiters,
      max = state.delimiters.length;

  for (i = max - 1; i >= 0; i--) {
    startDelim = delimiters[i];

    if (startDelim.marker !== 0x5F/* _ */ && startDelim.marker !== 0x2A/* * */) {
      continue;
    }

    // Process only opening markers
    if (startDelim.end === -1) {
      continue;
    }

    endDelim = delimiters[startDelim.end];

    // If the previous delimiter has the same marker and is adjacent to this one,
    // merge those into one strong delimiter.
    //
    // `<em><em>whatever</em></em>` -> `<strong>whatever</strong>`
    //
    isStrong = i > 0 &&
               delimiters[i - 1].end === startDelim.end + 1 &&
               delimiters[i - 1].token === startDelim.token - 1 &&
               delimiters[startDelim.end + 1].token === endDelim.token + 1 &&
               delimiters[i - 1].marker === startDelim.marker;

    ch = String.fromCharCode(startDelim.marker);

    token         = state.tokens[startDelim.token];
    token.type    = isStrong ? 'strong_open' : 'em_open';
    token.tag     = isStrong ? 'strong' : 'em';
    token.nesting = 1;
    token.markup  = isStrong ? ch + ch : ch;
    token.content = '';

    token         = state.tokens[endDelim.token];
    token.type    = isStrong ? 'strong_close' : 'em_close';
    token.tag     = isStrong ? 'strong' : 'em';
    token.nesting = -1;
    token.markup  = isStrong ? ch + ch : ch;
    token.content = '';

    if (isStrong) {
      state.tokens[delimiters[i - 1].token].content = '';
      state.tokens[delimiters[startDelim.end + 1].token].content = '';
      i--;
    }
  }
};

});
___scope___.file("lib/rules_inline/link.js", function(exports, require, module, __filename, __dirname){

// Process [link](<to> "stuff")

'use strict';

var normalizeReference   = require('../common/utils').normalizeReference;
var isSpace              = require('../common/utils').isSpace;


module.exports = function link(state, silent) {
  var attrs,
      code,
      label,
      labelEnd,
      labelStart,
      pos,
      res,
      ref,
      title,
      token,
      href = '',
      oldPos = state.pos,
      max = state.posMax,
      start = state.pos,
      parseReference = true;

  if (state.src.charCodeAt(state.pos) !== 0x5B/* [ */) { return false; }

  labelStart = state.pos + 1;
  labelEnd = state.md.helpers.parseLinkLabel(state, state.pos, true);

  // parser failed to find ']', so it's not a valid link
  if (labelEnd < 0) { return false; }

  pos = labelEnd + 1;
  if (pos < max && state.src.charCodeAt(pos) === 0x28/* ( */) {
    //
    // Inline link
    //

    // might have found a valid shortcut link, disable reference parsing
    parseReference = false;

    // [link](  <href>  "title"  )
    //        ^^ skipping these spaces
    pos++;
    for (; pos < max; pos++) {
      code = state.src.charCodeAt(pos);
      if (!isSpace(code) && code !== 0x0A) { break; }
    }
    if (pos >= max) { return false; }

    // [link](  <href>  "title"  )
    //          ^^^^^^ parsing link destination
    start = pos;
    res = state.md.helpers.parseLinkDestination(state.src, pos, state.posMax);
    if (res.ok) {
      href = state.md.normalizeLink(res.str);
      if (state.md.validateLink(href)) {
        pos = res.pos;
      } else {
        href = '';
      }
    }

    // [link](  <href>  "title"  )
    //                ^^ skipping these spaces
    start = pos;
    for (; pos < max; pos++) {
      code = state.src.charCodeAt(pos);
      if (!isSpace(code) && code !== 0x0A) { break; }
    }

    // [link](  <href>  "title"  )
    //                  ^^^^^^^ parsing link title
    res = state.md.helpers.parseLinkTitle(state.src, pos, state.posMax);
    if (pos < max && start !== pos && res.ok) {
      title = res.str;
      pos = res.pos;

      // [link](  <href>  "title"  )
      //                         ^^ skipping these spaces
      for (; pos < max; pos++) {
        code = state.src.charCodeAt(pos);
        if (!isSpace(code) && code !== 0x0A) { break; }
      }
    } else {
      title = '';
    }

    if (pos >= max || state.src.charCodeAt(pos) !== 0x29/* ) */) {
      // parsing a valid shortcut link failed, fallback to reference
      parseReference = true;
    }
    pos++;
  }

  if (parseReference) {
    //
    // Link reference
    //
    if (typeof state.env.references === 'undefined') { return false; }

    if (pos < max && state.src.charCodeAt(pos) === 0x5B/* [ */) {
      start = pos + 1;
      pos = state.md.helpers.parseLinkLabel(state, pos);
      if (pos >= 0) {
        label = state.src.slice(start, pos++);
      } else {
        pos = labelEnd + 1;
      }
    } else {
      pos = labelEnd + 1;
    }

    // covers label === '' and label === undefined
    // (collapsed reference link and shortcut reference link respectively)
    if (!label) { label = state.src.slice(labelStart, labelEnd); }

    ref = state.env.references[normalizeReference(label)];
    if (!ref) {
      state.pos = oldPos;
      return false;
    }
    href = ref.href;
    title = ref.title;
  }

  //
  // We found the end of the link, and know for a fact it's a valid link;
  // so all that's left to do is to call tokenizer.
  //
  if (!silent) {
    state.pos = labelStart;
    state.posMax = labelEnd;

    token        = state.push('link_open', 'a', 1);
    token.attrs  = attrs = [ [ 'href', href ] ];
    if (title) {
      attrs.push([ 'title', title ]);
    }

    state.md.inline.tokenize(state);

    token        = state.push('link_close', 'a', -1);
  }

  state.pos = pos;
  state.posMax = max;
  return true;
};

});
___scope___.file("lib/rules_inline/image.js", function(exports, require, module, __filename, __dirname){

// Process ![image](<src> "title")

'use strict';

var normalizeReference   = require('../common/utils').normalizeReference;
var isSpace              = require('../common/utils').isSpace;


module.exports = function image(state, silent) {
  var attrs,
      code,
      content,
      label,
      labelEnd,
      labelStart,
      pos,
      ref,
      res,
      title,
      token,
      tokens,
      start,
      href = '',
      oldPos = state.pos,
      max = state.posMax;

  if (state.src.charCodeAt(state.pos) !== 0x21/* ! */) { return false; }
  if (state.src.charCodeAt(state.pos + 1) !== 0x5B/* [ */) { return false; }

  labelStart = state.pos + 2;
  labelEnd = state.md.helpers.parseLinkLabel(state, state.pos + 1, false);

  // parser failed to find ']', so it's not a valid link
  if (labelEnd < 0) { return false; }

  pos = labelEnd + 1;
  if (pos < max && state.src.charCodeAt(pos) === 0x28/* ( */) {
    //
    // Inline link
    //

    // [link](  <href>  "title"  )
    //        ^^ skipping these spaces
    pos++;
    for (; pos < max; pos++) {
      code = state.src.charCodeAt(pos);
      if (!isSpace(code) && code !== 0x0A) { break; }
    }
    if (pos >= max) { return false; }

    // [link](  <href>  "title"  )
    //          ^^^^^^ parsing link destination
    start = pos;
    res = state.md.helpers.parseLinkDestination(state.src, pos, state.posMax);
    if (res.ok) {
      href = state.md.normalizeLink(res.str);
      if (state.md.validateLink(href)) {
        pos = res.pos;
      } else {
        href = '';
      }
    }

    // [link](  <href>  "title"  )
    //                ^^ skipping these spaces
    start = pos;
    for (; pos < max; pos++) {
      code = state.src.charCodeAt(pos);
      if (!isSpace(code) && code !== 0x0A) { break; }
    }

    // [link](  <href>  "title"  )
    //                  ^^^^^^^ parsing link title
    res = state.md.helpers.parseLinkTitle(state.src, pos, state.posMax);
    if (pos < max && start !== pos && res.ok) {
      title = res.str;
      pos = res.pos;

      // [link](  <href>  "title"  )
      //                         ^^ skipping these spaces
      for (; pos < max; pos++) {
        code = state.src.charCodeAt(pos);
        if (!isSpace(code) && code !== 0x0A) { break; }
      }
    } else {
      title = '';
    }

    if (pos >= max || state.src.charCodeAt(pos) !== 0x29/* ) */) {
      state.pos = oldPos;
      return false;
    }
    pos++;
  } else {
    //
    // Link reference
    //
    if (typeof state.env.references === 'undefined') { return false; }

    if (pos < max && state.src.charCodeAt(pos) === 0x5B/* [ */) {
      start = pos + 1;
      pos = state.md.helpers.parseLinkLabel(state, pos);
      if (pos >= 0) {
        label = state.src.slice(start, pos++);
      } else {
        pos = labelEnd + 1;
      }
    } else {
      pos = labelEnd + 1;
    }

    // covers label === '' and label === undefined
    // (collapsed reference link and shortcut reference link respectively)
    if (!label) { label = state.src.slice(labelStart, labelEnd); }

    ref = state.env.references[normalizeReference(label)];
    if (!ref) {
      state.pos = oldPos;
      return false;
    }
    href = ref.href;
    title = ref.title;
  }

  //
  // We found the end of the link, and know for a fact it's a valid link;
  // so all that's left to do is to call tokenizer.
  //
  if (!silent) {
    content = state.src.slice(labelStart, labelEnd);

    state.md.inline.parse(
      content,
      state.md,
      state.env,
      tokens = []
    );

    token          = state.push('image', 'img', 0);
    token.attrs    = attrs = [ [ 'src', href ], [ 'alt', '' ] ];
    token.children = tokens;
    token.content  = content;

    if (title) {
      attrs.push([ 'title', title ]);
    }
  }

  state.pos = pos;
  state.posMax = max;
  return true;
};

});
___scope___.file("lib/rules_inline/autolink.js", function(exports, require, module, __filename, __dirname){

// Process autolinks '<protocol:...>'

'use strict';


/*eslint max-len:0*/
var EMAIL_RE    = /^<([a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*)>/;
var AUTOLINK_RE = /^<([a-zA-Z][a-zA-Z0-9+.\-]{1,31}):([^<>\x00-\x20]*)>/;


module.exports = function autolink(state, silent) {
  var tail, linkMatch, emailMatch, url, fullUrl, token,
      pos = state.pos;

  if (state.src.charCodeAt(pos) !== 0x3C/* < */) { return false; }

  tail = state.src.slice(pos);

  if (tail.indexOf('>') < 0) { return false; }

  if (AUTOLINK_RE.test(tail)) {
    linkMatch = tail.match(AUTOLINK_RE);

    url = linkMatch[0].slice(1, -1);
    fullUrl = state.md.normalizeLink(url);
    if (!state.md.validateLink(fullUrl)) { return false; }

    if (!silent) {
      token         = state.push('link_open', 'a', 1);
      token.attrs   = [ [ 'href', fullUrl ] ];
      token.markup  = 'autolink';
      token.info    = 'auto';

      token         = state.push('text', '', 0);
      token.content = state.md.normalizeLinkText(url);

      token         = state.push('link_close', 'a', -1);
      token.markup  = 'autolink';
      token.info    = 'auto';
    }

    state.pos += linkMatch[0].length;
    return true;
  }

  if (EMAIL_RE.test(tail)) {
    emailMatch = tail.match(EMAIL_RE);

    url = emailMatch[0].slice(1, -1);
    fullUrl = state.md.normalizeLink('mailto:' + url);
    if (!state.md.validateLink(fullUrl)) { return false; }

    if (!silent) {
      token         = state.push('link_open', 'a', 1);
      token.attrs   = [ [ 'href', fullUrl ] ];
      token.markup  = 'autolink';
      token.info    = 'auto';

      token         = state.push('text', '', 0);
      token.content = state.md.normalizeLinkText(url);

      token         = state.push('link_close', 'a', -1);
      token.markup  = 'autolink';
      token.info    = 'auto';
    }

    state.pos += emailMatch[0].length;
    return true;
  }

  return false;
};

});
___scope___.file("lib/rules_inline/html_inline.js", function(exports, require, module, __filename, __dirname){

// Process html tags

'use strict';


var HTML_TAG_RE = require('../common/html_re').HTML_TAG_RE;


function isLetter(ch) {
  /*eslint no-bitwise:0*/
  var lc = ch | 0x20; // to lower case
  return (lc >= 0x61/* a */) && (lc <= 0x7a/* z */);
}


module.exports = function html_inline(state, silent) {
  var ch, match, max, token,
      pos = state.pos;

  if (!state.md.options.html) { return false; }

  // Check start
  max = state.posMax;
  if (state.src.charCodeAt(pos) !== 0x3C/* < */ ||
      pos + 2 >= max) {
    return false;
  }

  // Quick fail on second char
  ch = state.src.charCodeAt(pos + 1);
  if (ch !== 0x21/* ! */ &&
      ch !== 0x3F/* ? */ &&
      ch !== 0x2F/* / */ &&
      !isLetter(ch)) {
    return false;
  }

  match = state.src.slice(pos).match(HTML_TAG_RE);
  if (!match) { return false; }

  if (!silent) {
    token         = state.push('html_inline', '', 0);
    token.content = state.src.slice(pos, pos + match[0].length);
  }
  state.pos += match[0].length;
  return true;
};

});
___scope___.file("lib/rules_inline/entity.js", function(exports, require, module, __filename, __dirname){

// Process html entity - &#123;, &#xAF;, &quot;, ...

'use strict';

var entities          = require('../common/entities');
var has               = require('../common/utils').has;
var isValidEntityCode = require('../common/utils').isValidEntityCode;
var fromCodePoint     = require('../common/utils').fromCodePoint;


var DIGITAL_RE = /^&#((?:x[a-f0-9]{1,8}|[0-9]{1,8}));/i;
var NAMED_RE   = /^&([a-z][a-z0-9]{1,31});/i;


module.exports = function entity(state, silent) {
  var ch, code, match, pos = state.pos, max = state.posMax;

  if (state.src.charCodeAt(pos) !== 0x26/* & */) { return false; }

  if (pos + 1 < max) {
    ch = state.src.charCodeAt(pos + 1);

    if (ch === 0x23 /* # */) {
      match = state.src.slice(pos).match(DIGITAL_RE);
      if (match) {
        if (!silent) {
          code = match[1][0].toLowerCase() === 'x' ? parseInt(match[1].slice(1), 16) : parseInt(match[1], 10);
          state.pending += isValidEntityCode(code) ? fromCodePoint(code) : fromCodePoint(0xFFFD);
        }
        state.pos += match[0].length;
        return true;
      }
    } else {
      match = state.src.slice(pos).match(NAMED_RE);
      if (match) {
        if (has(entities, match[1])) {
          if (!silent) { state.pending += entities[match[1]]; }
          state.pos += match[0].length;
          return true;
        }
      }
    }
  }

  if (!silent) { state.pending += '&'; }
  state.pos++;
  return true;
};

});
___scope___.file("lib/rules_inline/balance_pairs.js", function(exports, require, module, __filename, __dirname){

// For each opening emphasis-like marker find a matching closing one
//
'use strict';


module.exports = function link_pairs(state) {
  var i, j, lastDelim, currDelim,
      delimiters = state.delimiters,
      max = state.delimiters.length;

  for (i = 0; i < max; i++) {
    lastDelim = delimiters[i];

    if (!lastDelim.close) { continue; }

    j = i - lastDelim.jump - 1;

    while (j >= 0) {
      currDelim = delimiters[j];

      if (currDelim.open &&
          currDelim.marker === lastDelim.marker &&
          currDelim.end < 0 &&
          currDelim.level === lastDelim.level) {

        // typeofs are for backward compatibility with plugins
        var odd_match = (currDelim.close || lastDelim.open) &&
                        typeof currDelim.length !== 'undefined' &&
                        typeof lastDelim.length !== 'undefined' &&
                        (currDelim.length + lastDelim.length) % 3 === 0;

        if (!odd_match) {
          lastDelim.jump = i - j;
          lastDelim.open = false;
          currDelim.end  = i;
          currDelim.jump = 0;
          break;
        }
      }

      j -= currDelim.jump + 1;
    }
  }
};

});
___scope___.file("lib/rules_inline/text_collapse.js", function(exports, require, module, __filename, __dirname){

// Merge adjacent text nodes into one, and re-calculate all token levels
//
'use strict';


module.exports = function text_collapse(state) {
  var curr, last,
      level = 0,
      tokens = state.tokens,
      max = state.tokens.length;

  for (curr = last = 0; curr < max; curr++) {
    // re-calculate levels
    level += tokens[curr].nesting;
    tokens[curr].level = level;

    if (tokens[curr].type === 'text' &&
        curr + 1 < max &&
        tokens[curr + 1].type === 'text') {

      // collapse two adjacent text nodes
      tokens[curr + 1].content = tokens[curr].content + tokens[curr + 1].content;
    } else {
      if (curr !== last) { tokens[last] = tokens[curr]; }

      last++;
    }
  }

  if (curr !== last) {
    tokens.length = last;
  }
};

});
___scope___.file("lib/rules_inline/state_inline.js", function(exports, require, module, __filename, __dirname){

// Inline parser state

'use strict';


var Token          = require('../token');
var isWhiteSpace   = require('../common/utils').isWhiteSpace;
var isPunctChar    = require('../common/utils').isPunctChar;
var isMdAsciiPunct = require('../common/utils').isMdAsciiPunct;


function StateInline(src, md, env, outTokens) {
  this.src = src;
  this.env = env;
  this.md = md;
  this.tokens = outTokens;

  this.pos = 0;
  this.posMax = this.src.length;
  this.level = 0;
  this.pending = '';
  this.pendingLevel = 0;

  this.cache = {};        // Stores { start: end } pairs. Useful for backtrack
                          // optimization of pairs parse (emphasis, strikes).

  this.delimiters = [];   // Emphasis-like delimiters
}


// Flush pending text
//
StateInline.prototype.pushPending = function () {
  var token = new Token('text', '', 0);
  token.content = this.pending;
  token.level = this.pendingLevel;
  this.tokens.push(token);
  this.pending = '';
  return token;
};


// Push new token to "stream".
// If pending text exists - flush it as text token
//
StateInline.prototype.push = function (type, tag, nesting) {
  if (this.pending) {
    this.pushPending();
  }

  var token = new Token(type, tag, nesting);

  if (nesting < 0) { this.level--; }
  token.level = this.level;
  if (nesting > 0) { this.level++; }

  this.pendingLevel = this.level;
  this.tokens.push(token);
  return token;
};


// Scan a sequence of emphasis-like markers, and determine whether
// it can start an emphasis sequence or end an emphasis sequence.
//
//  - start - position to scan from (it should point at a valid marker);
//  - canSplitWord - determine if these markers can be found inside a word
//
StateInline.prototype.scanDelims = function (start, canSplitWord) {
  var pos = start, lastChar, nextChar, count, can_open, can_close,
      isLastWhiteSpace, isLastPunctChar,
      isNextWhiteSpace, isNextPunctChar,
      left_flanking = true,
      right_flanking = true,
      max = this.posMax,
      marker = this.src.charCodeAt(start);

  // treat beginning of the line as a whitespace
  lastChar = start > 0 ? this.src.charCodeAt(start - 1) : 0x20;

  while (pos < max && this.src.charCodeAt(pos) === marker) { pos++; }

  count = pos - start;

  // treat end of the line as a whitespace
  nextChar = pos < max ? this.src.charCodeAt(pos) : 0x20;

  isLastPunctChar = isMdAsciiPunct(lastChar) || isPunctChar(String.fromCharCode(lastChar));
  isNextPunctChar = isMdAsciiPunct(nextChar) || isPunctChar(String.fromCharCode(nextChar));

  isLastWhiteSpace = isWhiteSpace(lastChar);
  isNextWhiteSpace = isWhiteSpace(nextChar);

  if (isNextWhiteSpace) {
    left_flanking = false;
  } else if (isNextPunctChar) {
    if (!(isLastWhiteSpace || isLastPunctChar)) {
      left_flanking = false;
    }
  }

  if (isLastWhiteSpace) {
    right_flanking = false;
  } else if (isLastPunctChar) {
    if (!(isNextWhiteSpace || isNextPunctChar)) {
      right_flanking = false;
    }
  }

  if (!canSplitWord) {
    can_open  = left_flanking  && (!right_flanking || isLastPunctChar);
    can_close = right_flanking && (!left_flanking  || isNextPunctChar);
  } else {
    can_open  = left_flanking;
    can_close = right_flanking;
  }

  return {
    can_open:  can_open,
    can_close: can_close,
    length:    count
  };
};


// re-export Token class to use in block rules
StateInline.prototype.Token = Token;


module.exports = StateInline;

});
___scope___.file("lib/presets/default.js", function(exports, require, module, __filename, __dirname){

// markdown-it default options

'use strict';


module.exports = {
  options: {
    html:         false,        // Enable HTML tags in source
    xhtmlOut:     false,        // Use '/' to close single tags (<br />)
    breaks:       false,        // Convert '\n' in paragraphs into <br>
    langPrefix:   'language-',  // CSS language prefix for fenced blocks
    linkify:      false,        // autoconvert URL-like texts to links

    // Enable some language-neutral replacements + quotes beautification
    typographer:  false,

    // Double + single quotes replacement pairs, when typographer enabled,
    // and smartquotes on. Could be either a String or an Array.
    //
    // For example, you can use '«»„“' for Russian, '„“‚‘' for German,
    // and ['«\xA0', '\xA0»', '‹\xA0', '\xA0›'] for French (including nbsp).
    quotes: '\u201c\u201d\u2018\u2019', /* “”‘’ */

    // Highlighter function. Should return escaped HTML,
    // or '' if the source string is not changed and should be escaped externaly.
    // If result starts with <pre... internal wrapper is skipped.
    //
    // function (/*str, lang*/) { return ''; }
    //
    highlight: null,

    maxNesting:   100            // Internal protection, recursion limit
  },

  components: {

    core: {},
    block: {},
    inline: {}
  }
};

});
___scope___.file("lib/presets/zero.js", function(exports, require, module, __filename, __dirname){

// "Zero" preset, with nothing enabled. Useful for manual configuring of simple
// modes. For example, to parse bold/italic only.

'use strict';


module.exports = {
  options: {
    html:         false,        // Enable HTML tags in source
    xhtmlOut:     false,        // Use '/' to close single tags (<br />)
    breaks:       false,        // Convert '\n' in paragraphs into <br>
    langPrefix:   'language-',  // CSS language prefix for fenced blocks
    linkify:      false,        // autoconvert URL-like texts to links

    // Enable some language-neutral replacements + quotes beautification
    typographer:  false,

    // Double + single quotes replacement pairs, when typographer enabled,
    // and smartquotes on. Could be either a String or an Array.
    //
    // For example, you can use '«»„“' for Russian, '„“‚‘' for German,
    // and ['«\xA0', '\xA0»', '‹\xA0', '\xA0›'] for French (including nbsp).
    quotes: '\u201c\u201d\u2018\u2019', /* “”‘’ */

    // Highlighter function. Should return escaped HTML,
    // or '' if the source string is not changed and should be escaped externaly.
    // If result starts with <pre... internal wrapper is skipped.
    //
    // function (/*str, lang*/) { return ''; }
    //
    highlight: null,

    maxNesting:   20            // Internal protection, recursion limit
  },

  components: {

    core: {
      rules: [
        'normalize',
        'block',
        'inline'
      ]
    },

    block: {
      rules: [
        'paragraph'
      ]
    },

    inline: {
      rules: [
        'text'
      ],
      rules2: [
        'balance_pairs',
        'text_collapse'
      ]
    }
  }
};

});
___scope___.file("lib/presets/commonmark.js", function(exports, require, module, __filename, __dirname){

// Commonmark default options

'use strict';


module.exports = {
  options: {
    html:         true,         // Enable HTML tags in source
    xhtmlOut:     true,         // Use '/' to close single tags (<br />)
    breaks:       false,        // Convert '\n' in paragraphs into <br>
    langPrefix:   'language-',  // CSS language prefix for fenced blocks
    linkify:      false,        // autoconvert URL-like texts to links

    // Enable some language-neutral replacements + quotes beautification
    typographer:  false,

    // Double + single quotes replacement pairs, when typographer enabled,
    // and smartquotes on. Could be either a String or an Array.
    //
    // For example, you can use '«»„“' for Russian, '„“‚‘' for German,
    // and ['«\xA0', '\xA0»', '‹\xA0', '\xA0›'] for French (including nbsp).
    quotes: '\u201c\u201d\u2018\u2019', /* “”‘’ */

    // Highlighter function. Should return escaped HTML,
    // or '' if the source string is not changed and should be escaped externaly.
    // If result starts with <pre... internal wrapper is skipped.
    //
    // function (/*str, lang*/) { return ''; }
    //
    highlight: null,

    maxNesting:   20            // Internal protection, recursion limit
  },

  components: {

    core: {
      rules: [
        'normalize',
        'block',
        'inline'
      ]
    },

    block: {
      rules: [
        'blockquote',
        'code',
        'fence',
        'heading',
        'hr',
        'html_block',
        'lheading',
        'list',
        'reference',
        'paragraph'
      ]
    },

    inline: {
      rules: [
        'autolink',
        'backticks',
        'emphasis',
        'entity',
        'escape',
        'html_inline',
        'image',
        'link',
        'newline',
        'text'
      ],
      rules2: [
        'balance_pairs',
        'emphasis',
        'text_collapse'
      ]
    }
  }
};

});
return ___scope___.entry = "index.js";
});
FuseBox.pkg("mdurl", {}, function(___scope___){
___scope___.file("index.js", function(exports, require, module, __filename, __dirname){

'use strict';


module.exports.encode = require('./encode');
module.exports.decode = require('./decode');
module.exports.format = require('./format');
module.exports.parse  = require('./parse');

});
___scope___.file("encode.js", function(exports, require, module, __filename, __dirname){


'use strict';


var encodeCache = {};


// Create a lookup array where anything but characters in `chars` string
// and alphanumeric chars is percent-encoded.
//
function getEncodeCache(exclude) {
  var i, ch, cache = encodeCache[exclude];
  if (cache) { return cache; }

  cache = encodeCache[exclude] = [];

  for (i = 0; i < 128; i++) {
    ch = String.fromCharCode(i);

    if (/^[0-9a-z]$/i.test(ch)) {
      // always allow unencoded alphanumeric characters
      cache.push(ch);
    } else {
      cache.push('%' + ('0' + i.toString(16).toUpperCase()).slice(-2));
    }
  }

  for (i = 0; i < exclude.length; i++) {
    cache[exclude.charCodeAt(i)] = exclude[i];
  }

  return cache;
}


// Encode unsafe characters with percent-encoding, skipping already
// encoded sequences.
//
//  - string       - string to encode
//  - exclude      - list of characters to ignore (in addition to a-zA-Z0-9)
//  - keepEscaped  - don't encode '%' in a correct escape sequence (default: true)
//
function encode(string, exclude, keepEscaped) {
  var i, l, code, nextCode, cache,
      result = '';

  if (typeof exclude !== 'string') {
    // encode(string, keepEscaped)
    keepEscaped  = exclude;
    exclude = encode.defaultChars;
  }

  if (typeof keepEscaped === 'undefined') {
    keepEscaped = true;
  }

  cache = getEncodeCache(exclude);

  for (i = 0, l = string.length; i < l; i++) {
    code = string.charCodeAt(i);

    if (keepEscaped && code === 0x25 /* % */ && i + 2 < l) {
      if (/^[0-9a-f]{2}$/i.test(string.slice(i + 1, i + 3))) {
        result += string.slice(i, i + 3);
        i += 2;
        continue;
      }
    }

    if (code < 128) {
      result += cache[code];
      continue;
    }

    if (code >= 0xD800 && code <= 0xDFFF) {
      if (code >= 0xD800 && code <= 0xDBFF && i + 1 < l) {
        nextCode = string.charCodeAt(i + 1);
        if (nextCode >= 0xDC00 && nextCode <= 0xDFFF) {
          result += encodeURIComponent(string[i] + string[i + 1]);
          i++;
          continue;
        }
      }
      result += '%EF%BF%BD';
      continue;
    }

    result += encodeURIComponent(string[i]);
  }

  return result;
}

encode.defaultChars   = ";/?:@&=+$,-_.!~*'()#";
encode.componentChars = "-_.!~*'()";


module.exports = encode;

});
___scope___.file("decode.js", function(exports, require, module, __filename, __dirname){


'use strict';


/* eslint-disable no-bitwise */

var decodeCache = {};

function getDecodeCache(exclude) {
  var i, ch, cache = decodeCache[exclude];
  if (cache) { return cache; }

  cache = decodeCache[exclude] = [];

  for (i = 0; i < 128; i++) {
    ch = String.fromCharCode(i);
    cache.push(ch);
  }

  for (i = 0; i < exclude.length; i++) {
    ch = exclude.charCodeAt(i);
    cache[ch] = '%' + ('0' + ch.toString(16).toUpperCase()).slice(-2);
  }

  return cache;
}


// Decode percent-encoded string.
//
function decode(string, exclude) {
  var cache;

  if (typeof exclude !== 'string') {
    exclude = decode.defaultChars;
  }

  cache = getDecodeCache(exclude);

  return string.replace(/(%[a-f0-9]{2})+/gi, function(seq) {
    var i, l, b1, b2, b3, b4, chr,
        result = '';

    for (i = 0, l = seq.length; i < l; i += 3) {
      b1 = parseInt(seq.slice(i + 1, i + 3), 16);

      if (b1 < 0x80) {
        result += cache[b1];
        continue;
      }

      if ((b1 & 0xE0) === 0xC0 && (i + 3 < l)) {
        // 110xxxxx 10xxxxxx
        b2 = parseInt(seq.slice(i + 4, i + 6), 16);

        if ((b2 & 0xC0) === 0x80) {
          chr = ((b1 << 6) & 0x7C0) | (b2 & 0x3F);

          if (chr < 0x80) {
            result += '\ufffd\ufffd';
          } else {
            result += String.fromCharCode(chr);
          }

          i += 3;
          continue;
        }
      }

      if ((b1 & 0xF0) === 0xE0 && (i + 6 < l)) {
        // 1110xxxx 10xxxxxx 10xxxxxx
        b2 = parseInt(seq.slice(i + 4, i + 6), 16);
        b3 = parseInt(seq.slice(i + 7, i + 9), 16);

        if ((b2 & 0xC0) === 0x80 && (b3 & 0xC0) === 0x80) {
          chr = ((b1 << 12) & 0xF000) | ((b2 << 6) & 0xFC0) | (b3 & 0x3F);

          if (chr < 0x800 || (chr >= 0xD800 && chr <= 0xDFFF)) {
            result += '\ufffd\ufffd\ufffd';
          } else {
            result += String.fromCharCode(chr);
          }

          i += 6;
          continue;
        }
      }

      if ((b1 & 0xF8) === 0xF0 && (i + 9 < l)) {
        // 111110xx 10xxxxxx 10xxxxxx 10xxxxxx
        b2 = parseInt(seq.slice(i + 4, i + 6), 16);
        b3 = parseInt(seq.slice(i + 7, i + 9), 16);
        b4 = parseInt(seq.slice(i + 10, i + 12), 16);

        if ((b2 & 0xC0) === 0x80 && (b3 & 0xC0) === 0x80 && (b4 & 0xC0) === 0x80) {
          chr = ((b1 << 18) & 0x1C0000) | ((b2 << 12) & 0x3F000) | ((b3 << 6) & 0xFC0) | (b4 & 0x3F);

          if (chr < 0x10000 || chr > 0x10FFFF) {
            result += '\ufffd\ufffd\ufffd\ufffd';
          } else {
            chr -= 0x10000;
            result += String.fromCharCode(0xD800 + (chr >> 10), 0xDC00 + (chr & 0x3FF));
          }

          i += 9;
          continue;
        }
      }

      result += '\ufffd';
    }

    return result;
  });
}


decode.defaultChars   = ';/?:@&=+$,#';
decode.componentChars = '';


module.exports = decode;

});
___scope___.file("format.js", function(exports, require, module, __filename, __dirname){


'use strict';


module.exports = function format(url) {
  var result = '';

  result += url.protocol || '';
  result += url.slashes ? '//' : '';
  result += url.auth ? url.auth + '@' : '';

  if (url.hostname && url.hostname.indexOf(':') !== -1) {
    // ipv6 address
    result += '[' + url.hostname + ']';
  } else {
    result += url.hostname || '';
  }

  result += url.port ? ':' + url.port : '';
  result += url.pathname || '';
  result += url.search || '';
  result += url.hash || '';

  return result;
};

});
___scope___.file("parse.js", function(exports, require, module, __filename, __dirname){

// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.

'use strict';

//
// Changes from joyent/node:
//
// 1. No leading slash in paths,
//    e.g. in `url.parse('http://foo?bar')` pathname is ``, not `/`
//
// 2. Backslashes are not replaced with slashes,
//    so `http:\\example.org\` is treated like a relative path
//
// 3. Trailing colon is treated like a part of the path,
//    i.e. in `http://example.org:foo` pathname is `:foo`
//
// 4. Nothing is URL-encoded in the resulting object,
//    (in joyent/node some chars in auth and paths are encoded)
//
// 5. `url.parse()` does not have `parseQueryString` argument
//
// 6. Removed extraneous result properties: `host`, `path`, `query`, etc.,
//    which can be constructed using other parts of the url.
//


function Url() {
  this.protocol = null;
  this.slashes = null;
  this.auth = null;
  this.port = null;
  this.hostname = null;
  this.hash = null;
  this.search = null;
  this.pathname = null;
}

// Reference: RFC 3986, RFC 1808, RFC 2396

// define these here so at least they only have to be
// compiled once on the first module load.
var protocolPattern = /^([a-z0-9.+-]+:)/i,
    portPattern = /:[0-9]*$/,

    // Special case for a simple path URL
    simplePathPattern = /^(\/\/?(?!\/)[^\?\s]*)(\?[^\s]*)?$/,

    // RFC 2396: characters reserved for delimiting URLs.
    // We actually just auto-escape these.
    delims = [ '<', '>', '"', '`', ' ', '\r', '\n', '\t' ],

    // RFC 2396: characters not allowed for various reasons.
    unwise = [ '{', '}', '|', '\\', '^', '`' ].concat(delims),

    // Allowed by RFCs, but cause of XSS attacks.  Always escape these.
    autoEscape = [ '\'' ].concat(unwise),
    // Characters that are never ever allowed in a hostname.
    // Note that any invalid chars are also handled, but these
    // are the ones that are *expected* to be seen, so we fast-path
    // them.
    nonHostChars = [ '%', '/', '?', ';', '#' ].concat(autoEscape),
    hostEndingChars = [ '/', '?', '#' ],
    hostnameMaxLen = 255,
    hostnamePartPattern = /^[+a-z0-9A-Z_-]{0,63}$/,
    hostnamePartStart = /^([+a-z0-9A-Z_-]{0,63})(.*)$/,
    // protocols that can allow "unsafe" and "unwise" chars.
    /* eslint-disable no-script-url */
    // protocols that never have a hostname.
    hostlessProtocol = {
      'javascript': true,
      'javascript:': true
    },
    // protocols that always contain a // bit.
    slashedProtocol = {
      'http': true,
      'https': true,
      'ftp': true,
      'gopher': true,
      'file': true,
      'http:': true,
      'https:': true,
      'ftp:': true,
      'gopher:': true,
      'file:': true
    };
    /* eslint-enable no-script-url */

function urlParse(url, slashesDenoteHost) {
  if (url && url instanceof Url) { return url; }

  var u = new Url();
  u.parse(url, slashesDenoteHost);
  return u;
}

Url.prototype.parse = function(url, slashesDenoteHost) {
  var i, l, lowerProto, hec, slashes,
      rest = url;

  // trim before proceeding.
  // This is to support parse stuff like "  http://foo.com  \n"
  rest = rest.trim();

  if (!slashesDenoteHost && url.split('#').length === 1) {
    // Try fast path regexp
    var simplePath = simplePathPattern.exec(rest);
    if (simplePath) {
      this.pathname = simplePath[1];
      if (simplePath[2]) {
        this.search = simplePath[2];
      }
      return this;
    }
  }

  var proto = protocolPattern.exec(rest);
  if (proto) {
    proto = proto[0];
    lowerProto = proto.toLowerCase();
    this.protocol = proto;
    rest = rest.substr(proto.length);
  }

  // figure out if it's got a host
  // user@server is *always* interpreted as a hostname, and url
  // resolution will treat //foo/bar as host=foo,path=bar because that's
  // how the browser resolves relative URLs.
  if (slashesDenoteHost || proto || rest.match(/^\/\/[^@\/]+@[^@\/]+/)) {
    slashes = rest.substr(0, 2) === '//';
    if (slashes && !(proto && hostlessProtocol[proto])) {
      rest = rest.substr(2);
      this.slashes = true;
    }
  }

  if (!hostlessProtocol[proto] &&
      (slashes || (proto && !slashedProtocol[proto]))) {

    // there's a hostname.
    // the first instance of /, ?, ;, or # ends the host.
    //
    // If there is an @ in the hostname, then non-host chars *are* allowed
    // to the left of the last @ sign, unless some host-ending character
    // comes *before* the @-sign.
    // URLs are obnoxious.
    //
    // ex:
    // http://a@b@c/ => user:a@b host:c
    // http://a@b?@c => user:a host:c path:/?@c

    // v0.12 TODO(isaacs): This is not quite how Chrome does things.
    // Review our test case against browsers more comprehensively.

    // find the first instance of any hostEndingChars
    var hostEnd = -1;
    for (i = 0; i < hostEndingChars.length; i++) {
      hec = rest.indexOf(hostEndingChars[i]);
      if (hec !== -1 && (hostEnd === -1 || hec < hostEnd)) {
        hostEnd = hec;
      }
    }

    // at this point, either we have an explicit point where the
    // auth portion cannot go past, or the last @ char is the decider.
    var auth, atSign;
    if (hostEnd === -1) {
      // atSign can be anywhere.
      atSign = rest.lastIndexOf('@');
    } else {
      // atSign must be in auth portion.
      // http://a@b/c@d => host:b auth:a path:/c@d
      atSign = rest.lastIndexOf('@', hostEnd);
    }

    // Now we have a portion which is definitely the auth.
    // Pull that off.
    if (atSign !== -1) {
      auth = rest.slice(0, atSign);
      rest = rest.slice(atSign + 1);
      this.auth = auth;
    }

    // the host is the remaining to the left of the first non-host char
    hostEnd = -1;
    for (i = 0; i < nonHostChars.length; i++) {
      hec = rest.indexOf(nonHostChars[i]);
      if (hec !== -1 && (hostEnd === -1 || hec < hostEnd)) {
        hostEnd = hec;
      }
    }
    // if we still have not hit it, then the entire thing is a host.
    if (hostEnd === -1) {
      hostEnd = rest.length;
    }

    if (rest[hostEnd - 1] === ':') { hostEnd--; }
    var host = rest.slice(0, hostEnd);
    rest = rest.slice(hostEnd);

    // pull out port.
    this.parseHost(host);

    // we've indicated that there is a hostname,
    // so even if it's empty, it has to be present.
    this.hostname = this.hostname || '';

    // if hostname begins with [ and ends with ]
    // assume that it's an IPv6 address.
    var ipv6Hostname = this.hostname[0] === '[' &&
        this.hostname[this.hostname.length - 1] === ']';

    // validate a little.
    if (!ipv6Hostname) {
      var hostparts = this.hostname.split(/\./);
      for (i = 0, l = hostparts.length; i < l; i++) {
        var part = hostparts[i];
        if (!part) { continue; }
        if (!part.match(hostnamePartPattern)) {
          var newpart = '';
          for (var j = 0, k = part.length; j < k; j++) {
            if (part.charCodeAt(j) > 127) {
              // we replace non-ASCII char with a temporary placeholder
              // we need this to make sure size of hostname is not
              // broken by replacing non-ASCII by nothing
              newpart += 'x';
            } else {
              newpart += part[j];
            }
          }
          // we test again with ASCII char only
          if (!newpart.match(hostnamePartPattern)) {
            var validParts = hostparts.slice(0, i);
            var notHost = hostparts.slice(i + 1);
            var bit = part.match(hostnamePartStart);
            if (bit) {
              validParts.push(bit[1]);
              notHost.unshift(bit[2]);
            }
            if (notHost.length) {
              rest = notHost.join('.') + rest;
            }
            this.hostname = validParts.join('.');
            break;
          }
        }
      }
    }

    if (this.hostname.length > hostnameMaxLen) {
      this.hostname = '';
    }

    // strip [ and ] from the hostname
    // the host field still retains them, though
    if (ipv6Hostname) {
      this.hostname = this.hostname.substr(1, this.hostname.length - 2);
    }
  }

  // chop off from the tail first.
  var hash = rest.indexOf('#');
  if (hash !== -1) {
    // got a fragment string.
    this.hash = rest.substr(hash);
    rest = rest.slice(0, hash);
  }
  var qm = rest.indexOf('?');
  if (qm !== -1) {
    this.search = rest.substr(qm);
    rest = rest.slice(0, qm);
  }
  if (rest) { this.pathname = rest; }
  if (slashedProtocol[lowerProto] &&
      this.hostname && !this.pathname) {
    this.pathname = '';
  }

  return this;
};

Url.prototype.parseHost = function(host) {
  var port = portPattern.exec(host);
  if (port) {
    port = port[0];
    if (port !== ':') {
      this.port = port.substr(1);
    }
    host = host.substr(0, host.length - port.length);
  }
  if (host) { this.hostname = host; }
};

module.exports = urlParse;

});
return ___scope___.entry = "index.js";
});
FuseBox.pkg("omject", {}, function(___scope___){
___scope___.file("src/index.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Omject_1 = require("./Omject");
exports.Omject = Omject_1.default;
exports.parentFn = Omject_1.parentFn;
var BasicOmjectClass_1 = require("./BasicOmjectClass");
exports.BasicOmjectClass = BasicOmjectClass_1.default;
var OmjectAug_1 = require("./OmjectAug");
exports.OmjectAug = OmjectAug_1.default;
// export { default as ObservableOmject } from "./ObservableOmject";
var EventEmitter_1 = require("./EventEmitter");
exports.EventEmitter = EventEmitter_1.default;
//# sourceMappingURL=index.js.map
});
___scope___.file("src/Omject.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function parentFn(target) {
    return () => target.omjectAug.parent;
}
exports.parentFn = parentFn;
//# sourceMappingURL=Omject.js.map
});
___scope___.file("src/BasicOmjectClass.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Omject_1 = require("./Omject");
const OmjectAug_1 = require("./OmjectAug");
class BasicOmjectClass {
    constructor(parent) {
        this.parent = Omject_1.parentFn(this);
        this.omjectAug = new OmjectAug_1.default(this, parent);
    }
    getAncestor(ancestorClass, skipThis = true) {
        return this.omjectAug.getAncestor(ancestorClass, skipThis);
    }
    on(eventName, cb) {
        return this.omjectAug.on(eventName, cb);
    }
    emit(event) {
        return this.omjectAug.emit(event);
    }
    bubble(event) {
        event.postOpBubbles = true;
        return this.omjectAug.emit(event);
    }
}
exports.default = BasicOmjectClass;
//# sourceMappingURL=BasicOmjectClass.js.map
});
___scope___.file("src/OmjectAug.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const EventEmitter_1 = require("./EventEmitter");
class OmjectAug {
    constructor(targetClass, parent) {
        this.targetClass = targetClass;
        this.parent = parent;
        this.eventEmitter = new EventEmitter_1.default(targetClass, () => this.parent);
    }
    getAncestor(ancestorClass, skipThis = true) {
        const targetClass = this.targetClass;
        if (skipThis === false && typeof ancestorClass === "string" && ancestorClass === targetClass.omjectId) {
            return targetClass;
        }
        else if (skipThis === false && typeof ancestorClass === "function" && targetClass instanceof ancestorClass) {
            return targetClass;
        }
        else if (this.parent) {
            return this.parent.omjectAug.getAncestor(ancestorClass, false);
        }
        return undefined;
    }
    on(eventName, cb) {
        this.eventEmitter.on(eventName, cb);
    }
    emit(event) {
        this.eventEmitter.emit(event);
    }
    bubble(event) {
        event.postOpBubbles = true;
        this.eventEmitter.emit(event);
    }
}
exports.default = OmjectAug;
//# sourceMappingURL=OmjectAug.js.map
});
___scope___.file("src/EventEmitter.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const OmEvent_1 = require("./OmEvent");
class EventEmitter {
    static checkForEmitFn(checkMe) {
        return typeof checkMe.emit === "function";
    }
    constructor(creator, getParent) {
        this.getParent = getParent;
        this.creator = creator;
        this.eventListeners = new Map();
    }
    on(eventName, cb) {
        if (!this.eventListeners.has(eventName)) {
            this.eventListeners.set(eventName, []);
        }
        this.eventListeners.get(eventName).push(cb);
    }
    emit(event) {
        const omEv = OmEvent_1.default.isOmEvent(event) ? (event) : (OmEvent_1.default.evolveEvent(event, this.creator));
        const listeners = this.eventListeners.get(event.type);
        for (let i = 0; listeners && i < listeners.length; i++) {
            listeners[i](omEv);
            if (omEv.immediatePropagationStopped) {
                break;
            }
        }
        if (OmEvent_1.default.shouldBubble(omEv)) {
            const parent = this.getParent();
            if (parent) {
                if (EventEmitter.checkForEmitFn(parent)) {
                    parent.emit(omEv);
                }
                else {
                    parent.omjectAug.emit(omEv);
                }
            }
        }
    }
}
exports.default = EventEmitter;
//# sourceMappingURL=EventEmitter.js.map
});
___scope___.file("src/OmEvent.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class OmEvent extends Event {
    constructor() {
        super(...arguments);
        this.immediatePropagationStopped = false;
    }
    static isOmEvent(testMe) {
        return testMe.immediatePropagationStopped !== undefined;
    }
    static evolveEvent(evolveMe, source) {
        evolveMe.immediatePropagationStopped = false;
        evolveMe.stopImmediatePropagation = () => {
            evolveMe.immediatePropagationStopped = true;
            evolveMe.stopImmediatePropagation();
        };
        evolveMe.source = source;
        return evolveMe;
    }
    static shouldBubble(omEv) {
        return !omEv.cancelBubble && (omEv.postOpBubbles
            || (omEv.postOpBubbles === undefined && omEv.bubbles));
    }
}
exports.default = OmEvent;
//# sourceMappingURL=OmEvent.js.map
});
return ___scope___.entry = "src/index.js";
});
FuseBox.pkg("process", {}, function(___scope___){
___scope___.file("index.js", function(exports, require, module, __filename, __dirname){

// From https://github.com/defunctzombie/node-process/blob/master/browser.js
// shim for using process in browser
if (FuseBox.isServer) {
	if (typeof __process_env__ !== "undefined") {
		Object.assign(global.process.env, __process_env__);
	}
	module.exports = global.process;
} else {
	// Object assign polyfill
	if (typeof Object.assign != "function") {
		Object.assign = function(target, varArgs) {
			// .length of function is 2
			"use strict";
			if (target == null) {
				// TypeError if undefined or null
				throw new TypeError("Cannot convert undefined or null to object");
			}

			var to = Object(target);

			for (var index = 1; index < arguments.length; index++) {
				var nextSource = arguments[index];

				if (nextSource != null) {
					// Skip over if undefined or null
					for (var nextKey in nextSource) {
						// Avoid bugs when hasOwnProperty is shadowed
						if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
							to[nextKey] = nextSource[nextKey];
						}
					}
				}
			}
			return to;
		};
	}

	var productionEnv = false; //require('@system-env').production;

	var process = (module.exports = {});
	var queue = [];
	var draining = false;
	var currentQueue;
	var queueIndex = -1;

	function cleanUpNextTick() {
		draining = false;
		if (currentQueue.length) {
			queue = currentQueue.concat(queue);
		} else {
			queueIndex = -1;
		}
		if (queue.length) {
			drainQueue();
		}
	}

	function drainQueue() {
		if (draining) {
			return;
		}
		var timeout = setTimeout(cleanUpNextTick);
		draining = true;

		var len = queue.length;
		while (len) {
			currentQueue = queue;
			queue = [];
			while (++queueIndex < len) {
				if (currentQueue) {
					currentQueue[queueIndex].run();
				}
			}
			queueIndex = -1;
			len = queue.length;
		}
		currentQueue = null;
		draining = false;
		clearTimeout(timeout);
	}

	process.nextTick = function(fun) {
		var args = new Array(arguments.length - 1);
		if (arguments.length > 1) {
			for (var i = 1; i < arguments.length; i++) {
				args[i - 1] = arguments[i];
			}
		}
		queue.push(new Item(fun, args));
		if (queue.length === 1 && !draining) {
			setTimeout(drainQueue, 0);
		}
	};

	// v8 likes predictible objects
	function Item(fun, array) {
		this.fun = fun;
		this.array = array;
	}
	Item.prototype.run = function() {
		this.fun.apply(null, this.array);
	};
	process.title = "browser";
	process.browser = true;
	process.env = {
		NODE_ENV: productionEnv ? "production" : "development"
	};
	if (typeof __process_env__ !== "undefined") {
		Object.assign(process.env, __process_env__);
	}
	process.argv = [];
	process.version = ""; // empty string to avoid regexp issues
	process.versions = {};

	function noop() {}

	process.on = noop;
	process.addListener = noop;
	process.once = noop;
	process.off = noop;
	process.removeListener = noop;
	process.removeAllListeners = noop;
	process.emit = noop;

	process.binding = function(name) {
		throw new Error("process.binding is not supported");
	};

	process.cwd = function() {
		return "/";
	};
	process.chdir = function(dir) {
		throw new Error("process.chdir is not supported");
	};
	process.umask = function() {
		return 0;
	};
}

});
return ___scope___.entry = "index.js";
});
FuseBox.pkg("punycode", {}, function(___scope___){
___scope___.file("punycode.js", function(exports, require, module, __filename, __dirname){

/*! https://mths.be/punycode v1.4.1 by @mathias */
;(function(root) {

	/** Detect free variables */
	var freeExports = typeof exports == 'object' && exports &&
		!exports.nodeType && exports;
	var freeModule = typeof module == 'object' && module &&
		!module.nodeType && module;
	var freeGlobal = typeof global == 'object' && global;
	if (
		freeGlobal.global === freeGlobal ||
		freeGlobal.window === freeGlobal ||
		freeGlobal.self === freeGlobal
	) {
		root = freeGlobal;
	}

	/**
	 * The `punycode` object.
	 * @name punycode
	 * @type Object
	 */
	var punycode,

	/** Highest positive signed 32-bit float value */
	maxInt = 2147483647, // aka. 0x7FFFFFFF or 2^31-1

	/** Bootstring parameters */
	base = 36,
	tMin = 1,
	tMax = 26,
	skew = 38,
	damp = 700,
	initialBias = 72,
	initialN = 128, // 0x80
	delimiter = '-', // '\x2D'

	/** Regular expressions */
	regexPunycode = /^xn--/,
	regexNonASCII = /[^\x20-\x7E]/, // unprintable ASCII chars + non-ASCII chars
	regexSeparators = /[\x2E\u3002\uFF0E\uFF61]/g, // RFC 3490 separators

	/** Error messages */
	errors = {
		'overflow': 'Overflow: input needs wider integers to process',
		'not-basic': 'Illegal input >= 0x80 (not a basic code point)',
		'invalid-input': 'Invalid input'
	},

	/** Convenience shortcuts */
	baseMinusTMin = base - tMin,
	floor = Math.floor,
	stringFromCharCode = String.fromCharCode,

	/** Temporary variable */
	key;

	/*--------------------------------------------------------------------------*/

	/**
	 * A generic error utility function.
	 * @private
	 * @param {String} type The error type.
	 * @returns {Error} Throws a `RangeError` with the applicable error message.
	 */
	function error(type) {
		throw new RangeError(errors[type]);
	}

	/**
	 * A generic `Array#map` utility function.
	 * @private
	 * @param {Array} array The array to iterate over.
	 * @param {Function} callback The function that gets called for every array
	 * item.
	 * @returns {Array} A new array of values returned by the callback function.
	 */
	function map(array, fn) {
		var length = array.length;
		var result = [];
		while (length--) {
			result[length] = fn(array[length]);
		}
		return result;
	}

	/**
	 * A simple `Array#map`-like wrapper to work with domain name strings or email
	 * addresses.
	 * @private
	 * @param {String} domain The domain name or email address.
	 * @param {Function} callback The function that gets called for every
	 * character.
	 * @returns {Array} A new string of characters returned by the callback
	 * function.
	 */
	function mapDomain(string, fn) {
		var parts = string.split('@');
		var result = '';
		if (parts.length > 1) {
			// In email addresses, only the domain name should be punycoded. Leave
			// the local part (i.e. everything up to `@`) intact.
			result = parts[0] + '@';
			string = parts[1];
		}
		// Avoid `split(regex)` for IE8 compatibility. See #17.
		string = string.replace(regexSeparators, '\x2E');
		var labels = string.split('.');
		var encoded = map(labels, fn).join('.');
		return result + encoded;
	}

	/**
	 * Creates an array containing the numeric code points of each Unicode
	 * character in the string. While JavaScript uses UCS-2 internally,
	 * this function will convert a pair of surrogate halves (each of which
	 * UCS-2 exposes as separate characters) into a single code point,
	 * matching UTF-16.
	 * @see `punycode.ucs2.encode`
	 * @see <https://mathiasbynens.be/notes/javascript-encoding>
	 * @memberOf punycode.ucs2
	 * @name decode
	 * @param {String} string The Unicode input string (UCS-2).
	 * @returns {Array} The new array of code points.
	 */
	function ucs2decode(string) {
		var output = [],
		    counter = 0,
		    length = string.length,
		    value,
		    extra;
		while (counter < length) {
			value = string.charCodeAt(counter++);
			if (value >= 0xD800 && value <= 0xDBFF && counter < length) {
				// high surrogate, and there is a next character
				extra = string.charCodeAt(counter++);
				if ((extra & 0xFC00) == 0xDC00) { // low surrogate
					output.push(((value & 0x3FF) << 10) + (extra & 0x3FF) + 0x10000);
				} else {
					// unmatched surrogate; only append this code unit, in case the next
					// code unit is the high surrogate of a surrogate pair
					output.push(value);
					counter--;
				}
			} else {
				output.push(value);
			}
		}
		return output;
	}

	/**
	 * Creates a string based on an array of numeric code points.
	 * @see `punycode.ucs2.decode`
	 * @memberOf punycode.ucs2
	 * @name encode
	 * @param {Array} codePoints The array of numeric code points.
	 * @returns {String} The new Unicode string (UCS-2).
	 */
	function ucs2encode(array) {
		return map(array, function(value) {
			var output = '';
			if (value > 0xFFFF) {
				value -= 0x10000;
				output += stringFromCharCode(value >>> 10 & 0x3FF | 0xD800);
				value = 0xDC00 | value & 0x3FF;
			}
			output += stringFromCharCode(value);
			return output;
		}).join('');
	}

	/**
	 * Converts a basic code point into a digit/integer.
	 * @see `digitToBasic()`
	 * @private
	 * @param {Number} codePoint The basic numeric code point value.
	 * @returns {Number} The numeric value of a basic code point (for use in
	 * representing integers) in the range `0` to `base - 1`, or `base` if
	 * the code point does not represent a value.
	 */
	function basicToDigit(codePoint) {
		if (codePoint - 48 < 10) {
			return codePoint - 22;
		}
		if (codePoint - 65 < 26) {
			return codePoint - 65;
		}
		if (codePoint - 97 < 26) {
			return codePoint - 97;
		}
		return base;
	}

	/**
	 * Converts a digit/integer into a basic code point.
	 * @see `basicToDigit()`
	 * @private
	 * @param {Number} digit The numeric value of a basic code point.
	 * @returns {Number} The basic code point whose value (when used for
	 * representing integers) is `digit`, which needs to be in the range
	 * `0` to `base - 1`. If `flag` is non-zero, the uppercase form is
	 * used; else, the lowercase form is used. The behavior is undefined
	 * if `flag` is non-zero and `digit` has no uppercase form.
	 */
	function digitToBasic(digit, flag) {
		//  0..25 map to ASCII a..z or A..Z
		// 26..35 map to ASCII 0..9
		return digit + 22 + 75 * (digit < 26) - ((flag != 0) << 5);
	}

	/**
	 * Bias adaptation function as per section 3.4 of RFC 3492.
	 * https://tools.ietf.org/html/rfc3492#section-3.4
	 * @private
	 */
	function adapt(delta, numPoints, firstTime) {
		var k = 0;
		delta = firstTime ? floor(delta / damp) : delta >> 1;
		delta += floor(delta / numPoints);
		for (/* no initialization */; delta > baseMinusTMin * tMax >> 1; k += base) {
			delta = floor(delta / baseMinusTMin);
		}
		return floor(k + (baseMinusTMin + 1) * delta / (delta + skew));
	}

	/**
	 * Converts a Punycode string of ASCII-only symbols to a string of Unicode
	 * symbols.
	 * @memberOf punycode
	 * @param {String} input The Punycode string of ASCII-only symbols.
	 * @returns {String} The resulting string of Unicode symbols.
	 */
	function decode(input) {
		// Don't use UCS-2
		var output = [],
		    inputLength = input.length,
		    out,
		    i = 0,
		    n = initialN,
		    bias = initialBias,
		    basic,
		    j,
		    index,
		    oldi,
		    w,
		    k,
		    digit,
		    t,
		    /** Cached calculation results */
		    baseMinusT;

		// Handle the basic code points: let `basic` be the number of input code
		// points before the last delimiter, or `0` if there is none, then copy
		// the first basic code points to the output.

		basic = input.lastIndexOf(delimiter);
		if (basic < 0) {
			basic = 0;
		}

		for (j = 0; j < basic; ++j) {
			// if it's not a basic code point
			if (input.charCodeAt(j) >= 0x80) {
				error('not-basic');
			}
			output.push(input.charCodeAt(j));
		}

		// Main decoding loop: start just after the last delimiter if any basic code
		// points were copied; start at the beginning otherwise.

		for (index = basic > 0 ? basic + 1 : 0; index < inputLength; /* no final expression */) {

			// `index` is the index of the next character to be consumed.
			// Decode a generalized variable-length integer into `delta`,
			// which gets added to `i`. The overflow checking is easier
			// if we increase `i` as we go, then subtract off its starting
			// value at the end to obtain `delta`.
			for (oldi = i, w = 1, k = base; /* no condition */; k += base) {

				if (index >= inputLength) {
					error('invalid-input');
				}

				digit = basicToDigit(input.charCodeAt(index++));

				if (digit >= base || digit > floor((maxInt - i) / w)) {
					error('overflow');
				}

				i += digit * w;
				t = k <= bias ? tMin : (k >= bias + tMax ? tMax : k - bias);

				if (digit < t) {
					break;
				}

				baseMinusT = base - t;
				if (w > floor(maxInt / baseMinusT)) {
					error('overflow');
				}

				w *= baseMinusT;

			}

			out = output.length + 1;
			bias = adapt(i - oldi, out, oldi == 0);

			// `i` was supposed to wrap around from `out` to `0`,
			// incrementing `n` each time, so we'll fix that now:
			if (floor(i / out) > maxInt - n) {
				error('overflow');
			}

			n += floor(i / out);
			i %= out;

			// Insert `n` at position `i` of the output
			output.splice(i++, 0, n);

		}

		return ucs2encode(output);
	}

	/**
	 * Converts a string of Unicode symbols (e.g. a domain name label) to a
	 * Punycode string of ASCII-only symbols.
	 * @memberOf punycode
	 * @param {String} input The string of Unicode symbols.
	 * @returns {String} The resulting Punycode string of ASCII-only symbols.
	 */
	function encode(input) {
		var n,
		    delta,
		    handledCPCount,
		    basicLength,
		    bias,
		    j,
		    m,
		    q,
		    k,
		    t,
		    currentValue,
		    output = [],
		    /** `inputLength` will hold the number of code points in `input`. */
		    inputLength,
		    /** Cached calculation results */
		    handledCPCountPlusOne,
		    baseMinusT,
		    qMinusT;

		// Convert the input in UCS-2 to Unicode
		input = ucs2decode(input);

		// Cache the length
		inputLength = input.length;

		// Initialize the state
		n = initialN;
		delta = 0;
		bias = initialBias;

		// Handle the basic code points
		for (j = 0; j < inputLength; ++j) {
			currentValue = input[j];
			if (currentValue < 0x80) {
				output.push(stringFromCharCode(currentValue));
			}
		}

		handledCPCount = basicLength = output.length;

		// `handledCPCount` is the number of code points that have been handled;
		// `basicLength` is the number of basic code points.

		// Finish the basic string - if it is not empty - with a delimiter
		if (basicLength) {
			output.push(delimiter);
		}

		// Main encoding loop:
		while (handledCPCount < inputLength) {

			// All non-basic code points < n have been handled already. Find the next
			// larger one:
			for (m = maxInt, j = 0; j < inputLength; ++j) {
				currentValue = input[j];
				if (currentValue >= n && currentValue < m) {
					m = currentValue;
				}
			}

			// Increase `delta` enough to advance the decoder's <n,i> state to <m,0>,
			// but guard against overflow
			handledCPCountPlusOne = handledCPCount + 1;
			if (m - n > floor((maxInt - delta) / handledCPCountPlusOne)) {
				error('overflow');
			}

			delta += (m - n) * handledCPCountPlusOne;
			n = m;

			for (j = 0; j < inputLength; ++j) {
				currentValue = input[j];

				if (currentValue < n && ++delta > maxInt) {
					error('overflow');
				}

				if (currentValue == n) {
					// Represent delta as a generalized variable-length integer
					for (q = delta, k = base; /* no condition */; k += base) {
						t = k <= bias ? tMin : (k >= bias + tMax ? tMax : k - bias);
						if (q < t) {
							break;
						}
						qMinusT = q - t;
						baseMinusT = base - t;
						output.push(
							stringFromCharCode(digitToBasic(t + qMinusT % baseMinusT, 0))
						);
						q = floor(qMinusT / baseMinusT);
					}

					output.push(stringFromCharCode(digitToBasic(q, 0)));
					bias = adapt(delta, handledCPCountPlusOne, handledCPCount == basicLength);
					delta = 0;
					++handledCPCount;
				}
			}

			++delta;
			++n;

		}
		return output.join('');
	}

	/**
	 * Converts a Punycode string representing a domain name or an email address
	 * to Unicode. Only the Punycoded parts of the input will be converted, i.e.
	 * it doesn't matter if you call it on a string that has already been
	 * converted to Unicode.
	 * @memberOf punycode
	 * @param {String} input The Punycoded domain name or email address to
	 * convert to Unicode.
	 * @returns {String} The Unicode representation of the given Punycode
	 * string.
	 */
	function toUnicode(input) {
		return mapDomain(input, function(string) {
			return regexPunycode.test(string)
				? decode(string.slice(4).toLowerCase())
				: string;
		});
	}

	/**
	 * Converts a Unicode string representing a domain name or an email address to
	 * Punycode. Only the non-ASCII parts of the domain name will be converted,
	 * i.e. it doesn't matter if you call it with a domain that's already in
	 * ASCII.
	 * @memberOf punycode
	 * @param {String} input The domain name or email address to convert, as a
	 * Unicode string.
	 * @returns {String} The Punycode representation of the given domain name or
	 * email address.
	 */
	function toASCII(input) {
		return mapDomain(input, function(string) {
			return regexNonASCII.test(string)
				? 'xn--' + encode(string)
				: string;
		});
	}

	/*--------------------------------------------------------------------------*/

	/** Define the public API */
	punycode = {
		/**
		 * A string representing the current Punycode.js version number.
		 * @memberOf punycode
		 * @type String
		 */
		'version': '1.4.1',
		/**
		 * An object of methods to convert from JavaScript's internal character
		 * representation (UCS-2) to Unicode code points, and back.
		 * @see <https://mathiasbynens.be/notes/javascript-encoding>
		 * @memberOf punycode
		 * @type Object
		 */
		'ucs2': {
			'decode': ucs2decode,
			'encode': ucs2encode
		},
		'decode': decode,
		'encode': encode,
		'toASCII': toASCII,
		'toUnicode': toUnicode
	};

	/** Expose `punycode` */
	// Some AMD build optimizers, like r.js, check for specific condition patterns
	// like the following:
	if (
		typeof define == 'function' &&
		typeof define.amd == 'object' &&
		define.amd
	) {
		define('punycode', function() {
			return punycode;
		});
	} else if (freeExports && freeModule) {
		if (module.exports == freeExports) {
			// in Node.js, io.js, or RingoJS v0.8.0+
			freeModule.exports = punycode;
		} else {
			// in Narwhal or RingoJS v0.7.0-
			for (key in punycode) {
				punycode.hasOwnProperty(key) && (freeExports[key] = punycode[key]);
			}
		}
	} else {
		// in Rhino or a web browser
		root.punycode = punycode;
	}

}(this));

});
return ___scope___.entry = "punycode.js";
});
FuseBox.pkg("seedrandom", {}, function(___scope___){
___scope___.file("index.js", function(exports, require, module, __filename, __dirname){

// A library of seedable RNGs implemented in Javascript.
//
// Usage:
//
// var seedrandom = require('seedrandom');
// var random = seedrandom(1); // or any seed.
// var x = random();       // 0 <= x < 1.  Every bit is random.
// var x = random.quick(); // 0 <= x < 1.  32 bits of randomness.

// alea, a 53-bit multiply-with-carry generator by Johannes Baagøe.
// Period: ~2^116
// Reported to pass all BigCrush tests.
var alea = require('./lib/alea');

// xor128, a pure xor-shift generator by George Marsaglia.
// Period: 2^128-1.
// Reported to fail: MatrixRank and LinearComp.
var xor128 = require('./lib/xor128');

// xorwow, George Marsaglia's 160-bit xor-shift combined plus weyl.
// Period: 2^192-2^32
// Reported to fail: CollisionOver, SimpPoker, and LinearComp.
var xorwow = require('./lib/xorwow');

// xorshift7, by François Panneton and Pierre L'ecuyer, takes
// a different approach: it adds robustness by allowing more shifts
// than Marsaglia's original three.  It is a 7-shift generator
// with 256 bits, that passes BigCrush with no systmatic failures.
// Period 2^256-1.
// No systematic BigCrush failures reported.
var xorshift7 = require('./lib/xorshift7');

// xor4096, by Richard Brent, is a 4096-bit xor-shift with a
// very long period that also adds a Weyl generator. It also passes
// BigCrush with no systematic failures.  Its long period may
// be useful if you have many generators and need to avoid
// collisions.
// Period: 2^4128-2^32.
// No systematic BigCrush failures reported.
var xor4096 = require('./lib/xor4096');

// Tyche-i, by Samuel Neves and Filipe Araujo, is a bit-shifting random
// number generator derived from ChaCha, a modern stream cipher.
// https://eden.dei.uc.pt/~sneves/pubs/2011-snfa2.pdf
// Period: ~2^127
// No systematic BigCrush failures reported.
var tychei = require('./lib/tychei');

// The original ARC4-based prng included in this library.
// Period: ~2^1600
var sr = require('./seedrandom');

sr.alea = alea;
sr.xor128 = xor128;
sr.xorwow = xorwow;
sr.xorshift7 = xorshift7;
sr.xor4096 = xor4096;
sr.tychei = tychei;

module.exports = sr;

});
___scope___.file("lib/alea.js", function(exports, require, module, __filename, __dirname){

// A port of an algorithm by Johannes Baagøe <baagoe@baagoe.com>, 2010
// http://baagoe.com/en/RandomMusings/javascript/
// https://github.com/nquinlan/better-random-numbers-for-javascript-mirror
// Original work is under MIT license -

// Copyright (C) 2010 by Johannes Baagøe <baagoe@baagoe.org>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.



(function(global, module, define) {

function Alea(seed) {
  var me = this, mash = Mash();

  me.next = function() {
    var t = 2091639 * me.s0 + me.c * 2.3283064365386963e-10; // 2^-32
    me.s0 = me.s1;
    me.s1 = me.s2;
    return me.s2 = t - (me.c = t | 0);
  };

  // Apply the seeding algorithm from Baagoe.
  me.c = 1;
  me.s0 = mash(' ');
  me.s1 = mash(' ');
  me.s2 = mash(' ');
  me.s0 -= mash(seed);
  if (me.s0 < 0) { me.s0 += 1; }
  me.s1 -= mash(seed);
  if (me.s1 < 0) { me.s1 += 1; }
  me.s2 -= mash(seed);
  if (me.s2 < 0) { me.s2 += 1; }
  mash = null;
}

function copy(f, t) {
  t.c = f.c;
  t.s0 = f.s0;
  t.s1 = f.s1;
  t.s2 = f.s2;
  return t;
}

function impl(seed, opts) {
  var xg = new Alea(seed),
      state = opts && opts.state,
      prng = xg.next;
  prng.int32 = function() { return (xg.next() * 0x100000000) | 0; }
  prng.double = function() {
    return prng() + (prng() * 0x200000 | 0) * 1.1102230246251565e-16; // 2^-53
  };
  prng.quick = prng;
  if (state) {
    if (typeof(state) == 'object') copy(state, xg);
    prng.state = function() { return copy(xg, {}); }
  }
  return prng;
}

function Mash() {
  var n = 0xefc8249d;

  var mash = function(data) {
    data = data.toString();
    for (var i = 0; i < data.length; i++) {
      n += data.charCodeAt(i);
      var h = 0.02519603282416938 * n;
      n = h >>> 0;
      h -= n;
      h *= n;
      n = h >>> 0;
      h -= n;
      n += h * 0x100000000; // 2^32
    }
    return (n >>> 0) * 2.3283064365386963e-10; // 2^-32
  };

  return mash;
}


if (module && module.exports) {
  module.exports = impl;
} else if (define && define.amd) {
  define(function() { return impl; });
} else {
  this.alea = impl;
}

})(
  this,
  (typeof module) == 'object' && module,    // present in node.js
  (typeof define) == 'function' && define   // present with an AMD loader
);



});
___scope___.file("lib/xor128.js", function(exports, require, module, __filename, __dirname){

// A Javascript implementaion of the "xor128" prng algorithm by
// George Marsaglia.  See http://www.jstatsoft.org/v08/i14/paper

(function(global, module, define) {

function XorGen(seed) {
  var me = this, strseed = '';

  me.x = 0;
  me.y = 0;
  me.z = 0;
  me.w = 0;

  // Set up generator function.
  me.next = function() {
    var t = me.x ^ (me.x << 11);
    me.x = me.y;
    me.y = me.z;
    me.z = me.w;
    return me.w ^= (me.w >>> 19) ^ t ^ (t >>> 8);
  };

  if (seed === (seed | 0)) {
    // Integer seed.
    me.x = seed;
  } else {
    // String seed.
    strseed += seed;
  }

  // Mix in string seed, then discard an initial batch of 64 values.
  for (var k = 0; k < strseed.length + 64; k++) {
    me.x ^= strseed.charCodeAt(k) | 0;
    me.next();
  }
}

function copy(f, t) {
  t.x = f.x;
  t.y = f.y;
  t.z = f.z;
  t.w = f.w;
  return t;
}

function impl(seed, opts) {
  var xg = new XorGen(seed),
      state = opts && opts.state,
      prng = function() { return (xg.next() >>> 0) / 0x100000000; };
  prng.double = function() {
    do {
      var top = xg.next() >>> 11,
          bot = (xg.next() >>> 0) / 0x100000000,
          result = (top + bot) / (1 << 21);
    } while (result === 0);
    return result;
  };
  prng.int32 = xg.next;
  prng.quick = prng;
  if (state) {
    if (typeof(state) == 'object') copy(state, xg);
    prng.state = function() { return copy(xg, {}); }
  }
  return prng;
}

if (module && module.exports) {
  module.exports = impl;
} else if (define && define.amd) {
  define(function() { return impl; });
} else {
  this.xor128 = impl;
}

})(
  this,
  (typeof module) == 'object' && module,    // present in node.js
  (typeof define) == 'function' && define   // present with an AMD loader
);



});
___scope___.file("lib/xorwow.js", function(exports, require, module, __filename, __dirname){

// A Javascript implementaion of the "xorwow" prng algorithm by
// George Marsaglia.  See http://www.jstatsoft.org/v08/i14/paper

(function(global, module, define) {

function XorGen(seed) {
  var me = this, strseed = '';

  // Set up generator function.
  me.next = function() {
    var t = (me.x ^ (me.x >>> 2));
    me.x = me.y; me.y = me.z; me.z = me.w; me.w = me.v;
    return (me.d = (me.d + 362437 | 0)) +
       (me.v = (me.v ^ (me.v << 4)) ^ (t ^ (t << 1))) | 0;
  };

  me.x = 0;
  me.y = 0;
  me.z = 0;
  me.w = 0;
  me.v = 0;

  if (seed === (seed | 0)) {
    // Integer seed.
    me.x = seed;
  } else {
    // String seed.
    strseed += seed;
  }

  // Mix in string seed, then discard an initial batch of 64 values.
  for (var k = 0; k < strseed.length + 64; k++) {
    me.x ^= strseed.charCodeAt(k) | 0;
    if (k == strseed.length) {
      me.d = me.x << 10 ^ me.x >>> 4;
    }
    me.next();
  }
}

function copy(f, t) {
  t.x = f.x;
  t.y = f.y;
  t.z = f.z;
  t.w = f.w;
  t.v = f.v;
  t.d = f.d;
  return t;
}

function impl(seed, opts) {
  var xg = new XorGen(seed),
      state = opts && opts.state,
      prng = function() { return (xg.next() >>> 0) / 0x100000000; };
  prng.double = function() {
    do {
      var top = xg.next() >>> 11,
          bot = (xg.next() >>> 0) / 0x100000000,
          result = (top + bot) / (1 << 21);
    } while (result === 0);
    return result;
  };
  prng.int32 = xg.next;
  prng.quick = prng;
  if (state) {
    if (typeof(state) == 'object') copy(state, xg);
    prng.state = function() { return copy(xg, {}); }
  }
  return prng;
}

if (module && module.exports) {
  module.exports = impl;
} else if (define && define.amd) {
  define(function() { return impl; });
} else {
  this.xorwow = impl;
}

})(
  this,
  (typeof module) == 'object' && module,    // present in node.js
  (typeof define) == 'function' && define   // present with an AMD loader
);



});
___scope___.file("lib/xorshift7.js", function(exports, require, module, __filename, __dirname){

// A Javascript implementaion of the "xorshift7" algorithm by
// François Panneton and Pierre L'ecuyer:
// "On the Xorgshift Random Number Generators"
// http://saluc.engr.uconn.edu/refs/crypto/rng/panneton05onthexorshift.pdf

(function(global, module, define) {

function XorGen(seed) {
  var me = this;

  // Set up generator function.
  me.next = function() {
    // Update xor generator.
    var X = me.x, i = me.i, t, v, w;
    t = X[i]; t ^= (t >>> 7); v = t ^ (t << 24);
    t = X[(i + 1) & 7]; v ^= t ^ (t >>> 10);
    t = X[(i + 3) & 7]; v ^= t ^ (t >>> 3);
    t = X[(i + 4) & 7]; v ^= t ^ (t << 7);
    t = X[(i + 7) & 7]; t = t ^ (t << 13); v ^= t ^ (t << 9);
    X[i] = v;
    me.i = (i + 1) & 7;
    return v;
  };

  function init(me, seed) {
    var j, w, X = [];

    if (seed === (seed | 0)) {
      // Seed state array using a 32-bit integer.
      w = X[0] = seed;
    } else {
      // Seed state using a string.
      seed = '' + seed;
      for (j = 0; j < seed.length; ++j) {
        X[j & 7] = (X[j & 7] << 15) ^
            (seed.charCodeAt(j) + X[(j + 1) & 7] << 13);
      }
    }
    // Enforce an array length of 8, not all zeroes.
    while (X.length < 8) X.push(0);
    for (j = 0; j < 8 && X[j] === 0; ++j);
    if (j == 8) w = X[7] = -1; else w = X[j];

    me.x = X;
    me.i = 0;

    // Discard an initial 256 values.
    for (j = 256; j > 0; --j) {
      me.next();
    }
  }

  init(me, seed);
}

function copy(f, t) {
  t.x = f.x.slice();
  t.i = f.i;
  return t;
}

function impl(seed, opts) {
  if (seed == null) seed = +(new Date);
  var xg = new XorGen(seed),
      state = opts && opts.state,
      prng = function() { return (xg.next() >>> 0) / 0x100000000; };
  prng.double = function() {
    do {
      var top = xg.next() >>> 11,
          bot = (xg.next() >>> 0) / 0x100000000,
          result = (top + bot) / (1 << 21);
    } while (result === 0);
    return result;
  };
  prng.int32 = xg.next;
  prng.quick = prng;
  if (state) {
    if (state.x) copy(state, xg);
    prng.state = function() { return copy(xg, {}); }
  }
  return prng;
}

if (module && module.exports) {
  module.exports = impl;
} else if (define && define.amd) {
  define(function() { return impl; });
} else {
  this.xorshift7 = impl;
}

})(
  this,
  (typeof module) == 'object' && module,    // present in node.js
  (typeof define) == 'function' && define   // present with an AMD loader
);


});
___scope___.file("lib/xor4096.js", function(exports, require, module, __filename, __dirname){

// A Javascript implementaion of Richard Brent's Xorgens xor4096 algorithm.
//
// This fast non-cryptographic random number generator is designed for
// use in Monte-Carlo algorithms. It combines a long-period xorshift
// generator with a Weyl generator, and it passes all common batteries
// of stasticial tests for randomness while consuming only a few nanoseconds
// for each prng generated.  For background on the generator, see Brent's
// paper: "Some long-period random number generators using shifts and xors."
// http://arxiv.org/pdf/1004.3115v1.pdf
//
// Usage:
//
// var xor4096 = require('xor4096');
// random = xor4096(1);                        // Seed with int32 or string.
// assert.equal(random(), 0.1520436450538547); // (0, 1) range, 53 bits.
// assert.equal(random.int32(), 1806534897);   // signed int32, 32 bits.
//
// For nonzero numeric keys, this impelementation provides a sequence
// identical to that by Brent's xorgens 3 implementaion in C.  This
// implementation also provides for initalizing the generator with
// string seeds, or for saving and restoring the state of the generator.
//
// On Chrome, this prng benchmarks about 2.1 times slower than
// Javascript's built-in Math.random().

(function(global, module, define) {

function XorGen(seed) {
  var me = this;

  // Set up generator function.
  me.next = function() {
    var w = me.w,
        X = me.X, i = me.i, t, v;
    // Update Weyl generator.
    me.w = w = (w + 0x61c88647) | 0;
    // Update xor generator.
    v = X[(i + 34) & 127];
    t = X[i = ((i + 1) & 127)];
    v ^= v << 13;
    t ^= t << 17;
    v ^= v >>> 15;
    t ^= t >>> 12;
    // Update Xor generator array state.
    v = X[i] = v ^ t;
    me.i = i;
    // Result is the combination.
    return (v + (w ^ (w >>> 16))) | 0;
  };

  function init(me, seed) {
    var t, v, i, j, w, X = [], limit = 128;
    if (seed === (seed | 0)) {
      // Numeric seeds initialize v, which is used to generates X.
      v = seed;
      seed = null;
    } else {
      // String seeds are mixed into v and X one character at a time.
      seed = seed + '\0';
      v = 0;
      limit = Math.max(limit, seed.length);
    }
    // Initialize circular array and weyl value.
    for (i = 0, j = -32; j < limit; ++j) {
      // Put the unicode characters into the array, and shuffle them.
      if (seed) v ^= seed.charCodeAt((j + 32) % seed.length);
      // After 32 shuffles, take v as the starting w value.
      if (j === 0) w = v;
      v ^= v << 10;
      v ^= v >>> 15;
      v ^= v << 4;
      v ^= v >>> 13;
      if (j >= 0) {
        w = (w + 0x61c88647) | 0;     // Weyl.
        t = (X[j & 127] ^= (v + w));  // Combine xor and weyl to init array.
        i = (0 == t) ? i + 1 : 0;     // Count zeroes.
      }
    }
    // We have detected all zeroes; make the key nonzero.
    if (i >= 128) {
      X[(seed && seed.length || 0) & 127] = -1;
    }
    // Run the generator 512 times to further mix the state before using it.
    // Factoring this as a function slows the main generator, so it is just
    // unrolled here.  The weyl generator is not advanced while warming up.
    i = 127;
    for (j = 4 * 128; j > 0; --j) {
      v = X[(i + 34) & 127];
      t = X[i = ((i + 1) & 127)];
      v ^= v << 13;
      t ^= t << 17;
      v ^= v >>> 15;
      t ^= t >>> 12;
      X[i] = v ^ t;
    }
    // Storing state as object members is faster than using closure variables.
    me.w = w;
    me.X = X;
    me.i = i;
  }

  init(me, seed);
}

function copy(f, t) {
  t.i = f.i;
  t.w = f.w;
  t.X = f.X.slice();
  return t;
};

function impl(seed, opts) {
  if (seed == null) seed = +(new Date);
  var xg = new XorGen(seed),
      state = opts && opts.state,
      prng = function() { return (xg.next() >>> 0) / 0x100000000; };
  prng.double = function() {
    do {
      var top = xg.next() >>> 11,
          bot = (xg.next() >>> 0) / 0x100000000,
          result = (top + bot) / (1 << 21);
    } while (result === 0);
    return result;
  };
  prng.int32 = xg.next;
  prng.quick = prng;
  if (state) {
    if (state.X) copy(state, xg);
    prng.state = function() { return copy(xg, {}); }
  }
  return prng;
}

if (module && module.exports) {
  module.exports = impl;
} else if (define && define.amd) {
  define(function() { return impl; });
} else {
  this.xor4096 = impl;
}

})(
  this,                                     // window object or global
  (typeof module) == 'object' && module,    // present in node.js
  (typeof define) == 'function' && define   // present with an AMD loader
);

});
___scope___.file("lib/tychei.js", function(exports, require, module, __filename, __dirname){

// A Javascript implementaion of the "Tyche-i" prng algorithm by
// Samuel Neves and Filipe Araujo.
// See https://eden.dei.uc.pt/~sneves/pubs/2011-snfa2.pdf

(function(global, module, define) {

function XorGen(seed) {
  var me = this, strseed = '';

  // Set up generator function.
  me.next = function() {
    var b = me.b, c = me.c, d = me.d, a = me.a;
    b = (b << 25) ^ (b >>> 7) ^ c;
    c = (c - d) | 0;
    d = (d << 24) ^ (d >>> 8) ^ a;
    a = (a - b) | 0;
    me.b = b = (b << 20) ^ (b >>> 12) ^ c;
    me.c = c = (c - d) | 0;
    me.d = (d << 16) ^ (c >>> 16) ^ a;
    return me.a = (a - b) | 0;
  };

  /* The following is non-inverted tyche, which has better internal
   * bit diffusion, but which is about 25% slower than tyche-i in JS.
  me.next = function() {
    var a = me.a, b = me.b, c = me.c, d = me.d;
    a = (me.a + me.b | 0) >>> 0;
    d = me.d ^ a; d = d << 16 ^ d >>> 16;
    c = me.c + d | 0;
    b = me.b ^ c; b = b << 12 ^ d >>> 20;
    me.a = a = a + b | 0;
    d = d ^ a; me.d = d = d << 8 ^ d >>> 24;
    me.c = c = c + d | 0;
    b = b ^ c;
    return me.b = (b << 7 ^ b >>> 25);
  }
  */

  me.a = 0;
  me.b = 0;
  me.c = 2654435769 | 0;
  me.d = 1367130551;

  if (seed === Math.floor(seed)) {
    // Integer seed.
    me.a = (seed / 0x100000000) | 0;
    me.b = seed | 0;
  } else {
    // String seed.
    strseed += seed;
  }

  // Mix in string seed, then discard an initial batch of 64 values.
  for (var k = 0; k < strseed.length + 20; k++) {
    me.b ^= strseed.charCodeAt(k) | 0;
    me.next();
  }
}

function copy(f, t) {
  t.a = f.a;
  t.b = f.b;
  t.c = f.c;
  t.d = f.d;
  return t;
};

function impl(seed, opts) {
  var xg = new XorGen(seed),
      state = opts && opts.state,
      prng = function() { return (xg.next() >>> 0) / 0x100000000; };
  prng.double = function() {
    do {
      var top = xg.next() >>> 11,
          bot = (xg.next() >>> 0) / 0x100000000,
          result = (top + bot) / (1 << 21);
    } while (result === 0);
    return result;
  };
  prng.int32 = xg.next;
  prng.quick = prng;
  if (state) {
    if (typeof(state) == 'object') copy(state, xg);
    prng.state = function() { return copy(xg, {}); }
  }
  return prng;
}

if (module && module.exports) {
  module.exports = impl;
} else if (define && define.amd) {
  define(function() { return impl; });
} else {
  this.tychei = impl;
}

})(
  this,
  (typeof module) == 'object' && module,    // present in node.js
  (typeof define) == 'function' && define   // present with an AMD loader
);



});
___scope___.file("seedrandom.js", function(exports, require, module, __filename, __dirname){

/*
Copyright 2014 David Bau.

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

(function (pool, math) {
//
// The following constants are related to IEEE 754 limits.
//

// Detect the global object, even if operating in strict mode.
// http://stackoverflow.com/a/14387057/265298
var global = (0, eval)('this'),
    width = 256,        // each RC4 output is 0 <= x < 256
    chunks = 6,         // at least six RC4 outputs for each double
    digits = 52,        // there are 52 significant digits in a double
    rngname = 'random', // rngname: name for Math.random and Math.seedrandom
    startdenom = math.pow(width, chunks),
    significance = math.pow(2, digits),
    overflow = significance * 2,
    mask = width - 1,
    nodecrypto;         // node.js crypto module, initialized at the bottom.

//
// seedrandom()
// This is the seedrandom function described above.
//
function seedrandom(seed, options, callback) {
  var key = [];
  options = (options == true) ? { entropy: true } : (options || {});

  // Flatten the seed string or build one from local entropy if needed.
  var shortseed = mixkey(flatten(
    options.entropy ? [seed, tostring(pool)] :
    (seed == null) ? autoseed() : seed, 3), key);

  // Use the seed to initialize an ARC4 generator.
  var arc4 = new ARC4(key);

  // This function returns a random double in [0, 1) that contains
  // randomness in every bit of the mantissa of the IEEE 754 value.
  var prng = function() {
    var n = arc4.g(chunks),             // Start with a numerator n < 2 ^ 48
        d = startdenom,                 //   and denominator d = 2 ^ 48.
        x = 0;                          //   and no 'extra last byte'.
    while (n < significance) {          // Fill up all significant digits by
      n = (n + x) * width;              //   shifting numerator and
      d *= width;                       //   denominator and generating a
      x = arc4.g(1);                    //   new least-significant-byte.
    }
    while (n >= overflow) {             // To avoid rounding up, before adding
      n /= 2;                           //   last byte, shift everything
      d /= 2;                           //   right using integer math until
      x >>>= 1;                         //   we have exactly the desired bits.
    }
    return (n + x) / d;                 // Form the number within [0, 1).
  };

  prng.int32 = function() { return arc4.g(4) | 0; }
  prng.quick = function() { return arc4.g(4) / 0x100000000; }
  prng.double = prng;

  // Mix the randomness into accumulated entropy.
  mixkey(tostring(arc4.S), pool);

  // Calling convention: what to return as a function of prng, seed, is_math.
  return (options.pass || callback ||
      function(prng, seed, is_math_call, state) {
        if (state) {
          // Load the arc4 state from the given state if it has an S array.
          if (state.S) { copy(state, arc4); }
          // Only provide the .state method if requested via options.state.
          prng.state = function() { return copy(arc4, {}); }
        }

        // If called as a method of Math (Math.seedrandom()), mutate
        // Math.random because that is how seedrandom.js has worked since v1.0.
        if (is_math_call) { math[rngname] = prng; return seed; }

        // Otherwise, it is a newer calling convention, so return the
        // prng directly.
        else return prng;
      })(
  prng,
  shortseed,
  'global' in options ? options.global : (this == math),
  options.state);
}
math['seed' + rngname] = seedrandom;

//
// ARC4
//
// An ARC4 implementation.  The constructor takes a key in the form of
// an array of at most (width) integers that should be 0 <= x < (width).
//
// The g(count) method returns a pseudorandom integer that concatenates
// the next (count) outputs from ARC4.  Its return value is a number x
// that is in the range 0 <= x < (width ^ count).
//
function ARC4(key) {
  var t, keylen = key.length,
      me = this, i = 0, j = me.i = me.j = 0, s = me.S = [];

  // The empty key [] is treated as [0].
  if (!keylen) { key = [keylen++]; }

  // Set up S using the standard key scheduling algorithm.
  while (i < width) {
    s[i] = i++;
  }
  for (i = 0; i < width; i++) {
    s[i] = s[j = mask & (j + key[i % keylen] + (t = s[i]))];
    s[j] = t;
  }

  // The "g" method returns the next (count) outputs as one number.
  (me.g = function(count) {
    // Using instance members instead of closure state nearly doubles speed.
    var t, r = 0,
        i = me.i, j = me.j, s = me.S;
    while (count--) {
      t = s[i = mask & (i + 1)];
      r = r * width + s[mask & ((s[i] = s[j = mask & (j + t)]) + (s[j] = t))];
    }
    me.i = i; me.j = j;
    return r;
    // For robust unpredictability, the function call below automatically
    // discards an initial batch of values.  This is called RC4-drop[256].
    // See http://google.com/search?q=rsa+fluhrer+response&btnI
  })(width);
}

//
// copy()
// Copies internal state of ARC4 to or from a plain object.
//
function copy(f, t) {
  t.i = f.i;
  t.j = f.j;
  t.S = f.S.slice();
  return t;
};

//
// flatten()
// Converts an object tree to nested arrays of strings.
//
function flatten(obj, depth) {
  var result = [], typ = (typeof obj), prop;
  if (depth && typ == 'object') {
    for (prop in obj) {
      try { result.push(flatten(obj[prop], depth - 1)); } catch (e) {}
    }
  }
  return (result.length ? result : typ == 'string' ? obj : obj + '\0');
}

//
// mixkey()
// Mixes a string seed into a key that is an array of integers, and
// returns a shortened string seed that is equivalent to the result key.
//
function mixkey(seed, key) {
  var stringseed = seed + '', smear, j = 0;
  while (j < stringseed.length) {
    key[mask & j] =
      mask & ((smear ^= key[mask & j] * 19) + stringseed.charCodeAt(j++));
  }
  return tostring(key);
}

//
// autoseed()
// Returns an object for autoseeding, using window.crypto and Node crypto
// module if available.
//
function autoseed() {
  try {
    var out;
    if (nodecrypto && (out = nodecrypto.randomBytes)) {
      // The use of 'out' to remember randomBytes makes tight minified code.
      out = out(width);
    } else {
      out = new Uint8Array(width);
      (global.crypto || global.msCrypto).getRandomValues(out);
    }
    return tostring(out);
  } catch (e) {
    var browser = global.navigator,
        plugins = browser && browser.plugins;
    return [+new Date, global, plugins, global.screen, tostring(pool)];
  }
}

//
// tostring()
// Converts an array of charcodes to a string
//
function tostring(a) {
  return String.fromCharCode.apply(0, a);
}

//
// When seedrandom.js is loaded, we immediately mix a few bits
// from the built-in RNG into the entropy pool.  Because we do
// not want to interfere with deterministic PRNG state later,
// seedrandom will not call math.random on its own again after
// initialization.
//
mixkey(math.random(), pool);

//
// Nodejs and AMD support: export the implementation as a module using
// either convention.
//
if ((typeof module) == 'object' && module.exports) {
  module.exports = seedrandom;
  // When in node.js, try using crypto package for autoseeding.
  try {
    nodecrypto = require('crypto');
  } catch (ex) {}
} else if ((typeof define) == 'function' && define.amd) {
  define(function() { return seedrandom; });
}

// End anonymous scope, and pass initial values.
})(
  [],     // pool: entropy pool starts empty
  Math    // math: package containing random, pow, and seedrandom
);

});
return ___scope___.entry = "index.js";
});
FuseBox.pkg("tslib", {}, function(___scope___){
___scope___.file("tslib.js", function(exports, require, module, __filename, __dirname){

/*! *****************************************************************************
Copyright (c) Microsoft Corporation. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at http://www.apache.org/licenses/LICENSE-2.0

THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
MERCHANTABLITY OR NON-INFRINGEMENT.

See the Apache Version 2.0 License for specific language governing permissions
and limitations under the License.
***************************************************************************** */
/* global global, define, System, Reflect, Promise */
var __extends;
var __assign;
var __rest;
var __decorate;
var __param;
var __metadata;
var __awaiter;
var __generator;
var __exportStar;
var __values;
var __read;
var __spread;
var __await;
var __asyncGenerator;
var __asyncDelegator;
var __asyncValues;
var __makeTemplateObject;
var __importStar;
var __importDefault;
(function (factory) {
    var root = typeof global === "object" ? global : typeof self === "object" ? self : typeof this === "object" ? this : {};
    if (typeof define === "function" && define.amd) {
        define("tslib", ["exports"], function (exports) { factory(createExporter(root, createExporter(exports))); });
    }
    else if (typeof module === "object" && typeof module.exports === "object") {
        factory(createExporter(root, createExporter(module.exports)));
    }
    else {
        factory(createExporter(root));
    }
    function createExporter(exports, previous) {
        if (exports !== root) {
            if (typeof Object.create === "function") {
                Object.defineProperty(exports, "__esModule", { value: true });
            }
            else {
                exports.__esModule = true;
            }
        }
        return function (id, v) { return exports[id] = previous ? previous(id, v) : v; };
    }
})
(function (exporter) {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };

    __extends = function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };

    __assign = Object.assign || function (t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    };

    __rest = function (s, e) {
        var t = {};
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
            t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === "function")
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) if (e.indexOf(p[i]) < 0)
                t[p[i]] = s[p[i]];
        return t;
    };

    __decorate = function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    __param = function (paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); }
    };

    __metadata = function (metadataKey, metadataValue) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
    };

    __awaiter = function (thisArg, _arguments, P, generator) {
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
            function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
            function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    };

    __generator = function (thisArg, body) {
        var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
        return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
        function verb(n) { return function (v) { return step([n, v]); }; }
        function step(op) {
            if (f) throw new TypeError("Generator is already executing.");
            while (_) try {
                if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
                if (y = 0, t) op = [op[0] & 2, t.value];
                switch (op[0]) {
                    case 0: case 1: t = op; break;
                    case 4: _.label++; return { value: op[1], done: false };
                    case 5: _.label++; y = op[1]; op = [0]; continue;
                    case 7: op = _.ops.pop(); _.trys.pop(); continue;
                    default:
                        if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                        if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                        if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                        if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                        if (t[2]) _.ops.pop();
                        _.trys.pop(); continue;
                }
                op = body.call(thisArg, _);
            } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
            if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
        }
    };

    __exportStar = function (m, exports) {
        for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
    };

    __values = function (o) {
        var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
        if (m) return m.call(o);
        return {
            next: function () {
                if (o && i >= o.length) o = void 0;
                return { value: o && o[i++], done: !o };
            }
        };
    };

    __read = function (o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m) return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
        }
        catch (error) { e = { error: error }; }
        finally {
            try {
                if (r && !r.done && (m = i["return"])) m.call(i);
            }
            finally { if (e) throw e.error; }
        }
        return ar;
    };

    __spread = function () {
        for (var ar = [], i = 0; i < arguments.length; i++)
            ar = ar.concat(__read(arguments[i]));
        return ar;
    };

    __await = function (v) {
        return this instanceof __await ? (this.v = v, this) : new __await(v);
    };

    __asyncGenerator = function (thisArg, _arguments, generator) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var g = generator.apply(thisArg, _arguments || []), i, q = [];
        return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
        function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
        function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
        function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r);  }
        function fulfill(value) { resume("next", value); }
        function reject(value) { resume("throw", value); }
        function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
    };

    __asyncDelegator = function (o) {
        var i, p;
        return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
        function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
    };

    __asyncValues = function (o) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var m = o[Symbol.asyncIterator], i;
        return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
        function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
        function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
    };

    __makeTemplateObject = function (cooked, raw) {
        if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
        return cooked;
    };

    __importStar = function (mod) {
        if (mod && mod.__esModule) return mod;
        var result = {};
        if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
        result["default"] = mod;
        return result;
    };

    __importDefault = function (mod) {
        return (mod && mod.__esModule) ? mod : { "default": mod };
    };

    exporter("__extends", __extends);
    exporter("__assign", __assign);
    exporter("__rest", __rest);
    exporter("__decorate", __decorate);
    exporter("__param", __param);
    exporter("__metadata", __metadata);
    exporter("__awaiter", __awaiter);
    exporter("__generator", __generator);
    exporter("__exportStar", __exportStar);
    exporter("__values", __values);
    exporter("__read", __read);
    exporter("__spread", __spread);
    exporter("__await", __await);
    exporter("__asyncGenerator", __asyncGenerator);
    exporter("__asyncDelegator", __asyncDelegator);
    exporter("__asyncValues", __asyncValues);
    exporter("__makeTemplateObject", __makeTemplateObject);
    exporter("__importStar", __importStar);
    exporter("__importDefault", __importDefault);
});

});
return ___scope___.entry = "tslib.js";
});
FuseBox.pkg("uc.micro", {}, function(___scope___){
___scope___.file("categories/P/regex.js", function(exports, require, module, __filename, __dirname){

module.exports=/[!-#%-\*,-/:;\?@\[-\]_\{\}\xA1\xA7\xAB\xB6\xB7\xBB\xBF\u037E\u0387\u055A-\u055F\u0589\u058A\u05BE\u05C0\u05C3\u05C6\u05F3\u05F4\u0609\u060A\u060C\u060D\u061B\u061E\u061F\u066A-\u066D\u06D4\u0700-\u070D\u07F7-\u07F9\u0830-\u083E\u085E\u0964\u0965\u0970\u09FD\u0AF0\u0DF4\u0E4F\u0E5A\u0E5B\u0F04-\u0F12\u0F14\u0F3A-\u0F3D\u0F85\u0FD0-\u0FD4\u0FD9\u0FDA\u104A-\u104F\u10FB\u1360-\u1368\u1400\u166D\u166E\u169B\u169C\u16EB-\u16ED\u1735\u1736\u17D4-\u17D6\u17D8-\u17DA\u1800-\u180A\u1944\u1945\u1A1E\u1A1F\u1AA0-\u1AA6\u1AA8-\u1AAD\u1B5A-\u1B60\u1BFC-\u1BFF\u1C3B-\u1C3F\u1C7E\u1C7F\u1CC0-\u1CC7\u1CD3\u2010-\u2027\u2030-\u2043\u2045-\u2051\u2053-\u205E\u207D\u207E\u208D\u208E\u2308-\u230B\u2329\u232A\u2768-\u2775\u27C5\u27C6\u27E6-\u27EF\u2983-\u2998\u29D8-\u29DB\u29FC\u29FD\u2CF9-\u2CFC\u2CFE\u2CFF\u2D70\u2E00-\u2E2E\u2E30-\u2E49\u3001-\u3003\u3008-\u3011\u3014-\u301F\u3030\u303D\u30A0\u30FB\uA4FE\uA4FF\uA60D-\uA60F\uA673\uA67E\uA6F2-\uA6F7\uA874-\uA877\uA8CE\uA8CF\uA8F8-\uA8FA\uA8FC\uA92E\uA92F\uA95F\uA9C1-\uA9CD\uA9DE\uA9DF\uAA5C-\uAA5F\uAADE\uAADF\uAAF0\uAAF1\uABEB\uFD3E\uFD3F\uFE10-\uFE19\uFE30-\uFE52\uFE54-\uFE61\uFE63\uFE68\uFE6A\uFE6B\uFF01-\uFF03\uFF05-\uFF0A\uFF0C-\uFF0F\uFF1A\uFF1B\uFF1F\uFF20\uFF3B-\uFF3D\uFF3F\uFF5B\uFF5D\uFF5F-\uFF65]|\uD800[\uDD00-\uDD02\uDF9F\uDFD0]|\uD801\uDD6F|\uD802[\uDC57\uDD1F\uDD3F\uDE50-\uDE58\uDE7F\uDEF0-\uDEF6\uDF39-\uDF3F\uDF99-\uDF9C]|\uD804[\uDC47-\uDC4D\uDCBB\uDCBC\uDCBE-\uDCC1\uDD40-\uDD43\uDD74\uDD75\uDDC5-\uDDC9\uDDCD\uDDDB\uDDDD-\uDDDF\uDE38-\uDE3D\uDEA9]|\uD805[\uDC4B-\uDC4F\uDC5B\uDC5D\uDCC6\uDDC1-\uDDD7\uDE41-\uDE43\uDE60-\uDE6C\uDF3C-\uDF3E]|\uD806[\uDE3F-\uDE46\uDE9A-\uDE9C\uDE9E-\uDEA2]|\uD807[\uDC41-\uDC45\uDC70\uDC71]|\uD809[\uDC70-\uDC74]|\uD81A[\uDE6E\uDE6F\uDEF5\uDF37-\uDF3B\uDF44]|\uD82F\uDC9F|\uD836[\uDE87-\uDE8B]|\uD83A[\uDD5E\uDD5F]/
});
___scope___.file("index.js", function(exports, require, module, __filename, __dirname){

'use strict';

exports.Any = require('./properties/Any/regex');
exports.Cc  = require('./categories/Cc/regex');
exports.Cf  = require('./categories/Cf/regex');
exports.P   = require('./categories/P/regex');
exports.Z   = require('./categories/Z/regex');

});
___scope___.file("properties/Any/regex.js", function(exports, require, module, __filename, __dirname){

module.exports=/[\0-\uD7FF\uE000-\uFFFF]|[\uD800-\uDBFF][\uDC00-\uDFFF]|[\uD800-\uDBFF](?![\uDC00-\uDFFF])|(?:[^\uD800-\uDBFF]|^)[\uDC00-\uDFFF]/
});
___scope___.file("categories/Cc/regex.js", function(exports, require, module, __filename, __dirname){

module.exports=/[\0-\x1F\x7F-\x9F]/
});
___scope___.file("categories/Cf/regex.js", function(exports, require, module, __filename, __dirname){

module.exports=/[\xAD\u0600-\u0605\u061C\u06DD\u070F\u08E2\u180E\u200B-\u200F\u202A-\u202E\u2060-\u2064\u2066-\u206F\uFEFF\uFFF9-\uFFFB]|\uD804\uDCBD|\uD82F[\uDCA0-\uDCA3]|\uD834[\uDD73-\uDD7A]|\uDB40[\uDC01\uDC20-\uDC7F]/
});
___scope___.file("categories/Z/regex.js", function(exports, require, module, __filename, __dirname){

module.exports=/[ \xA0\u1680\u2000-\u200A\u202F\u205F\u3000]/
});
return ___scope___.entry = "index.js";
});
FuseBox.pkg("vanilla-observables", {}, function(___scope___){
___scope___.file("src/index.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Observables_1 = require("./Observables");
exports.default = Observables_1.default;
var BasicObservableClass_1 = require("./BasicObservableClass");
exports.BasicObservableClass = BasicObservableClass_1.default;
//# sourceMappingURL=index.js.map
});
___scope___.file("src/Observables.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Observables {
    constructor() {
        this.map = new Map();
        this.observers = new Map();
    }
    get(propName) {
        return this.map.get(propName);
    }
    set(propName, value) {
        this.map.set(propName, value);
        const observers = this.observers.get(propName);
        if (observers) {
            observers.forEach((cb) => cb(value));
        }
    }
    observe(propName, cb) {
        cb(this.get(propName));
        this.onChange(propName, cb);
    }
    onChange(propName, cb) {
        if (this.observers.has(propName) === false) {
            this.observers.set(propName, []);
        }
        this.observers.get(propName).push(cb);
    }
}
exports.default = Observables;
//# sourceMappingURL=Observables.js.map
});
___scope___.file("src/BasicObservableClass.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Observables_1 = require("./Observables");
const createObservableWrapper_1 = require("./createObservableWrapper");
// type RestrictedStyleAttribute = "color" | "background-color" | "font-weight";
// type IAllPropsObject<PROP extends string> = {
//   [T in PROP]: any;
// }
class BasicObservableClass {
    constructor() {
        this.observables = new Observables_1.default();
    }
    observable(propName, perms) {
        return createObservableWrapper_1.default(propName, this.observables, perms);
    }
}
exports.default = BasicObservableClass;
//# sourceMappingURL=BasicObservableClass.js.map
});
___scope___.file("src/createObservableWrapper.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function createObservableWrapper(propName, observables, permissions = "rwo") {
    const observableWrapper = () => {
        return observables.get(propName);
    };
    if (permissions.indexOf("w") !== -1) {
        observableWrapper.set = (newVal) => {
            observables.set(propName, newVal);
            return newVal;
        };
    }
    if (permissions.indexOf("o") !== -1) {
        observableWrapper.observe = (cb) => {
            return observables.observe(propName, cb);
        };
    }
    return observableWrapper;
}
exports.default = createObservableWrapper;
//# sourceMappingURL=createObservableWrapper.js.map
});
return ___scope___.entry = "src/index.js";
});
FuseBox.import("fusebox-hot-reload").connect(4444, "", false)

FuseBox.import("default/index.js");
FuseBox.main("default/index.js");
})
(function(e){function r(e){var r=e.charCodeAt(0),n=e.charCodeAt(1);if((m||58!==n)&&(r>=97&&r<=122||64===r)){if(64===r){var t=e.split("/"),i=t.splice(2,t.length).join("/");return[t[0]+"/"+t[1],i||void 0]}var o=e.indexOf("/");if(o===-1)return[e];var a=e.substring(0,o),f=e.substring(o+1);return[a,f]}}function n(e){return e.substring(0,e.lastIndexOf("/"))||"./"}function t(){for(var e=[],r=0;r<arguments.length;r++)e[r]=arguments[r];for(var n=[],t=0,i=arguments.length;t<i;t++)n=n.concat(arguments[t].split("/"));for(var o=[],t=0,i=n.length;t<i;t++){var a=n[t];a&&"."!==a&&(".."===a?o.pop():o.push(a))}return""===n[0]&&o.unshift(""),o.join("/")||(o.length?"/":".")}function i(e){var r=e.match(/\.(\w{1,})$/);return r&&r[1]?e:e+".js"}function o(e){if(m){var r,n=document,t=n.getElementsByTagName("head")[0];/\.css$/.test(e)?(r=n.createElement("link"),r.rel="stylesheet",r.type="text/css",r.href=e):(r=n.createElement("script"),r.type="text/javascript",r.src=e,r.async=!0),t.insertBefore(r,t.firstChild)}}function a(e,r){for(var n in e)e.hasOwnProperty(n)&&r(n,e[n])}function f(e){return{server:require(e)}}function u(e,n){var o=n.path||"./",a=n.pkg||"default",u=r(e);if(u&&(o="./",a=u[0],n.v&&n.v[a]&&(a=a+"@"+n.v[a]),e=u[1]),e)if(126===e.charCodeAt(0))e=e.slice(2,e.length),o="./";else if(!m&&(47===e.charCodeAt(0)||58===e.charCodeAt(1)))return f(e);var s=x[a];if(!s){if(m&&"electron"!==_.target)throw"Package not found "+a;return f(a+(e?"/"+e:""))}e=e?e:"./"+s.s.entry;var l,d=t(o,e),c=i(d),p=s.f[c];return!p&&c.indexOf("*")>-1&&(l=c),p||l||(c=t(d,"/","index.js"),p=s.f[c],p||"."!==d||(c=s.s&&s.s.entry||"index.js",p=s.f[c]),p||(c=d+".js",p=s.f[c]),p||(p=s.f[d+".jsx"]),p||(c=d+"/index.jsx",p=s.f[c])),{file:p,wildcard:l,pkgName:a,versions:s.v,filePath:d,validPath:c}}function s(e,r,n){if(void 0===n&&(n={}),!m)return r(/\.(js|json)$/.test(e)?h.require(e):"");if(n&&n.ajaxed===e)return console.error(e,"does not provide a module");var i=new XMLHttpRequest;i.onreadystatechange=function(){if(4==i.readyState)if(200==i.status){var n=i.getResponseHeader("Content-Type"),o=i.responseText;/json/.test(n)?o="module.exports = "+o:/javascript/.test(n)||(o="module.exports = "+JSON.stringify(o));var a=t("./",e);_.dynamic(a,o),r(_.import(e,{ajaxed:e}))}else console.error(e,"not found on request"),r(void 0)},i.open("GET",e,!0),i.send()}function l(e,r){var n=y[e];if(n)for(var t in n){var i=n[t].apply(null,r);if(i===!1)return!1}}function d(e){if(null!==e&&["function","object","array"].indexOf(typeof e)!==-1&&!e.hasOwnProperty("default"))return Object.isFrozen(e)?void(e.default=e):void Object.defineProperty(e,"default",{value:e,writable:!0,enumerable:!1})}function c(e,r){if(void 0===r&&(r={}),58===e.charCodeAt(4)||58===e.charCodeAt(5))return o(e);var t=u(e,r);if(t.server)return t.server;var i=t.file;if(t.wildcard){var a=new RegExp(t.wildcard.replace(/\*/g,"@").replace(/[.?*+^$[\]\\(){}|-]/g,"\\$&").replace(/@@/g,".*").replace(/@/g,"[a-z0-9$_-]+"),"i"),f=x[t.pkgName];if(f){var p={};for(var v in f.f)a.test(v)&&(p[v]=c(t.pkgName+"/"+v));return p}}if(!i){var g="function"==typeof r,y=l("async",[e,r]);if(y===!1)return;return s(e,function(e){return g?r(e):null},r)}var w=t.pkgName;if(i.locals&&i.locals.module)return i.locals.module.exports;var b=i.locals={},j=n(t.validPath);b.exports={},b.module={exports:b.exports},b.require=function(e,r){var n=c(e,{pkg:w,path:j,v:t.versions});return _.sdep&&d(n),n},m||!h.require.main?b.require.main={filename:"./",paths:[]}:b.require.main=h.require.main;var k=[b.module.exports,b.require,b.module,t.validPath,j,w];return l("before-import",k),i.fn.apply(k[0],k),l("after-import",k),b.module.exports}if(e.FuseBox)return e.FuseBox;var p="undefined"!=typeof ServiceWorkerGlobalScope,v="undefined"!=typeof WorkerGlobalScope,m="undefined"!=typeof window&&"undefined"!=typeof window.navigator||v||p,h=m?v||p?{}:window:global;m&&(h.global=v||p?{}:window),e=m&&"undefined"==typeof __fbx__dnm__?e:module.exports;var g=m?v||p?{}:window.__fsbx__=window.__fsbx__||{}:h.$fsbx=h.$fsbx||{};m||(h.require=require);var x=g.p=g.p||{},y=g.e=g.e||{},_=function(){function r(){}return r.global=function(e,r){return void 0===r?h[e]:void(h[e]=r)},r.import=function(e,r){return c(e,r)},r.on=function(e,r){y[e]=y[e]||[],y[e].push(r)},r.exists=function(e){try{var r=u(e,{});return void 0!==r.file}catch(e){return!1}},r.remove=function(e){var r=u(e,{}),n=x[r.pkgName];n&&n.f[r.validPath]&&delete n.f[r.validPath]},r.main=function(e){return this.mainFile=e,r.import(e,{})},r.expose=function(r){var n=function(n){var t=r[n].alias,i=c(r[n].pkg);"*"===t?a(i,function(r,n){return e[r]=n}):"object"==typeof t?a(t,function(r,n){return e[n]=i[r]}):e[t]=i};for(var t in r)n(t)},r.dynamic=function(r,n,t){this.pkg(t&&t.pkg||"default",{},function(t){t.file(r,function(r,t,i,o,a){var f=new Function("__fbx__dnm__","exports","require","module","__filename","__dirname","__root__",n);f(!0,r,t,i,o,a,e)})})},r.flush=function(e){var r=x.default;for(var n in r.f)e&&!e(n)||delete r.f[n].locals},r.pkg=function(e,r,n){if(x[e])return n(x[e].s);var t=x[e]={};return t.f={},t.v=r,t.s={file:function(e,r){return t.f[e]={fn:r}}},n(t.s)},r.addPlugin=function(e){this.plugins.push(e)},r.packages=x,r.isBrowser=m,r.isServer=!m,r.plugins=[],r}();return m||(h.FuseBox=_),e.FuseBox=_}(this))
//# sourceMappingURL=app.js.map?tm=1547354502265