import {div} from "el-tool";
import {BasicOmjectClass, Omject} from "omject";
import Synergy from "../../Synergy";

import "./board.scss";

// type SubNodes = "officeSpace";
type IRGB = {r: number, g:number, b:number};
function toRGB(hsv: {h: number, s:number, v:number}): IRGB {
		const h = ((hsv.h % 1) + 1) % 1;
		const s = Math.max(0, Math.min(hsv.s, 1));
		const v = Math.max(0, Math.min(hsv.v, 1));

		const i = Math.floor(h * 6);
		const f = h * 6 - i;
		const p = v * (1 - s);
		const q = v * (1 - f * s);
		const t = v * (1 - (1 - f) * s);

		let R;
		let G;
		let B;
		switch (i % 6) {
			case 0: R = v; G = t; B = p; break;
			case 1: R = q; G = v; B = p; break;
			case 2: R = p; G = v; B = t; break;
			case 3: R = p; G = q; B = v; break;
			case 4: R = t; G = p; B = v; break;
			case 5: R = v; G = p; B = q; break;
		}
		return { r: Math.floor(R * 255), g: Math.floor(G * 255), b: Math.floor(B * 255) };
	}


export default class Board extends BasicOmjectClass {
	// public readonly subNodes: {[key in SubNodes]?: HTMLElement};
	public readonly officeSpaceEl: HTMLElement;
	private game: Synergy;
	public readonly domNode: HTMLElement;

	constructor(parent: Omject) {
		super(parent);
		this.game = this.getAncestor("Synergy");

		this.domNode = div("Board", [
			div("SynergyTrack", (new Array(36)).fill(null).map((_, index) => {
				const hsv = {
					h: (index/36) * .48, 
					s: 0.75, 
					v: 1 - ((index/36) * .2),
				};
				const borderColor = toRGB(hsv);
				hsv.s -= 0.5;
				const fillColor = toRGB(hsv);

				return div(`TrackStep ${index%5 === 0 ? "big" : "small"}`,[
					div("Background", {style: {
						background: `rgba(${fillColor.r}, ${fillColor.g}, ${fillColor.b}, 0.25)`,
						borderColor: `rgba(${borderColor.r}, ${borderColor.g}, ${borderColor.b}, 1)`,
					}}),
					div("Value", {
						// style: {color: `rgba(${fillColor.r}, ${fillColor.g}, ${fillColor.b}, 1)`},
					},(index - 10) + ""),
				])
			})),
			div("MissionControl", [
				div("MissionCardSpace", "Mission Card"),

				div("ProjectCardSpace", div("Text", "Project Card")),
				div("ProjectCardSpace", div("Text", "Project Card")),
				div("ProjectCardSpace", div("Text", "Project Card")),

				div("CleaningAndQuarters", [
					div("Cleaning", "Cleaning Service"),
					div("Quarters", "Quarter Calander"),
				]),
				div("ProjectCardSpace", div("Text", "Project Card")),
				div("ProjectCardSpace", div("Text", "Project Card")),
				div("ProjectCardSpace", div("Text", "Project Card")),
			]),
			div("Building", [
				div("Shadow"),
				this.officeSpaceEl = div("OfficeSpace", {
					on: {
						dragover: (event: DragEvent) => {
							event.preventDefault();
							event.dataTransfer.dropEffect = 'move';
							const dragID = event.dataTransfer.getData("text/plain");
							if (dragID === "employeeCard") {
								const employeeCard = this.game.dragDepo.employeeCard;
								if (employeeCard.isOnBoard()) {
									const chunkSize = this.officeSpaceEl.offsetWidth / (7*6);
									console.log(chunkSize, chunkSize * 7 * 6);
									const bounds = this.officeSpaceEl.getBoundingClientRect();

									let x = event.clientX - bounds.left;
									x -= x % chunkSize;
									let y = event.clientY - bounds.top;
									y -= y % chunkSize;
									
									employeeCard.domNode.style.top = y + "px";
									employeeCard.domNode.style.left = x + "px";
								}
							}
						},
						drop: (event: DragEvent) => {
							event.preventDefault();
							const dragID = event.dataTransfer.getData("text/plain");
							if (dragID === "employeeCard") {
								const employeeCard = this.game.dragDepo.employeeCard;
								employeeCard.moveToBoard();
							}
						},
					},
					// innards: genFlooring(5*6, 7*6),
				}),
				div("Walls", [
					div("Brick top"),
					div("Brick bottom"),
					div("Brick left"),
					div("Brick right"),

					div("Window top_left width_3"),
					div("Window top_center width_5"),
					div("Window top_right width_3"),
					//
					div("Window left width_7"),
					//
					div("Window right_1 width_1"),
					div("Window right_2 width_1"),
					div("Window right_3 width_1"),
					div("Window right_bottom width_5"),
					//
					div("Window bottom_left width_3"),
					div("Window bottom_center width_3"),
					div("Window bottom_right width_5"),
				]),
			]),
		]);
	}
}