import {div, setInnards} from "el-tool";
import {BasicOmjectClass, Omject} from "omject";
import {EmployeeDeck} from "../../Entities/Decks"
import {Employee} from "../../Entities/Decks/Employee";
import Synergy from "../../Synergy";

import "./setup.scss";

type Stage = "numPlayers" | "secret_agendas" | "chooseCharacter" | "skill";

export default class SetupGame extends BasicOmjectClass{
	public domNode: HTMLElement;
	private views: {[key in Stage]?: HTMLElement};
	private game: Synergy;

	constructor(parent: Omject) {
		super(parent);
		this.game = this.getAncestor(Synergy);
		this.domNode = div("SetupGame");
		this.views = {};
		this.updateUI()
	}

	private updateUI() {
		let view: HTMLElement;
		const players = this.game.players;
		console.log(players);
		if (players === undefined) {
			view = this.getnitNumPlayers(); 
		} else if (players.findIndex((player) => !player) !== -1) {
			view = this.getnitChooseCharacter();
		} else {
			this.game.modalView.set("none");
			return;
		}
		
		setInnards(this.domNode, [view]);
	}

	private getnitNumPlayers() {
		if (this.views.numPlayers) { return this.views.numPlayers; }

		const createSelection = (num: number) => div("Num", {
			onClick: () => {
				this.game.players = ((new Array(num)).fill(undefined));
				this.updateUI();
			}
		}, num + "");

		return this.views.numPlayers = div("SelectNumPlayers", [
			div("Directions", "Select number of players"),
			div("Selections", [
				createSelection(1),
				createSelection(2),
				createSelection(3),
				createSelection(4),
			]),
		]);
	}

	private getnitChooseCharacter() {
		if (this.views.chooseCharacter) { return this.views.chooseCharacter; }

		const {game} = this;

		const choices: Array<Array<Employee>> = game.players.map((player) => {
			if (player !== undefined) { return null; }
			return game.employeeDeck.draw(3);
		});

		const chooseCharacter = div("ChooseCharacter");

		const setPlayerCharacter = (playerNum: number, character: Employee) => {
			game.players[playerNum] = character;
		}

		const updateChoices = () => {
			const playerNum = choices.findIndex((choice) => !!choice);
			if (playerNum !== -1) { 
				const chooseFrom = choices[playerNum];
				setInnards(chooseCharacter, chooseFrom.map(
					(employee) => div("SelectEmployee", {
						onClick: () => {
							choices[playerNum] = undefined;
							game.employeeDeck.placeCardsAtBottom(chooseFrom);
							setPlayerCharacter(playerNum, employee);
							updateChoices();
						}
					},[
						employee.domNode
					])
				));
			} else {
				game.employeeDeck.shuffle();
				this.updateUI(); 
			}			
		};
		updateChoices();

		return this.views.chooseCharacter = chooseCharacter;
	}
}











