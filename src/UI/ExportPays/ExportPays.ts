import {ProjectList} from "../../Entities/Decks/Project/ProjectDeck";
import Project from "../../Entities/Decks/Project/Project";
import { div } from "el-tool";

export default function ExportPays() {
	return div("PayList", {
		hackableHTML: ProjectList.map((proj) => proj.pay).join("<br>"),
	});
}