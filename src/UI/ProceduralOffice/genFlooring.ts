import {div} from "el-tool";
import "./flooring.scss";

// const tileTypes = ["carpet_red", "carpet_olive", "carpet_brown", "carpet_dark_grey"];
const tileTypes = ["concrete"];

export default function genFlooring(rows: number, cols: number) {
	const out = div("Flooring", {
		style: {
			gridTemplateColumns: `repeat(${cols}, auto)`,
			gridTemplateRows: `repeat(${rows}, auto)`,
		}
	});
	for (let r = 0; r < rows; r++) {
		for (let c = 0; c < cols; c++) {
			const type = tileTypes[Math.floor(Math.random() * tileTypes.length)];
			const isRotated = r%2 === c%2 ;
			div(`Tile --type_${type} ${isRotated ? "--rotated": ""}`, {
				appendTo: out,
			});
		}
	}
	return out;
}