import {div} from "el-tool";
import { alea } from "seedrandom";

import "./office.scss";

export default function genProceduralOffice(seed?: string) {
	seed = seed || Math.random() +"";
	// const canvas = document.createElement("canvas");
	// canvas.width = canvas.height = 350;
	// const painter = canvas.getContext("2d");
	const arng = alea(seed);
	const random = (high: number = 1, low: number = 0, floor?: number) => {
		const range = high - low;
		let num = low + (arng() * range);
		if (floor !== undefined) {
			num -= (num % floor);
		}
		return num;
	}

	const skies = [
		"linear-gradient(#00dbff, #fcfcfc)",
		"linear-gradient(#f5d9fc, #f9f4e8)",
		"linear-gradient(#abf8fb, #fcfefe)",
		"linear-gradient(#43497a, #586c6c)",
		"linear-gradient(#00ffe4, #edffff)",
		// "#EFF", "#EEF", "#EEE", "#DEF", "#DFF"
	];
	const skyColor = skies[random(skies.length, null, 1)];

	const numCols = random(3 + 1, 2, 1);
	const numRows = random(8 + 1, 4, 1);
	const frs = ["1fr", "1.5fr", "2fr"];
	const colSizes = (new Array(numCols)).fill(null).map(() => frs[random(frs.length, null, 1)]);

	const bgDistance = random() > 0.5 ? "bg-near" : "bg-far";
	const floorColors = [
		"#BBB", 
		"#AAA", 
		"#999", 
		"rgb(53, 74, 117)",
		"rgb(81, 69, 54)",
	];

	let wall: HTMLElement;
	let floor: HTMLElement;
	const office = div(`Office ${bgDistance}`,
		{style: {
			backgroundImage: skyColor
		}},
		[
			wall = div("Wall", {
				style: {
					gridTemplateColumns: colSizes.join(" "),
					gridTemplateRows: (new Array(numRows)).fill("1fr").join(" "),
				},
			}),
			floor = div("Floor", {
				style: {
					height: `${random(30,15)}%`,
					backgroundColor: floorColors[random(floorColors.length, null, 1)],
				},
			}),
		]
	)

	const allMaterials = ["glass", "wood", "brick", "stone", "paint"];
	const	materials = [
		...allMaterials.splice(random(allMaterials.length, null, 1), 1),
		...allMaterials.splice(random(allMaterials.length, null, 1), 1),
	];

	const paintIndex = materials.indexOf("paint");
	if (paintIndex !== -1) {
		const paints = ["paint-blue", "paint-beige", "paint-grey", "paint-dark-grey"];
		materials.splice(paintIndex, 1, paints[random(paints.length, null, 1)]);
	}

	const halfRows = Math.floor(numRows/2);
	const noWindow = materials.indexOf("glass") === -1;
	let windowHeight: number;
	let materialNum: number;
	if (noWindow) {
		windowHeight = random(halfRows + 1, 2, 1);
		if (numCols === 3) {
			materialNum = 0;
		} else {
			const bestWindowMaterials = ["paint", "wood", "brick", "stone"];
			for(let i = 0; materialNum === undefined && i < bestWindowMaterials.length; i++) {
				const targetIndex = materials.indexOf(bestWindowMaterials[i]);
				if (targetIndex !== -1) { materialNum = targetIndex; }
			}
		}
	}

	let matIndex = 0;
	for (let col = 0; col < numCols;) {
		const material = materials[matIndex];
		let span = Math.min(random(2 + 1, 1, 1), numCols - col);
		if (matIndex === 0) {
			span = Math.min(numCols -1, span);
		}
		
		let rowsAboveWindow = numRows;
		if (matIndex%2 === materialNum) {
			rowsAboveWindow -= (windowHeight + (numRows - halfRows));
		}

		if (rowsAboveWindow !== 0) {
			wall.appendChild(div(`Material ${material}`, {
				style: {
					gridRowStart: 1+"",
					gridRowEnd: `span ${rowsAboveWindow}`,
					gridColumnStart: (col+1)+"",
					gridColumnEnd: `span ${span}`,
				},
			}));
		}

		if (matIndex%2 === materialNum && noWindow) {
			wall.appendChild(div(`Material glass`, {
				style: {
					gridRowStart: rowsAboveWindow+1+"",
					gridRowEnd: `span ${windowHeight}`,
					gridColumnStart: (col+1)+"",
					gridColumnEnd: `span ${span}`,
				},
			}));

			wall.appendChild(div(`Material ${material}`, {
				style: {
					gridRowStart: halfRows+1+"",
					gridRowEnd: `span ${numRows - halfRows}`,
					gridColumnStart: (col+1)+"",
					gridColumnEnd: `span ${span}`,
				},
			}));
		}

		col += span;
		matIndex = (matIndex + 1) % materials.length;
	}

	

	// painter.fillStyle = skyColor;
	// painter.fillRect(0, 0, canvas.width, canvas.height);

	return office;
}