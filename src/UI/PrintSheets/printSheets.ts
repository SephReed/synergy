import { div } from "el-tool";
import EmployeeDeck, { rawEmployeeList } from "../../Entities/Decks/Employee/EmployeeDeck";
import { Employee } from "../../Entities/Decks/Employee";

import {ProjectList} from "../../Entities/Decks/Project/ProjectDeck";
import {AgendaList} from "../../Entities/Decks/Agenda/AgendaDeck";
import {EventCardList} from "../../Entities/Decks/Event/EventCardDeck";
import {OptionCardList} from "../../Entities/Decks/Option/OptionCardDeck";
import {QuirkCardList} from "../../Entities/Decks/Quirk/QuirkDeck";
import {SpecialtyList} from "../../Entities/Decks/Specialty/SpecialtyDeck";
import {MissionList} from "../../Entities/Decks/Mission/MissionDeck";
import {PlaceableList} from "../../Entities/Decks/Placeable/PlaceableDeck";

// import AgendaCard from "../../Entities/AgendaCard/AgendaCard";
import Instructions from "../../Entities/Instructions/Instructions";

import "./printSheet.scss"


export type SheetType = "employee" | "project" | "agenda" | "event" | "option" | "quirk" | "specialty" | "mission" | "placeable";

export default function printSheets(type: SheetType = "agenda") {
	return div(`Print-Sheet ${type}`, () => {
		if (type === "employee") {
			return rawEmployeeList
				.filter((raw) => raw.pay && raw.name !== "Examplo")
				.map((raw) => (new Employee(null, raw)))
				.map((card) => card.domNode);
		} else if (type === "project") {
			return ProjectList.filter((proj) => proj.raw.tasks).map((proj) => proj.domNode);
		} else if (type === "agenda") {
			// return AgendaList.map((card) => card.domNode);
			return Instructions.agendaCard.domNode;
		} else if (type === "event") {
			return EventCardList.filter((card) => (card.raw.description || "").length).map((card) => card.domNode);
		} else if (type === "option") {
			return OptionCardList.filter((card) => (card.raw.description || "").length).map((card) => card.domNode);
		} else if (type === "quirk") {
			return QuirkCardList.map((card) => card.domNode);
		} else if (type === "specialty") {
			return SpecialtyList.map((card) => card.domNode);
		} else if (type === "mission") {
			return MissionList.map((card) => card.domNode);
		} else if (type === "placeable") {
			return PlaceableList.map((card) => card.domNode);
		}
	});	
}