export default interface IRawOptionCard {
	name: string;
	description: string;
	repeatable: boolean;
	footers?: string;
}