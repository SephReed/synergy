import {div} from "el-tool";
import {Omject} from "omject";
import OptionCard from "./OptionCard";
import IRawOptionCard from "./IRawOptionCard";
import Deck from "../Deck";
import * as OptionCardSheet from "../../../Registry/sheets/json/Synergy - Options.json";


const OptionCardList: Array<OptionCard> = [];
OptionCardSheet.forEach((card) => {
	const raw: IRawOptionCard = {
		name: card["Name"],
		description: card.Description,
		repeatable: card.Repeatable,
	};
	const repeat = card["Repeat Item"] || 1;
	for (let i = 0; i < repeat; i++) {
		OptionCardList.push(new OptionCard(raw));
	}
});
export {OptionCardList};

export default class OptionCardDeck extends Deck<OptionCard> {
	constructor(parent: Omject) {
		super(parent, () => OptionCardList, OptionCard.getBackNode);
	}
}

