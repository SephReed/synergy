import {div, span, br} from "el-tool";
import IRawOptionCard from "./IRawOptionCard";
import {fromMD, injectSymbols} from "../../../Util";

import "./optionCard.scss";

type BubbleType = "left_bubble" | "right_bubble" | "alert"
export interface IBubble {
	type: BubbleType,
	content: string,
}

export default class OptionCard {
	public readonly domNode: HTMLElement;
	private isSquinched: Map<HTMLElement, boolean>;
	private bubbleEls: HTMLElement[];

	constructor(public raw: IRawOptionCard) {
		this.isSquinched = new Map();
		let qualitiesEl: HTMLElement;
		this.domNode = div("Option-Card front", [
			div("Title", [
				qualitiesEl = div("Qualities", [
					// raw.repeatable ? div("Quality repeatable") : null,
				]),
				div("Name", raw.name),
				qualitiesEl.cloneNode(true) as HTMLElement,
			]),
			div("Description", [
				div("Fade"),
				...this.bubbleEls = this.createBubbles().map((bubble) => 
					div(`Bubble --${bubble.type}`, [
						div("Content", fromMD(injectSymbols(bubble.content))),
					]),
				),
			]),
		]);
		requestAnimationFrame(() => requestAnimationFrame(() => this.squinchBubbles()));
	}

	private createBubbles(): IBubble[] {
		const out = ((this.raw.description + "").split(/\n/g) || []).filter((str) => str.trim() !== "").map((content) => {
			let type: BubbleType = "left_bubble";
			if (content.startsWith(">")) {
				type = "right_bubble"; 
			} else if (content.startsWith("!")) {
				type = "alert"; 
			} 
			return {
				type,
				content: content.replace(/^[>!]/, ""),
			}
		});
		if (this.raw.repeatable) {
			out.push({type: "left_bubble", content: "**Repeatable**: You may return this card to your hand after the *actions phase*."});
		}
		return out;
	}

	private squinchBubbles() {
		this.bubbleEls.forEach((bubble) => this.isSquinched.set(bubble, false));

		const squinchFurther = () => {
			let promises = this.bubbleEls.map((bubble) => {
				if (this.isSquinched.get(bubble)) { return; }
				return new Promise((resolve) => {
					const startHeight = bubble.offsetHeight;
					const startWidth = bubble.offsetWidth;
					bubble.style.width = (startWidth - 1) + "px";
					requestAnimationFrame(() => requestAnimationFrame(() => {
						if (bubble.offsetHeight !== startHeight) {
							bubble.style.width = startWidth + "px";
							this.isSquinched.set(bubble, true);
						}
						resolve();
					}));
				})
			});
			promises = promises.filter((it) => !!it);
			if (promises.length) {
				Promise.all(promises).then(() => squinchFurther());
			} else {
				console.log("SQUINCHED");
			}
		}
		squinchFurther();
	}

	public static getBackNode(): HTMLElement {
		return div("Option-Card back", "OptionCard CARD")
	}
}