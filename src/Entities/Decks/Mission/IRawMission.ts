export default interface IRawMission {
	title: string;
	goal: string;
	quarters: number;
	difficulty: number;
	funds: number;
	deadline: boolean;
}