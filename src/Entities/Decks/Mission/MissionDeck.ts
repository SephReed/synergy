import {div} from "el-tool";
import {Omject} from "omject";
import Mission from "./Mission";
import IRawMission from "./IRawMission";
import Deck from "../Deck";
import * as MissionSheet from "../../../Registry/sheets/json/Synergy - Missions.json";

// const commonActions = MissionSheet.find((row) => row["id"] === "allPlayers")["Actions"];
// console.log(commonActions);
const MissionList: Array<Mission> = MissionSheet
// .filter((row) => ["allPlayers", "examp12le"].includes(row.id) === false)
.map((row) => 
	new Mission({
		title: row["Title"],
		goal: row["Goal"],
		quarters: row["Quarters"],
		difficulty: row["Difficulty"],
		funds: row["Funds"],
		deadline: row["Deadline"],
	}),
);

export {MissionList};

export default class MissionDeck extends Deck<Mission> {
	constructor(parent: Omject) {
		super(
			parent, 
			() => MissionList, 
			Mission.getBackNode);
	}
}

