import {div, span} from "el-tool";
import IRawMission from "./IRawMission";
import {fromMD, injectSymbols} from "../../../Util"

import "./mission.scss";

export default class Mission {
	public readonly domNode: HTMLElement;

	constructor(public raw: IRawMission) {
		this.domNode = div(`Mission-Card front`, [
			div("Title", raw.title),
			div("Info", [
				div("Overview", [
					div("Row difficulty", [
						div("Name", "Difficulty: "),
						div("Value", [
							"Beginner",
							"Easy",
							"Intermediate",
							"Expert",
							][raw.difficulty],
						),
					]),
					div("Row length", [
						div("Name", "Length:"),
						div("Value", raw.quarters ? `~${raw.quarters * 8}min` : `N/A`),
					]),
					div("Row lengthBeginner", [
						div("Name", "Length (beginner):"),
						div("Value", raw.quarters ? `~${raw.quarters * 12}min` : `N/A`),
					]), 
					div("Row funds", [
						div("Name", "Funds:"),
						div("Value", `${raw.funds}k`),
					]),
					div("Row ending", [
						div("Name", raw.deadline ? "Deadline:" : "Target Quarter:"),
						div("Value", raw.quarters ? `${raw.quarters}Q` : `N/A`),
					]), 
				]),
				
				div("Goal", fromMD(injectSymbols(raw.goal))),
			]),
		]);
	}

	public static getBackNode(): HTMLElement {
		return div("Mission-Card back", "Mission CARD")
	}
}