import {div, span, br} from "el-tool";
import IRawEventCard from "./IRawEventCard";
import fromMD from "../../../Util/fromMD"

import "./eventCard.scss";

const signOffs = [
	"Best",
	"Sincerely",
	"Thanks and Pie",
	"Regards",
	"Cheers",
]

const companyWideEmails = [
	"Announcements",
	"Office",
	"Events",
]

// interface IAction {
// 	type: "good" | "bad" | "optional" | "choose",

// }

export default class EventCard {
	public readonly domNode: HTMLElement;

	constructor(public raw: IRawEventCard) {
		const emailer = companyWideEmails[Math.floor(Math.random() * companyWideEmails.length)];
		this.domNode = div("Event-Card front", [
			div("Title", [
				div(`Avatar ${emailer.toLowerCase()}`, emailer.charAt(0)),
				div("Subject", raw.name + ""),
				div("From", `from: ${emailer}`),
				div("Star"),
				// div(`Star ${raw.isMajor ? "major" : ""}`, raw.isMajor ? "★" : "☆")
			]),
			
			div("Description", [
				fromMD(raw.description + ""),
				// raw.description + "",
				// div("SignOff", [
				// 	`${signOffs[Math.floor(Math.random() * signOffs.length)]},`,
				// 	br(),
				// 	// "Management",
				// 	emailer,
				// ]),
			]),
			div("Actions", this.parseAction(raw.actions)),
			
			// div("Footers", {hackableHTML: raw.footers.replace(/\n/g, "<br>")}),
		]);
	}

	private parseAction(rawActs: string = ""):HTMLElement[]{
		return (rawActs.match(/#[^#]+/g) || []).map((rawAct) => {
			let type: string;
			const lines = rawAct.replace(/#(\w+)/, (match, g1) => {
				type = g1.toLowerCase();
				return "";
			});
			let allParts: HTMLElement[] = [];
			lines.split(/\n+/g).filter((it) => it.length).map((line) => {
				const parts = line.match(/.+?(:|$)/g);
				parts.forEach((part, index) => {
					const isCondition = parts.length > 1 && index === 0;
					const isResult = parts.length > 1 && index !== 0;
					allParts.push(
						div(
							`Part ${isCondition ? "condition" : (isResult ? "result" : "")}`, 
							parts.length !== 1 ? null : {
								style: { gridColumnEnd: "span 2" },
							},
							fromMD(this.injectSymbols(part))
						),
					);
				});
			});

			return div(`Action ${type}`, allParts);
		})
	}

	private injectSymbols(rawHtml: string) {
		return rawHtml.replace(/{{(.+?)}}/g, (match, g1) => 
			`<span class="Icon ${g1.toLowerCase()}">${g1.charAt(0)}</span>`
		)
		.replace(/_(.+?)_/g, (match, g1) => `<u>${g1}</u>`)
		.replace(/SYN/g, `<span class="Icon synergy">s</span>`);
	}

	public static getBackNode(): HTMLElement {
		return div("Event-Card back", "Event Card")
	}
}