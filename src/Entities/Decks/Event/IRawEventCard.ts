export default interface IRawEventCard {
	name: string;
	description: string;
	isMajor: boolean;
	footers?: string;
	actions: string;
}