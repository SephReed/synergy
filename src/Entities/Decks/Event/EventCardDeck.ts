import {div} from "el-tool";
import {Omject} from "omject";
import EventCard from "./EventCard";
import IRawEventCard from "./IRawEventCard";
import Deck from "../Deck";
import * as EventCardSheet from "Registry/sheets/json/Synergy - Events.json";


const EventCardList: Array<EventCard> = [];
EventCardSheet.forEach((card) => {
	const raw: IRawEventCard = {
		name: card["Name"],
		description: card.Description,
		isMajor: card["Is Major Event"],
		actions: card["Actions"] + "",
	};
	const repeat = card.Repeat || 1;
	for (let i = 0; i < repeat; i++) {
		EventCardList.push(new EventCard(raw));
	}
});
export {EventCardList};

export default class EventCardDeck extends Deck<EventCard> {
	constructor(parent: Omject) {
		super(parent, () => EventCardList, EventCard.getBackNode);
	}
}

