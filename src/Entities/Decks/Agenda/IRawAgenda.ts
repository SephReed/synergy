export default interface IRawAgenda {
	name: string;
	description: string;
	footers?: string;
}