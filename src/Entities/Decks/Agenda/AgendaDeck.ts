import {div} from "el-tool";
import {Omject} from "omject";
import Agenda from "./Agenda";
import IRawAgenda from "./IRawAgenda";
import Deck from "../Deck";
import * as AgendaSheet from "../../../Registry/sheets/json/Synergy - Agendas.json";

type FooterKeywords = "hiree" | "subtract";

const AgendaList: Array<Agenda> = [];
AgendaSheet.forEach((card) => {
	const matchedItems: FooterKeywords[] = [];
	let footers = "";
	const description: string = (card.Description as string).replace(/hiree|subtract/ig, (match) => {
		const keyword: FooterKeywords = match.toLowerCase() as any;
		if (matchedItems.includes(keyword) === false) {
			if (matchedItems.length > 0) { footers += "\n"; }
			matchedItems.push(keyword);
			const footerSymbol = (new Array(matchedItems.length)).fill("*").join("");
			footers += footerSymbol;
			if (keyword === "hiree") {
				footers += "Players do not count as hirees.  Terminated hirees still count.";
			} else if (keyword === "subtract") {
				footers += "Minimum PVP from this card is zero"
			}
			// return match + footerSymbol;
		}
		return match;
	});
	// const hireeIndex = description.indexOf("hiree");
	// const subtractIndex = description.indexOf("subtract");
	// if (hireeIndex !== -1 || subtractIndex !== -1)
	
	const raw: IRawAgenda = {
		name: card["Name"],
		description,
		footers,
	};
	AgendaList.push(new Agenda(raw));
});
export {AgendaList};

export default class AgendaDeck extends Deck<Agenda> {
	constructor(parent: Omject) {
		super(parent, () => AgendaList, Agenda.getBackNode);
	}
}

