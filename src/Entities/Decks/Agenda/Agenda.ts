import {div, span} from "el-tool";
import IRawAgenda from "./IRawAgenda";

import "./agenda.scss";

export default class Project {
	public readonly domNode: HTMLElement;

	constructor(private raw: IRawAgenda) {
		this.domNode = div("Agenda-Card front", [
			div("Name", raw.name),
			div("Description", raw.description),
			div("Footers", {hackableHTML: raw.footers.replace(/\n/g, "<br>")}),
		]);
	}

	public static getBackNode(): HTMLElement {
		return div("Agenda-Card back", "AGENDA CARD")
	}
}