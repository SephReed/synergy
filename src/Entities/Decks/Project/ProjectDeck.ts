import {div} from "el-tool";
import {Omject} from "omject";
import Project from "./Project";
import IRawProject from "./IRawProject";
import Deck from "../Deck";
import * as ProjectSheet from "../../../Registry/sheets/json/Synergy - Projects.json";

const ProjectList: Array<Project> = [];
ProjectSheet.forEach((card) => {
	const raw: IRawProject = {
		name: card["Card Name"],
		description: card["Description"],
		tasks: card["Task Cost"],
		pay: card["Pay"],
		payOld: card["PayOld"],
		effect: card["Effect"],
		anchor: card["Anchor"],
	};
	ProjectList.push(new Project(raw));
});
export {ProjectList};

export default class ProjectDeck extends Deck<Project> {
	constructor(parent: Omject) {
		super(parent, () => ProjectList, Project.getBackNode);
	}
}

