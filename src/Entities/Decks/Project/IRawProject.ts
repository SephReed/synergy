export default interface IRawProject {
	name: string;
	description: string;
	tasks: string;
	pay: number;
	payOld: number;
	effect: number;
	anchor: boolean;
}