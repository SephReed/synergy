import {div} from "el-tool";
import IRawProject from "./IRawProject";
import {rawEmployeeList} from "../Employee/EmployeeDeck";
import {fromMD, minimizeWidth} from "../../../Util";

import "./project.scss";

const EMPLOYEE_TASKS = 3;

export interface IJob {
	name: string;
	count: number;
	artLevel?: number;
	logicLevel?: number;
	charismaLevel?: number;
	patienceLevel?: number;
	skillRarity?: number;
	pay?: number;
}

export default class Project {
	public readonly domNode: HTMLElement;
	public readonly pay: number;
	public readonly jobStages: Array<IJob[]>;

	constructor(public raw: IRawProject) {
		const {name, description, tasks, pay, anchor } = raw;
		this.jobStages = this.parseTasks(tasks);
		this.pay = this.calculatePay();

		if (this.pay !== pay) {
			console.log(`Pays do not match for project "${name}".  Old = ${pay}, new = ${this.pay}`);
		}

		this.domNode = div("Project-Card front", [
			div("Header", [
				div("Name", name),
				// div("Pay", `${this.pay}k :: ${pay}k :: ${Math.floor(raw.payOld * 2/3)}k`),
				// div("Pay", `${this.pay}k :: ${Math.floor(raw.payOld * 2/3)}k`),
				div("Pay", `${this.pay}k`),
				// anchor ? div("Anchor") : null,
			]),
			div("Instructions", [
				...this.jobStages.map((stage, stageNum) => 
					div("Stage", [
						stageNum === 0 ? null : div("ThenSpacer", "THEN"),
						div("Jobs", stage.map((job) => 
							div("Job card", [
								!job.name ? null : div("Name", job.name),
								// div("StackHere"),
								minimizeWidth(div("Count", (new Array(job.count)).fill(null).map(() => div("TaskCheckbox", "✔")))),
								div("Rarity", [
									(job.skillRarity * 100).toFixed(2), 
									"% ", 
									// (1 / job.skillRarity).toFixed(2), 
									// "x ", 
									// job.pay.toFixed(2),
									// "k = ",
									// (job.pay * (1 / job.skillRarity)).toFixed(2),
									// "k"
								]),
								div("Skills", () => {
									const {artLevel, logicLevel, charismaLevel, patienceLevel} = job;
									if (!artLevel && !logicLevel && !charismaLevel && !patienceLevel) { return "Any"; }
									return [
										["art", artLevel],
										["logic", logicLevel],
										["charisma", charismaLevel],
										["patience", patienceLevel],
									].map(([name, level]: [string, number]) => 
										!level ? null : div(`Skill ${name}`, 
											(new Array(level)).fill(null).map(() => div(`Token ${name}`)),
										),
									)
								}),
							]),
						)),
					]),
				),
				anchor ? div("Anchor card", [
					div("Icon"),
					"Anchor Project - You may pay -1 SYN to put this card back into any cofounders hand after completion"
				]) : null,
			]),
			description ? minimizeWidth(div("Description", fromMD(description))) : null,
		]);
	}

	private parseTasks(instructions: string): Array<IJob[]> {
		if (typeof instructions !== "string") { return []; }
		const stages = instructions.split(/\->/g).map((str) => str.trim());
		return stages.map((stage) => {
			const jobs = stage.match(/(\[[^\]]+?\])? ?\(\d+\) ?[1-3ALCP]+/g).map((str) => str.trim());
			return jobs.map((jobStr) => {
				const job: IJob = {} as any;
				const nameMatch = jobStr.match(/\[[^\]]+?\]/);
				if (nameMatch !== null) {
					job.name = nameMatch[0].slice(1, nameMatch.length-2);
				}
				const counts = jobStr.match(/\(\d+\)|[1-3][ALCP]/g);
				counts.forEach((count) => {
					const num = parseInt(count.replace(/\D/g, "")) || 0;
					if (/\(\d+\)/.test(count)) {
						job.count = num;
					} else {
						if (count.includes("A")) { 
							job.artLevel = num;
						} else if (count.includes("L")) { 
							job.logicLevel = num;
						} else if (count.includes("C")) { 
							job.charismaLevel = num;
						} else if (count.includes("P")) { 
							job.patienceLevel = num;
						}
					}
				});
				job.artLevel = job.artLevel || 0;
				job.logicLevel = job.logicLevel || 0;
				job.charismaLevel = job.charismaLevel || 0;
				job.patienceLevel = job.patienceLevel || 0;
				return job;
			})
		})
	}

	private calculatePay(): number {
		const trueEmployees = rawEmployeeList.filter((emee) => emee.pay > 0); // filter incomplete employee rows

		let paySum = 0;
		this.jobStages.forEach((stage) => {
			stage.forEach((job) => {
				// employees who can do the job
				const canDoers = trueEmployees.filter((emee) => 
					emee.art >= job.artLevel
					&& emee.logic >= job.logicLevel
					&& emee.charisma >= job.charismaLevel
					&& emee.patience >= job.patienceLevel
				).sort((a, b) => a.pay - b.pay);
				canDoers.forEach((emee) => emee.jobs++);  // keep records of how many jobs an employee is assignable to

				job.skillRarity = canDoers.length / trueEmployees.length;
				const minSkillCost = canDoers[0].pay * job.count/EMPLOYEE_TASKS

				// non-overqualified employees
				const shouldDoers = canDoers.filter((emee) => 
					(!job.artLevel || emee.art - job.artLevel <= 1)
					&& (!job.logicLevel ||emee.logic - job.logicLevel <= 1)
					&& (!job.charismaLevel || emee.charisma - job.charismaLevel <= 1)
					&& (!job.patienceLevel || emee.patience - job.patienceLevel <= 1)
				).sort((a, b) => a.pay - b.pay);

				const medianEmployee = shouldDoers[Math.floor(shouldDoers.length/2)];
				const medianSkillCost = medianEmployee.pay * job.count/EMPLOYEE_TASKS;

				// const skillRarityBonus = 1 / job.skillRarity;
				const skillRarityBonus = Math.pow(1 / job.skillRarity, 2/3);
				job.pay = 2; // co-founder assign cost
				job.pay += (minSkillCost + medianSkillCost)/2 * skillRarityBonus * this.raw.effect;
				paySum += job.pay;
			})
		});
		return paySum < 15 ? Math.floor(paySum) : Math.round(paySum/5) * 5;
	}

	public static getBackNode(): HTMLElement {
		return div("Project-Card back", [
			div("Name", "PROJECT CARD"),
			div("Icon", "+"),
		])
	}
}