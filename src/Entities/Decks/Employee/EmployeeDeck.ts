import {div} from "el-tool";
import {Omject} from "omject";
import Employee from "./Employee";
import IRawEmployee from "./IRawEmployee";
import Deck from "../Deck";
import * as employeeSheet from "../../../Registry/sheets/json/Synergy - Employees.json";

const rawEmployeeList: Array<IRawEmployee> = [];
// const nameProps = ["Name1", "Name2", "Name3", "Name4"];	
const colors = ["purple", "orange", "forest", "teal"];
employeeSheet.forEach((card, index) => {
	if (!card.Card) { return; }
	const raw: IRawEmployee = {
		teamColor: colors[index % colors.length],
		art: parseInt(card.Art) || 0,
		logic: parseInt(card.Logic) || 0,
		charisma: parseInt(card.Charisma) || 0,
		patience: parseInt(card.Patience) || 0,
		name: card.Name,
		animal: card.Animal,
		concept: card.Concept,
		pay: card.Pay,
		jobs: 0,
		minSyn: card.MinSyn,
	};
	rawEmployeeList.push(raw);
});
export {rawEmployeeList};

export default class EmployeeDeck extends Deck<Employee> {
	constructor(parent: Omject) {
		super(parent, function() {
			return rawEmployeeList.map((raw) => new Employee(this, raw))
		}, Employee.getBackNode);
	}

	protected onClick() {
		const card = this.draw();
		this.game.subNodes.drawnCards.appendChild(card.domNode);
		const quirk = this.game.quirkDeck.draw();
		card.setQuirk(quirk);
	}
}
