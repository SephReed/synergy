export default interface IEmployee {
	// color: string,
	teamColor: string,
	art: number,
	logic: number,
	charisma: number,
	patience: number,
	name: string,
	animal: string,
	concept: string,
	pay: number,
	jobs: number,
	minSyn: number,
}