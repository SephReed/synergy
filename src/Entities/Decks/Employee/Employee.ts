import {div, span} from "el-tool";
import IRawEmployee from "./IRawEmployee";
import { BasicOmjectClass, Omject } from "omject"
import Game from "../../../Synergy";
import {genProceduralOffice} from "../../../UI/ProceduralOffice";
import QuirkCard from "../Quirk/QuirkCard"
import {parseRawText} from "../../../Util"

import * as employeeImages from "../../../assets/Employees/Character/";
import * as employeeBgs from "../../../assets/Employees/Backgrounds/";
console.log(employeeImages);

import "./employee.scss";
import imgSrc from "./logo.png";
console.log(imgSrc);

const dragImage = new Image(); 
dragImage.src = imgSrc;

const handFonts = [
	"Cedarville-Cursive",
	"Gloria-Hallelujah",
	"Gochi-Hand",
	// "Just-Another-Hand",
	"Kalam",
	"La-Belle-Aurore",
	"Nothing-You-Could-Do",
	"Over-the-Rainbow",
	"Reenie-Beanie",
	"Rock-Salt",
	"Shadows-Into-Light",
	// "Tangerine",
	"Walter-Turncoat",
];


export default class Employee extends BasicOmjectClass {
	private _domNode: HTMLElement;
	private quirkEl: HTMLElement;
	private game: Game;
	private quirk: QuirkCard;

	constructor(
		parent: Omject,
		public raw: IRawEmployee,
	) { 
		super(parent); 
		this.game = this.getAncestor("Synergy");
		// console.log(parent, this.game, this.parent());
	}

	public get domNode(): HTMLElement {
		if (this._domNode === undefined) {
			const {name, animal, art, logic, charisma, patience, concept, teamColor, pay, minSyn} = this.raw;
			this._domNode = div(`Employee-Card front team_${teamColor}`, {
				attr: {draggable: "true"},
				on: {
					dragstart: (event: DragEvent) => {
						this.game.dragDepo.employeeCard = this;
						if (this.isOnBoard()) {
						  event.dataTransfer.setDragImage(document.createElement('img'), 0, 0);
						} 
						event.dataTransfer.effectAllowed = 'move';
						event.dataTransfer.setData("text/plain", "employeeCard");
					}, 
					dragend: (event: DragEvent) => {
						this.game.dragDepo.employeeCard = undefined;
					}
				}
			}, [
				div("Employee-bg", [
					div("Floor"),
					div("Desk"),
					div("Fade"),
					div("TeamColor"),
				]),
				div("Employee-fg", [
					div(`Name --font_${handFonts[Math.floor(Math.random() * handFonts.length)]}`, name.replace(/\(.\)/g, "")),
					div("Needs", [
						div("Synergy", parseRawText(`${minSyn <= -10 ? "Any" : minSyn}SYN`)),
						" / ",
						div("Cost", `${pay}k`),
					]),
					div("Photo", {
						style: {transform: `rotate(${(Math.random() *1) -.5}deg)`},
					},[
						div("Photo-bg", [genProceduralOffice()]),
						() => {
							const svgPath = (animal ? employeeImages[animal.replace(/\-/g, "")] : null) || employeeImages["Frog"];
							if (svgPath !== undefined) {
								return {hackableHTML: `<img src=${svgPath.default}>`};
							}
							return null;
						}
					]),
					div("Stats", [
							["ART", "art", art],
							["LOG", "logic", logic],
							["CHA", "charisma", charisma],
							["PAT", "patience", patience],
						].map(([label, name, level]: [string, string, number]) => {
							return div(`Stat ${name}`, [
								div("StatLabel", label),
								...((new Array(level)).fill(1).map(() => span(`StatIcon ${name}`)) || []),
							]);
						}),
					),
					this.quirkEl = div("Quirk", [
						div("Info", "Place Quirk Card Here"),
					]),
				]),
			]);
		}
		return this._domNode;
	}

	public static getBackNode(): HTMLElement {
		return div("Employee-Card back", "EMPLOYEE CARD");
	}

	public moveToBoard() {
		this.game.board.officeSpaceEl.appendChild(this.domNode);
	}

	public isOnBoard() {
		return this.game.board.officeSpaceEl.contains(this.domNode);
	}

	public setQuirk(quirk: QuirkCard) {
		this.quirk = quirk;
		this.quirkEl.appendChild(quirk.domNode);
	}
}