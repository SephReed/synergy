export default interface IRawQuirkCard {
	name: string;
	description: string;
	common: boolean;
	alluring: boolean;
	impulsive: boolean;
	messy: boolean;
}