import {div} from "el-tool";
import {Omject} from "omject";
import QuirkCard from "./QuirkCard";
import IRawQuirkCard from "./IRawQuirkCard";
import Deck from "../Deck";
import * as QuirkCardSheet from "../../../Registry/sheets/json/Synergy - Quirks.json";


const QuirkCardList: Array<QuirkCard> = [];
QuirkCardSheet.forEach((card) => {
	const raw: IRawQuirkCard = {
		name: card["Name"],
		description: card.Description,
		common: card.Common,
		alluring: card.Alluring,
		impulsive: card.Impulsive,
		messy: card.Messy,
	};
	const repeat = card.Repeat || 1;
	for (let i = 0; i < repeat; i++) {
		QuirkCardList.push(new QuirkCard(raw));
	}
});
export {QuirkCardList};

export default class QuirkCardDeck extends Deck<QuirkCard> {
	constructor(parent: Omject) {
		super(parent, () => QuirkCardList, QuirkCard.getBackNode);
	}
}

