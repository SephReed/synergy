import {div, span, br} from "el-tool";
import IRawQuirkCard from "./IRawQuirkCard";
import {fromMD, injectSymbols} from "../../../Util/";

import "./quirkCard.scss";

export default class QuirkCard {
	public readonly domNode: HTMLElement;

	constructor(public raw: IRawQuirkCard) {
		this.domNode = div("Quirk-Card front", [
			div("Quirk-bg", [
				div("Floor"),
				div("Desk"),
				div("Fade"),
			]),
			div("Quirk-fg", [
				div("Title", [
					(raw.name || "").toUpperCase(),
				]),
				div("Description", [
					fromMD(injectSymbols((raw.description + "").replace(/\n/g, "<br>"))),
					() => {
						const qualities = [
							raw.alluring ? this.createQuality("Alluring") : null,
							raw.common ? this.createQuality("Common") : null,
							raw.impulsive ? this.createQuality("Impulsive") : null,
							raw.messy ? this.createQuality("Messy") : null,
						].filter((it) => !!it);
						return qualities.length ? div("Qualities", qualities) : null;
					},
				]),
			])
		]);
	}

	public createQuality(name: string):any {
		return fromMD(injectSymbols(`{{${name.toUpperCase()}}} ${name}`));
	}

	public static getBackNode(): HTMLElement {
		return div("Quirk-Card back", "Quirk Card")
	}
}