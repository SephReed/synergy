import {div} from "el-tool";
import {Omject} from "omject";
import Placeable from "./Placeable";
import IRawPlaceable from "./IRawPlaceable";
import Deck from "../Deck";
import * as PlaceableSheet from "../../../Registry/sheets/json/Synergy - Placeables.json";


const PlaceableList: Array<Placeable> = [];
PlaceableSheet.forEach((card) => {
	const raw: IRawPlaceable = {
		id: card["id"],
		name: card["Name"],
		effect: card["Effect"],
		type: card["Type"],
		cost: card["Upkeep Cost"],
	};
	PlaceableList.push(new Placeable(raw));
});
export {PlaceableList};

export default class PlaceableDeck extends Deck<Placeable> {
	constructor(parent: Omject) {
		super(parent, () => PlaceableList, Placeable.getBackNode);
	}
}

