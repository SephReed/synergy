export default interface IRawPlaceable {
	id: string;
	name: string;
	effect: string;
	type: "pet" | "furniture";
	upkeepCost: number;
}