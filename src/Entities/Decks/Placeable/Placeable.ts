import {div, span, br} from "el-tool";
import IRawPlaceable from "./IRawPlaceable";
import {fromMD, injectSymbols, parseRawText} from "../../../Util/";

import "./placeable.scss";

export default class Placeable {
	public readonly domNode: HTMLElement;

	constructor(public raw: IRawPlaceable) {
		this.domNode = div(`Placeable-Card front ${raw.id}`, [
			div("Topbar", [
				div("Name", raw.name),
				div("Effect", parseRawText(raw.effect)),
				raw.cost ? div("Cost", raw.cost + "k") : null,
			]),
		]);
	}

	public static getBackNode(): HTMLElement {
		return div("Placeable-Card back", "Placeable Card")
	}
}