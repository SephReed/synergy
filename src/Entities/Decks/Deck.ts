import {div} from "el-tool";
import {BasicOmjectClass, Omject} from "omject";
import Synergy from "../../Synergy";

export default class Deck<CARD_TYPE extends {domNode: HTMLElement}> extends BasicOmjectClass {
	private cards: Array<CARD_TYPE>;
	private _domNode: HTMLElement;
	protected game: Synergy;

	constructor(
		parent: Omject,
		private deckSource: () => Array<CARD_TYPE>, 
		private cardBack?: HTMLElement | (() => HTMLElement)
	) {
		super(parent);
		this.game = this.getAncestor("Synergy");
		this.reset();
	}

	public get domNode() {
		if (!this._domNode) {
			this._domNode = div("Deck", {
				onClick: () => this.onClick(),
			}, [
				!this.cardBack ? null : (
					typeof this.cardBack === "function" ? this.cardBack() : this.cardBack.cloneNode(true) as HTMLElement
				),
			]);
		}
		return this._domNode;
	}

	protected onClick() {
		const card = this.draw();
		this.game.subNodes.drawnCards.appendChild(card.domNode);
	}

	public reset() {
		this.cards = [].concat(this.deckSource());
		this.shuffle();
	}

	public shuffle() {
		const { cards } = this;
		const deckSize = cards.length;
		for (let i = 0; i < deckSize; i++) {
			const source = cards[i];
			const randomIndex = Math.floor(Math.random() * deckSize);
			cards[i] = cards[randomIndex];
			cards[randomIndex] = source;
		}
	}

	public draw(): CARD_TYPE;
	public draw(numCards: number): Array<CARD_TYPE>;
	public draw(numCards?: number) {
		if (numCards === undefined || numCards === 1) {
			return this.cards.pop();
		} else {
			const out: Array<CARD_TYPE> = [];
			for (let i = 0; i < numCards; i++) {
				out.push(this.cards.pop());
			}
			return out;
		}
	}

	public placeCardsAtop(cards: Array<CARD_TYPE>) {
		this.cards.concat(cards);
	}

	public placeCardsAtBottom(cards: Array<CARD_TYPE>) {
		this.cards.unshift(...cards);
	}
}