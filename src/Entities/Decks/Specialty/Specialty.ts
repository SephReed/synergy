import {div, span} from "el-tool";
import IRawSpecialty from "./IRawSpecialty";
import {fromMD, injectSymbols, parseRawText} from "../../../Util"
import {RawInstructions} from "../../Instructions/Instructions"
import {CommonActions} from "./SpecialtyDeck";
// const CommonActions = "";

import "./Specialty.scss";

const agenda = RawInstructions.find((inst) => inst.id === "agendaCardText");
const symbols = RawInstructions.find((inst) => inst.id === "symbolGlossary");

export default class Specialty {
	public readonly domNode: HTMLElement;

	constructor(public raw: IRawSpecialty) {
		this.domNode = div(`Specialty-Card front ${raw.id}`, [
			div("Specialty-bg", [
				div("Fade", [
					div("Fill none", raw.name),
					div("Fill"),
					div("Fill"),
					div("Fill"),
				]),
			]),
			div("Specialty-fg", [
				div("Content", [
					div("Name", raw.name),
					div("Overview", [
						div("InlineTitle", "OVERVIEW:"),
						div("Blurb", raw.overview),
					]),

					div("FounderSpace", [
						div("PlaceHere", "Place Founder Character Card Here"),
					]),

					div("Actions", [
						div("ActionList special", [
							div("Title", "SPECIAL ACTIONS"),
							div("List", Specialty.parseActions(raw.specialActions)),
						]),
						div("ActionList common", [
							div("Title", "COMMON ACTIONS"),
							Specialty.genCommonActions(),
						]),
					]),

					div("Agenda", [
						div("Title", "QUARTERLY AGENDA"),
						parseRawText(agenda.blurb),
					]),

					div("CheatSheet", [
						div("Symbols", [
							div("Title", symbols.title.toUpperCase()),
							div("List", parseRawText(symbols.blurb.replace(/\n/g, "<br>"))),
						]),
						div("WorkerVenn", [
							// div("Founder", "Founder"),
							// div("Worker", "Worker"),
							// div("Employee", "Employee"),
						]),
					]),
				]),
				
			]),
		]);
	}

	public generateMiniView(): HTMLElement {
		const raw = this.raw;
		return div(`Specialty-Card front mini_view ${raw.id}`, [
			div("Specialty-bg", [
				div("Fade", [
					div("Fill none", raw.name),
					div("Fill"),
					div("Fill"),
					div("Fill"),
				]),
			]),
			div("Specialty-fg", [
				div("IsMini", fromMD("(Cut from full placard)")),
				div("Content", [
					div("Name", raw.name),
					div("Overview", [
						div("Title", "OVERVIEW:"),
						div("Blurb", raw.overview),
					]),

					div("Actions", [
						div("ActionList special", [
							div("Title", "SPECIAL ACTIONS"),
							div("List", Specialty.parseActions(raw.specialActions)),
						]),
					]),
				]),
			]),
		]);
	}

	private static parseActions(acts: string) {
		return acts.split(/\n/g).map(
			(act) => div(
				"Action", 
				fromMD(injectSymbols(act)),
			),
		);
	}

	public static genCommonActions(): HTMLElement {
		return div("List common_actions", Specialty.parseActions(CommonActions));
	}

	public static getBackNode(): HTMLElement {
		return div("Specialty-Card back", "Specialty CARD")
	}
}