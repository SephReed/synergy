export default interface IRawSpecialty {
	id: string;
	name: string;
	specialActions: string;
	overview: string;
}