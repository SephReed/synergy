import {div} from "el-tool";
import {Omject} from "omject";
import Specialty from "./Specialty";
import IRawSpecialty from "./IRawSpecialty";
import Deck from "../Deck";
import * as SpecialtySheet from "../../../Registry/sheets/json/Synergy - Specialties.json";

const CommonActions = SpecialtySheet.find((row) => row["id"] === "allPlayers")["Actions"];

const SpecialtyList: Array<Specialty> = SpecialtySheet
.filter((row) => ["allPlayers", "examp12le"].includes(row.id) === false)
.map((row) => 
	new Specialty({
		id: row["id"],
		name: row["Name"],
		specialActions: row["Actions"],
		overview: row["Overview"],
	}),
);

export {SpecialtyList, CommonActions};

export default class SpecialtyDeck extends Deck<Specialty> {
	constructor(parent: Omject) {
		super(
			parent, 
			() => SpecialtyList, 
			Specialty.getBackNode);
	}
}

