export {default as SpecialtyDeck, SpecialtyList} from "./SpecialtyDeck";
export {default as Specialty} from "./Specialty";