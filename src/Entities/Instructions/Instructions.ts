import {addClass, div, append, create} from "el-tool";
import { fromMD, injectSymbols } from "../../Util";
import { SpecialtyList } from "../../Entities/Decks/Specialty/SpecialtyDeck";
import Specialty from "../../Entities/Decks/Specialty/Specialty";
import { AgendaCard } from "./AgendaCard";
import { Employee, RawEmployeeList} from "../../Entities/Decks/Employee";
import { OptionCardList } from "../../Entities/Decks/Option/OptionCardDeck";
import { ProjectList } from "../../Entities/Decks/Project/ProjectDeck";
import { MissionList } from "../../Entities/Decks/Mission/MissionDeck";
import { QuirkCardList } from "../../Entities/Decks/Quirk/QuirkDeck";
import { EventCardList } from "../../Entities/Decks/Event/EventCardDeck";
import Board from "../../UI/Board/Board";

abstract class SimpleComponent <EL_TYPE extends HTMLElement = HTMLElement> {
	protected _domNode: EL_TYPE;
	public get domNode(): EL_TYPE {
		if (this._domNode === undefined) {
			this.render();
			if (this._domNode === undefined) {
				throw new Error("Component._domNode must not be undefined after render()")
			}
		}
		return this._domNode;
	}

	protected abstract render(): void;
}

import IRawInstruction from "./IRawInstruction";
import * as EventCardSheet from "Registry/sheets/json/Synergy - Instructions.json";

import "./instructions.scss";

const RawInstructions: IRawInstruction[] = EventCardSheet.map((card) => { 
	return {
		id: card["id"] + "",
		title: card["Title"] + "",
		blurb: card["Blurb"] + "",
		isGlossary: card["Glossary Item"],
		majorItem: card["Major Item"],
	};
});
export {RawInstructions};


export default class Instructions extends SimpleComponent {
	public static agendaCard: AgendaCard = new AgendaCard(
		RawInstructions.find((inst) => inst.id === "agendaCardText"),
	);

	protected render() {
		this._domNode = div("Instructions-Book", [
			this.createInstructionFromId("firstThing"),
			this.createInstructionFromId("overview"),
			this.createInstructionFromId("firstTimePlaying"),
			this.createInstructionFromId("gettingStarted"),
			this.createInstructionFromId("iconGlossary"),
			this.createInstructionFromId("keytermGlossary"),
			...RawInstructions
				.filter((item) => item.isGlossary)
				.sort((a, b) => a.title > b.title)
				.map((inst) => addClass(this.createItem(inst), "glossaryItem")),
			this.createInstructionFromId("examplePlay"),
		]);
	}

	private createItem(inst: IRawInstruction) {
		// const isShort = inst.blurb.length < 250;
		const smallItem = inst.isGlossary && !inst.majorItem;
		return div(`Instruction ${smallItem ? "isShort" : ""}`, [
			div("Title", {hackableHTML:  injectSymbols(inst.title) + (smallItem ? " - " : "")}),
			div("Blurb", fromMD(injectSymbols(inst.blurb.replace(/\n/g, "\n\n")))),	
			this.getSpecialContent(inst),
		]);
	}

	private createInstructionFromId(id: string) {
		const inst = RawInstructions.find((item) => item.id === id);
		return addClass(this.createItem(inst), id);
	}

	private getSpecialContent(inst: IRawInstruction) {
		if (inst.id === "specialtyCards") {
			return div("SpecialtyCards", SpecialtyList
				// .filter((card) => card.raw.id !== "example")
				.sort((a, b) => !a.raw.id.startsWith("example") && a.raw.name > b.raw.name)
				.map((spec) => {
					const desc = RawInstructions.find((it) => it.id === (spec.raw.id +"Description"))
					let card: HTMLBRElement;
					let keys: string[];
					if (spec.raw.id === "exampleSpecialty") {
						card = div("CropView", spec.domNode);
						keys = ["founderSpace", "overview", "specialActions", "commonActions", "agenda", "cheatsheet"];
					} else {
						card = spec.generateMiniView();
					}
					return this.createCardDesc(inst.id, card, desc ? desc.blurb.replace(/\n/g, "\n\n") : "N/A", keys);
				}),
			);

		} else if (inst.id === "agendaCard") {
			const desc = RawInstructions.find((inst) => inst.id === "agendaCardDescription");
			return this.createCardDesc(inst.id, Instructions.agendaCard.domNode, desc.blurb);

		} else if (inst.id === "characterCard") {
			const desc = RawInstructions.find((inst) => inst.id === "characterCardDescription") || {blurb: "N/A"};
			const card = new Employee(null, RawEmployeeList.find((raw) => raw.name === "Examplo"));
			return this.createCardDesc(inst.id, card.domNode, desc.blurb, ["image", "pay", "quirk", "skills"]);

		} else if (inst.id === "commonActions") {
			const desc = RawInstructions.find((inst) => inst.id === "commonActionsDescription") || {blurb: "N/A"};
			const card = SpecialtyList.find((card) => card.raw.id === "exampleSpecialty");
			// const Specialty = require("../../Entities/Decks/Specialty/Specialty");
			// console.log(Specialty);
			// const card = Specialty.default.genCommonActions();
			return this.createCardDesc(inst.id, div("CropView", card.domNode.cloneNode(true)), desc.blurb);

		// option card
		} else if (inst.id === "optionCard") {
			const desc = RawInstructions.find((inst) => inst.id === "optionCardDescription") || {blurb: "N/A"};
			const card = OptionCardList.find((card) => card.raw.name === "Change Snacks");
			return this.createCardDesc(inst.id, card.domNode, desc.blurb, ["description", "actions", "repeatable"]);

		// project card
		} else if (inst.id === "projectCard") {
			const desc = RawInstructions.find((inst) => inst.id === "projectCardDescription") || {blurb: "N/A"};
			const card = ProjectList.find((card) => card.raw.name === "Ad Campaign");
			return this.createCardDesc(inst.id, card.domNode, desc.blurb, ["payout", "job", "tasks", "skills", "stageSplit"]);

		// mission card
		} else if (inst.id === "missionCard") {
			const desc = RawInstructions.find((inst) => inst.id === "missionCardDescription") || {blurb: "N/A"};
			const card = MissionList.find((card) => card.raw.title === "The Churn Factory");
			return this.createCardDesc(inst.id, card.domNode, desc.blurb, ["specs", "goal", "endpointType"]);

		// quirk card
		} else if (inst.id === "quirkCard") {
			const desc = RawInstructions.find((inst) => inst.id === "quirkCardDescription") || {blurb: "N/A"};
			const card = QuirkCardList.find((card) => card.raw.name === "The Stain");
			return this.createCardDesc(inst.id, card.domNode, desc.blurb, ["specialEffects", "qualities"]);

		// event card
		} else if (inst.id === "eventCard") {
			const desc = RawInstructions.find((inst) => inst.id === "eventCardDescription") || {blurb: "N/A"};
			const card = EventCardList.find((card) => card.raw.name.startsWith("Donuts"));
			return this.createCardDesc(inst.id, card.domNode, desc.blurb, ["description", "actions"]);
		
		// half-count
		} else if (inst.id === "halfCount") {
			const fullCount = create("tr", create("td", "Full-Count"));
			const halfCount = create("tr", create("td", "Half-Count"));
			const quarterCount = create("tr", create("td", "Quarter-Count"));
			for (let i = 1; i <= 20; i++) {
				create("td", {appendTo: fullCount}, i);
				create("td", {appendTo: halfCount}, Math.floor(i/2)+"");
				create("td", {appendTo: quarterCount}, Math.floor(i/4)+"");
			}
			return create("table", {class: inst.id}, [
				fullCount, 
				halfCount,
				quarterCount
			]);

		// boardOverview
		} else if (inst.id === "boardOverview") {
			const card = new Board();
			const desc = RawInstructions.find((inst) => inst.id === "boardOverviewDescription") || {blurb: "N/A"};
			return this.createCardDesc(inst.id, div("CropView", card.domNode), desc.blurb, [
				"synergyTrack", "officeSpace", "frontDoor", "missionCard", "projectCards", "cleaningService", "quarterTrack"
			]);
		}
		return null;
	}

	private createCardDesc(id: string, card: HTMLElement, desc: string, keyMarkers: string[] = []) {
		return div(`CardAndDesc ${id}`, [
			div("Card", [
				card,
				div("KeyMarkers", keyMarkers.map(
					(keyMark, index) => div(
						`KeyMarker ${keyMark}`,
						{ style: {
							transform: `rotate(${-2 + Math.random() * 4}deg)`,
						}},
						"ABCDEFGHI".charAt(index)
					)),
				),
				
			]),
			div("Desc", fromMD(injectSymbols(desc))),
		]);
	}
}