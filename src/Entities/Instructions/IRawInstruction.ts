export default interface IRawInstruction {
	id: string;
	title: string;
	blurb: string;
	isGlossary: boolean;
	majorItem: boolean;
}