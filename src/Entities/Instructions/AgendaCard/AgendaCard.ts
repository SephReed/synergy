import {div, span} from "el-tool";
import {RawInstructions} from "../Instructions";
import IRawInstruction from "../IRawInstruction";
import {fromMD, injectSymbols} from "../../../Util";

import "./agendaCard.scss";

export default class AgendaCard {
	public readonly domNode: HTMLElement;

	constructor(raw: IRawInstruction) {
		this.domNode = div("Agenda-Card front", [
			div("Title", "This Quarters Agenda"),
			div("Blurb", fromMD(injectSymbols(raw.blurb))),
			// div("Footers", {hackableHTML: raw.footers.replace(/\n/g, "<br>")}),
		]);
	}

	public static getBackNode(): HTMLElement {
		return div("Agenda-Card back", "AGENDA CARD")
	}
}