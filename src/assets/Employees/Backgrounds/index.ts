import * as CityAtNightFromRoof from "./City-at-night-from-roof.jpg";  export {CityAtNightFromRoof};
import * as OfficeWithPlants from "./Office-with-plants(chunked).jpg"; export {OfficeWithPlants};
import * as OfficeHallPeek from "./Office-Hall-Peek(chunked).jpg"; export {OfficeHallPeek};
import * as OfficeUnionedWoodDesks from "./OfficeUnionedWoodDesks.jpg"; export {OfficeUnionedWoodDesks};
import * as LobbyBigWindows from "./LobbyBigWindows(chunked).png"; export {LobbyBigWindows};