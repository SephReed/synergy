export default function minimizeWidth(domNode: HTMLElement) {
	const squinchFurther = () => {
		// console.log("Squink")
		const startHeight = domNode.offsetHeight;
		const startWidth = domNode.offsetWidth;
		if (startWidth === 0) { return; }
		
		domNode.style.width = (startWidth - 1) + "px";
		// console.log("Squink", startWidth, domNode.style.width);
		requestAnimationFrame(() => requestAnimationFrame(() => {
			if (domNode.offsetHeight !== startHeight) {
				domNode.style.width = startWidth + "px";
			} else {
				squinchFurther();
			}
		}));
	}
	requestAnimationFrame(() => requestAnimationFrame(squinchFurther));
	return domNode;
}