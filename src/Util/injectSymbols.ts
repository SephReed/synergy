export default function injectSymbols(rawHtml: string) {
	return rawHtml.replace(/{{(.+?)}}/g, (match, g1) => 
		`<span class="Icon ${g1.toLowerCase()}">${g1.charAt(0)}</span>`
	)
	.replace(/_(.+?)_/g, (match, g1) => `<u>${g1}</u>`)
	.replace(/SYN/g, `<span class="Icon synergy">s</span>`);
}