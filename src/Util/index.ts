import fromMD from "./fromMD"; export { fromMD };
import injectSymbols from "./injectSymbols"; export { injectSymbols };
// export {default as fromMD} from "./fromMD";
export {default as minimizeWidth} from "./minimizeWidth";
// export {default as injectSymbols} from "./injectSymbols";

export function parseRawText(rawText: string = ""){ 
	return fromMD(injectSymbols(rawText)); 
}

