import { a, create, br, div, span, Innards, RawHTML } from "el-tool";
import * as MarkdownIt from "markdown-it";
const MD = MarkdownIt({
	html: true,
	xhtmlOut: true,
});

export default function fromMD(str: string): RawHTML {
	return {hackableHTML: MD.render(str)};
}