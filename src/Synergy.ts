import {byId, div, setInnards} from "el-tool";
import {BasicObservableClass} from "vanilla-observables";
import Setup from "./UI/Setup";
import {Omject, OmjectAug, parentFn} from "omject";
import {EmployeeDeck, ProjectDeck, OptionCardDeck, EventCardDeck, AgendaDeck, QuirkDeck} from "./Entities/Decks";
import { Employee } from "./Entities/Decks/Employee";
import genFlooring from "./UI/ProceduralOffice/genFlooring";
import Board from "./UI/Board/Board";

import "./game.scss";

type ModalView = "none" | "setup";
type SidebarView = "none" | "employees" | "projects";
type SubNodes = "modals" | "table" | "drawnCards";

type PropNames = "modalView" | "projectDeck";

export default class Synergy extends BasicObservableClass<PropNames> implements Omject {
	public readonly omjectId = "Synergy";
	public omjectAug = new OmjectAug(this, null);
	public parent = parentFn(this);

	// HTML Elements
	public domNode: HTMLElement;
	public readonly subNodes: {[key in SubNodes]?: HTMLElement};

  // Observable
	public readonly modalView = this.observable<ModalView>("modalView");

	// SubClasses
	private setup: Setup;
	public readonly employeeDeck: EmployeeDeck;
	public readonly projectDeck = this.observable<ProjectDeck>("projectDeck");
	public readonly optionDeck: OptionCardDeck;
	public readonly eventDeck: EventCardDeck;
	public readonly agendaDeck: AgendaDeck;
	public readonly quirkDeck: QuirkDeck;
	public readonly board: Board;

	public players: any[];

	// Depos
	public dragDepo: {
		employeeCard?: Employee,
	} = {};

	constructor() {
		super();
		this.projectDeck.set(new ProjectDeck(this));
		this.employeeDeck = new EmployeeDeck(this);
		this.eventDeck = new EventCardDeck(this);
		this.optionDeck = new OptionCardDeck(this);
		this.agendaDeck = new AgendaDeck(this);
		this.quirkDeck = new QuirkDeck(this);

		this.subNodes = {};
		const nodes = this.subNodes;
		this.domNode = div("Game", [
			nodes.modals = div("Modals"),
			div("PlayArea", [
				nodes.table = div("Table", {
					onClick: (event) => {
						for (let ptr = event.target as HTMLElement; !!ptr; ptr = ptr.parentNode as any) {
							if (ptr.className && ptr.className.indexOf("Card") !== -1) { return; }
						}
						const classList = nodes.table.classList;
						classList.contains("--zoomed_out") ? classList.remove("--zoomed_out") : classList.add("--zoomed_out")
					},
				}, [
					div("Decks", [
						this.employeeDeck.domNode,
						this.projectDeck().domNode,
						this.eventDeck.domNode,
						this.optionDeck.domNode,
						this.agendaDeck.domNode,
						this.quirkDeck.domNode,
					]),
					(this.board = new Board(this)).domNode,
				]),
			]),
			nodes.drawnCards = div("DrawnCards"),
		]);

		this.modalView.observe((view) => {
			let viewEl: HTMLElement;
			if (view === "setup") {
				const setup = this.setup = (this.setup || new Setup(this));
				viewEl = setup.domNode;
			}
			this.subNodes.modals.style.display = viewEl ? "" : "none";
			setInnards(this.subNodes.modals, [viewEl]);
		})

		// this.modalView.set("setup");
	}

	protected onBirth() {}
}