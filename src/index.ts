import { create, div, setInnards } from "el-tool";
import genPrintSheet from "./UI/PrintSheets/printSheets";
import { genProceduralOffice } from "./UI/ProceduralOffice";
import ExportPays from "./UI/ExportPays/ExportPays";
import Instructions from "./Entities/Instructions/Instructions";
import SetupGame from "./UI/Setup"
import Synergy from "./Synergy";
import "./main.scss";

const search = window.location.search;
const today = new Date();

if (search.indexOf("cards") !== -1) {
	document.title = `Synergy - Cards Print (${today.getFullYear()}-${today.getMonth()}-${today.getDate()})`;

	setInnards(document.body, [
		genPrintSheet("employee"),
		// genPrintSheet("project"),
		// genPrintSheet("agenda"),
		// genPrintSheet("event"),
		genPrintSheet("option"),
		// genPrintSheet("quirk"),
		genPrintSheet("specialty"),
		// genPrintSheet("mission"),
		genPrintSheet("placeable"),
	]);
} else if (search.indexOf("randBg") !== -1) {
	let officeWrap: HTMLElement;
	const replaceOffice = () => setInnards(officeWrap, [genProceduralOffice()]);
	setInnards(document.body, [
		officeWrap = div("OfficeWrap"),
		create("button", {onClick: replaceOffice}, "ReRoll"),
	]);
	replaceOffice();
} else if (search.indexOf("exportPays") !== -1) {
	setInnards(document.body, ExportPays());

} else if (search.indexOf("instructions") !== -1) {
	document.title = `Synergy - Instructions (${today.getFullYear()}-${today.getMonth()}-${today.getDate()})`;
	const instructions = new Instructions();
	setInnards(document.body, instructions.domNode);

} else {
	const client = new Synergy();
	if (search.indexOf("board") !== -1) {
		document.title = "Synergy - Board Print";
		setInnards(document.body, [client.board.domNode]);
	} else {
		setInnards(document.body, [client.domNode]);
	}
}

// interface Layered extends Array<number | Layered> { }
// function flatten(flattenMe: Layered) {
// 	const flat: number[] = [];
// 	const permeate = (layered: Layered) => {
// 		layered.forEach((item) => {
// 			if(Array.isArray(item)) {
// 				permeate(item);
// 			} else {
// 				flat.push(item);
// 			}
// 		})
// 	}
// 	permeate(flattenMe);
// 	return flat;
// }

// Kris Bowman