###Quest Name
*(Good puns are encouraged. Players will see this title on the quest shields.)*

The Underlings Faux Pas

------

###Quest Shield Text
*(Two to three sentences for what the players will be told about the quest. This text will be used to help create what appears on the quest shield. This should contain no spoilers about the quest.)*

What a dunce!  The Magicians underling, clever as she was, has all the social graces of a pig in heat, and belched mid sentence in the face of one of FAIRYTALE-LANDS richest nobleman.  Of course the magician had no choice but to exile her from her studies, but she took a very important book on her way.

------

###Quest Type
[x] - Puzzle (Solve a puzzle to beat the quest)
[x] - Combat (Defeat an enemy in combat to beat the quest)
[] - Adventure (Role play to beat the quest)

------

###Quest Duration
*(Use your best guess. Combat quests last a minimum of 15 minutes.)*
[] - 15 minutes
[] - 20 minutes
[] - 30 minutes
[x] - 45 minutes

------

###Primary Danger
*(Bandits on the road, Orcs in the woods, Building collapse, Laser gazebo... etc.)*

The magicians underling and her novice level traps.

------

###Quest Summary
*(Two to three sentences detailing the quest from cast/crew point of view. What will guests be doing?)*

------

###Quest Steps
*(The sequence of people and/or places the heroes must visit. Write in the form of "Quest Giver >> Location >> Quest Giver". You may have multiple locations in sequence if your quest calls for it. Don't specify a place within the physical Scare property -- just name a location as we will name it for guests. We will figure out how to map the quests onto our physical space later. The quest giver is almost always the Guard Captain but you can pick among the fixed characters in Haven: Guard Captain, Mage Captain, Tavern Keeper, Oracle, Temple Cleric.)*

------

###Cast and Crew (NPCs/Techs/GMs)
*(List all the cast and crew needed to staff your quest excluding the quest giver. Quests that require zero cast and zero crew are highly favored!)*

------

###Props
*(List any props that we will have to create/acquire to make your quest happen.)*

------

###Quest Details
*(Complete description of the quest in all its glorious detail. Be as thorough as you feel you need to be to communicate just how awesome your quest is.)*

------

###Reward
*(What is the prize heroes earn for finishing your quest?)*

------

###Your Name
