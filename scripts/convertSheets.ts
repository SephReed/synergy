import * as fs from "fs";
import * as pathTool from "path";

const sheetsDir = "./src/Registry/sheets"
const csvDir = pathTool.join(sheetsDir, "csv");
const csvFileNames = fs.readdirSync(csvDir);
csvFileNames.filter((csvFileName) => 
	fs.lstatSync(pathTool.join(csvDir, csvFileName)).isDirectory() === false,
).forEach((csvFileName) => {
	const rawCsvFile = fs.readFileSync(pathTool.join(csvDir, csvFileName)) + "";

	const rows = rawCsvFile.split("\r\n");

	const splitPointsRegex = /"(""|[^"])+?"|,/g;
	const array2d = rows.map((row) => {
		let lastPoint = 0;
		const cols: string[] = [];
		let match: RegExpExecArray;
		while ((match = splitPointsRegex.exec(row)) !== null) {
	    if (match[0] === ",") {
	    	cols.push(row.substring(lastPoint, match.index));
	    	lastPoint = match.index + 1;
	    }
		}
		cols.push(row.slice(lastPoint));
		return cols.map((datum) => 
			datum.replace(/^,?"?|"$/g, "")
			.replace(/""/g, `\"`)
			.replace(/\r/g, "")
		);
	})

	const out = [];
	const propsRow = array2d[0];
	array2d.forEach((row, i) => {
		if (i === 0) { return; }
		const addMe: any = {};
		row.forEach((datum, j) => {
			let parsedData: any;
			if (isNaN(Number(datum)) === false) {
				parsedData = Number(datum);
			// } else if (isNaN(parseFloat(datum)) === false) {
			// 	parsedData = parseFloat(datum);
			} else if (datum === "TRUE") {
				parsedData = true;
			} else if (datum === "FALSE") {
				parsedData = false;
			} else {
				parsedData = datum;
			}
			addMe[propsRow[j]] = parsedData;
		});
		out.push(addMe);
	});

	const jsonDir = pathTool.join(sheetsDir, "json", csvFileName.replace("csv", "json"));
	fs.writeFileSync(jsonDir, JSON.stringify(out, null, 2));
});



	// const lineRegex = /((\\\n)|[^\n])+/g;
	// const datumRegex = /,?((".+?"(?=([,\n]|$)))|([^",][^,]*))|,/g;
	// const array2d: string[][] = rawCsvFile.match(lineRegex).map((row) => 
	// 	row.match(datumRegex).map((datum) => datum.replace(/^,?"?|"$/g, "").replace(/""/g, `\"`).trim()),
	// );

	// //,? = may begin with comma
	// //	("[\s\S]+?"(?=([,\n]|$))) = could be anything in quotes ending with newline, comma, or EOF
	// //	([^",][^,\n]*) = could also be anything without quotes, commas, or newlines
	// //, = can also be an empty cell
	// //\n = can also be a new line (for next row)
	// // const datumRegex = /,?(("[\s\S]+?"(?=([,\n]|$)))|([^",\n][^,\n]*))|,|\n/g;
	// const datumRegex = /,?(("(""|[^"])+?"(?=([,\n]|$)))|([^",\n][^,\n]*))|,|\n/g;
	// const allDatum = rawCsvFile.match(datumRegex).map((datum) => datum.replace(/^,?"?|"$/g, "").replace(/""/g, `\"`).replace(/\r/g, ""));
	// let row = 0;
	// let col = 0;
	// console.log(allDatum);
	// const array2d: string[][] = [[]];
	// allDatum.forEach((datum) => {
	// 	if (datum === "\n") {
	// 		row++;
	// 		col = 0;
	// 		array2d[row] = [];
	// 	} else {
	// 		array2d[row][col] = datum;
	// 		col++;
	// 	}
	// });
	// console.log(array2d);